// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { Outlet } from 'react-router-dom';

import './app.scss';

import { LayoutBlockContainer } from 'core-dist';
import { DigiLayoutBlock, DigiTypography } from 'arbetsformedlingen-react-dist';

import Header from './components/header/Header';
import Footer from './components/footer/Footer';

export function App() {
	return (
		<DigiTypography>
			<div className="app">
				<Header />
				<main className="app__main">
					<Outlet />
				</main>
				<Footer />
			</div>
		</DigiTypography>
	);
}

export default App;
