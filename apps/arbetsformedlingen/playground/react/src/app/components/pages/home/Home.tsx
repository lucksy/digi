import {
  DigiLayoutBlock,
  DigiTypographyHeadingJumbo,
  DigiList,
} from 'arbetsformedlingen-react-dist';
import { LayoutBlockContainer } from 'arbetsformedlingen-dist';

import { useNavigate } from 'react-router-dom';

const Home = () => {
  const navigate = useNavigate();

  function clickHandler(e: any) {
    navigate(e.target.afHref)
  }

  return (
    <DigiLayoutBlock afVerticalPadding>
      <DigiTypographyHeadingJumbo 
        afText="Designsystemet - React"
      ></DigiTypographyHeadingJumbo>
      <DigiLayoutBlock 
        afMarginTop 
        afMarginBottom 
        afContainer={LayoutBlockContainer.NONE}
      >
        <DigiList>
        </DigiList> 
      </DigiLayoutBlock>
    </DigiLayoutBlock>
  );
};

export default Home;
