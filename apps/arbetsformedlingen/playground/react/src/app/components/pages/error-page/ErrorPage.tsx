import { useRouteError } from "react-router-dom";
import { DigiNotificationErrorPage, DigiTypography } from "arbetsformedlingen-react-dist";
import { ErrorPageStatusCodes } from "arbetsformedlingen-dist";

const ErrorPage = () => {
  const error: any = useRouteError();
  console.error(error);

  return (
    <DigiTypography>
      <DigiNotificationErrorPage
        afHttpStatusCode={ErrorPageStatusCodes.NOT_FOUND}
      ></DigiNotificationErrorPage>
    </DigiTypography>
  )
}

export default ErrorPage;