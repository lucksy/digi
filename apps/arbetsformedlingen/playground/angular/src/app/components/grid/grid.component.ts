import { Component, OnInit } from '@angular/core';
import { LayoutBlockContainer } from 'arbetsformedlingen-dist';

@Component({
	selector: 'at-grid',
	templateUrl: './grid.component.html',
	styleUrls: ['./grid.component.scss']
})
export class GridComponent implements OnInit {
	LayoutBlockContainer = LayoutBlockContainer;
	
	constructor() {}

	ngOnInit(): void {}
}
