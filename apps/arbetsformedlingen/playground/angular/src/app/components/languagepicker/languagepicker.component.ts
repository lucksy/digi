import { Component, OnInit } from '@angular/core';
import { INavigationContextMenuItem } from 'dist/libs/arbetsformedlingen/dist/types';

@Component({
	selector: 'at-languagepicker',
	templateUrl: './languagepicker.component.html',
	styleUrls: ['./languagepicker.component.scss']
})
export class LanguagepickerComponent implements OnInit {
	constructor() {}
	debugText = '';
	selectedLanguage = '';
	languages: INavigationContextMenuItem[] = [
		{
			index: 0,
			type: 'button',
			text: 'العربية (Arabiska)',
			lang: 'ar',
			value: 'ar',
			dir: 'rtl'
		},
		{
			index: 1,
			type: 'button',
			text: 'دری (Dari)',
			lang: 'prs',
			value: 'prs',
			dir: 'rtl'
		},
		{
			index: 2,
			type: 'button',
			text: 'به پارسی (Persiska)',
			lang: 'fa',
			value: 'fa',
			dir: 'rtl'
		},
		{
			index: 3,
			type: 'button',
			text: 'English (Engelska)',
			lang: 'en',
			value: 'en',
			dir: 'ltr'
		},
		{
			index: 4,
			type: 'button',
			text: 'Русский (Ryska)',
			lang: 'ru',
			value: 'ru',
			dir: 'ltr'
		},
		{
			index: 5,
			type: 'button',
			text: 'Af soomaali (Somaliska)',
			lang: 'so',
			value: 'so',
			dir: 'ltr'
		},
		{
			index: 6,
			type: 'button',
			text: 'Svenska',
			lang: 'sv',
			value: 'sv',
			dir: 'ltr'
		},
		{
			index: 7,
			type: 'button',
			text: 'ትግርኛ (Tigrinska)',
			lang: 'ti',
			value: 'ti',
			dir: 'ltr'
		}
	];
	languagesJson = [...this.languages];
	languagesJsonString = JSON.stringify(this.languagesJson);
	
	id ="myid";
	buttonText = "Hello world"
	startSelected = 1;
	afIcon = "language";



	ngOnInit(): void {}

	public changeId(e){
		this.id = e.target.value;
	}
	public onSelect(e) {
		console.log('Angular On Selected');
		console.log(e.detail);
		console.log(e);
		this.selectedLanguage = e.detail;
	}

	public onDemoButtonClick() {
		function shuffle(array) {
			let currentIndex = array.length,
				randomIndex;

			// While there remain elements to shuffle.
			while (currentIndex != 0) {
				// Pick a remaining element.
				randomIndex = Math.floor(Math.random() * currentIndex);
				currentIndex--;

				// And swap it with the current element.
				[array[currentIndex], array[randomIndex]] = [
					array[randomIndex],
					array[currentIndex]
				];
			}

			return array;
		}

		const demoarray = [...this.languagesJson];
		this.languagesJson = shuffle(demoarray);
	}
	
	public onSkanskaButtonClick() {
		this.languagesJson = [
			...this.languagesJson,
			{ index: 10, type: 'button', text: 'Skånska', lang: 'pag', value: 'pag', dir: 'ltr' }
		];
	}
	public onRemoveLanguageButtonClick() {
		this.languagesJson.pop();
		this.languagesJson = [...this.languagesJson];
	}
}
