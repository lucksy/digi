import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { ReactiveFormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { Route, RouterModule } from '@angular/router';

import { DigiArbetsformedlingenAngularModule } from 'arbetsformedlingen-angular-dist';

import { ArticleDemoComponent } from './components/article-demo/article-demo.component';
import { CalendarComponent } from './components/calendar/calendar.component';
import { ChartsComponent } from './components/charts/charts.component';
import { DatepickerComponent } from './components/datepicker/datepicker.component';
import { FileUploadComponent } from './components/file-upload/file-upload.component';
import { FooterComponent } from './components/framework/footer/footer.component';
import { FormComponent } from './components/form/form.component';
import { FormcontrolInvalidPipe } from './shared/pipes/control-invalid.pipe';
import { FormReceiptComponent } from './components/receipt/receipt.component';
import { GridComponent } from './components/grid/grid.component';
import { HeaderComponent } from './components/framework/header/header.component';
import { HomeComponent } from './components/home/home.component';
import { LanguagepickerComponent } from './components/languagepicker/languagepicker.component';
import { LinkButtonsComponent } from './components/link-buttons/link-buttons.component';
import { LinksComponent } from './components/links/links.component';
import { MultifilterPocComponent } from './components/multifilter-poc/multifilter-poc.component';
import { NavigationDesignsystemComponent } from './components/navigation-demo/navigation-designsystem/navigation-designsystem.component';
import { NavigationExternalComponent } from './components/navigation-demo/navigation-external/navigation-external.component';
import { NavigationIntranetComponent } from './components/navigation-demo/navigation-intranet/navigation-intranet.component';
import { NavigationPageComponent } from './components/navigation-demo/navigation-page/navigation-page.component';
import { NavigationtabsComponent } from './components/navigationtabs/navigationtabs.component';
import { NyaIkonerComponent } from './components/nya-ikoner/nya-ikoner.component';
import { SharedNavigationComponent } from './components/navigation-demo/shared-navigation/shared-navigation.component';
import { SidenavComponent } from './components/framework/sidenav/sidenav.component';
import { StepsComponent } from './components/steps/steps.component';
import { TimepickerComponent } from './components/timepicker/timepicker.component';
import { ValidationComponent } from './components/validering/validation.component';
import { ToolsLanguagepickerComponent } from './components/tools-languagepicker/tools-languagepicker.component';
import { WheelOfFortuneComponent } from './components/wheel-of-fortune/wheel-of-fortune.component';
import { TestFormSelectComponent } from './components/test-form-select/test-form-select.component';


const routes: Route[] = [
	{
		path: '',
		component: HomeComponent
	},
	{
		path: 'article',
		component: ArticleDemoComponent
	},
	{
		path: 'form',
		component: FormComponent
	},
	{
		path: 'timepicker',
		component: TimepickerComponent,
		data: {
			fullwidth: true,
			hideSidenav: true
		}
	},
	{
		path: 'grid',
		component: GridComponent,
		data: {
			fullwidth: true,
			hideSidenav: true
		}
	},
	{
		path: 'steps',
		component: StepsComponent
	},
	{
		path: 'navigation',
		component: NavigationPageComponent,
		data: {
			sidenavDemo: true
		}
	},
	{
		path: 'fileupload',
		component: FileUploadComponent,
		data: {
			sidenavDemo: true
		}
	},
	{
		path: 'calendar',
		component: CalendarComponent,
		data: {
			sidenavDemo: true
		}
	},
	{
		path: 'links',
		component: LinksComponent,
		data: {
			sidenavDemo: false
		}
	},
	{
		path: 'languagepicker',
		component: LanguagepickerComponent,
		data: {
			fullwidth: true,
			hideSidenav: true
		}
	},
	{
		path: 'navigationtabs',
		component: NavigationtabsComponent,
		data: {
			fullwidth: false,
			hideSidenav: true
		}
	},
	{
		path: 'receipt',
		component: FormReceiptComponent,
		data: {
			fullwidth: true,
			hideSidenav: true
		}
	},
	{
		path: 'validering',
		component: ValidationComponent,
		data: {
			fullwidth: false,
			hideSidenav: true
		}
	},
	{
		path: 'link-buttons',
		component: LinkButtonsComponent,
		data: {
			fullwidth: false,
			hideSidenav: true
		}
	},
	{
		path: 'charts',
		component: ChartsComponent
	},
	{
		path: 'tools-languagepicker',
		component: ToolsLanguagepickerComponent
	},
	{
		path: 'charts',
		component: ChartsComponent
	},
	{
		path: 'datepicker',
		component: DatepickerComponent
	},
	{
		path: 'multifilter-poc',
		component: MultifilterPocComponent,
		data: {
			fullwidth: true,
			hideSidenav: true
		}
	},
	{
		path: 'test-form-select',
		component: TestFormSelectComponent
	},
	{
		path: 'wheel-of-fortune',
		component: WheelOfFortuneComponent
	},
	{ path: '**', redirectTo: '', pathMatch: 'full' }
];

@NgModule({
	declarations: [
		AppComponent,
		ArticleDemoComponent,
		CalendarComponent,
		ChartsComponent,
		DatepickerComponent,
		FileUploadComponent,
		FooterComponent,
		FormComponent,
		FormcontrolInvalidPipe,
		FormReceiptComponent,
		GridComponent,
		HeaderComponent,
		HomeComponent,
		LanguagepickerComponent,
		LinkButtonsComponent,
		LinksComponent,
		MultifilterPocComponent,
		NavigationDesignsystemComponent,
		NavigationExternalComponent,
		NavigationIntranetComponent,
		NavigationPageComponent,
		NavigationtabsComponent,
		NyaIkonerComponent,
		SharedNavigationComponent,
		SidenavComponent,
		StepsComponent,
		TimepickerComponent,
		LanguagepickerComponent,
		NavigationtabsComponent,
		FormReceiptComponent,
		ValidationComponent,
		LinkButtonsComponent,
		ChartsComponent,
		ToolsLanguagepickerComponent,
		DatepickerComponent,
		MultifilterPocComponent,
		WheelOfFortuneComponent,
		TestFormSelectComponent
	],
	imports: [
		BrowserModule,
		ReactiveFormsModule,
		DigiArbetsformedlingenAngularModule,
		RouterModule.forRoot(routes)
	],
	providers: [],
	bootstrap: [AppComponent]
})
export class AppModule {}
