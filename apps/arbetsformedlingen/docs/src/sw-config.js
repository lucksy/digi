'use strict';
const glob = require("glob");
const fs = require('fs')
const path = require('path')

const distPath = '/../../../dist/apps/docs/www';

const setSwVersion = function () {
  const fileName = path.join(__dirname, distPath + '/service-worker.js');
  console.log('\nSetting service worker version...');
  fs.readFile(fileName, 'utf8', function (err, data) {
    if (err) {
      return console.log(err);
    }
    var result = data.replace(
      /__swVersion__/g,
      `'digi-docs-${process.env.NX_TASK_HASH.slice(2, 7)}'`
    );

    fs.writeFile(fileName, result, 'utf8', function (err) {
      if (err) return console.log(err);
    });
  });
  console.log('\nSuccessfully set service worker version!\n');
};

module.exports.setSwVersion = setSwVersion;


const precacheFiles = function () {
  const fileName = path.join(__dirname, distPath + '/service-worker.js');
  console.log('\nAdding precache files to service worker...');
  fs.readFile(fileName, 'utf8', function (err, data) {
    if (err) {
      return console.log(err);
    }

    glob.glob(path.join(__dirname, distPath+"/build/**/*.*"), {}, function (er, files) {
      console.log("\nFinding all files to cache...");
      const filesString = files.map(file => {
        const parsed = path.parse(file);
        return `${path.join("/build/", parsed.name + parsed.ext).replace(/\\/g,"/")}`
      });
      filesString.push('index.html');
    
    var result = data.replace(
      /precacheAndRoute\(\[\]\)/g,
      `precacheAndRoute(${JSON.stringify(
        filesString.map(str => {
          let returnObj = {};
          returnObj.url = str;
          str.match(/p-(.*).(.*)/) || (returnObj.revision = process.env.NX_TASK_HASH.slice(2, 7));
          return returnObj;
        })
        //filesString
        ,null,1)})`
    );

    fs.writeFile(fileName, result, 'utf8', function (err) {
      if (err) return console.log(err);
      console.log('\nSuccessfully added precache files to service worker!\n');
    });
  })
  });
};

module.exports.precacheFiles = precacheFiles;