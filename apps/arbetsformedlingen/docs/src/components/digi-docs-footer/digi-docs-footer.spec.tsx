import { newSpecPage } from '@stencil/core/testing';
import { DigiDocsFooter } from './digi-docs-footer';

describe('digi-docs-footer', () => {
  it('renders', async () => {
    const {root} = await newSpecPage({
      components: [DigiDocsFooter],
      html: '<digi-docs-footer></digi-docs-footer>'
    });
    expect(root).toEqualHtml(`
      <digi-docs-footer>
        <mock:shadow-root>
          <div>
            Hello, World! I'm
          </div>
        </mock:shadow-root>
      </digi-docs-footer>
    `);
  });

  it('renders with values', async () => {
    const {root} = await newSpecPage({
      components: [DigiDocsFooter],
      html: `<digi-docs-footer first="Stencil" last="'Don't call me a framework' JS"></digi-docs-footer>`
    });
    expect(root).toEqualHtml(`
      <digi-docs-footer first="Stencil" last="'Don't call me a framework' JS">
        <mock:shadow-root>
          <div>
            Hello, World! I'm Stencil 'Don't call me a framework' JS
          </div>
        </mock:shadow-root>
      </digi-docs-footer>
    `);
  });
});
