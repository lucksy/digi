import { Component, h, Listen } from '@stencil/core';
import { FooterVariation } from '@digi/arbetsformedlingen';
import { router } from '../../global/router';
import state from '../../store/store';
import { href } from 'stencil-router-v2';

@Component({
  tag: 'digi-docs-footer',
  styleUrl: 'digi-docs-footer.scss',
  scoped: true
})
export class DigiDocsFooter {
  Router = router;

  @Listen('afOnClick')
  clickHandler(e: any) {
    const href = e.detail.target.getAttribute('href');
    if (e.target.matches('digi-link') && href[0] === '/') {
      e.detail.preventDefault();
      this.Router.push(href);
    }
  }

  render() {
    return (
      <digi-footer afVariation={FooterVariation.SMALL}>
        <div slot="content-top">
          <div>
            <digi-footer-card af-type="icon">
              <ul>
                <li>
                  <a
                    {...href(`${state.routerRoot}tillganglighetsredogorelse`)}
                  >
                    <digi-icon-accessibility-universal></digi-icon-accessibility-universal>
                    Tillgänglighetsredogörelse
                  </a>
                </li>
                <li>
                  <a href="https://gitlab.com/arbetsformedlingen/designsystem">
                    <digi-icon-open-source></digi-icon-open-source>
                    Designsystemet är licensierat under Apache License 2.0
                  </a>
                </li>
                <li>
                  <a href="mailto:designsystem@arbetsformedlingen.se"><digi-icon-external-link-alt></digi-icon-external-link-alt>
                    Mejla vår funktionsbrevlåda
                  </a>
                </li>
              </ul>
            </digi-footer-card>
          </div>
          <div></div>
          <div></div>
        </div>
        <div slot="content-bottom-left">
          <a aria-label='Designsystem startsida' {...href('/')}>
            <digi-logo af-variation="large" af-color="secondary"></digi-logo>
          </a>
        </div>
        <div slot="content-bottom-right">
          <p>Följ oss på</p>
          <a href="https://www.facebook.com/Arbetsformedlingen">Facebook</a>
          <a href="https://www.youtube.com/Arbetsformedlingen">Youtube</a>
          <a href="https://www.linkedin.com/company/arbetsformedlingen">Linkedin</a>
          <a href="https://www.instagram.com/arbetsformedlingen">Instagram</a>
        </div>
      </digi-footer>
    );
  }
}
