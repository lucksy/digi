import { Component, h, State, Fragment, Prop } from '@stencil/core';
import { LayoutBlockVariation } from 'libs/core/package/src';
import { router } from '../../global/router';

@Component({
	tag: 'digi-docs-rekai',
	styleUrl: 'digi-docs-rekai.scss',
	shadow: true
})
export class DigiDocsRekai {
	Router = router;

	@State() rekAiPredictions = [];
	@State() isLoadingRekAi: boolean = true;
	@State() isLoadingShowMore: boolean = false;
	@State() searchKeyword: string = '';

	@Prop() nrofhits: number = 20;
	@Prop() page: number = 1;

	componentDidLoad() {
		var options = {
			overwrite: {
				nrofhits: this.nrofhits,
				addcontent: true,
				keywords: true
			}
		};

		window.__rekai?.predict(options, this.setRekAiPredictions.bind(this));
		window.__rekai?.checkAndCreatePredictions(window.__rekai.customer);
	}

	searchRekAiPredictions = (e: any) => {
		e.preventDefault();
		// const keyword = e.target.value;
		this.searchKeyword = e.target.value;

		if (this.searchKeyword.length < 3 && this.searchKeyword.length !== 0) return;

		const options = {
			overwrite: {
				nrofhits: this.nrofhits,
				addcontent: true,
				keywords: true,
				term: this.searchKeyword
			}
		};

		window.__rekai?.predict(options, this.setRekAiPredictions.bind(this));
		window.__rekai?.checkAndCreatePredictions(window.__rekai.customer);
	};

	setRekAiPredictions = (data: any) => {
		this.rekAiPredictions = data.predictions;
		this.rekAiPredictions.reverse();
		this.isLoadingRekAi = false;
	};

	addToRekAiPredictions = (data: any) => {
		const reductedPredictions = data.predictions.slice(-this.nrofhits);

		reductedPredictions.reverse();
		this.rekAiPredictions = [...this.rekAiPredictions, ...reductedPredictions];
	};

	handleShowMore = () => {
		if (this.rekAiPredictions.length > 20) return;

		this.isLoadingShowMore = true;
		this.page = this.page + 1;

		const hits = this.nrofhits * this.page;

		const options = {
			overwrite: {
				nrofhits: hits,
				addcontent: true,
				keywords: true
			}
		};

		window.__rekai?.predict(options, this.addToRekAiPredictions.bind(this));
		// window.__rekai?.checkAndCreatePredictions(window.__rekai.customer);
	};

	clickHandler(e, href) {
		e.preventDefault();
		this.Router.push(href);
	}

	render() {
		return (
			<digi-layout-block afVariation={LayoutBlockVariation.PRIMARY} class={'digi-docs-wrapper'}>
										<digi-typography>
							<h2
								class="digi-docs-rekai__heading"
							>
								Utforska designsystemet
							</h2>

						</digi-typography>
			<nav class={'digi-docs-rekai'} aria-label="Tagg meny">
				<ul class={'digi-docs-rekai_list'}>
					<li>
					<form
						class={`chip chip-input-wrapper chip-input-wrapper--icon-right `}
						autocomplete="off"
						onSubmit={(e) => e.preventDefault()}
					>
						<digi-icon-search></digi-icon-search>
						<input
							type="text"
							name=""
							id="search-startpage"
							// class="tag-"
							onInput={(e) => this.searchRekAiPredictions(e)}
							placeholder=" "
							tabindex={1}
						/>
						<label htmlFor="search-startpage">Vad söker du?</label>
					</form>
					</li>
					{this.isLoadingRekAi ? (
						<li class={`chip chip--nolink `}>Laddar...</li>
					) : this.rekAiPredictions.length > 0 ? (
						<Fragment>
							{this.rekAiPredictions.map((prediction) => {
								return (
									<a onClick={ (e) => this.clickHandler(e, prediction.url) } href={prediction.url}>
										<li key={prediction.pageid} class="chip">
											<span
												class={`digi-docs-rekai_newtag ${
													prediction.pubdate ? '' : 'digi-docs-rekai_newtag--hidden'
												}`}
											>
												Ny
											</span>
											<p>{prediction.title}</p>
										</li>
									</a>
								);
							})}
							{/* {this.searchKeyword.length < 1 && this.rekAiPredictions.length < 20 && (
								<li
									onClick={this.handleShowMore}
									class={'chip chip--nolink chip--dark'}
								>
									<button type="button">
										<digi-icon-plus></digi-icon-plus>
										Visa mer
									</button>
								</li>
							)} */}
						</Fragment>
					) : (
						<Fragment>
							<li class={`chip chip--nolink `}>Inget matchade din sökning...</li>
							{/* <li class={'chip'}>...men du kanske gillar några av dessa</li> */}
						</Fragment>
					)}
				</ul>
			</nav>
			</digi-layout-block>
		);
	}
}
