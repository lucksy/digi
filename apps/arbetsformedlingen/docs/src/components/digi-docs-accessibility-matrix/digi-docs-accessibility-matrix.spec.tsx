import { newSpecPage } from '@stencil/core/testing';
import { DigiDocsAccessibilityMatrix } from './digi-docs-accessibility-matrix';

describe('digi-docs-accessibility-matrix', () => {
	it('renders', async () => {
		const { root } = await newSpecPage({
			components: [DigiDocsAccessibilityMatrix],
			html: '<digi-docs-accessibility-matrix></digi-docs-accessibility-matrix>'
		});
		expect(root).toEqualHtml(`
      <accessibility-matrix>
        <mock:shadow-root>
          <div>
            Hello, World! I'm
          </div>
        </mock:shadow-root>
      </accessibility-matrix>
    `);
	});

	it('renders with values', async () => {
		const { root } = await newSpecPage({
			components: [DigiDocsAccessibilityMatrix],
			html: `<digi-docs-accessibility-matrix first="Stencil" last="'Don't call me a framework' JS"></accessibility-matrix>`
		});
		expect(root).toEqualHtml(`
      <digi-docs-accessibility-matrix first="Stencil" last="'Don't call me a framework' JS">
        <mock:shadow-root>
          <div>
            Hello, World! I'm Stencil 'Don't call me a framework' JS
          </div>
        </mock:shadow-root>
      </accessibility-matrix>
    `);
	});
});
