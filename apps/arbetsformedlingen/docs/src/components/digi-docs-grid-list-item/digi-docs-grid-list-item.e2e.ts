import { newE2EPage } from '@stencil/core/testing';

describe('digi-docs-grid-list-item', () => {
	it('renders', async () => {
		const page = await newE2EPage();

		await page.setContent(
			'<digi-docs-grid-list-item></digi-docs-grid-list-item>'
		);
		const element = await page.find('digi-docs-grid-list-item');
		expect(element).toHaveClass('hydrated');
	});

	it('renders changes to the name data', async () => {
		const page = await newE2EPage();

		await page.setContent(
			'<digi-docs-grid-list-item></digi-docs-grid-list-item>'
		);
		const component = await page.find('digi-docs-grid-list-item');
		const element = await page.find('digi-docs-grid-list-item >>> div');
		expect(element.textContent).toEqual(`Hello, World! I'm `);

		component.setProperty('first', 'James');
		await page.waitForChanges();
		expect(element.textContent).toEqual(`Hello, World! I'm James`);

		component.setProperty('last', 'Quincy');
		await page.waitForChanges();
		expect(element.textContent).toEqual(`Hello, World! I'm James Quincy`);

		component.setProperty('middle', 'Earl');
		await page.waitForChanges();
		expect(element.textContent).toEqual(`Hello, World! I'm James Earl Quincy`);
	});
});
