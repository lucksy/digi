import { newSpecPage } from '@stencil/core/testing';
import { GridListItem } from './digi-docs-grid-list-item';

describe('digi-docs-grid-list-item', () => {
	it('renders', async () => {
		const { root } = await newSpecPage({
			components: [GridListItem],
			html: '<digi-docs-grid-list-item></digi-docs-grid-list-item>'
		});
		expect(root).toEqualHtml(`
      <digi-docs-grid-list-item>
        <mock:shadow-root>
          <div>
            Hello, World! I'm
          </div>
        </mock:shadow-root>
      </digi-docs-grid-list-item>
    `);
	});

	it('renders with values', async () => {
		const { root } = await newSpecPage({
			components: [GridListItem],
			html: `<digi-docs-grid-list-item first="Stencil" last="'Don't call me a framework' JS"></digi-docs-grid-list-item>`
		});
		expect(root).toEqualHtml(`
      <digi-docs-grid-list-item first="Stencil" last="'Don't call me a framework' JS">
        <mock:shadow-root>
          <div>
            Hello, World! I'm Stencil 'Don't call me a framework' JS
          </div>
        </mock:shadow-root>
      </digi-docs-grid-list-item>
    `);
	});
});
