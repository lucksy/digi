import { newSpecPage } from '@stencil/core/testing';
import { GridList } from './digi-docs-grid-list';

describe('digi-docs-grid-list', () => {
	it('renders', async () => {
		const { root } = await newSpecPage({
			components: [GridList],
			html: '<digi-docs-grid-list></digi-docs-grid-list>'
		});
		expect(root).toEqualHtml(`
      <digi-docs-grid-list>
        <mock:shadow-root>
          <div>
            Hello, World! I'm
          </div>
        </mock:shadow-root>
      </digi-docs-grid-list>
    `);
	});

	it('renders with values', async () => {
		const { root } = await newSpecPage({
			components: [GridList],
			html: `<digi-docs-grid-list first="Stencil" last="'Don't call me a framework' JS"></digi-docs-grid-list>`
		});
		expect(root).toEqualHtml(`
      <digi-docs-grid-list first="Stencil" last="'Don't call me a framework' JS">
        <mock:shadow-root>
          <div>
            Hello, World! I'm Stencil 'Don't call me a framework' JS
          </div>
        </mock:shadow-root>
      </digi-docs-grid-list>
    `);
	});
});
