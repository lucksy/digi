import { Component, h, Prop, State, Watch } from '@stencil/core';
import { DocsColorSwatchVariation } from '../color-swatch/digi-docs-color-swatch-variation.enum';
import { TokenFormat } from '../../token-format.enum';

@Component({
  tag: 'digi-docs-color-token',
  styleUrl: 'digi-docs-color-token.scss',
  scoped: true
})
export class DigiDocsColorToken {
  private _token;
  @State() _tokenFormat: TokenFormat = TokenFormat.CUSTOM_PROPERTY;

  /**
   * Design token
   */
  @Prop() token: string;

  @Prop() tokenVariation: DocsColorSwatchVariation = DocsColorSwatchVariation.RECTANGLE;

  @Prop() tokenFormat: TokenFormat = TokenFormat.CUSTOM_PROPERTY; 

  @Watch('tokenFormat')
  tokenFormatChangeHandler() {
    this._tokenFormat = this.tokenFormat;
  }

  componentWillLoad() {
    this._token = JSON.parse(this.token);
    this.tokenFormatChangeHandler();
  }

  get customProperty() {
    return `--${this._token.name}`
  }

  get scss() {
    return `$${this._token.name}`
  }

  render() {
    return (
      <div class="digi-docs-color-token">
        <div class="digi-docs-color-token__swatch">
          <digi-docs-color-swatch
            token={this.token}
            token-variation={this.tokenVariation}
          ></digi-docs-color-swatch>
        </div>
        <div class="digi-docs-color-token__content">
          <h4 class="digi-docs-color-token__heading">{this._token.original.name}</h4>
          <div>
            {this._token.comment && (
              <p>{this._token.comment}</p>
            )}
          </div>
          <div class="digi-docs-color-token__code">
            {this._tokenFormat == TokenFormat.CUSTOM_PROPERTY && (
              <digi-code afCode={this.customProperty}></digi-code>
            )}
            {this._tokenFormat == TokenFormat.SASS && (
              <digi-code afCode={this.scss}></digi-code>
            )}
          </div>
        </div>
      </div>
    );
  }
}
