import { newE2EPage } from '@stencil/core/testing';

describe('digi-docs-token-copy', () => {
	it('renders', async () => {
		const page = await newE2EPage();

		await page.setContent('<digi-docs-token-copy></digi-docs-token-copy>');
		const element = await page.find('digi-docs-token-copy');
		expect(element).toHaveClass('hydrated');
	});

	it('renders changes to the name data', async () => {
		const page = await newE2EPage();

		await page.setContent('<digi-docs-token-copy></digi-docs-token-copy>');
		const component = await page.find('digi-docs-token-copy');
		const element = await page.find('digi-docs-token-copy >>> div');
		expect(element.textContent).toEqual(`Hello, World! I'm `);

		component.setProperty('first', 'James');
		await page.waitForChanges();
		expect(element.textContent).toEqual(`Hello, World! I'm James`);

		component.setProperty('last', 'Quincy');
		await page.waitForChanges();
		expect(element.textContent).toEqual(`Hello, World! I'm James Quincy`);

		component.setProperty('middle', 'Earl');
		await page.waitForChanges();
		expect(element.textContent).toEqual(`Hello, World! I'm James Earl Quincy`);
	});
});
