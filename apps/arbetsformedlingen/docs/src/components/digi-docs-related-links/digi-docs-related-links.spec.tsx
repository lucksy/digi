import { newSpecPage } from '@stencil/core/testing';
import { RelatedLinks } from './digi-docs-related-links';

describe('digi-docs-related-links', () => {
	it('renders', async () => {
		const { root } = await newSpecPage({
			components: [RelatedLinks],
			html: '<digi-docs-related-links></digi-docs-related-links>'
		});
		expect(root).toEqualHtml(`
      <digi-docs-related-links>
        <mock:shadow-root>
          <div>
            Hello, World! I'm
          </div>
        </mock:shadow-root>
      </digi-docs-related-links>
    `);
	});

	it('renders with values', async () => {
		const { root } = await newSpecPage({
			components: [RelatedLinks],
			html: `<digi-docs-related-links first="Stencil" last="'Don't call me a framework' JS"></digi-docs-related-links>`
		});
		expect(root).toEqualHtml(`
      <digi-docs-related-links first="Stencil" last="'Don't call me a framework' JS">
        <mock:shadow-root>
          <div>
            Hello, World! I'm Stencil 'Don't call me a framework' JS
          </div>
        </mock:shadow-root>
      </digi-docs-related-links>
    `);
	});
});
