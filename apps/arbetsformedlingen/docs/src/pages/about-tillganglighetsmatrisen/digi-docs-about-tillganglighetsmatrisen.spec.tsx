import { newSpecPage } from '@stencil/core/testing';
import { DigiDocsAboutTillganglighetsmatrisen } from './digi-docs-about-tillganglighetsmatrisen';

describe('digi-docs-about-tillganglighetsmastrisen', () => {
	it('renders', async () => {
		const { root } = await newSpecPage({
			components: [DigiDocsAboutTillganglighetsmatrisen],
			html:
				'<digi-docs-about-tillganglighetsmatrisen></-digi-docs-about-tillganglighetsmatrisen>'
		});
		expect(root).toEqualHtml(`
      <digi-docs-about-tillganglighetsmatrisen>
        <mock:shadow-root>
          <div>
            Hello, World! I'm
          </div>
        </mock:shadow-root>
      </digi-docs-about-tillganglighetsmatrisen>
    `);
	});

	it('renders with values', async () => {
		const { root } = await newSpecPage({
			components: [DigiDocsAboutTillganglighetsmatrisen],
			html: `<digi-docs-about-tillganglighetsmatrisen first="Stencil" last="'Don't call me a framework' JS"></digi-docs-about-tillganglighetsmatrisen>`
		});
		expect(root).toEqualHtml(`
      <digi-docs-about-tillganglighetsmatrisen first="Stencil" last="'Don't call me a framework' JS">
        <mock:shadow-root>
          <div>
            Hello, World! I'm Stencil 'Don't call me a framework' JS
          </div>
        </mock:shadow-root>
      </digi-docs-about-tillganglighetsmatrisen>
    `);
	});
});
