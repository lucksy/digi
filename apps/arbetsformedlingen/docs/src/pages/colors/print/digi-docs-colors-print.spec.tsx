import { newSpecPage } from '@stencil/core/testing';
import { DigiDocsColorsPrint } from './digi-docs-colors-print';

describe('digi-docs-colors-print', () => {
  it('renders', async () => {
    const { root } = await newSpecPage({
      components: [DigiDocsColorsPrint],
      html: '<digi-docs-colors-print></digi-docs-colors-print>'
    });
    expect(root).toEqualHtml(`
      <digi-docs-colors-print>
        <mock:shadow-root>
          <div>
            Hello, World! I'm
          </div>
        </mock:shadow-root>
      </digi-docs-colors-print>
    `);
  });

  it('renders with values', async () => {
    const { root } = await newSpecPage({
      components: [DigiDocsColorsPrint],
      html: `<digi-docs-colors-print first="Stencil" last="'Don't call me a framework' JS"></digi-docs-colors-print>`
    });
    expect(root).toEqualHtml(`
      <digi-docs-colors-print first="Stencil" last="'Don't call me a framework' JS">
        <mock:shadow-root>
          <div>
            Hello, World! I'm Stencil 'Don't call me a framework' JS
          </div>
        </mock:shadow-root>
      </digi-docs-colors-print>
    `);
  });
});
