import { newSpecPage } from '@stencil/core/testing';
import { DigiDocsColors } from './digi-docs-colors';

describe('digi-docs-colors', () => {
  it('renders', async () => {
    const { root } = await newSpecPage({
      components: [DigiDocsColors],
      html: '<digi-docs-colors></digi-docs-colors>'
    });
    expect(root).toEqualHtml(`
      <digi-docs-colors>
        <mock:shadow-root>
          <div>
            Hello, World! I'm
          </div>
        </mock:shadow-root>
      </digi-docs-colors>
    `);
  });

  it('renders with values', async () => {
    const { root } = await newSpecPage({
      components: [DigiDocsColors],
      html: `<digi-docs-colors first="Stencil" last="'Don't call me a framework' JS"></digi-docs-colors>`
    });
    expect(root).toEqualHtml(`
      <digi-docs-colors first="Stencil" last="'Don't call me a framework' JS">
        <mock:shadow-root>
          <div>
            Hello, World! I'm Stencil 'Don't call me a framework' JS
          </div>
        </mock:shadow-root>
      </digi-docs-colors>
    `);
  });
});
