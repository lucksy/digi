import { Component, h, State } from '@stencil/core';
import state from '../../store/store';
import { router } from '../../global/router';
import { LayoutBlockContainer } from '@digi/arbetsformedlingen';
import { match, Route } from 'stencil-router-v2';

@Component({
  tag: 'digi-docs-colors',
  styleUrl: 'digi-docs-colors.scss',
  scoped: true
})
export class DigiDocsColors {
  Router = router;

  @State() isMobile: boolean;
  @State() isOpen: boolean;
  @State() activeTab: number;

  getTabId(): number {
    const tabName = state.router.activePath;

    switch (tabName) {
      case '/grafisk-profil/farger/oversikt':
        return 0;
      case '/grafisk-profil/farger/farg-tokens':
        return 1
      case '/grafisk-profil/farger/tryck-farger':
        return 2;
      default:
        return 0;
    }
  }

  toggleNavTabHandler(e) {
    const tag = e.target.dataset.tag;
    this.Router.push(`${state.routerRoot}grafisk-profil/farger/${tag}`);
  }

  componentWillLoad() {
    this.activeTab = this.getTabId();
  }

  render() {
    return (
      <div class="digi-docs-colors">
        <digi-docs-page-layout>
          <digi-layout-block>
            <digi-typography-heading-jumbo af-text="Färger"></digi-typography-heading-jumbo>
            <digi-navigation-tabs
              af-aria-label="Innehåll för färger"
              af-init-active-tab={this.activeTab}
            >
              <digi-navigation-tab
                onAfOnToggle={(e) => this.toggleNavTabHandler(e)}
                data-tag="oversikt"
                afAriaLabel="Översikt"
              ></digi-navigation-tab>
              <digi-navigation-tab
                onAfOnToggle={(e) => this.toggleNavTabHandler(e)}
                data-tag="farg-tokens"
                afAriaLabel="Färgtokens"
              ></digi-navigation-tab>
              <digi-navigation-tab
                onAfOnToggle={(e) => this.toggleNavTabHandler(e)}
                data-tag="tryck-farger"
                afAriaLabel="Tryckfärger"
              ></digi-navigation-tab>
            </digi-navigation-tabs>
          </digi-layout-block>

          <digi-layout-block af-container={LayoutBlockContainer.NONE}>
            <this.Router.Switch>
              <Route
                path={match("/grafisk-profil/farger/oversikt")}
                render={() => (
                  <digi-docs-colors-description />
                )}
              />
              <Route
                path={match("/grafisk-profil/farger/farg-tokens")}
                render={() => (
                  <digi-docs-colors-tokens />
                )}
              />
              <Route
                path={match("/grafisk-profil/farger/tryck-farger")}
                render={() => (
                  <digi-docs-colors-print />
                )}
              />
            </this.Router.Switch>
          </digi-layout-block>
        </digi-docs-page-layout>
      </div>
    );
  }
}
