import { newSpecPage } from '@stencil/core/testing';
import { Christmas2023 } from './digi-docs-christmas-2023';

describe('digi-docs-christmas-2023', () => {
	it('renders', async () => {
		const { root } = await newSpecPage({
			components: [Christmas2023],
			html: '<digi-docs-christmas-2023></digi-docs-christmas-2023>'
		});
		expect(root).toEqualHtml(`
      <digi-docs-christmas-2023>
        <mock:shadow-root>
          <div>
            Hello, World! I'm
          </div>
        </mock:shadow-root>
      </digi-docs-christmas-2023>
    `);
	});

	it('renders with values', async () => {
		const { root } = await newSpecPage({
			components: [Christmas2023],
			html: `<digi-docs-christmas-2023 first="Stencil" last="'Don't call me a framework' JS"></digi-docs-christmas-2023>`
		});
		expect(root).toEqualHtml(`
      <digi-docs-christmas-2023 first="Stencil" last="'Don't call me a framework' JS">
        <mock:shadow-root>
          <div>
            Hello, World! I'm Stencil 'Don't call me a framework' JS
          </div>
        </mock:shadow-root>
      </digi-docs-christmas-2023>
    `);
	});
});
