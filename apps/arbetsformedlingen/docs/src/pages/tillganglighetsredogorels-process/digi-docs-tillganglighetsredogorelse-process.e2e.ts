import { newE2EPage } from '@stencil/core/testing';

describe('digi-docs-tillganglighetsredogorelse-process', () => {
  it('renders', async () => {
    const page = await newE2EPage();

    await page.setContent('<digi-docs-tillganglighetsredogorelse-process></digi-docs-tillganglighetsredogorelse-process>');
    const element = await page.find('digi-docs-tillganglighetsredogorelse-process');
    expect(element).toHaveClass('hydrated');
  });

  it('renders changes to the name data', async () => {
    const page = await newE2EPage();

    await page.setContent('<digi-docs-tillganglighetsredogorelse-process></digi-docs-tillganglighetsredogorelse-process>');
    const component = await page.find('digi-docs-tillganglighetsredogorelse-process');
    const element = await page.find('digi-docs-tillganglighetsredogorelse-process >>> div');
    expect(element.textContent).toEqual(`Hello, World! I'm `);

    component.setProperty('first', 'James');
    await page.waitForChanges();
    expect(element.textContent).toEqual(`Hello, World! I'm James`);

    component.setProperty('last', 'Quincy');
    await page.waitForChanges();
    expect(element.textContent).toEqual(`Hello, World! I'm James Quincy`);

    component.setProperty('middle', 'Earl');
    await page.waitForChanges();
    expect(element.textContent).toEqual(`Hello, World! I'm James Earl Quincy`);
  });
});
