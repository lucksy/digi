import { Component, h, State } from '@stencil/core';
import state from '../../../store/store';
import { router } from '../../../global/router';
import { Route } from 'stencil-router-v2';

import { LayoutBlockContainer } from '@digi/arbetsformedlingen';

@Component({
	tag: 'digi-docs-icons',
	styleUrl: 'digi-docs-icons.scss',
	scoped: true
})
export class DigiDocsIcons {
	Router = router;

	@State() isMobile: boolean;
	@State() isOpen: boolean;
	@State() tabId: number;
	@State() pageName = 'Ikonbibliotek';

	componentWillLoad() {
		this.setTabId();
	}

	setTabId(): void {
		const tabName: string = state.router.activePath;

		switch (tabName) {
			case '/ikoner/bibliotek':
				this.tabId = 0;
				break;
			case '/ikoner/beskrivning':
				this.tabId = 1;
				break;
			case '/ikoner/api':
				this.tabId = 2;
				break;
			default:
				this.tabId = 0;
				break;
		}
	}

	toggleNavTabHandler(e) {
		const tag = e.target.dataset.tag;
		this.Router.push(`${state.routerRoot}ikoner/${tag}`);
	}

	abstractLinkHandler(e) {
		e.detail.preventDefault();
		window.open(e.target.afHref, '_blank');
	}

	render() {
		return (
			<div class="digi-docs-icons">
				<digi-docs-page-layout af-page-heading={this.pageName}>
					<span slot="preamble">
						Ikonerna på den här sidan är framtagna för att vara tillgängliga, användbara och att erbjuda en enhetlig upplevelse i våra tjänster.
					</span>
					<digi-layout-block af-container={LayoutBlockContainer.STATIC}>
						<digi-layout-container af-no-gutter af-margin-bottom af-margin-top>
							<p class="digi-docs-icons__link-abstract">
								<digi-link-button
									onAfOnClick={(e) => this.abstractLinkHandler(e)}
									afHref="https://share.goabstract.com/0b507974-e49c-4d34-81c2-4ccd5711b317?mode=design&sha=cfa673f30bb4a41cee9cb0ef96d9aa4bc0148b6a"
									af-href="https://share.goabstract.com/0b507974-e49c-4d34-81c2-4ccd5711b317?mode=design&sha=cfa673f30bb4a41cee9cb0ef96d9aa4bc0148b6a"
								>
									Ikonbiblioteket i Abstract
								</digi-link-button>
							</p>

							<p>
								Om du behöver en ikon som inte finns idag så kan du höra av dig till oss
								så hjälper vi till med att skapa den och lägga upp den i designsystemets
								utvecklingsmiljö.
							</p>
							<digi-link
								af-variation="small"
								afHref={`mailto:designsystem@arbetsformedlingen.se`}
								class="digi-docs__accessibility-link"
							>
								<digi-icon-envelope slot="icon"></digi-icon-envelope>
								Mejla designsystem@arbetsformedlingen.se
							</digi-link>
						</digi-layout-container>
						<digi-navigation-tabs
							af-aria-label="Innehåll för ikoner"
							af-init-active-tab={this.tabId}
						>
							<digi-navigation-tab
								onAfOnToggle={(e) => this.toggleNavTabHandler(e)}
								data-tag="bibliotek"
								afAriaLabel="Bibliotek"
							></digi-navigation-tab>
							<digi-navigation-tab
								onAfOnToggle={(e) => this.toggleNavTabHandler(e)}
								data-tag="beskrivning"
								afAriaLabel="Beskrivning"
							></digi-navigation-tab>
							<digi-navigation-tab
								onAfOnToggle={(e) => this.toggleNavTabHandler(e)}
								data-tag="api"
								afAriaLabel="Kod"
							></digi-navigation-tab>
						</digi-navigation-tabs>
					</digi-layout-block>
					<digi-layout-block af-container={LayoutBlockContainer.NONE}>
						<this.Router.Switch>
							<Route path="/ikoner/bibliotek">
								<digi-docs-icons-library />
							</Route>
							<Route path="/ikoner/beskrivning">
								<digi-docs-icons-description />
							</Route>
							<Route path="/ikoner/api">
								<digi-docs-icons-api />
							</Route>
						</this.Router.Switch>
					</digi-layout-block>
				</digi-docs-page-layout>
			</div>
		);
	}
}
