import { newE2EPage } from '@stencil/core/testing';

describe('digi-docs-work-with-digi-core-react', () => {
	it('renders', async () => {
		const page = await newE2EPage();

		await page.setContent(
			'<digi-docs-work-with-digi-core-react></digi-docs-work-with-digi-core-react>'
		);
		const element = await page.find('digi-docs-work-with-digi-core-react');
		expect(element).toHaveClass('hydrated');
	});

	it('renders changes to the name data', async () => {
		const page = await newE2EPage();

		await page.setContent(
			'<digi-docs-work-with-digi-core-react></digi-docs-work-with-digi-core-react>'
		);
		const component = await page.find('digi-docs-work-with-digi-core-react');
		const element = await page.find(
			'digi-docs-work-with-digi-core-react >>> div'
		);
		expect(element.textContent).toEqual(`Hello, World! I'm `);

		component.setProperty('first', 'James');
		await page.waitForChanges();
		expect(element.textContent).toEqual(`Hello, World! I'm James`);

		component.setProperty('last', 'Quincy');
		await page.waitForChanges();
		expect(element.textContent).toEqual(`Hello, World! I'm James Quincy`);

		component.setProperty('middle', 'Earl');
		await page.waitForChanges();
		expect(element.textContent).toEqual(`Hello, World! I'm James Earl Quincy`);
	});
});
