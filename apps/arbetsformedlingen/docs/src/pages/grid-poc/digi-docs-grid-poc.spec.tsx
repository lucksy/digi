import { newSpecPage } from '@stencil/core/testing';
import { GridPoc } from './digi-docs-grid-poc';

describe('digi-docs-grid-poc', () => {
	it('renders', async () => {
		const { root } = await newSpecPage({
			components: [GridPoc],
			html: '<digi-docs-grid-poc></digi-docs-grid-poc>'
		});
		expect(root).toEqualHtml(`
      <digi-docs-grid-poc>
        <mock:shadow-root>
          <div>
            Hello, World! I'm
          </div>
        </mock:shadow-root>
      </digi-docs-grid-poc>
    `);
	});

	it('renders with values', async () => {
		const { root } = await newSpecPage({
			components: [GridPoc],
			html: `<digi-docs-grid-poc first="Stencil" last="'Don't call me a framework' JS"></digi-docs-grid-poc>`
		});
		expect(root).toEqualHtml(`
      <digi-docs-grid-poc first="Stencil" last="'Don't call me a framework' JS">
        <mock:shadow-root>
          <div>
            Hello, World! I'm Stencil 'Don't call me a framework' JS
          </div>
        </mock:shadow-root>
      </digi-docs-grid-poc>
    `);
	});
});
