import { newE2EPage } from '@stencil/core/testing';

describe('digi-docs-accessibility-laws-and-guidelines', () => {
  it('renders', async () => {
    const page = await newE2EPage();

    await page.setContent(
      '<digi-docs-accessibility-laws-and-guidelines></digi-docs-accessibility-laws-and-guidelines>'
    );
    const element = await page.find(
      'digi-docs-accessibility-laws-and-guidelines'
    );
    expect(element).toHaveClass('hydrated');
  });

  it('renders changes to the name data', async () => {
    const page = await newE2EPage();

    await page.setContent(
      '<digi-docs-accessibility-laws-and-guidelines></digi-docs-accessibility-laws-and-guidelines>'
    );
    const component = await page.find(
      'digi-docs-accessibility-laws-and-guidelines'
    );
    const element = await page.find(
      'digi-docs-accessibility-laws-and-guidelines >>> div'
    );
    expect(element.textContent).toEqual(`Hello, World! I'm `);

    component.setProperty('first', 'James');
    await page.waitForChanges();
    expect(element.textContent).toEqual(`Hello, World! I'm James`);

    component.setProperty('last', 'Quincy');
    await page.waitForChanges();
    expect(element.textContent).toEqual(`Hello, World! I'm James Quincy`);

    component.setProperty('middle', 'Earl');
    await page.waitForChanges();
    expect(element.textContent).toEqual(`Hello, World! I'm James Earl Quincy`);
  });
});
