import { Component, getAssetPath, h } from '@stencil/core';
import { router } from '../../../global/router';


@Component({
	tag: 'digi-about-digital-accessibility-intro',
	styleUrl: 'digi-about-digital-accessibility-intro.scss',
	scoped: true
})
export class AboutDigitalAccessibilityIntro {
	Router = router;
  
	linkHandler(e) {
		e.detail.preventDefault();
		this.Router.push(e.target.afHref);
	}

	render() {
		return (
			<host>
				<digi-layout-container>
				<div class="digi-about-digital-accessibility-intro">
					<div class="digi-about-digital-accessibility-intro__text">
					<digi-typography-heading-jumbo
						af-text= "Om digital tillgänglighet"
						af-level="h1"
					></digi-typography-heading-jumbo>
						<digi-typography-preamble class="digi-about-digital-accessibility-intro__preamble">
						Arbetsförmedlingens användare har olika förutsättningar och behov. Vårt
							mål är att allt digitalt som produceras på Arbetsförmedlingen ska vara
							lätt att förstå och enkelt att använda, oavsett vem användaren är.
						</digi-typography-preamble>
						<h2>Vad innebär digital tillgänglighet?</h2>
							<p>
								Digital tillgänglighet innebär att vi utgår från användarnas behov. Både
								de tjänster vi utvecklar och vår digitala arbetsmiljö ska vara lätt att
								förstå och enkelt att använda.
							</p>
					</div>
					<div class="digi-about-digital-accessibility-intro__media">
						<digi-media-image
							class="digi-about-digital-accessibility-intro__media-image"
							afUnlazy
							afSrc={getAssetPath('/assets/images/about-digital-accessibility/Frame1.png')}
							afAlt="Person i centrum med vissa geometriska former runtomkring, illustration."
						></digi-media-image>
					</div>
				</div>
				</digi-layout-container>
			</host>
		);
	}
}
