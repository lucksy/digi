import { newSpecPage } from '@stencil/core/testing';
import { AboutDigitalAccessibilityIntro } from './digi-about-digital-accessibility-intro';

describe('digi-pattern-agentive-intro', () => {
	it('renders', async () => {
		const { root } = await newSpecPage({
			components: [AboutDigitalAccessibilityIntro],
			html: '<digi-about-digital-accessibility-intro></digi-about-digital-accessibility-intro>'
		});
		expect(root).toEqualHtml(`
      <digi-about-digital-accessibility-intro>
        <mock:shadow-root>
          <div>
            Hello, World! I'm
          </div>
        </mock:shadow-root>
      </digi-about-digital-accessibility-intro>
    `);
	});

	it('renders with values', async () => {
		const { root } = await newSpecPage({
			components: [AboutDigitalAccessibilityIntro],
			html: `<digi-about-digital-accessibility-intro first="Stencil" last="'Don't call me a framework' JS"></digi-about-digital-accessibility-intro>`
		});
		expect(root).toEqualHtml(`
      <digi-about-digital-accessibility-intro first="Stencil" last="'Don't call me a framework' JS">
        <mock:shadow-root>
          <div>
            Hello, World! I'm Stencil 'Don't call me a framework' JS
          </div>
        </mock:shadow-root>
      </digi-about-digital-accessibility-intro>
    `);
	});
});
