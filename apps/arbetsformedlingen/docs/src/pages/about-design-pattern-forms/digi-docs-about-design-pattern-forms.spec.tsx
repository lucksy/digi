import { newSpecPage } from '@stencil/core/testing';
import { AboutDesignPatternForms } from './digi-docs-about-design-pattern-forms';

describe('digi-docs-about-design-pattern-forms', () => {
	it('renders', async () => {
		const { root } = await newSpecPage({
			components: [AboutDesignPatternForms],
			html:
				'<digi-docs-about-design-pattern-forms></digi-docs-about-design-pattern-forms>'
		});
		expect(root).toEqualHtml(`
      <digi-docs-about-design-pattern-forms>
        <mock:shadow-root>
          <div>
            Hello, World! I'm
          </div>
        </mock:shadow-root>
      </digi-docs-about-design-pattern-forms>
    `);
	});

	it('renders with values', async () => {
		const { root } = await newSpecPage({
			components: [AboutDesignPatternForms],
			html: `<digi-docs-about-design-pattern-forms first="Stencil" last="'Don't call me a framework' JS"></digi-docs-about-design-pattern-forms>`
		});
		expect(root).toEqualHtml(`
      <digi-docs-about-design-pattern-forms first="Stencil" last="'Don't call me a framework' JS">
        <mock:shadow-root>
          <div>
            Hello, World! I'm Stencil 'Don't call me a framework' JS
          </div>
        </mock:shadow-root>
      </digi-docs-about-design-pattern-forms>
    `);
	});
});
