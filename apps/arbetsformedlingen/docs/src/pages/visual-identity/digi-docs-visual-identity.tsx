import { Component, h } from '@stencil/core';


@Component({
  tag: 'digi-docs-visual-identity',
  styleUrl: 'digi-docs-visual-identity.scss',
  scoped: true
})
export class DigiDocsVisualIdentity {
  
  render() {
    return (
      <div class="digi-docs-visual-identity">
        <digi-docs-page-layout
          af-edit-href="pages/digi-docs-visual-identity/digi-docs-visual-identity.tsx"
        >
          <digi-typography>
            <digi-layout-block>
              <digi-typography-heading-jumbo af-text="Arbetsförmedlingens grafiska profil"></digi-typography-heading-jumbo>
              <digi-typography-preamble>
                Här finner du all information som är relevant för dig som
                producerar material och tjänster under Arbetsförmedlingens
                varumärke.
              </digi-typography-preamble>
              <div class="digi-docs-visual-identity">

              <h2>Varumärket Arbetsförmedlingen</h2>
              <p>
                Läs mer om Arbetsförmedlingens värdegrund, uppdrag, vision och
                bakgrund.
              </p>
              <h2>Logotyp </h2>
              <p>
                Allt du behöver veta om hur och när Arbetsförmedlingens logotyp
                skall stå som avsändare.
              </p>
              <h2>Färgpaletten </h2>
              <p>
                Profilfärger och funktionsfärger. Förklaringar för när de
                används och verktyg för att hitta rätt färg för rätt tillfälle.
              </p>
              <h2>Typografi</h2>
              <p>
                När och hur vi använder våra olika typsnitt. Nedladdning av
                typsnittsfiler.
              </p>
              <h2>Grafik</h2>
              <p>
                Vi har fler olika typer av grafik. Förklaringar på när och hur
                de skall användas. Illustrationer, informationsgrafik, diagram
                och ikonbibliotek.
              </p>
              <h2>Bildspråk</h2>
              <p>
                Genomgång av bildpolicy och hur du hittar rätt foto i
                mediabanken.
              </p>
              <h2>Klarspråk</h2>
              <p>
                Hur vi jobbar systematiskt med namngivningar, termer och
                uttryck.
              </p>
              <div class='manual-button'>
              {/* <digi-link-button afHref="https://start.arbetsformedlingen.se/download/18.78b85875178db26ae1b432/1619526542071/grafisk-manual.pdf" af-target="_blank"> */}
              <digi-link-button afHref="https://space.arbetsformedlingen.se/sites/byggstenendesignsystem/Delade%20dokument/Resurser/grafisk-manual.pdf" af-target="_blank">
                Grafiska manualen som PDF
              </digi-link-button>
              </div>
              </div>
            </digi-layout-block>
          </digi-typography>
        </digi-docs-page-layout>      
        </div>
    );
  }
}
