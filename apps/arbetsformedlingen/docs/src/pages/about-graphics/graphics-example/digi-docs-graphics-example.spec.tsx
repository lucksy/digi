import { newSpecPage } from '@stencil/core/testing';
import { GraphicsExample } from './digi-docs-graphics-example';

describe('digi-docs-graphics-example', () => {
	it('renders', async () => {
		const { root } = await newSpecPage({
			components: [GraphicsExample],
			html: '<digi-docs-graphics-example></digi-docs-graphics-example>'
		});
		expect(root).toEqualHtml(`
      <digi-docs-graphics-example>
        <mock:shadow-root>
          <div>
            Hello, World! I'm
          </div>
        </mock:shadow-root>
      </digi-docs-graphics-example>
    `);
	});

	it('renders with values', async () => {
		const { root } = await newSpecPage({
			components: [GraphicsExample],
			html: `<digi-docs-graphics-example first="Stencil" last="'Don't call me a framework' JS"></digi-docs-graphics-example>`
		});
		expect(root).toEqualHtml(`
      <digi-docs-graphics-example first="Stencil" last="'Don't call me a framework' JS">
        <mock:shadow-root>
          <div>
            Hello, World! I'm Stencil 'Don't call me a framework' JS
          </div>
        </mock:shadow-root>
      </digi-docs-graphics-example>
    `);
	});
});
