import { Component, h, State } from '@stencil/core';
import { LayoutBlockVariation } from '@digi/arbetsformedlingen';

@Component({
  tag: 'digi-docs-example-page',
  styleUrl: 'digi-docs-example-page.scss',
  scoped: true
})
export class DigiDocsExamplePage {
  @State() pageName = 'Example page';

  render() {
    return (
      <host>
        <digi-docs-page-layout
          af-page-heading={this.pageName}
          af-edit-href="pages/_example-page/digi-docs-example-page.tsx"
        >
          <span slot="preamble">
            Lorem ipsum I am a preamble, look at me!
          </span>
          <digi-layout-container af-margin-bottom af-margin-top>
            <article>
              <p>Has af-margin-top and af-margin-bottom</p>
            </article>
          </digi-layout-container>
          <digi-layout-container af-margin-bottom>
            <article>
              <h2>Only one H2 within layout container</h2>
              <p>Any number of P within layout container</p>
              <p>Has af-margin-bottom</p>
            </article>
          </digi-layout-container>
          <digi-layout-container af-margin-bottom>
            <article>
              <h2>Only one H2 within layout container</h2>
              <h3>Any number of H3 withing layout container</h3>
              <p>Any number of P within layout container</p>
              <h3>Some heading again</h3>
              <p>Any number of P within layout container</p>
              <p>Any number of P within layout container</p>
              <p>Has af-margin-bottom</p>
            </article>
          </digi-layout-container>
          <digi-layout-block
            af-variation={LayoutBlockVariation.SYMBOL}
            af-vertical-padding
            af-margin-bottom
          >
            <h2>Some heading</h2>
            <ul>
              <li>has af-vertical-padding</li>
              <li>Has af-margin-bottom</li>
            </ul>
          </digi-layout-block>
          <digi-layout-block
            af-variation={LayoutBlockVariation.SECONDARY}
            af-vertical-padding
            af-margin-bottom
          >
            <h2>Some heading</h2>
            <ul>
              <li>has af-vertical-padding</li>
              <li>Has af-margin-bottom</li>
            </ul>
          </digi-layout-block>
        </digi-docs-page-layout>
      </host>
    );
  }
}