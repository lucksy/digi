import { Component, h } from '@stencil/core';

@Component({
  tag: 'digi-docs-introduktion',
  styleUrl: 'digi-docs-introduktion.scss',
  scoped: true
})
export class Introduktion {
  render() {
    return (
      <div class="digi-docs-introduktion">
        <digi-docs-page-layout af-edit-href="pages/introduktion/digi-docs-introduktion.tsx">
          <digi-layout-block afMarginBottom>
            <digi-typography-heading-jumbo af-text="Introduktion"></digi-typography-heading-jumbo>
            <digi-typography-preamble>
              Designsystemet består av två delar. Den viktigaste delen är alla
              vi, människorna, som delar en gemensam filosofi om att vi vill
              samarbeta och tillsammans ta fram bästa tänkbara tjänster och
              produkter för våra användare.
            </digi-typography-preamble>
            <digi-typography>
              <div>
                <p>
                  Den andra delen är hur vi ska göra detta. Själva
                  verktygslådan. Designsystemet som byggsten samlar
                  återanvändbara verktyg, processer och riktlinjer på ett
                  ställe. När vi både har en gemensam samarbetsmodell och
                  konkret verktygslåda kan vi alla bidra till att hela tiden
                  förbättra både designsystemet och våra produkter.
                  <br />
                  <br />
                  Vi kan känna oss trygga i att det vi tar fram lever upp till
                  lagkrav och varumärkesriktlinjer. Det i sin tur ger våra
                  användare en mer sammanhållen och tillgänglig upplevelse så de
                  kan känna sig trygga i att de får utföra sitt ärende på ett
                  bra och säkert sätt.
                  <br />
                  <br />
                  Arbetsförmedlingens designsystem består av de här delarna:
                </p>

                <digi-list>
                  <li>
                    <span class="digi-docs__bold-text">
                      En väletablerad samarbetsmodell:
                    </span>{' '}
                    Vi ska utveckla designsystemet tillsammans och för detta har
                    byggstenen en väletablerad samarbetsmodell som gör det
                    enkelt att både komma igång och hitta sätt att bidra in till
                    designsystemet.
                  </li>
                  <li>
                    <span class="digi-docs__bold-text">Designmönster: </span>
                    Design- och interaktionsmönster för hur gränssnitt bör
                    utformas för att lösa specifika behov.
                    <span class="digi-docs__bold-text">
                      {' '}
                      Vi har påbörjat arbetet med att ta fram designmönster och
                      principer.
                    </span>
                  </li>
                  <li>
                    <span class="digi-docs__bold-text">Designprinciper: </span>
                    Ramverk kring hur en designer ska och inte ska göra i olika
                    situationer.{' '}
                    <span class="digi-docs__bold-text">
                      Vi har påbörjat arbetet med att ta fram designmönster och
                      principer.
                    </span>
                  </li>
                  <li>
                    <span class="digi-docs__bold-text">Grafisk profil: </span>
                    Verktyg och riktlinjer för hur du jobbar med
                    Arbetsförmedlingens varumärke och grafiska profil.
                  </li>
                  <li>
                    <span class="digi-docs__bold-text">
                      Komponentbibliotek:{' '}
                    </span>
                    Komponentbiblioteket är uppdelat i design (Sketch +
                    Abstract) och kod (Webbkomponenter via NPM).
                  </li>
                  <li>
                    <span class="digi-docs__bold-text">
                      Språk och översättningar:{' '}
                    </span>
                    Riktlinjer för hur vi förhåller oss och arbetar med andra
                    språk än svenska.
                  </li>
                  <li>
                    <span class="digi-docs__bold-text">Praktisk copy: </span>
                    Riktlinjer för systemtexter och mikrocopy.
                  </li>
                  <li>
                    <span class="digi-docs__bold-text">
                      Tillgänglighetsriktlinjer:{' '}
                    </span>
                    Kunskapsstöd för hur produkter skapar lösningar för alla.
                  </li>
                  <li>
                    <span class="digi-docs__bold-text">
                      Dokumentationsplattform:{' '}
                    </span>
                    Designsystemets webbplats och dokumentationsplattform på{' '}
                    <digi-link
                      af-variation="small"
                      afHref="https://designsystem.arbetsformedlingen.se/" af-href="https://designsystem.arbetsformedlingen.se/"
                    >
                      designsystem.arbetsformedlingen.se
                    </digi-link>
                  </li>
                </digi-list>
              </div>
            </digi-typography>
          </digi-layout-block>
        </digi-docs-page-layout>
      </div>
    );
  }
}
