import { Component, h, State } from '@stencil/core';
import { router } from '../../global/router';
import state from '../../store/store';

@Component({
	tag: 'digi-docs-about-design-pattern-button',
	styleUrl: 'digi-docs-about-design-pattern-button.scss',
	scoped: true
})
export class AboutDesignPatternButton {
	@State() pageName = 'Knappar';
	Router = router;

	linkClickHandler(e) {
		e.detail.preventDefault();
		this.Router.push(e.target.afHref);
	}

	render() {
		return (
			<host>
				<digi-docs-page-layout
					af-page-heading={this.pageName}
					af-edit-href="pages/about-design-pattern-button/digi-docs-about-design-pattern-button.tsx"
				>
					<span slot="preamble">
						Knappar används för att utföra en handling eller exekvera en funktion.
						Knappar finns alltid tillsammans med en annan komponent. Det finns en
						primär och en sekundär knapp. Inaktiv knapp ska inte användas. Alla
						knappar ska alltid gå att interagera med. Knapparna finns i tre olika
						varianter: Primära, sekundära och funktionsknappar. Den sistnämnda måste
						ha en ikon. De finns i tre olika storlekar: Stor, mellan och liten.
						Mellanstorleken är förvald och det är den som rekommenderas att användas.
					</span>
					<br />

					<digi-layout-block
						af-variation="secondary"
						afVerticalPadding
						af-no-gutter
						afMarginTop
					>
						<br />
						<digi-layout-columns af-variation={state.responsiveTwoColumns}>
							<div>
								<h2>Knappar ihop med andra komponenter </h2>
								<p>
									Använd en tydlig skillnad mellan primär och sekundär knapp. Placera
									alltid den primära knappen till höger och den sekundära till vänster.
									Om det inte finns plats för två knappar på samma rad, så placera alltid
									den sekundära knappen överst. I mobilt gränssnitt ska knapparna ligga
									på full bredd på sidan. Ibland är det svårt att döpa dessa knappar på
									ett bra och konsekvent sätt. Försök att använda en så enkel och kort
									benämning på knappen som möjligt.
								</p>
								<br />
								<h2>Primär knapp</h2>
								<p>
									Den primära interaktionen betyder att användaren accepterar handlingen
									och vill gå vidare och avsluta eller till nästa steg om ett sådant
									finns. Används alltid om det endast finns en knapp på sidan och ska
									alltid vara mest framträdande i relation till andra knappar.
								</p>
								<br />
								<h2>Sekundär knapp</h2>
								<p>
									Den sekundära interaktionen betyder att användaren inte accepterar
									handlingen, som till exempel att avbryta ett flöde, säga nej tack eller
									liknande. En sekundär interaktion får endast användas om det finns
									mer än en knapp på sidan. En sekundär knapp får i vissa fall ersättas
									med en funktionsknapp, men endast om det gör interaktionen tydligare
									för användare.
								</p>
								<br />
								<h2>Funktionsknappar</h2>
								<p>
									Det är en sekundär interaktion och ser ut som länkar. Dessa används för
									att utföra en handling, till exempel rensa eller ändra.
								</p>
								<br />
								<h2>Logiskt flöde på knapparna</h2>
								<p>
									Ett naturligt flöde/beteende med en primär och sekundär knapp
									tillsammans i en komponent är att läsa innehållet, sedan gå vidare till
									att exempelvis ångra eller gå tillbaka till föregående innan man
									accepterar och går vidare till nästa steg eller en bekräftelse på att
									flödet är klart.
								</p>
							</div>

							<div>
								<digi-layout-block af-variation="primary" afVerticalPadding>
									<div class="pattern_image_wrapper">
										<digi-media-image
											// af-fullwidth="true"
											afUnlazy
											afHeight="390"
											afWidth="618"
											afSrc="/assets/images/pattern/button-pattern/button-pattern-example-1.svg"
											afAlt="Exempel på korrekt ordning av en primär och sekundär knapp med den primära till höger och den sekundära till vänster."
										></digi-media-image>
									</div>
								</digi-layout-block>
								<br />
								<br />
								{/* <digi-layout-block af-variation="primary" afVerticalPadding>
									<div class="pattern_image_wrapper">
										<digi-media-image
											// af-fullwidth="true"
											afUnlazy
											afHeight="390"
											afWidth="618"
											afSrc="/assets/images/pattern/button-pattern/button-pattern-example-2.svg"
											afAlt="Exempel på inkorrekt ordning på en primära och en sekundära knapp med de primära till väster och den sekundära till höger."
										></digi-media-image>
									</div>
								</digi-layout-block> */}
							</div>
						</digi-layout-columns>
						<br />
						<br />
						<hr></hr>
						<br />
						<br />
						<digi-layout-columns af-variation={state.responsiveTwoColumns}>
							<digi-layout-container>
								<digi-layout-block af-variation="primary" afVerticalPadding>
									<h3 class="pattern_button_header_wrapper">Layout Desktop</h3>
									<div class="pattern_button_container">
										<digi-button af-variation="primary" af-size="large">
											Primär knapp
										</digi-button>
									</div>
									<br /> {''}
									<digi-list>
										<li>
											Primär knapp när det bara är en interaktion på sidan eller i en
											komponent.
										</li>
										<li>Ligger alltid i vänsterkant</li>
										<li>Texten är alltid centrerad i knappen</li>
									</digi-list>{' '}
									<br />
								</digi-layout-block>

								<br />
								<br />
								<digi-layout-block
									af-variation="primary"
									afVerticalPadding
									// af-margin-top="true"
									// af-container="LayoutBlockContainer.NONE"
								>
									{/* 12 i padding  för L */}
									{/* 8 i padding  för S */}
									<div class="pattern_button_container">
										<div class="buttons_wrapper">
											<digi-button af-variation="secondary" af-size="large">
												Sekundär knappar
											</digi-button>{' '}
										</div>
										<br />
										<div class="buttons_wrapper">
											<digi-button af-variation="primary" af-size="large">
												Primär knapp
											</digi-button>
										</div>
									</div>{' '}
									{''}
									<br />
									<digi-list>
										<li>Primär knapp alltid till höger om den sekundära knappen</li>
										<li>Båda knapparna ligger alltid i vänsterkant på sidan</li>
										<li>Texten är alltid centrerad i knappen</li>
									</digi-list>{' '}
									<br />
								</digi-layout-block>
								<br />
								<digi-layout-block
									af-variation="primary"
									afVerticalPadding
									af-margin-top="true"
								>
									<div class="pattern_button_container">
										<div class="buttons_wrapper">
											<digi-button af-size="medium" af-variation="function">
												<digi-icon-envelope slot="icon" aria-hidden />
												Funktionsknapp
											</digi-button>
										</div>
										<div class="buttons_wrapper">
											<digi-button af-variation="primary" af-size="large">
												Primär knapp
											</digi-button>
										</div>
									</div>
									<br />
									<br />
									<digi-list>
										<li>Funktionsknapp alltid till vänster om den primära knappen</li>
										<li>Båda ligger alltid i vänsterkant på sidan</li>
										<li> Texten är alltid centrerad i knappen</li>
									</digi-list>{' '}
									<br />
								</digi-layout-block>
								<br />

								<div class="pattern_mobile">
									{' '}
									<h3 class="pattern_button_header_wrapper">Layout Mobil</h3>
									<div class="pattern_mobile_button">
										<digi-button af-variation="secondary" af-size="large">
											Sekundär knappar
										</digi-button>
										{''}

										<digi-button af-variation="primary" af-size="large">
											Primär knapp
										</digi-button>
										{''}
									</div>
									<digi-list>
										<li> Fullbredd på knapparna är rekommenderat</li>
										<li>Texten är alltid centrerad i knappen</li>
									</digi-list>{' '}
								</div>
							</digi-layout-container>
							<digi-layout-container>
								<h2>Layout</h2>
								<p>
									För knappar i desktop förespråkar vi att dem alltid ligger i
									vänsterkant på en rad. Det gör det enklare för användaren när hen
									följer en rak linje från vänster till höger, jämnfört med att ögat
									hoppar från texten i vänsterkant till mitten eller höger om i en
									kolumn.
								</p>
								<p>
									Mobila enheter är smala och har per automatik inte utrymme för knappar
									bredvid varandra. Vi förespråkar då full bredd på ytan. Det gör det
									enklare för användaren att hen följer flödet rakt nedåt.
								</p>
								<p>
									Denna design hjälper oss att samordna det visuella uttrycket för
									knappar i mobil och desktop- storlekar. Det blir även enklare för
									skärmläsare att navigera genom en sådan struktur.
								</p>
								<br />
								<h2>Logiskt flöde och förloppssteg</h2>
								<p>
									För mer komplicerade sidor med många primära och sekundära knappar när
									det till exempel är flera händelsekort på en och samma sida, kan det
									dock vara bättre att använda två olika storlekar för att dels särskilja
									hierarkin mellan knapparna och dels för att korta ner sidans längd och
									göra den mer kompakt och översiktlig.{' '}
								</p>
								<br />
								<p>
									Ibland finns det en mängd information med flera primära, sekundära och
									funktionsknappar. Detta kan t.ex vara ett flöde under ”mina sidor” i
									inloggat läge, där det kan finnas flera händelsekort på raken. Då kan
									det dock vara bättre att använda två olika storlekar för att särskilja
									hierarkin mellan knapparna.
								</p>
								<br />
								<p>
									I mobilt läge finns möjlighet att dela upp händelsekorten med hjälp av
									fler sidor. Då kan du behöva använda en komponent med förloppssteg
									eller att användaren kommer direkt till nästa sida genom att trycka på
									den primära knappen. Genom att visa på flera steg, guidar vi användaren
									att bryta ner uppgiften till mindre hanterbara delmoment. Så att hen
									förstår vad som redan är gjort och vad som är kvar att göra. Det går
									naturligtvis att trycka på den sekundära knappen för att komma tillbaka
									till det föregående händelsekortet.
								</p>

								<br />

								<h2>Knappar i modaler</h2>
								<p>
									Använd knappar i modaler med eftertanke, vi vill helst undvika modaler
									då den tar bort användaren från den kontext hen är i. Av
									tillgänglighetsskäl är det mycket viktigt att skriva kod för modalen
									korrekt, så att en skärmläsare får rätt läsordning. Om du använder
									knappkomponent i en modal, så tänk på att det passar kortare
									information. Längre information med knappar bör ligga på sidan och inte
									i en modal.
								</p>
							</digi-layout-container>
						</digi-layout-columns>
						<br />
						<br />
						<digi-layout-block af-variation="secondary" afVerticalPadding>
							<br />
							<hr></hr>
							<br />
							<digi-layout-columns af-variation={state.responsiveTwoColumns}>
								<div>
									<h4>Formulär</h4>
									<p>Se relaterade designmönster och komponenter</p>
									<digi-docs-related-links>
										<digi-link-button
											afHref="/designmonster/formular"
											af-target="_blank"
											af-size="medium"
											af-variation="secondary"
											onAfOnClick={(e) => this.linkClickHandler(e)}
										>
											Formulär
										</digi-link-button>
									</digi-docs-related-links>
								</div>
								<div>
									<h4>Spacing</h4>
									<p>Se relaterade designmönster och komponenter</p>
									<digi-docs-related-links>
										<digi-link-button
											afHref="/designmonster/spacing"
											af-target="_blank"
											af-size="medium"
											af-variation="secondary"
											onAfOnClick={(e) => this.linkClickHandler(e)}
										>
											Spacing
										</digi-link-button>
									</digi-docs-related-links>
								</div>
							</digi-layout-columns>
						</digi-layout-block>
					</digi-layout-block>
				</digi-docs-page-layout>
			</host>
		);
	}
}
