import { newSpecPage } from '@stencil/core/testing';
import { AboutDesignPatternLoaders } from './digi-docs-about-design-pattern-loaders';

describe('digi-docs-about-design-pattern-loaders', () => {
	it('renders', async () => {
		const { root } = await newSpecPage({
			components: [AboutDesignPatternLoaders],
			html:
				'<digi-docs-about-design-pattern-loaders></digi-docs-about-design-pattern-loaders>'
		});
		expect(root).toEqualHtml(`
      <digi-docs-about-design-pattern-loaders>
        <mock:shadow-root>
          <div>
            Hello, World! I'm
          </div>
        </mock:shadow-root>
      </digi-docs-about-design-pattern-loaders>
    `);
	});

	it('renders with values', async () => {
		const { root } = await newSpecPage({
			components: [AboutDesignPatternLoaders],
			html: `<digi-docs-about-design-pattern-loaders first="Stencil" last="'Don't call me a framework' JS"></digi-docs-about-design-pattern-loaders>`
		});
		expect(root).toEqualHtml(`
      <digi-docs-about-design-pattern-loaders first="Stencil" last="'Don't call me a framework' JS">
        <mock:shadow-root>
          <div>
            Hello, World! I'm Stencil 'Don't call me a framework' JS
          </div>
        </mock:shadow-root>
      </digi-docs-about-design-pattern-loaders>
    `);
	});
});
