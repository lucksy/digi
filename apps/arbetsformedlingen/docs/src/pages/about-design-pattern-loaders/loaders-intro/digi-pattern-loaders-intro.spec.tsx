import { newSpecPage } from '@stencil/core/testing';
import { LoadersIntro } from './digi-pattern-loaders-intro';

describe('digi-pattern-loaders-intro', () => {
	it('renders', async () => {
		const { root } = await newSpecPage({
			components: [LoadersIntro],
			html: '<digi-pattern-loaders-intro></digi-pattern-loaders-intro>'
		});
		expect(root).toEqualHtml(`
      <digi-pattern-loaders-intro>
        <mock:shadow-root>
          <div>
            Hello, World! I'm
          </div>
        </mock:shadow-root>
      </digi-pattern-loaders-intro>
    `);
	});

	it('renders with values', async () => {
		const { root } = await newSpecPage({
			components: [LoadersIntro],
			html: `<digi-pattern-loaders-intro first="Stencil" last="'Don't call me a framework' JS"></digi-pattern-loaders-intro>`
		});
		expect(root).toEqualHtml(`
      <digi-pattern-loaders-intro first="Stencil" last="'Don't call me a framework' JS">
        <mock:shadow-root>
          <div>
            Hello, World! I'm Stencil 'Don't call me a framework' JS
          </div>
        </mock:shadow-root>
      </digi-pattern-loaders-intro>
    `);
	});
});
