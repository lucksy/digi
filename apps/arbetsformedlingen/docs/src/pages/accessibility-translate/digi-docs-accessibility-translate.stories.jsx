import { h } from '@stencil/core';

export default {
  title: 'DigiDocsTranslate',
  args: {
    first: 'John',
    middle: 'S',
    last: 'Doe'
  }
};

export const Primary = (args) => {
  return <digi-docs-translate {...args}></digi-docs-translate>;
};
