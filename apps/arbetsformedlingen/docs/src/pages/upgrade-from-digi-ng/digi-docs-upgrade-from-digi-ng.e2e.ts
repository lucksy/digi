import { newE2EPage } from '@stencil/core/testing';

describe('digi-docs-upgrade-from-digi-ng', () => {
  it('renders', async () => {
    const page = await newE2EPage();

    await page.setContent(
      '<digi-docs-upgrade-from-digi-ng></digi-docs-upgrade-from-digi-ng>'
    );
    const element = await page.find('digi-docs-upgrade-from-digi-ng');
    expect(element).toHaveClass('hydrated');
  });

  it('renders changes to the name data', async () => {
    const page = await newE2EPage();

    await page.setContent(
      '<digi-docs-upgrade-from-digi-ng></digi-docs-upgrade-from-digi-ng>'
    );
    const component = await page.find('digi-docs-upgrade-from-digi-ng');
    const element = await page.find('digi-docs-upgrade-from-digi-ng >>> div');
    expect(element.textContent).toEqual(`Hello, World! I'm `);

    component.setProperty('first', 'James');
    await page.waitForChanges();
    expect(element.textContent).toEqual(`Hello, World! I'm James`);

    component.setProperty('last', 'Quincy');
    await page.waitForChanges();
    expect(element.textContent).toEqual(`Hello, World! I'm James Quincy`);

    component.setProperty('middle', 'Earl');
    await page.waitForChanges();
    expect(element.textContent).toEqual(`Hello, World! I'm James Earl Quincy`);
  });
});
