import { newSpecPage } from '@stencil/core/testing';
import { AboutComponent } from './digi-docs-about-component';

describe('digi-docs-about-component', () => {
	it('renders', async () => {
		const { root } = await newSpecPage({
			components: [AboutComponent],
			html: '<digi-docs-about-component></digi-docs-about-component>'
		});
		expect(root).toEqualHtml(`
      <digi-docs-about-component>
        <mock:shadow-root>
          <div>
            Hello, World! I'm
          </div>
        </mock:shadow-root>
      </digi-docs-about-component>
    `);
	});

	it('renders with values', async () => {
		const { root } = await newSpecPage({
			components: [AboutComponent],
			html: `<digi-docs-about-component first="Stencil" last="'Don't call me a framework' JS"></digi-docs-about-component>`
		});
		expect(root).toEqualHtml(`
      <digi-docs-about-component first="Stencil" last="'Don't call me a framework' JS">
        <mock:shadow-root>
          <div>
            Hello, World! I'm Stencil 'Don't call me a framework' JS
          </div>
        </mock:shadow-root>
      </digi-docs-about-component>
    `);
	});
});
