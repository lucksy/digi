import { newSpecPage } from '@stencil/core/testing';
import { AboutDesignPatternPictogram } from './digi-docs-about-design-pattern-pictogram';

describe('digi-docs-about-design-pattern-pictogram', () => {
	it('renders', async () => {
		const { root } = await newSpecPage({
			components: [AboutDesignPatternPictogram],
			html:
				'<digi-docs-about-design-pattern-pictogram></digi-docs-about-design-pattern-pictogram>'
		});
		expect(root).toEqualHtml(`
      <digi-docs-about-design-pattern-pictogram>
        <mock:shadow-root>
          <div>
            Hello, World! I'm
          </div>
        </mock:shadow-root>
      </digi-docs-about-design-pattern-pictogram>
    `);
	});

	it('renders with values', async () => {
		const { root } = await newSpecPage({
			components: [AboutDesignPatternPictogram],
			html: `<digi-docs-about-design-pattern-pictogram first="Stencil" last="'Don't call me a framework' JS"></digi-docs-about-design-pattern-pictogram>`
		});
		expect(root).toEqualHtml(`
      <digi-docs-about-design-pattern-pictogram first="Stencil" last="'Don't call me a framework' JS">
        <mock:shadow-root>
          <div>
            Hello, World! I'm Stencil 'Don't call me a framework' JS
          </div>
        </mock:shadow-root>
      </digi-docs-about-design-pattern-pictogram>
    `);
	});
});
