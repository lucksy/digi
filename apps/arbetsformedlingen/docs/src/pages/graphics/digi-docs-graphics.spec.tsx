import { newSpecPage } from '@stencil/core/testing';
import { DigiDocsGraphics } from './digi-docs-graphics';

describe('digi-docs-graphics', () => {
  it('renders', async () => {
    const { root } = await newSpecPage({
      components: [DigiDocsGraphics],
      html: '<digi-docs-graphics></digi-docs-graphics>'
    });
    expect(root).toEqualHtml(`
      <digi-docs-graphics>
        <mock:shadow-root>
          <div>
            Hello, World! I'm
          </div>
        </mock:shadow-root>
      </digi-docs-graphics>
    `);
  });

  it('renders with values', async () => {
    const { root } = await newSpecPage({
      components: [DigiDocsGraphics],
      html: `<digi-docs-graphics first="Stencil" last="'Don't call me a framework' JS"></digi-docs-graphics>`
    });
    expect(root).toEqualHtml(`
      <digi-docs-graphics first="Stencil" last="'Don't call me a framework' JS">
        <mock:shadow-root>
          <div>
            Hello, World! I'm Stencil 'Don't call me a framework' JS
          </div>
        </mock:shadow-root>
      </digi-docs-graphics>
    `);
  });
});
