import { newSpecPage } from '@stencil/core/testing';
import { DigiDocsAboutMicrocopy } from './digi-docs-about-microcopy';

describe('digi-docs-about-microcopy', () => {
  it('renders', async () => {
    const { root } = await newSpecPage({
      components: [DigiDocsAboutMicrocopy],
      html: '<digi-docs-about-microcopy></digi-docs-about-microcopy>'
    });
    expect(root).toEqualHtml(`
      <digi-docs-about-microcopy>
        <mock:shadow-root>
          <div>
            Hello, World! I'm
          </div>
        </mock:shadow-root>
      </digi-docs-about-microcopy>
    `);
  });

  it('renders with values', async () => {
    const { root } = await newSpecPage({
      components: [DigiDocsAboutMicrocopy],
      html: `<digi-docs-about-microcopy first="Stencil" last="'Don't call me a framework' JS"></digi-docs-about-microcopy>`
    });
    expect(root).toEqualHtml(`
      <digi-docs-about-microcopy first="Stencil" last="'Don't call me a framework' JS">
        <mock:shadow-root>
          <div>
            Hello, World! I'm Stencil 'Don't call me a framework' JS
          </div>
        </mock:shadow-root>
      </digi-docs-about-microcopy>
    `);
  });
});
