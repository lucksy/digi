import { newSpecPage } from '@stencil/core/testing';
import { DigiDocsWorkWithDigiCore } from './digi-docs-work-with-digi-core';

describe('digi-docs-work-with-digi-core', () => {
  it('renders', async () => {
    const { root } = await newSpecPage({
      components: [DigiDocsWorkWithDigiCore],
      html: '<digi-docs-work-with-digi-core></digi-docs-work-with-digi-core>'
    });
    expect(root).toEqualHtml(`
      <digi-docs-work-with-digi-core>
        <mock:shadow-root>
          <div>
            Hello, World! I'm
          </div>
        </mock:shadow-root>
      </digi-docs-work-with-digi-core>
    `);
  });

  it('renders with values', async () => {
    const { root } = await newSpecPage({
      components: [DigiDocsWorkWithDigiCore],
      html: `<digi-docs-work-with-digi-core first="Stencil" last="'Don't call me a framework' JS"></digi-docs-work-with-digi-core>`
    });
    expect(root).toEqualHtml(`
      <digi-docs-work-with-digi-core first="Stencil" last="'Don't call me a framework' JS">
        <mock:shadow-root>
          <div>
            Hello, World! I'm Stencil 'Don't call me a framework' JS
          </div>
        </mock:shadow-root>
      </digi-docs-work-with-digi-core>
    `);
  });
});
