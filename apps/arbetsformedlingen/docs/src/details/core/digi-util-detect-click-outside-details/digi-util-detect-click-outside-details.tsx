import { Component, Prop, h } from '@stencil/core';

@Component({
  tag: 'digi-util-detect-click-outside-details',
  styleUrl: 'digi-util-detect-click-outside-details.scss'
})
export class DigiUtilDetectClickOutSideDetails {
  @Prop() component: string;

  render() {
    return (
      <div class="digi-util-detect-click-outside-details">
        <digi-typography>
          <digi-layout-block af-variation="primary">
            <digi-typography-preamble>
              <digi-code af-code={`<digi-util-detect-click-outside>`} />
              övervakar klickhändelser och skapar events när klicket är inuti eller utanför.
              Kan till exempel användas för att stänga en meny när man klickar utanför den.
              <br />
              <br />
              <digi-link af-variation="small" afHref="https://digi-core.netlify.app/?path=/docs/util-digi-util-detect-click-outside--standard" af-target="_blank">
                <digi-icon-arrow-right slot="icon"></digi-icon-arrow-right>
                Länk till dokumentation i Storybook
              </digi-link>
            </digi-typography-preamble>
          </digi-layout-block>
        </digi-typography>
      </div>
    );
  }
}
