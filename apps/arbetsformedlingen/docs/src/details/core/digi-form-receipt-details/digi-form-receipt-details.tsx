import { Component, h, Host, Prop, State } from '@stencil/core';
import { CodeExampleLanguage } from 'libs/core/package/src';
import { FormReceiptVariation, FormReceiptType, InfoCardVariation } from '@digi/arbetsformedlingen';
import { href } from 'stencil-router-v2';

@Component({
  tag: 'digi-form-receipt-details',
  styleUrl: 'digi-form-receipt-details.scss'
})
export class DigiFormReceipt {
  @Prop() component: string;
  @Prop() afShowOnlyExample: boolean;
  @Prop() afHideControls: boolean;
  @Prop() afHideCode: boolean;

  @State() formReceiptText: string = "";
  @State() formReceiptVariation: FormReceiptVariation = FormReceiptVariation.FULLWIDTH;
  @State() formReceiptType: FormReceiptType = FormReceiptType.CENTER;

  get formReceiptCode() {
    return {
			[CodeExampleLanguage.HTML]: `\
<digi-form-receipt
	af-text="Tack, din rapport är inskickad!"
	af-variation="${this.formReceiptVariation}"
	af-type="${this.formReceiptType}"
></digi-form-receipt>`,
			[CodeExampleLanguage.ANGULAR]: `\
<digi-form-receipt
	[attr.af-text]="'Tack, din rapport är inskickad!'"
	[attr.af-variation]="FormReceiptVariation.${Object.keys(FormReceiptVariation).find(key => FormReceiptVariation[key] === this.formReceiptVariation)}"
	[attr.af-type]="FormReceiptType.${Object.keys(FormReceiptType).find(key => FormReceiptType[key] === this.formReceiptType)}"
/>`,
			[CodeExampleLanguage.REACT]: `\
<DigiFormReceipt
	afText="Tack, din rapport är inskickad!"
	afVariation={FormReceiptVariation.${Object.keys(FormReceiptVariation).find(key => FormReceiptVariation[key] === this.formReceiptVariation)}}
	afType={FormReceiptType.${Object.keys(FormReceiptType).find(key => FormReceiptType[key] === this.formReceiptType)}}
/>`,
		};
  }

  render() {
    return (
      <Host>
        <div class="digi-form-receipt-details">
        <digi-typography>
					{!this.afShowOnlyExample && (
            <digi-typography-preamble>
              Kvittensen används för att bekräfta ett korrekt slutfört formulär (eller flöde). T.ex så kan användaren ha genomfört en inskrivning eller annat viktigt formulär-flöde som kan vara en obligatorisk uppgift för hen att utföra. Då är det mycket viktigt att tydligt signalera att uppgiften är klar.
            </digi-typography-preamble>
            )}
          <digi-layout-container af-no-gutter af-margin-bottom>
            <article>
            {!this.afShowOnlyExample && (<h2>Exempel</h2>)}
            <digi-code-example
              af-code={JSON.stringify(this.formReceiptCode)}
              af-hide-controls={this.afHideControls ? 'true' : 'false'}
              af-hide-code={this.afHideCode ? 'true' : 'false'}
            >
              <digi-form-receipt
                afText="Tack, din rapport är inskickad!"
                afVariation={this.formReceiptVariation}
                afType={this.formReceiptType}
              />
              <div class="slot__controls" slot="controls">
                <digi-form-fieldset
                  afName="Type"
                  afLegend="Typ"
                  onChange={(e) =>
                    (this.formReceiptType = (e.target as any).value)
                  }
                >
                  <digi-form-radiobutton
                    afName="Type"
                    afLabel="Centrerad"
                    afValue={FormReceiptType.CENTER}
                    afChecked={this.formReceiptType === FormReceiptType.CENTER}
                  />
                  <digi-form-radiobutton
                    afName="Type"
                    afLabel="Vänsterställd"
                    afValue={FormReceiptType.START}
                    afChecked={this.formReceiptType === FormReceiptType.START}
                  />
                </digi-form-fieldset>
                <digi-form-fieldset
                  afName="Variation"
                  afLegend="Variation"
                  onChange={(e) =>
                    (this.formReceiptVariation = (e.target as any).value)
                  }
                >
                  <span>
                    <digi-form-radiobutton
                      afName="Variation"
                      afLabel="Fullbredd"
                      afValue={FormReceiptVariation.FULLWIDTH}
                      afChecked={this.formReceiptVariation === FormReceiptVariation.FULLWIDTH}
                    />
                    <digi-form-radiobutton
                      afName="Variation"
                      afLabel="Grid"
                      afValue={FormReceiptVariation.GRID}
                      afChecked={this.formReceiptVariation === FormReceiptVariation.GRID}
                    />
                  </span>
                </digi-form-fieldset>
              </div>
            </digi-code-example>
            </article>
          </digi-layout-container>
          {!this.afShowOnlyExample && (
            <digi-layout-container af-no-gutter af-margin-bottom>
            <digi-info-card
							afHeading="Bra att tänka på"
							afHeadingLevel={`h3` as any}
							afVariation={InfoCardVariation.SECONDARY}
						>
              <digi-list>
                <li>
                  En mycket stor andel av våra inkommande samtal handlar om att vi ska bekräfta att vår kund lyckats skicka in rätt uppgifter till oss.
                </li>
              </digi-list>
            </digi-info-card>
            <digi-info-card
							afHeading="Riktlinjer"
							afHeadingLevel={`h3` as any}
							afVariation={InfoCardVariation.PRIMARY}
						>
              <digi-list>
                <li>
                  Se kvittens i kontext i <a {...href('/designmonster/validering')}>designmönster</a> 
                </li>
                <li>
                  Variationen Fullbred skapar en egen layout block som fyller ut utrymmet horisontellt. Med variationen Grid så lägger sig komponenten i en annan layout block och ärver samma stilsättning.
                </li>
                <li>
                  Detta är en komponent som ska bara användas som kvittens, därför ska inte ikonen bytas ut.
                </li>
                <li>
                  Se till att justeringen av kvittens komponenten matchar justeringen av tillhörande innehåll.
                </li>
              </digi-list>
            </digi-info-card>
            </digi-layout-container>
          )}
          </digi-typography>
        </div>
      </Host>
    );
  }
}
