import { newE2EPage } from '@stencil/core/testing';

describe('digi-notification-error-page-details', () => {
	it('renders', async () => {
		const page = await newE2EPage();

		await page.setContent(
			'<digi-notification-error-page-details></digi-notification-error-page-details>'
		);
		const element = await page.find('digi-notification-error-page-details');
		expect(element).toHaveClass('hydrated');
	});

	it('renders changes to the name data', async () => {
		const page = await newE2EPage();

		await page.setContent(
			'<digi-notification-error-page-details></digi-notification-error-page-details>'
		);
		const component = await page.find('digi-notification-error-page-details');
		const element = await page.find(
			'digi-notification-error-page-details >>> div'
		);
		expect(element.textContent).toEqual(`Hello, World! I'm `);

		component.setProperty('first', 'James');
		await page.waitForChanges();
		expect(element.textContent).toEqual(`Hello, World! I'm James`);

		component.setProperty('last', 'Quincy');
		await page.waitForChanges();
		expect(element.textContent).toEqual(`Hello, World! I'm James Quincy`);

		component.setProperty('middle', 'Earl');
		await page.waitForChanges();
		expect(element.textContent).toEqual(`Hello, World! I'm James Earl Quincy`);
	});
});
