import { Component, h, Prop, State } from '@stencil/core';
import {
	CodeExampleLanguage
} from '@digi/arbetsformedlingen';

const getRandomCategories = (count: number): Array<any> => {
	const names = ["Ett", "Två", "Tre", "Fyra", "Fem", "Sex", "Sju", "Åtta", "Nio", "Tio"];
	let categories = [];
	for (let i = 0; i < count && i < names.length; i++) {
		categories.push({
			name: "Kategori " + names[i],
			hits: Math.floor(Math.random() * 10),
			selected: Math.random() > .5
		});
	}
	return categories;
}

@Component({
	tag: 'digi-form-category-filter-details',
	styleUrl: 'digi-form-category-filter-details.scss'
})
export class DigiFormCategoryFilter {
	@Prop() component: string;
	@Prop() afShowOnlyExample: boolean;
	@Prop() afHideControls: boolean;
	@Prop() afHideCode: boolean;

	@State() startCollapsed: boolean = true;
	@State() preselected: boolean = false;
	@State() showHits: boolean = true;

	visible: number = 4;
	categories: Array<any> = getRandomCategories(10);

	toggleStartCollapsed(e: any) {
		this.startCollapsed = e.target.afChecked;
	}
	togglePreselected(e: any) {
		this.preselected = e.target.afChecked;
	}
	toggleDisplayHits(e: any) {
		this.showHits = e.target.afChecked;
	}
	getCategories(): Array<any> {
		if (this.showHits && this.preselected)
			return this.categories;
		if (this.showHits)
			return this.categories.map(c => { return { name: c.name, hits: c.hits }; });
		if (this.preselected)
			return this.categories.map(c => { return { name: c.name, selected: c.selected }; });
		return this.categories.map(c => { return { name: c.name }; });
	}

	get textareaCode() {
		return {
			[CodeExampleLanguage.HTML]: `\
<digi-form-category-filter
	af-categories="[{
		name: '${this.categories[0].name}'${this.showHits ? ",\n		hits: " + this.categories[0].hits : ""}${this.preselected ? ",\n		selected: " + this.categories[0].selected : ""}
	}]"
	af-visible-collapsed="${this.visible}"
	af-start-collapsed="${this.startCollapsed}"
>
</digi-form-category-filter>`,
			[CodeExampleLanguage.ANGULAR]: `\
<digi-form-category-filter
	[attr.af-categories]="[{
		name: '${this.categories[0].name}'${this.showHits ? ",\n		hits: " + this.categories[0].hits : ""}${this.preselected ? ",\n		selected: " + this.categories[0].selected : ""}
	}]"
	[attr.af-visible-collapsed]="${this.visible}"
	[attr.af-start-collapsed]="${this.startCollapsed}"
>
</digi-form-category-filter>`,
			[CodeExampleLanguage.REACT]: `\
<DigiFormCategoryFilter
	afCategories={[{
		name: "${this.categories[0].name}"${this.showHits ? ",\n		hits: " + this.categories[0].hits : ""}${this.preselected ? ",\n		selected: " + this.categories[0].selected : ""}
	}]}
	afVisibleCollapsed={${this.visible}}
	afStartCollapsed={${this.startCollapsed}}
>
</DigiFormCategoryFilter>`
		};
	}

	render() {
		return (
			<div class="digi-form-category-filter-details">
				<digi-typography>
					{!this.afShowOnlyExample && (
						<digi-typography-preamble>Kategorifilter tar in en lista av kategorier och skickar ut en filtrerad lista på kategorier baserat på vilka kategorier användaren väljer.</digi-typography-preamble>
					)}
					<digi-layout-container af-no-gutter af-margin-bottom>
						{!this.afShowOnlyExample && (<h2>Exempel</h2>)}
						<digi-code-example af-code={JSON.stringify(this.textareaCode)}
							af-hide-controls={this.afHideControls ? 'true' : 'false'} af-hide-code={this.afHideCode ? 'true' : 'false'}>
							<div>
								<digi-form-category-filter
									afCategories={this.getCategories()}
									afVisibleCollapsed={this.visible}
									afStartCollapsed={this.startCollapsed}
								></digi-form-category-filter>
							</div>
							<div class="slot__controls" slot="controls">
								<digi-form-fieldset
									afName="DisplayHits"
									afLegend="Visa antal träffar"
								>
									<digi-form-checkbox
										afName="DisplayHits"
										afLabel="Visa"
										afChecked={this.showHits}
										onAfOnChange={(e) => { this.toggleDisplayHits(e); }}
									/>
								</digi-form-fieldset>
								<digi-form-fieldset
									afName="Preselected"
									afLegend="Förvalda kategorier"
								>
									<digi-form-checkbox
										afName="Preselected"
										afLabel="Visa"
										afChecked={this.preselected}
										onAfOnChange={(e) => { this.togglePreselected(e); }}
									/>
								</digi-form-fieldset>
							</div>
						</digi-code-example>
					</digi-layout-container>
					{!this.afShowOnlyExample && (
						<digi-layout-block
							class='digi-form-category-filter-details--example-block'
							af-variation="symbol"
							af-vertical-padding
							af-margin-bottom
						>
							<h2>Viktigt att veta</h2>
							<digi-list>
								<li>Varje kategori som skickas in måste ha ett <b>unikt namn</b>.</li>
								<li>Listan som skickas ut via <digi-code af-code="af-on-selected-category-change" /> är en lista av namn på de kategorier som är valda.</li>
								<li>Varje kategori kan skickas in med dessa alternativ: antal träffar <digi-code af-code="hits: number" /> och ifall den ska vara vald <digi-code af-code="selected: boolean" />.</li>
							</digi-list>
						</digi-layout-block>
					)}
				</digi-typography>
			</div>
		);
	}
}
