import { newSpecPage } from '@stencil/core/testing';
import { DigiQuoteMultiContainer } from './digi-quote-multi-container-details';

describe('digi-quote-multi-container-details', () => {
	it('renders', async () => {
		const { root } = await newSpecPage({
			components: [DigiQuoteMultiContainer],
			html:
				'<digi-quote-multi-container-details></digi-quote-multi-container-details>'
		});
		expect(root).toEqualHtml(`
      <digi-quote-multi-container-details>
        <mock:shadow-root>
          <div>
            Hello, World! I'm
          </div>
        </mock:shadow-root>
      </digi-quote-multi-container-details>
    `);
	});

	it('renders with values', async () => {
		const { root } = await newSpecPage({
			components: [DigiQuoteMultiContainer],
			html: `<digi-quote-multi-container-details first="Stencil" last="'Don't call me a framework' JS"></digi-quote-multi-container-details>`
		});
		expect(root).toEqualHtml(`
      <digi-quote-multi-container-details first="Stencil" last="'Don't call me a framework' JS">
        <mock:shadow-root>
          <div>
            Hello, World! I'm Stencil 'Don't call me a framework' JS
          </div>
        </mock:shadow-root>
      </digi-quote-multi-container-details>
    `);
	});
});
