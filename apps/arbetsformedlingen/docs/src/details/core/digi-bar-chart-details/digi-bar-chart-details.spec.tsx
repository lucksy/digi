import { newSpecPage } from '@stencil/core/testing';
import { DigiBarChart } from './digi-bar-chart-details';

describe('digi-bar-chart-details', () => {
	it('renders', async () => {
		const { root } = await newSpecPage({
			components: [DigiBarChart],
			html: '<digi-bar-chart-details></digi-bar-chart-details>'
		});
		expect(root).toEqualHtml(`
      <digi-bar-chart-details>
        <mock:shadow-root>
          <div>
            Hello, World! I'm
          </div>
        </mock:shadow-root>
      </digi-bar-chart-details>
    `);
	});

	it('renders with values', async () => {
		const { root } = await newSpecPage({
			components: [DigiBarChart],
			html: `<digi-bar-chart-details first="Stencil" last="'Don't call me a framework' JS"></digi-bar-chart-details>`
		});
		expect(root).toEqualHtml(`
      <digi-bar-chart-details first="Stencil" last="'Don't call me a framework' JS">
        <mock:shadow-root>
          <div>
            Hello, World! I'm Stencil 'Don't call me a framework' JS
          </div>
        </mock:shadow-root>
      </digi-bar-chart-details>
    `);
	});
});
