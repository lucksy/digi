import { newSpecPage } from '@stencil/core/testing';
import { DigiToolsFeedback } from './digi-tools-feedback-details';

describe('digi-tools-feedback-details', () => {
	it('renders', async () => {
		const { root } = await newSpecPage({
			components: [DigiToolsFeedback],
			html: '<digi-tools-feedback-details></digi-tools-feedback-details>'
		});
		expect(root).toEqualHtml(`
      <digi-tools-feedback-details>
        <mock:shadow-root>
          <div>
            Hello, World! I'm
          </div>
        </mock:shadow-root>
      </digi-tools-feedback-details>
    `);
	});

	it('renders with values', async () => {
		const { root } = await newSpecPage({
			components: [DigiToolsFeedback],
			html: `<digi-tools-feedback-details first="Stencil" last="'Don't call me a framework' JS"></digi-tools-feedback-details>`
		});
		expect(root).toEqualHtml(`
      <digi-tools-feedback-details first="Stencil" last="'Don't call me a framework' JS">
        <mock:shadow-root>
          <div>
            Hello, World! I'm Stencil 'Don't call me a framework' JS
          </div>
        </mock:shadow-root>
      </digi-tools-feedback-details>
    `);
	});
});
