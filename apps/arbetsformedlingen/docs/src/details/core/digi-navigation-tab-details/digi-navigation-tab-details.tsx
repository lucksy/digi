import { Component, Prop, h } from '@stencil/core';
import { CodeExampleLanguage } from '@digi/arbetsformedlingen';

@Component({
	tag: 'digi-navigation-tab-details',
	styleUrl: 'digi-navigation-tab-details.scss'
})
export class DigiNavigationTabDetails {
	@Prop() component: string;
	@Prop() afShowOnlyExample: boolean;
	@Prop() afHideControls: boolean;
	@Prop() afHideCode: boolean;

	get navigationTabCode() {
		return {
			[CodeExampleLanguage.HTML]: `\
<digi-navigation-tabs
	af-aria-label="En tablist label"
>
	<digi-navigation-tab 
		af-aria-label="Flik 1" 
		af-id="flikb1"
	>
		<p>Jag är flik 1</p>
	</digi-navigation-tab>
	<digi-navigation-tab 
		af-aria-label="Flik 2" 
		af-id="flikb2"
	>
		<p>Jag är flik 2</p>
	</digi-navigation-tab>
	<digi-navigation-tab 
		af-aria-label="Flik 3" 
		af-id="flikb3"
	>
		<p>Jag är flik 3</p>
	</digi-navigation-tab>
</digi-navigation-tabs>`,
			[CodeExampleLanguage.ANGULAR]: `\
<digi-navigation-tabs
	[attr.af-aria-label]="En tablist label"
>
	<digi-navigation-tab 
		[attr.af-aria-label]="'Flik 1'" 
		[attr.af-id]="flikb1"
	>
		<p>Jag är flik 1</p>
	</digi-navigation-tab>
	<digi-navigation-tab 
		[attr.af-aria-label]="'Flik 2'" 
		[attr.af-id]="flikb2"
	>
		<p>Jag är flik 2</p>
	</digi-navigation-tab>
	<digi-navigation-tab 
		[attr.af-aria-label]="'Flik 3'" 
		[attr.af-id]="flikb3"
	>
		<p>Jag är flik 3</p>
	</digi-navigation-tab>
</digi-navigation-tabs>`,
			[CodeExampleLanguage.REACT]: `\
<DigiNavigationTabs
	afAriaLabel="En tablist label"
>
	<DigiNavigationTab 
		afAriaLabel="Flik 1" 
		afId="flikb1"
	>
		<p>Jag är flik 1</p>
	</DigiNavigationTab>
	<DigiNavigationTab 
		afAriaLabel="Flik 2" 
		afId="flikb2"
	>
		<p>Jag är flik 2</p>
	</DigiNavigationTab>
	<DigiNavigationTab 
		afAriaLabel="Flik 3" 
		afId="flikb3"
	>
		<p>Jag är flik 3</p>
	</DigiNavigationTab>
</DigiNavigationTabs>`
		};
	}
	render() {
		return (
			<div class="digi-navigation-tab-details">

				<digi-typography>
					{!this.afShowOnlyExample && (
            <digi-typography-preamble>
              Flikkomponenten används av{' '}
              <a href="/komponenter/digi-navigation-tabs/oversikt">
                <digi-code af-code="<digi-navigation-tabs>" />
              </a>{' '}
              för att skapa ett flikfält.
            </digi-typography-preamble>
					)}
					<digi-layout-container af-no-gutter af-margin-bottom>
						{!this.afShowOnlyExample && (<h2>Exempel</h2>)}
						<digi-code-example af-code={JSON.stringify(this.navigationTabCode)}
							af-hide-controls={this.afHideControls ? 'true' : 'false'} af-hide-code={this.afHideCode ? 'true' : 'false'}>
							<digi-navigation-tabs afAriaLabel="En tablist label">
								<digi-navigation-tab afAriaLabel="Flik 1" afId="flikb1">
									<p>Jag är flik 1</p>
								</digi-navigation-tab>
								<digi-navigation-tab afAriaLabel="Flik 2" afId="flikb2">
									<p>Jag är flik 2</p>
								</digi-navigation-tab>
								<digi-navigation-tab afAriaLabel="Flik 3" afId="flikb3">
									<p>Jag är flik 3</p>
								</digi-navigation-tab>
							</digi-navigation-tabs>
						</digi-code-example>
					</digi-layout-container>
					{!this.afShowOnlyExample && (
						<digi-layout-container af-no-gutter af-margin-bottom afMarginTop>
							<h2>Beskrivning</h2>
							<p>
								Komponenten används av digi-navigation-tabs och fungerar ej fristående
								utan{' '}
								<a href="/komponenter/digi-navigation-tabs/oversikt">
									<digi-code af-code="<digi-navigation-tabs>" />
								</a>
								..
								<digi-code af-code="af-aria-label" /> sätter attrubutet aria-label och
								visningsnamnet för fliken och
								<digi-code af-code="af-id" /> används för att sätta flikens id-attibut.
							</p>
						</digi-layout-container>
					)}
				</digi-typography>

			</div>
		);
	}
}
