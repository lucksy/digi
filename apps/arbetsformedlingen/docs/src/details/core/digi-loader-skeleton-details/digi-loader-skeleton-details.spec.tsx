import { newSpecPage } from '@stencil/core/testing';
import { DigiLoaderSkeleton } from './digi-loader-skeleton-details';

describe('digi-loader-skeleton-details', () => {
	it('renders', async () => {
		const { root } = await newSpecPage({
			components: [DigiLoaderSkeleton],
			html: '<digi-loader-skeleton-details></digi-loader-skeleton-details>'
		});
		expect(root).toEqualHtml(`
      <digi-loader-skeleton-details>
        <mock:shadow-root>
          <div>
            Hello, World! I'm
          </div>
        </mock:shadow-root>
      </digi-loader-skeleton-details>
    `);
	});

	it('renders with values', async () => {
		const { root } = await newSpecPage({
			components: [DigiLoaderSkeleton],
			html: `<digi-loader-skeleton-details first="Stencil" last="'Don't call me a framework' JS"></digi-loader-skeleton-details>`
		});
		expect(root).toEqualHtml(`
      <digi-loader-skeleton-details first="Stencil" last="'Don't call me a framework' JS">
        <mock:shadow-root>
          <div>
            Hello, World! I'm Stencil 'Don't call me a framework' JS
          </div>
        </mock:shadow-root>
      </digi-loader-skeleton-details>
    `);
	});
});
