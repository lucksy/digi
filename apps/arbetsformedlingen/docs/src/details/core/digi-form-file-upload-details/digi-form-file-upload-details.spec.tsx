import { newSpecPage } from '@stencil/core/testing';
import { DigiFormFileUpload } from './digi-form-file-upload-details';

describe('digi-form-file-upload-details', () => {
	it('renders', async () => {
		const { root } = await newSpecPage({
			components: [DigiFormFileUpload],
			html: '<digi-form-file-upload-details></digi-form-file-upload-details>'
		});
		expect(root).toEqualHtml(`
      <digi-form-file-upload-details>
        <mock:shadow-root>
          <div>
            Hello, World! I'm
          </div>
        </mock:shadow-root>
      </digi-form-file-upload-details>
    `);
	});

	it('renders with values', async () => {
		const { root } = await newSpecPage({
			components: [DigiFormFileUpload],
			html: `<digi-form-file-upload-details first="Stencil" last="'Don't call me a framework' JS"></digi-form-file-upload-details>`
		});
		expect(root).toEqualHtml(`
      <digi-form-file-upload-details first="Stencil" last="'Don't call me a framework' JS">
        <mock:shadow-root>
          <div>
            Hello, World! I'm Stencil 'Don't call me a framework' JS
          </div>
        </mock:shadow-root>
      </digi-form-file-upload-details>
    `);
	});
});
