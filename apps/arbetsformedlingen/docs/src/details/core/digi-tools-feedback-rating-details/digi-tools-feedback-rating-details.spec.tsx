import { newSpecPage } from '@stencil/core/testing';
import { DigiToolsFeedbackRating } from './digi-tools-feedback-rating-details';

describe('digi-tools-feedback-rating-details', () => {
	it('renders', async () => {
		const { root } = await newSpecPage({
			components: [DigiToolsFeedbackRating],
			html:
				'<digi-tools-feedback-rating-details></digi-tools-feedback-rating-details>'
		});
		expect(root).toEqualHtml(`
      <digi-tools-feedback-rating-details>
        <mock:shadow-root>
          <div>
            Hello, World! I'm
          </div>
        </mock:shadow-root>
      </digi-tools-feedback-rating-details>
    `);
	});

	it('renders with values', async () => {
		const { root } = await newSpecPage({
			components: [DigiToolsFeedbackRating],
			html: `<digi-tools-feedback-rating-details first="Stencil" last="'Don't call me a framework' JS"></digi-tools-feedback-rating-details>`
		});
		expect(root).toEqualHtml(`
      <digi-tools-feedback-rating-details first="Stencil" last="'Don't call me a framework' JS">
        <mock:shadow-root>
          <div>
            Hello, World! I'm Stencil 'Don't call me a framework' JS
          </div>
        </mock:shadow-root>
      </digi-tools-feedback-rating-details>
    `);
	});
});
