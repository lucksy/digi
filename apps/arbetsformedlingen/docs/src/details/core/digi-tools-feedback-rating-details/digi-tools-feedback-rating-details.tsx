import { Component, h, Prop, State } from '@stencil/core';
import {
	CodeExampleLanguage,
	FeedbackRatingVariation,
	FeedbackRatingType
} from '@digi/arbetsformedlingen';

@Component({
	tag: 'digi-tools-feedback-rating-details',
	styleUrl: 'digi-tools-feedback-rating-details.scss'
})
export class DigiToolsFeedbackRating {
	@Prop() component: string;
	@Prop() afShowOnlyExample: boolean;
	@Prop() afHideControls: boolean;
	@Prop() afHideCode: boolean;

	@State() showExample: boolean = true;
	@State() feedbackRatingVariation: FeedbackRatingVariation =
		FeedbackRatingVariation.PRIMARY;
	@State() feedbackRatingType: FeedbackRatingType =
	FeedbackRatingType.FULLWIDTH;
	@State() afVariation: string;

	get ratingCode() {
		return {
			[CodeExampleLanguage.HTML]: `\
    <digi-tools-feedback-rating
      af-variation="${this.feedbackRatingVariation}"
      af-type="${this.feedbackRatingType}"
      af-product="Designsystem"  
      af-application="Feedback rating component"
      af-ratings="Mycket dåligt, Ganska dåligt, Helt okej, Ganska bra, Mycket bra"  
      af-link-text="Skriv gärna mer feedback här"
      af-link="https://webropol.se/">
    </digi-tools-feedback-rating>`,
			[CodeExampleLanguage.ANGULAR]: `\
    <digi-tools-feedback-rating
      [attr.af-variation]="feedbackRatingVariation.${Object.keys(
							FeedbackRatingVariation
						).find(
							(key) => FeedbackRatingVariation[key] === this.feedbackRatingVariation
						)}"
      [attr.af-type]="feedbackRatingType.${Object.keys(
							FeedbackRatingType
						).find(
							(key) => FeedbackRatingType[key] === this.feedbackRatingType
						)}"
      [attr.af-product]="Designsystem"
      [attr.af-application]="Feedback rating component"
      [attr.af-ratings]="Mycket dåligt, Ganska dåligt, Helt okej, Ganska bra, Mycket bra"
      [attr.af-link-text]="Skriv gärna mer feedback här"
      [attr.af-link]="https://webropol.se/">
    </digi-tools-feedback-rating>`,
			[CodeExampleLanguage.REACT]: `\
    <DigiToolsFeedbackRating
      afVariation="feedbackRatingVariation.${Object.keys(
							FeedbackRatingVariation
						).find(
							(key) => FeedbackRatingVariation[key] === this.feedbackRatingVariation
						)}"
      afType="feedbackRatingType.${Object.keys(
							FeedbackRatingType
						).find(
							(key) => FeedbackRatingType[key] === this.feedbackRatingType
						)}"
      afProduct="Designsystem"
      afApplication="Feedback rating component"
      afRatings="Mycket dåligt, Ganska dåligt, Helt okej, Ganska bra, Mycket bra"
      afLinkText="Skriv gärna mer feedback här"
      afLink="https://webropol.se/">
    </DigiToolsFeedbackRating>`
		};
	}

	render() {
		return (
			<div class="digi-tools-feedback-rating-details">
				<digi-typography>
					{!this.afShowOnlyExample && (
						<digi-typography-preamble>
							Betygsättningskomponenten används för att snabbt samla in feedback i
							tjänster och andra flöden i form av betyg 1 till 5.
						</digi-typography-preamble>
					)}
				</digi-typography>
				<digi-layout-container af-no-gutter af-margin-bottom>
					{!this.afShowOnlyExample && <h2>Exempel</h2>}
					<digi-code-example
						af-code={JSON.stringify(this.ratingCode)}
						af-hide-controls={this.afHideControls ? 'true' : 'false'}
						af-hide-code={this.afHideCode ? 'true' : 'false'}
					>
						<div class="slot__controls" slot="controls">
							<digi-form-fieldset
								afName="Variant"
								afLegend="Variant"
								onChange={(e) =>
									(this.feedbackRatingVariation = (e.target as any).value)
								}
							>
								<digi-form-radiobutton
									afName="Variant"
									afLabel="Primär"
									afValue={FeedbackRatingVariation.PRIMARY}
									afChecked={
										this.feedbackRatingVariation === FeedbackRatingVariation.PRIMARY
									}
								/>
								<digi-form-radiobutton
									afName="Variant"
									afLabel="Sekundär"
									afValue={FeedbackRatingVariation.SECONDARY}
									afChecked={
										this.feedbackRatingVariation === FeedbackRatingVariation.SECONDARY
									}
								/>
							</digi-form-fieldset>
							<digi-form-fieldset
								afName="Typ"
								afLegend="Typ"
								onChange={(e) =>
									(this.feedbackRatingType = (e.target as any).value)
								}
							>
								<digi-form-radiobutton
									afName="Typ"
									afLabel="Fullbredd"
									afValue={FeedbackRatingType.FULLWIDTH}
									afChecked={
										this.feedbackRatingType === FeedbackRatingType.FULLWIDTH
									}
								/>
								<digi-form-radiobutton
									afName="Typ"
									afLabel="Grid"
									afValue={FeedbackRatingType.GRID}
									afChecked={
										this.feedbackRatingType === FeedbackRatingType.GRID
									}
								/>
							</digi-form-fieldset>
						</div>
						<digi-tools-feedback-rating
							af-variation={this.feedbackRatingVariation}
							af-type={this.feedbackRatingType}
							afProduct="Designsystem"
							afApplication="Feedback rating component"
							af-link-text="Skriv gärna mer feedback här"
							af-link="https://webropol.se/"
						></digi-tools-feedback-rating>
					</digi-code-example>
				</digi-layout-container>
				<digi-typography>
					{!this.afShowOnlyExample && (
						<digi-layout-container af-no-gutter af-margin-bottom>
							<h2>Beskrivning</h2>
							<p>
								När man valt ett betyg och klickat på Skicka-knappen så skickas ett
								event, afOnSubmitFeedback. Om en lyssnare implementeras kan man använda
								detta event för att fånga nödvändig data t. ex för spårning.
							</p>
							<p>
								Komponenten har utvecklats främst för (men inte begränsad till) spårning
								med Piwik pro. Det finns totalt tre events som triggas vid respektive händelser, dessa tre är:
							</p>
							<digi-list>
								<li>Klick på Skicka-knappen</li>
								<li>Klick på en stjärna</li>
								<li>Klick på den eventuella länken efter inskick</li>
							</digi-list>
							<h3>Varianter</h3>
							<p>
								Komponenten finns i två varianter med olika utseenden på stjärnorna, som
								man växlar mellan med
								<digi-code af-code="af-variation" />.
							</p>
							<p>
								Komponenten finns även i en fullbredd- och grid-variant som styrs med
								<digi-code af-code="af-type" />.
							</p>
							<h3>Anpassningar</h3>
							<p>
								Frågan i rubriken kan anpassas med hjälp av{' '}
								<digi-code af-code="af-question" />.
							</p>
							<p>
								Textbeskrivningen på betygsskalan kan modifieras med{' '}
								<digi-code af-code="af-ratings" /> och måste följa samma formatering som
								i kodexemplet ovan, dvs. att alternativen är separerade med ett
								kommatecken och mellanslag.
							</p>
							<p>
								Om en extern länk till ett formulär ska visas efter att användaren har
								skickat in sitt svar, skickas URL:en in med{' '}
								<digi-code af-code="af-link" /> samt den obligatoriska länkbeskrivningen
								med <digi-code af-code="af-link-text" />
							</p>
							<digi-layout-block
								af-variation="symbol"
								af-vertical-padding
								af-margin-bottom
							>
								<h2>Obligatoriska attribut</h2>
								<ul>
									<p>
										Vid användning av komponenten med spårning måste följande attribut
										skickas med för att data ska registreras korrekt i Piwik Pro:
									</p>
									<li>Produkt (enligt produktkatalogen)</li>
									<li>Tjänst (enligt produktkatalogen)</li>
								</ul>
							</digi-layout-block>
						</digi-layout-container>
					)}
				</digi-typography>
			</div>
		);
	}
}
