import { Component, h, Prop, State } from '@stencil/core';
import { CodeExampleLanguage, ToolsFeedbackBannerType } from '@digi/arbetsformedlingen';

@Component({
	tag: 'digi-tools-feedback-banner-details',
	styleUrl: 'digi-tools-feedback-banner-details.scss'
})
export class DigiToolsFeedbackBanner {
	@Prop() component: string;
  @Prop() afShowOnlyExample: boolean;
  @Prop() afHideControls: boolean;
  @Prop() afHideCode: boolean;

  @State() showExample: boolean = true;
  @State() feedbackBannerType: ToolsFeedbackBannerType = ToolsFeedbackBannerType.FULLWIDTH;

  get bannerCode() {
    return {
      [CodeExampleLanguage.HTML]: `\
<digi-tools-feedback-banner
af-heading="Hjälp oss att bli bättre" af-text="Skicka in dina synpunkter och förslag på förbättringar på (tjänst)" af-type="${this.feedbackBannerType}">
  <a href="www.arbetsformedlingen.se">Extern länk 1</a>
  <a href="www.arbetsformedlingen.se">Extern länk 2</a>
  <a href="www.arbetsformedlingen.se">Extern länk 3</a>
</digi-tools-feedback-banner>`,
      [CodeExampleLanguage.ANGULAR]: `\
<digi-tools-feedback-banner
[attr.af-heading]="Hjälp oss att bli bättre" [attr.af-text]="Skicka in dina synpunkter och förslag på förbättringar på (tjänst)" [attr.af-type]="feedbackBannerType.${Object.keys(ToolsFeedbackBannerType).find(
    (key) => ToolsFeedbackBannerType[key] === this.feedbackBannerType)}">
  <a href="www.arbetsformedlingen.se">Extern länk 1</a>
  <a href="www.arbetsformedlingen.se">Extern länk 2</a>
  <a href="www.arbetsformedlingen.se">Extern länk 3</a>
</digi-tools-feedback-banner>`,
      [CodeExampleLanguage.REACT]: `\
<DigiToolsFeedbackBanner
afHeading="Hjälp oss att bli bättre" afText="Skicka in dina synpunkter och förslag på förbättringar på (tjänst)" afType="${Object.keys(ToolsFeedbackBannerType).find(
  (key) => ToolsFeedbackBannerType[key] === this.feedbackBannerType)}">
  <a href="www.arbetsformedlingen.se">Extern länk 1</a>
  <a href="www.arbetsformedlingen.se">Extern länk 2</a>
  <a href="www.arbetsformedlingen.se">Extern länk 3</a>
</DigiToolsFeedbackBanner>`
    };
  }


	render() {
		return (
				<div class="digi-tools-feedback-banner-details">
					          <digi-typography>
          {!this.afShowOnlyExample && (
            <digi-typography-preamble>
              Feedback-banner används för att direktlänka till en enkät för att samla in synpunkter.
            </digi-typography-preamble>
          )}
          </digi-typography>
          <digi-layout-container af-no-gutter af-margin-bottom>
            {!this.afShowOnlyExample && (<h2>Exempel</h2>)}
              <digi-code-example af-code={JSON.stringify(this.bannerCode)}
                af-hide-controls={this.afHideControls ? 'true' : 'false'}
                af-hide-code={this.afHideCode ? 'true' : 'false'}>
              <div class="slot__controls" slot="controls">
                <digi-form-fieldset
                  afName="Variant på bredd"
                  afLegend="Variant på bredd"
                  onChange={(e) => (this.feedbackBannerType = (e.target as any).value)}
                >
                  <digi-form-radiobutton
                    afName="Variant på bredd"
                    afLabel="Fullbredd"
                    afValue={ToolsFeedbackBannerType.FULLWIDTH}
                    afChecked={this.feedbackBannerType === ToolsFeedbackBannerType.FULLWIDTH}
                  />
                  <digi-form-radiobutton
                    afName="Variant på bredd"
                    afLabel="Grid"
                    afValue={ToolsFeedbackBannerType.GRID}
                    afChecked={this.feedbackBannerType === ToolsFeedbackBannerType.GRID}
                  />
                </digi-form-fieldset>
              </div>
              <digi-tools-feedback-banner afType={this.feedbackBannerType} afHeading="Hjälp oss att bli bättre" afText="Skicka in dina synpunkter och förslag på förbättringar på (tjänst)">
                      <a href="www.arbetsformedlingen.se">Extern länk 1</a>
                      <a href="www.arbetsformedlingen.se">Extern länk 2</a>
                      <a href="www.arbetsformedlingen.se">Extern länk 3</a>
              </digi-tools-feedback-banner>
            </digi-code-example>
          </digi-layout-container>
          <digi-typography>
          {!this.afShowOnlyExample && (
            <digi-layout-container af-no-gutter af-margin-bottom>
              <h2>Beskrivning</h2>
              <h3>Varianter</h3>
              <p>Bannern är utformad att vara i fullbredd per default eftersom den placeras med fördel ovanför footern på en sida. Den går dock att ställa om till en mindre storlek (grid) med hjälp av {' '}
                <digi-code af-code="af-type"/>.</p>
              <h3>Valfritt textinnehåll</h3>
              <p>Rubriken och paragrafen skickas in med hjälp av {' '}
                <digi-code af-code="af-heading"/> och <digi-code af-code="af-text"/>.</p>
                <digi-layout-block
            af-variation="symbol"
            af-vertical-padding
            af-margin-bottom
          >
            <h2>Riktlinjer</h2>
            <ul>
              <li>
                Det finns två typer av komponenten för feedbackformulär: Fullbredd och
                Grid. Fullbredd skapar en egen layout-block som fyller ut utrymmet
                horisontellt, medan Grid lägger sig i en annan layout-block och ärver
                samma stilsättning som omgivande element.
              </li>              
              <li>
                Se till att justeringen av feedback komponenten matchar justeringen av
                tillhörande innehåll.
              </li>
            </ul>
          </digi-layout-block>
            </digi-layout-container>
          )}
        </digi-typography>
				</div>
		);
	}
}
