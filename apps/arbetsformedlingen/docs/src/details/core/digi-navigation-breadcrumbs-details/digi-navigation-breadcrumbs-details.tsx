import { Component, Prop, h } from '@stencil/core';
import { CodeExampleLanguage, InfoCardVariation } from '@digi/arbetsformedlingen';

@Component({
	tag: 'digi-navigation-breadcrumbs-details',
	styleUrl: 'digi-navigation-breadcrumbs-details.scss'
})
export class DigiNavigationBreadcrumbsDetails {
	@Prop() component: string;
	@Prop() afShowOnlyExample: boolean;
	@Prop() afHideControls: boolean;
	@Prop() afHideCode: boolean;
	get breadcrumbCode() {
		return {
			[CodeExampleLanguage.HTML]: `\
<digi-navigation-breadcrumbs
	af-current-page="Nuvarande sida"
>
	<a href="/">Start</a>
	<a href="/contact-us">Undersida 1</a>
	<a href="#">Undersida 2</a>
</digi-navigation-breadcrumbs>`,
			[CodeExampleLanguage.ANGULAR]: `\
<digi-navigation-breadcrumbs
	[attr.af-current-page]="'Nuvarande sida'"
>
	<a href="/">Start</a>
	<a href="/contact-us">Undersida 1</a>
	<a href="#">Undersida 2</a>
</digi-navigation-breadcrumbs>`,
			[CodeExampleLanguage.REACT]: `\
<DigiNavigationBreadcrumbs
	afCurrentPage="Nuvarande sida"
>
	<a href="/">Start</a>
	<a href="/contact-us">Undersida 1</a>
	<a href="#">Undersida 2</a>
</DigiNavigationBreadcrumbs>`
		};
	}
	render() {
		return (
			<div class="digi-navigation-breadcrumbs-details">

				<digi-typography>
					{!this.afShowOnlyExample && (
            <digi-typography-preamble>
              Brödsmulor (Breadcrumbs) används för att göra det extra tydligt
              var i strukturen användaren befinner sig och för att förtydliga
              sidans sammanhang. Brödsmulor kan också användas för att
              navigera uppåt i strukturen.
            </digi-typography-preamble>
					)}

					<digi-layout-container af-no-gutter af-margin-bottom>
						{!this.afShowOnlyExample && (<h2>Exempel</h2>)}
						<digi-code-example af-code={JSON.stringify(this.breadcrumbCode)}
							af-hide-controls={this.afHideControls ? 'true' : 'false'} af-hide-code={this.afHideCode ? 'true' : 'false'}>
							<digi-navigation-breadcrumbs afCurrentPage="Nuvarande sida">
								<a href="/">Start</a>
								<a href="/contact-us">Undersida 1</a>
								<a href="#">Undersida 2</a>
							</digi-navigation-breadcrumbs>
						</digi-code-example>

					</digi-layout-container>
					{!this.afShowOnlyExample && (
						<digi-layout-container afMarginBottom afNoGutter>
							<h2>Generellt</h2>
							<p>
								<digi-list>
									<li>
										För att kunna skapa en anpassad routing är det upp till
										applikationen som använder komponenten att förhindra
										klick-eventet genom <digi-code af-code="afOnClick " />.
									</li>

									<li>
										Nuvarande sida sätts via propsen {' '}
										<digi-code af-code="af-current-page='Nuvarande sida'" /> och
										förväntar sig en textsträng som representerar namnet.
									</li>
									<li>
										Brödsmulorna visas upp via en slot och förväntar sig <digi-code af-code="<a>" />-element
										med ett <digi-code af-code="href" />-attribut och en text.
									</li>
									<li>
										Komponenten manipulerar <digi-code af-code="<a>" />-elementen som skickas in via sloten
										och omsluter varje med ett <digi-code af-code="<li>" />-element.
									</li>
								</digi-list>
							</p>
						<digi-info-card
							afHeading="Riktlinjer"
							afHeadingLevel={`h3` as any}
							afVariation={InfoCardVariation.PRIMARY}
						>
							<digi-list>
								<li>
									Brödsmulor används aldrig för att visa steg i en process,
									till exempel i ett registreringsflöde. I det fallet ska
									brödsmulan sluta vid ingångssidan till flödet och se likadan
									ut genom alla sidor i flödet.
								</li>
								<li>
									Brödsmulor kan i vissa fall kompletteras med länk. Till
									exempel i navigering mellan ett sökresultat och en
									annons/profil.
								</li>
								<li>
									Brödsmulor placeras ovanför innehållet och under sidhuvudet,
									till vänster på sidan.
								</li>
								<li>
									Den sista brödsmulan ska avse sidan man befinner sig på.
								</li>
								<li>
									Varje brödsmula, förutom den sista, ska vara länkad till
									respektive sida.
								</li>
								<li>
									Brödsmulans namn ska vara samma som sidans namn, vilken även
									syns i huvudnavigationen. Sidans namn är inte alltid
									detsamma som rubrikens namn (H1).
								</li>
								<li>
									Den första brödsmulan ska vara länkad till startsidan och ha
									namnet "Start".
								</li>
								<li>
									Brödsmulor ligger direkt på bakgrundsfärgen, utan någon egen
									platta/bakgrund.
								</li>
								<li>Ikoner ska aldrig användas i brödsmulor.</li>
								<li>
									Brödsmulorna ska inte förkortas/trunkeras om kedjan blir
									lång. För att behålla syftet med orientering i struktur och
									sammanhang ska långa brödsmulor radbrytas.
								</li>
							</digi-list>
							</digi-info-card>
						</digi-layout-container>
					)}
				</digi-typography>

			</div>
		);
	}
}
