import { newSpecPage } from '@stencil/core/testing';
import { DigiBadgeNotification } from './digi-badge-notification-details';

describe('digi-badge-notification-details', () => {
	it('renders', async () => {
		const { root } = await newSpecPage({
			components: [DigiBadgeNotification],
			html: '<digi-badge-notification-details></digi-badge-notification-details>'
		});
		expect(root).toEqualHtml(`
      <digi-badge-notification-details>
        <mock:shadow-root>
          <div>
            Hello, World! I'm
          </div>
        </mock:shadow-root>
      </digi-badge-notification-details>
    `);
	});

	it('renders with values', async () => {
		const { root } = await newSpecPage({
			components: [DigiBadgeNotification],
			html: `<digi-badge-notification-details first="Stencil" last="'Don't call me a framework' JS"></digi-badge-notification-details>`
		});
		expect(root).toEqualHtml(`
      <digi-badge-notification-details first="Stencil" last="'Don't call me a framework' JS">
        <mock:shadow-root>
          <div>
            Hello, World! I'm Stencil 'Don't call me a framework' JS
          </div>
        </mock:shadow-root>
      </digi-badge-notification-details>
    `);
	});
});
