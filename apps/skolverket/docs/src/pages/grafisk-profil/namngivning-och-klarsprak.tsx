import { Component, h } from '@stencil/core';
import Helmet from '@stencil/helmet';

@Component({
	tag: 'page-namngivning-och-klarsprak',
	scoped: true
})
export class PageNamngivningOchKlarsprak {
	render() {
		return (
			<article>
				<Helmet>
					<title>Namngivning och klarspråk</title>
				</Helmet>
				<h1>Namngivning och klarspråk</h1>
				<p>Så här gör man med namngivning och klarspråk</p>
			</article>
		);
	}
}
