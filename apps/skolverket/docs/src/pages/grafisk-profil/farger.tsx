import { Component, getAssetPath, h } from '@stencil/core';
import { ArticlePage } from '@digi/skolverket-docs/components/ArticlePage';

@Component({
	tag: 'page-farger',
	scoped: true
})
export class PageFarger {
	render() {
		return (
			<ArticlePage
				head={<title>Färger - översikt</title>}
				heading="Färger - översikt"
				preamble="Skolverkets färger är en viktig del i vårt visuella uttryck. Vi använder
			ett antal olika färger för olika situationer och funktioner. Här kan du
			läsa hur du kan använda Skolverkets profilfärger."
			>
				<p>
					Färgerna är framtagna för att fungera i såväl digital som i tryckt form.
					Hur färgerna får användas kan dock variera något beroende på om det handlar
					om tryck eller digitala kanaler.
				</p>
				<p>
					Tänk på att rätt kombinerade färger garanterar god läsbarhet och
					tillgänglighet. Skolverkets samtliga system omfattas av lagen om
					tillgänglighet för digital offentlig service. Vi följer riktlinjerna i WCAG
					2.1 AA som ett verktyg för att upprätthålla detta. Därför är det viktigt
					att du alltid utgår från riktlinjerna i vår grafiska profil.
				</p>
				<h2>Profilfärger</h2>
				<p>
					Det här är Skolverkets profilfärger. Förutom profilfärgerna finns även ett
					antal kompletterande färger som får användas till exempel i digitala
					gränssnitt som funktionsfärger och skuggning. Mer information om färgerna
					och tillåtna variationer hittar du under fliken färgtokens.
				</p>
				<p>
					<digi-media-image
						afSrc={getAssetPath('/assets/images/profilfarger-oversikt.png')}
						afAlt={'Översikt över Skolverkets profilfärger'}
						afWidth={'1022'}
						afHeight={'777'}
						afFullwidth
					/>
				</p>
				<h3>Primärfärger</h3>
				<p>
					Skolverket har två primärfärger. Vår huvudsakliga profilfärg är Primärfärg
					1 - Plommon och är den färg som används i logotypen. På webbplatsen (
					<a href="skolverket.se">skolverket.se</a>) används dock primärfärg 2 -
					December som accentfärg.
				</p>
				<h3>Sekundärfärger</h3>
				<p>
					Sekundärfärgerna används i huvudsak som bakgrundsfärger kombinerade med
					primärfärgerna. Sekundärfärgerna får även användas tonade i 50 % och 25 %.
				</p>
				<h3>Accentfärger</h3>
				<p>
					Accentfärgerna används främst till dekorelement och illustrationer.
					Accentfärgerna får aldrig användas som textfärg. Två av accentfärgerna har
					anpassats för diagram.
				</p>
				<h3>Komplementfärger</h3>
				<p>
					Komplementfärgerna används vid behov av ytterligare färger, exempelvis i
					diagram och grafik. Komplementfärgerna kan även användas som textfärg.
				</p>
				<h2>Så använder du färg på text</h2>
				<p>
					För att garantera en god läsbarhet krävs att all text har god kontrast mot
					textens bakgrund. Det är därför viktigt att vi följer principerna nedan.
				</p>
				<p>
					Den huvudsakliga regeln är att använda svart eller någon av primärfärgerna
					som textfärg. Texten kan ligga på vit bakgrund eller på någon av de tre
					sekundärfärgerna.
				</p>
				<p>
					Alternativet är att använda vitt eller någon av sekundärfärgerna som
					textfärg, på en bakgrund i någon av primärfärgerna.
				</p>
				<p>
					<ul>
						<li>
							<strong>Primärfärgerna</strong> fungerar väl för text och kan kombineras
							med samtliga sekundärfärger. De kan antingen användas som textfärg eller
							som bakgrundsfärg.
						</li>
						<li>
							<strong>Sekundärfärgerna</strong> kan kombineras med båda primärfärgerna.
							De kan antingen användas som textfärg eller som bakgrundsfärg.
						</li>
						<li>
							<strong>Accentfärgerna</strong> får aldrig användas till text, varken som
							bakgrundsfärg eller som textfärg. Kontrasten blir inte tillräckligt god.
							Accentfärgerna används för att tillföra kulör och värme i grafik och
							grafiska element.
						</li>
						<li>
							<strong>Komplementfärgerna</strong> har tagits fram som ett komplement
							till övriga färger. De kan vid behov användas till text på vit bakgrund
							eller som bakgrund till vit text. Som textfärg får de aldrig kombineras
							med primär- eller accentfärgerna.
						</li>
					</ul>
				</p>
				<p>
					<digi-media-image
						afSrc={getAssetPath('/assets/images/profilfarger-dos-och-donts.png')}
						afAlt={'Översikt över tillåtna färgkombinationer'}
						afWidth={'1606'}
						afHeight={'965'}
						afFullwidth
					/>
				</p>
			</ArticlePage>
		);
	}
}
