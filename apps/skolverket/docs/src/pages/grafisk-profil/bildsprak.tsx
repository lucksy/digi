import { Component, getAssetPath, h } from '@stencil/core';
import { ArticlePage } from '@digi/skolverket-docs/components/ArticlePage';

@Component({
	tag: 'page-bildsprak',
	scoped: true
})
export class PageBildsprak {
	render() {
		return (
			<ArticlePage
				head={<title>Bildspråk</title>}
				preamble="Foton är en viktig del i vår kommunikation för att representera människor
			och inspirera genom att förmänskliga hur det är att utbilda sig."
				heading="Bildspråk"
			>
				<p>
					Hellre ingen bild än fel bild! Ett foto kan tolkas på många olika sätt.
					Därför är det viktigt att tänka till både en och två gånger extra för att
					ta rätt beslut. Samt ta hjälp av andra som till vardags jobbar med foton
					inom myndigheten om du känner dig osäker.
				</p>
				<h2>Skolverkets bildmanér</h2>
				<p>
					Genom att fokusera på människor och riktiga berättelser kan vi förankra
					informationen om skolsystemet i verkligheten och göra den mer relaterbar.
					Alla bilder vi använder ska bygga på Skolverkets värdeord:
				</p>
				<p>
					<ul>
						<li>aktiv och saklig</li>
						<li>påverka och förbättra</li>
						<li>öppenhet och dialog</li>
					</ul>
				</p>
				<p>
					<digi-media-image
						afSrc={getAssetPath('/assets/images/bildsprak-collage.jpg')}
						afAlt={'Collage av elever som arbetar i olika skolmiljöer'}
						afWidth={'674'}
						afHeight={'225'}
					/>
				</p>
				<h3>Så väljer du rätt bilder</h3>
				<p>Välj bildmaterial som:</p>
				<p>
					<ul>
						<li>känns äkta och naturligt - undvik alltför arrangerade motiv</li>
						<li>är relevant i förhållande till ämnet</li>
						<li>håller god estetisk nivå, och hög teknisk kvalitet</li>
						<li>har individen i centrum - Låt personligheten komma fram</li>
						<li>
							har naturliga färger, men eftersträva en lugn palett som harmoniserar med
							färgpaletten
						</li>
					</ul>
				</p>
				<p>
					<digi-notification-detail>
						<h4 slot="heading">Undvik!</h4>
						<p>
							Undvik att använda bilder som enbart blir bakgrunder och tänk på att det
							ska gå att ta till sig informationen som finns i bilden även utan att
							kunna se den.
						</p>
					</digi-notification-detail>
				</p>
				<h3>Bildkategorier i vårt kommunikation</h3>
				<h4>Individen i centrum</h4>
				<p>
					Porträttbilder på främst barn och elever, men också skolans personal. Det
					behöver inte alltid vara en person, utan kan lika gärna vara två eller tre
					individer tillsammans.
				</p>
				<h4>Verksamhetsmiljön</h4>
				<p>
					Bilder där lärare och barn/elever är aktiva inom de miljöer och den
					verksamhet som Skolverket ansvarar för.
				</p>
				<h4>Dialog och samverkan</h4>
				<p>
					Bilder som speglar samverkan och lärande (mellan lärare och elev, elev och
					elev, lärare och lärare och rektor och lärare). Vi strävar alltid efter
					bilder som speglar ett normkritiskt och inkluderande perspektiv på skolans
					verksamhet.
				</p>
				<h2>Bildbanken</h2>
				<p>
					Vår gemensamma bildbank består av bilder som alla medarbetare kan ladda
					ner. Dessa bilder kan användas till presentationer, webbsidor, annonser
					eller publikationer. Notera att du måste deklarera hur du ska använda
					bilden i bildbanken - denna rutin är till för att undvika onödig
					dubbelpublicering. Observera att bilderna i bildbanken endast får användas
					i samband med uppdrag som avser Skolverket.
				</p>
				<h3>Bilder är personuppgifter</h3>
				<p>
					Enligt dataskyddsförordningen (GDPR) så räknas bilder på människor som
					personuppgifter. Därför måste alla bilder Skolverket publicerar ha ett
					syfte med rättslig grund. Använd därför endast bilder som finns
					tillgängliga i bildbanken. Har du frågor om bildbanken eller är osäker på
					om du får använda en bild - kontakta{' '}
					<a href="mailto:digitala.produktioner@skolverket.se">
						digitala.produktioner@skolverket.se
					</a>
				</p>
				<p>
					Läs mer om GDPR och vad som gäller för publicering av media på nätet på{' '}
					<a href="https://www.imy.se/verksamhet/dataskydd/vi-guidar-dig/">
						Integritetsskyddsmyndighetens (IMY) webbplats
					</a>
					.
				</p>
				<h2>Mer om bildspråk och bildval</h2>
				<p>
					På Kanalen hittar du mer information och stöd om hur du använder
					bildbanken, hur du bör tänka kring bildval samt hur vi hanterar
					personuppgifter.
				</p>
				<digi-list-link>
					<li>
						<a href="https://kanalen.skolverket.se/stod-i-arbetet/kommunikation/text-bild-och-form">
							Kanalens sida för foto och film
						</a>
					</li>
				</digi-list-link>
			</ArticlePage>
		);
	}
}
