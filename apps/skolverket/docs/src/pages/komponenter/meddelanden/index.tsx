import { Component, h } from '@stencil/core';
import { Category } from '@digi/taxonomies';
import { CategoryPage } from '@digi/skolverket-docs/components/CategoryPage';

@Component({
	tag: 'page-category-meddelanden',
	scoped: true
})
export class PageCategoryMeddelanden {
	render() {
		return (
			<CategoryPage
				category={Category.NOTIFICATION}
				preamble="För att skapa ett pålitligt och konsekvent system där Skolverket är en tydlig avsändare samtidigt som tjänster och siter särskiljande kommer till sin rätt har ett system i tre nivåer tagits fram."
			></CategoryPage>
		);
	}
}
