import { Component, h } from '@stencil/core';
import { Category } from '@digi/taxonomies';
import { CategoryPage } from '@digi/skolverket-docs/components/CategoryPage';

@Component({
	tag: 'page-category-kort',
	scoped: true
})
export class PageCategoryKort {
	render() {
		return (
			<CategoryPage
				category={Category.CARD}
				preamble="Kort och komponenter för att gruppera innehåll."
			></CategoryPage>
		);
	}
}
