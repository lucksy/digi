import { Component, h } from '@stencil/core';
import { Category } from '@digi/taxonomies';
import { CategoryPage } from '@digi/skolverket-docs/components/CategoryPage';

@Component({
	tag: 'page-category-knappar',
	scoped: true
})
export class PageCategoryKnappar {
	render() {
		return (
			<CategoryPage
				category={Category.BUTTON}
				preamble="Knappar för att utföra handlingar eller exekvera funktioner."
			></CategoryPage>
		);
	}
}
