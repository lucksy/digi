import { Component, h } from '@stencil/core';
import { Category } from '@digi/taxonomies';
import { CategoryPage } from '@digi/skolverket-docs/components/CategoryPage';
import { href } from 'stencil-router-v2';

@Component({
	tag: 'page-category-layout',
	scoped: true
})
export class PageCategoryLayout {
	render() {
		return (
			<CategoryPage
				category={Category.LAYOUT}
				preamble="Rader, kolumner, basgrid och annat för att skapa layouter."
			>
				<p>
					<digi-notification-detail>
						<h2 slot="heading">Bygger du en sidvy?</h2>
						<p>
							Då hittar du nog vad du söker i kategorin för{' '}
							<a {...href('/komponenter/sidblock-och-delar')}>sidblock och delar</a>.
						</p>
					</digi-notification-detail>
				</p>
			</CategoryPage>
		);
	}
}
