import { Component, h } from '@stencil/core';
import Helmet from '@stencil/helmet';
import { PageBlockHeroVariation } from '@digi/skolverket';
import { href } from 'stencil-router-v2';

@Component({
	tag: 'page-category-komponenter',
	scoped: true
})
export class PageCategoryKomponenter {
	render() {
		return (
			<article>
				<Helmet>
					<title>Om komponenter</title>
				</Helmet>
				<digi-page-block-hero
					afVariation={PageBlockHeroVariation.SUB}
					afBackgroundImage={'/assets/images/om-komponenter-hero.jpg'}
				>
					<h1 slot="heading">Om komponenter</h1>
					<digi-typography-preamble>
						Komponenterna är fundamentet i designsystemet. De är byggstenarna som
						skapar visuella och funktionella kopplingar mellan produkter och tjänster.
					</digi-typography-preamble>
					<digi-list-link>
						<li>
							<a {...href('/kom-igang/jobba-med-kod')}>
								Så kommer du igång med komponenterna
							</a>
						</li>
					</digi-list-link>
				</digi-page-block-hero>

				<digi-docs-component-list />
			</article>
		);
	}
}
