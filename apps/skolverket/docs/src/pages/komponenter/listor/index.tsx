import { Component, h } from '@stencil/core';
import { Category } from '@digi/taxonomies';
import { CategoryPage } from '@digi/skolverket-docs/components/CategoryPage';

@Component({
	tag: 'page-category-listor',
	scoped: true
})
export class PageCategoryListor {
	render() {
		return (
			<CategoryPage
				category={Category.LIST}
				preamble="Länklistor och andra vanligt förekommande listtyper."
			></CategoryPage>
		);
	}
}
