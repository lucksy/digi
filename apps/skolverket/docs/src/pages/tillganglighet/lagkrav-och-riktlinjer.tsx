import { ArticlePage } from '@digi/skolverket-docs/components/ArticlePage';
import { Component, h } from '@stencil/core';

@Component({
	tag: 'page-lagkrav-och-riktlinjer',
	scoped: true,
	styles: `a { display: block })`
})
export class PageLagkravOchRiktlinjer {
	render() {
		return (
			<ArticlePage
				head={<title>Lagkrav och riktlinjer</title>}
				heading="Lagkrav och riktlinjer"
				preamble="Flera av våra lagar i Sverige ställer krav på tillgänglighet, vilket Skolverket som myndighet är skyldig att följa. Varje person som är anställd som tjänsteman på myndigheten har ett ansvar för genomförande av funktionshinderspolitiken. Därför ska Skolverket verka för att våra lokaler, verksamhet och information är tillgänglig för alla, oavsett förutsättningar och förmågor. Att avstå från tillgänglighet, såväl fysisk som digital, kan i vissa fall klassas som diskriminering."
			>
				<p>
					Genom Skolverkets designsystem underlättar vi förvaltningen av våra
					digitala tjänster och produkter. Design och programkod blir lättare att
					hantera för våra utvecklingsteam och kräver mindre underhåll, när vi
					använder våra egenutvecklade komponenter som följer aktuella riktlinjer.
				</p>
				<p>
					<a href="https://www.regeringen.se/49bbd3/contentassets/1485f931d7b842c18fd670cb3715b0f5/en-strategi-for-genomforande-av-funktionshinderspolitiken-2011-2016">
						Regeringens strategi för genomförande av funktionshinderpolitiken (.pdf)
					</a>
					<a href="https://www.mfd.se/organisation/vanliga-fragor-och-svar/fragor-om-delaktighet-och-tillganglighet/">
						Frågor om tillgänglighet hos Myndigheten för delaktighet
					</a>
				</p>
				<h2>Lagen om tillgänglighet till digital offentlig service, DOS-lagen</h2>
				<p>
					Den lag som det förmodligen pratas mest om är lagen om tillgänglighet till
					digital offentlig service, eller DOS-lagen som den oftast kallas. DOS-lagen
					började gälla den första januari 2018 och är den svenska implementationen
					av EU's webbtillgänglighetsdirektiv. Lagen beskriver hur offentliga
					webbplatser, appar och dokument ska vara utformade för att uppnå krav på
					tillgänglighet.
				</p>
				<p>
					Till offentliga webbplatser räknas i huvudsak myndighetens externa
					webbplatser, intranät och extranät.
				</p>
				<p>
					DOS-lagen innehåller i sig inte några konkreta krav på tillgänglighet, utan
					pekar istället på EN-standarden EN-301549: Tillgänglighetskrav lämpliga vid
					offentlig upphandling av IKT produkter och tjänster i Europa. Denna
					standard pekar i sin tur på en internationell standard som är mer bekant
					för de flesta, nämligen WCAG (Web Content Accessibility Guidelines). I
					slutänden är det alltså WCAG som ligger till grund för kraven i DOS-lagen.
					Den version av WCAG som gäller just nu är version 2.1.
				</p>
				<p>
					Du som arbetar med Skolverkets tjänster som riktar sig till våra kunder,
					till exempel lärare, ska följa DOS-lagen och därmed riktlinjerna om
					tillgänglighet. Detta gäller även dig som arbetar med interna tjänster som
					omfattas av vårt intranät eftersom även våra medarbetare har varierande
					förmågor. Skolverket är därför angeläget om att värna om arbetsmiljön på
					myndigheten och att vara en attraktiv arbetsgivare.
				</p>
				<p>
					<a href="https://www.digg.se/digital-tillganglighet/">
						Digital tillgänglighet hos Myndigheten för digital förvaltning
					</a>
					<a href="https://webbriktlinjer.se/lagkrav/webbdirektivet/">
						Webbdirektivet på webbriktlinjer.se
					</a>
					<a href="https://www.riksdagen.se/sv/dokument-lagar/dokument/svensk-forfattningssamling/lag-20181937-om-tillganglighet-till-digital_sfs-2018-1937">
						Lag (2018:1937) om tillgänglighet till digital offentlig service hos
						Riksdagen
					</a>
					<a href="https://www.w3.org/TR/WCAG21/">
						WCAG 2.1 (Web Content Guidelines) hos W3C
					</a>
				</p>
				<h2>Lagen om offentlig upphandling, LOU</h2>
				<p>
					Lag (2016:1145) om offentlig upphandling är en lång och snårig lag som
					gäller för alla upphandlande myndigheter i Sverige. Den beskriver vad som
					gäller när en myndighet ska köpa in olika typer av produkter, exempelvis
					digitala plattformar eller system för tidrapportering. Den första januari
					2017 ändrades lagen att innehålla en ny paragraf om tekniska krav.
				</p>
				<h3>Kapitel 9, paragraf 2:</h3>
				<p>
					“När det som anskaffas ska användas av fysiska personer ska de tekniska
					specifikationerna bestämmas med beaktande av samtliga användares behov,
					däribland tillgängligheten för personer med funktionsnedsättning.”.
				</p>
				<p>
					Stycket ovan säger att den upphandlande myndigheten ska ställa krav på
					tillgänglighet i de produkter som myndigheten upphandlar och som övergår
					ett visst tröskelvärde. I praktiken innebär det att som lägst ställa på
					krav på tillgänglighet enligt EN-standarden EN 301549: Tillgänglighetskrav
					lämpliga vid offentlig upphandling av IKT produkter och tjänster i Europa.
					Det är samma standard som DOS-lagen pekar på.
				</p>
				<p>
					<a href="https://webbriktlinjer.se/tillganglighet/juridiska-krav/">
						Juridiska krav på tillgänglighet på Webbriktlinjer.se
					</a>
					<a href="https://www.riksdagen.se/sv/dokument-lagar/dokument/svensk-forfattningssamling/lag-20161145-om-offentlig-upphandling_sfs-2016-1145">
						Lagen om offentlig upphandling hos Riksdagen
					</a>
					<a href="https://www.upphandlingsmyndigheten.se/regler-och-lagstiftning/troskelvarden-och-direktupphandlingsgranser/">
						Tröskelvärden och direktupphandlingsgränser hos Upphandlingsmyndigheten
					</a>
				</p>
				<h2>Diskrimineringslagen och arbetsmiljölagen</h2>
				<p>
					Sedan den första januari 2015 kan bristande tillgänglighet klassas som
					grund för diskriminering. Organisationer med verksamhetsområden där det
					finns uttryckliga förbud mot diskriminering, exempelvis utbildningsområdet,
					ska vidta åtgärder för tillgänglighet. Webbplatser eller webbgränssnitt
					nämns inte specifikt, men däremot tillgänglig information och
					kommunikation. Exempel på åtgärder är att information ska kunna erbjudas i
					alternativa format, att det ska finnas olika sätt att kontakta en myndighet
					och att biljetter ska kunna tillhandahållas på alternativa sätt.
				</p>
				<p>
					Arbetsmiljölagen förhåller sig till diskrimineringslagen genom bestämmelser
					som bland annat omfattar webbgränssnitt, exempelvis intranät och gränssnitt
					för administration.
				</p>
				<p>
					<a href="https://webbriktlinjer.se/lagkrav/diskrimineringslagen/">
						Bristande tillgänglighet som diskrimineringsform hos DiGG
					</a>
				</p>
				<h2>WCAG och EN-301549</h2>
				<p>
					WCAG (Web Content Accessibility Guidelines) är en internationell standard
					för webbtillgänglighet. Standarden har arbetats fram av W3C (World Wide Web
					Consortium) och utgör även en ISO-standard (ISO 30071-1:2019). Även om WCAG
					är framtagen för innehåll som publiceras på webben, gäller standarden även
					på dokument och appar.
				</p>
				<p>
					I avsnitten om DOS-lagen och lagen om offentlig upphandling ovan utgör
					EN-standarden en viktig del eftersom den gäller som grund för kraven i
					svensk tillgänglighetslagstiftning. EN-standarden pekar för det mesta på
					tillgänglighetskraven i WCAG, men innehåller också några egna kriterier.
				</p>
				<p>
					Det är för närvarande version 2.1 av WCAG som gäller internationellt och
					för offentlig sektor i Sverige. Version 2.2 av WCAG är planerad att
					publiceras i september 2022, men det är oklart om EN 301549 kommer att
					harmoniseras med WCAG 2.2.
				</p>
				<p>
					<a href="https://www.etsi.org/deliver/etsi_en/301500_301599/301549/03.02.01_60/en_301549v030201p.pdf">
						EN-301549 Accessibility requirements for ICT products and services version
						3.2.1 hos Etsi.org (.pdf)
					</a>
					<a href="https://webbriktlinjer.se/lagkrav/diskrimineringslagen/">
						WCAG 2.1 (Web Content Guidelines) hos W3C
					</a>
					<a href="https://webbriktlinjer.se/lagkrav/webbdirektivet/krav-en-301549-utover-wcag-2-1-aa/">
						Krav i EN 301549 utöver WCAG 2.1 på Webbriktlinjer.se
					</a>
				</p>
			</ArticlePage>
		);
	}
}
