import { Component, h } from '@stencil/core';
import Helmet from '@stencil/helmet';

@Component({
	tag: 'page-lista-med-tillganglighetsbrister',
	scoped: true
})
export class PageListaMedTillganglighetsbrister {
	render() {
		return (
			<article>
				<Helmet>
					<title>Lista med tillgänglighetsbrister</title>
				</Helmet>
				<h1>Lista med tillgänglighetsbrister</h1>
				<p>Så här gör man med lista med tillgänglighetsbrister</p>
			</article>
		);
	}
}
