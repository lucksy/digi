import { Component, h, Prop } from '@stencil/core';
import 'skolverket-dist';

import state from '@digi/skolverket-docs/store/store';
import { InternalRouterState } from 'stencil-router-v2/dist/types';
import { router } from '@digi/skolverket-docs/global/routes/router';
import { PageRoutes } from '@digi/skolverket-docs/global/routes/PageRoutes';
import { ComponentRoutes } from '@digi/skolverket-docs/global/routes/ComponentRoutes';
@Component({
	tag: 'digi-docs',
	scoped: true
})
export class DigiDocs {
	Router = router;

	@Prop() afRoot: string = '/';

	componentWillLoad() {
		state.routerRoot = this.afRoot;

		state.router.activePath = this.Router.activePath;

		this.setNewRouterPaths();

		this.Router.onChange(
			'activePath',
			(_activePath: InternalRouterState['activePath']) => {
				this.setOldRouterPaths();

				state.router.activePath = _activePath;
				this.setNewRouterPaths();

				window.scrollTo(0, 0);
			}
		);

		if ('serviceWorker' in navigator) {
			navigator.serviceWorker.getRegistration().then((registration) => {
				if (registration?.active) {
					navigator.serviceWorker.addEventListener('controllerchange', () =>
						window.location.reload()
					);
				}
			});
		}

		this.setMatomoTagManager();
	}

	setMatomoTagManager() {
		var _paq = (window._paq = window._paq || []);
		/* tracker methods like "setCustomDimension" should be called before "trackPageView" */
		_paq.push(['trackPageView']);
		_paq.push(['enableLinkTracking']);
		(function () {
			var u = 'https://matomo.skolverket.se/';
			_paq.push(['setTrackerUrl', u + 'matomo.php']);
			_paq.push(['setSiteId', '25']);
			var d = document,
				g = d.createElement('script'),
				s = d.getElementsByTagName('script')[0];
			g.async = true;
			g.src = u + 'matomo.js';
			s.parentNode.insertBefore(g, s);
		})();
	}

	setOldRouterPaths() {
		state.router.oldPaths = [
			...state.router.activePath.split('/').filter((item) => item !== '')
		];
	}

	setNewRouterPaths() {
		state.router.newPaths = [
			...state.router.activePath.split('/').filter((item) => item !== '')
		];
	}

	render() {
		return (
			<digi-page>
				<digi-docs-header slot="header" />
				<main>
					<this.Router.Switch>
						<PageRoutes />
						<ComponentRoutes />
					</this.Router.Switch>
				</main>
				<digi-docs-footer slot="footer" />
			</digi-page>
		);
	}
}
