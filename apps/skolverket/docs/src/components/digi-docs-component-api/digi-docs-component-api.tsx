import { Component, h, Prop } from '@stencil/core';
import { components } from '@digi/skolverket-docs/data/componentData';
import { ComponentLink } from '../ComponentLink';
import { ComponentTag } from '@digi/skolverket-docs/global/routes/componentTagsAndNames.generated';

@Component({
	tag: 'digi-docs-component-api',
	styleUrl: 'digi-docs-component-api.scss',
	scoped: true
})
export class DigiDocsComponentApi {
	@Prop() tag: ComponentTag;

	protected get componentClassName() {
		const clearAndUpper = (text) => text.replace(/-/, '').toUpperCase();
		return this.tag.replace(/(^\w|-\w)/g, clearAndUpper);
	}

	render() {
		const component = components.find((c) => c.tag === this.tag);

		return (
			<digi-layout-container class="digi-docs-component-api">
				<digi-layout-rows>
					<digi-card-box>
						<digi-typography-heading-section>
							<h2>Meta</h2>
						</digi-typography-heading-section>
						<digi-typography>
							<div>
								<h3>DOM-elementets typ</h3>
								<p>HTML{this.componentClassName}Element</p>
							</div>
						</digi-typography>
					</digi-card-box>
					{component.props?.length > 0 && (
						<digi-card-box>
							<digi-typography-heading-section>
								<h2>Attribut</h2>
							</digi-typography-heading-section>
							<digi-typography>
								<digi-layout-rows>
									{component.props.map((prop) => (
										<div>
											<h3>
												{prop.attr}
												{prop.required && ' (Obligatorisk)'}
											</h3>
											<p>
												<strong>Property:</strong> {prop.name}
											</p>
											{prop.docs && (
												<p>
													<strong>Beskrivning:</strong> {prop.docs}
												</p>
											)}
											<p>
												<strong>Typ:</strong> {prop.type}
											</p>
											{prop.default && (
												<p>
													<strong>Förvalt:</strong> {prop.default}
												</p>
											)}
										</div>
									))}
								</digi-layout-rows>
							</digi-typography>
						</digi-card-box>
					)}
					{component.events?.length > 0 && (
						<digi-card-box>
							<digi-typography-heading-section>
								<h2>Events</h2>
							</digi-typography-heading-section>
							<digi-typography>
								<digi-layout-rows>
									{component.events.map((event) => (
										<div>
											<h3>{event.event}</h3>
											{event.docs && (
												<p>
													<strong>Beskrivning:</strong> {event.docs}
												</p>
											)}
											<p>
												<strong>Detail:</strong> <digi-code af-code={event.detail} />
											</p>
										</div>
									))}
								</digi-layout-rows>
							</digi-typography>
						</digi-card-box>
					)}
					{component.methods?.length > 0 && (
						<digi-card-box>
							<digi-typography-heading-section>
								<h2>Metoder</h2>
							</digi-typography-heading-section>
							<digi-typography>
								<digi-layout-rows>
									{component.methods.map((method) => (
										<div>
											<h3>{method.name}</h3>
											{method.docs && (
												<p>
													<strong>Beskrivning:</strong> {method.docs}
												</p>
											)}
											{method.returns?.type && (
												<p>
													<strong>Returtyp:</strong> {method.returns?.type}
												</p>
											)}
											<p>
												<strong>Signatur:</strong> <digi-code af-code={method.signature} />
											</p>
										</div>
									))}
								</digi-layout-rows>
							</digi-typography>
						</digi-card-box>
					)}
					{component.styles?.length > 0 && (
						<digi-card-box>
							<digi-typography-heading-section>
								<h2>Tokens</h2>
							</digi-typography-heading-section>
							<digi-typography>
								<ul>
									{component.styles.map((token) => (
										<li>
											{token.name}: {token.docs}
										</li>
									))}
								</ul>
							</digi-typography>
						</digi-card-box>
					)}
					{component.dependents?.length > 0 && (
						<digi-list-link>
							<h2 slot="heading">Används av</h2>
							{component.dependents.map((dependent) => {
								return (
									<li>
										<ComponentLink tag={dependent} />
									</li>
								);
							})}
						</digi-list-link>
					)}
					{component.dependencies?.length > 0 && (
						<digi-list-link>
							<h2 slot="heading">Använder sig av</h2>
							{component.dependencies.map((dependency) => {
								return (
									<li>
										<ComponentLink tag={dependency} />
									</li>
								);
							})}
						</digi-list-link>
					)}
				</digi-layout-rows>
			</digi-layout-container>
		);
	}
}
