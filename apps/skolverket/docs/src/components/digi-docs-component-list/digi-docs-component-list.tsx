import { Component, Fragment, h, Host, Prop, State } from '@stencil/core';
import state from '@digi/skolverket-docs/store/store';
import { ComponentLink } from '../ComponentLink';
import { ComponentTag } from '@digi/skolverket-docs/global/routes/componentTagsAndNames.generated';
import { Category } from '@digi/taxonomies';

@Component({
	tag: 'digi-docs-component-list',
	styleUrl: 'digi-docs-component-list.scss',
	scoped: true
})
export class ComponentList {
	@State() searchFilter: string = '';
	@State() searchIsStuck: boolean = false;

	@Prop() category?: Category;

	searchFilterChangeHandler(e) {
		const inputValue = e.detail.target.value;
		this.searchFilter = inputValue;
	}

	intersectionHandler(e) {
		console.log(e.detail);
		this.searchIsStuck = !e.detail;
	}

	render() {
		return (
			<Host>
				<digi-util-intersection-observer
					afOptions={{ threshold: [1] }}
					onAfOnChange={(e) => this.intersectionHandler(e)}
				>
					<digi-layout-block
						afVerticalPadding
						class={{ 'is-stuck': this.searchIsStuck }}
					>
						<digi-form-input-search
							afLabel={'Filtrera komponenter'}
							afHideButton
							onAfOnInput={(e) => this.searchFilterChangeHandler(e)}
						/>
					</digi-layout-block>
				</digi-util-intersection-observer>
				<digi-page-block-cards>
					<h2 slot="heading">
						{this.category ? (
							<Fragment>
								Alla komponenter i kategorin "{this.category.toLowerCase()}"
							</Fragment>
						) : (
							<Fragment>Här listar vi alla våra komponenter</Fragment>
						)}
					</h2>
					{state.components
						.filter(
							(component) => !this.category || component.category === this.category
						)
						.filter((component) => !component.tag.includes('digi-icon-'))
						.filter((component) => {
							if (
								!component.docsTags
									?.find((tag) => tag.name === 'swedishName')
									?.text.toLowerCase()
									.includes('under utveckling')
							) {
								return component;
							}
						})
						.filter((component) => {
							if (this.searchFilter !== '') {
								if (
									component.tag
										.toLowerCase()
										.includes(this.searchFilter.toLowerCase()) ||
									component.docsTags
										?.find((tag) => tag.name === 'swedishName')
										?.text.toLowerCase()
										.includes(this.searchFilter.toLowerCase())
								) {
									return true;
								}
								return false;
							}
							return true;
						})
						.sort((a, b) => {
							const aSwedishName = a.docsTags?.find(
								(tag) => tag.name === 'swedishName'
							)?.text;

							const bSwedishName = b.docsTags?.find(
								(tag) => tag.name === 'swedishName'
							)?.text;

							return aSwedishName?.localeCompare(bSwedishName);
						})
						.map((component) => {
							const TagName = component.tag + '-details';
							return (
								<digi-card-link>
									<h3 slot="link-heading">
										<ComponentLink tag={component.tag as ComponentTag} />
									</h3>
									<div
										slot="image"
										style={{
											width: '100%',
											height: '100%',
											display: 'flex',
											'align-items': 'center',
											'justify-content': 'center'
										}}
									>
										<TagName inert afShowOnlyExample afHideControls afHideCode></TagName>
									</div>
								</digi-card-link>
							);
						})}
				</digi-page-block-cards>
			</Host>
		);
	}
}
