import { FunctionalComponent, h, VNode } from '@stencil/core';
import Helmet from '@stencil/helmet';
import { TypographyVariation } from '@digi/skolverket';
import { Tocify } from '@digi/skolverket-docs/components/Tocify';
import { Category } from '@digi/taxonomies';

interface CategoryPageProps {
	heading?: string;
	preamble?: string;
	head?: VNode[];
	category: Category;
}

export const CategoryPage: FunctionalComponent<CategoryPageProps> = (
	{ heading, preamble, head, category },
	children
) => (
	<section
		style={{ 'padding-block': 'calc(var(--digi--responsive-grid-gutter) * 2)' }}
	>
		{head && (
			<Helmet>
				<title>Kategori - {category.toLowerCase()}</title>
				{head}
			</Helmet>
		)}
		<digi-layout-container>
			<article>
				<digi-typography afVariation={TypographyVariation.LARGE}>
					<h1>{heading || category}</h1>
					{preamble && (
						<digi-typography-preamble>
							{preamble}
							{!preamble.endsWith('.') && '.'}
						</digi-typography-preamble>
					)}
					<Tocify>{children}</Tocify>
				</digi-typography>
			</article>
		</digi-layout-container>
		<section>
			<digi-docs-component-list category={category} />
		</section>
	</section>
);
