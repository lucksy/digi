import { Component, Prop, h, State, Fragment } from '@stencil/core';
import { CodeExampleLanguage } from '@digi/skolverket';
import { ComponentDetails } from '@digi/skolverket-docs/components/ComponentDetails';
import { ComponentLink } from '../components/ComponentLink';

@Component({
	tag: 'digi-navigation-main-menu-details',
	scoped: true
})
export class DigiNavigationMainMenuDetails {
	@Prop() afShowOnlyExample: boolean;
	@Prop() afHideControls: boolean;
	@Prop() afHideCode: boolean;
	@State() totalPages: number = 6;
	@State() showResultName: boolean = false;

	get navigationMainMenuCode() {
		return {
			[CodeExampleLanguage.HTML]: `\
<digi-navigation-main-menu>
  <nav>
    <ul>
      <li>
        <button>Huvudingång 1</button>
          <digi-navigation-main-menu-panel>
            <a href="#" slot="main-link">Huvudingång 1</a>
            <ul>
              <li>
                <a href="#">Ingång 1 nivå 2.1</a>
              </li>
              <li>
                <a href="#">Ingång 1 nivå 2.2</a>
                  <ul>
                    <li>
                      <a href="#">Ingång 1 nivå 2.2 nivå 3.1</a>
                    </li>
                    <li>
                      <a href="#">Ingång 1 nivå 2.2 nivå 3.2</a>
                      <ul>
                        <li>
                          <a href="#">Ingång 1 nivå 3.2 nivå 4.1</a>
                        </li>
                        <li>
                          <a href="#" aria-current="page">
                            Ingång 1 nivå 3.2 nivå 4.2
                          </a>
                        </li>
                      </ul>
                    </li>
                  </ul>
                </li>
                <li>
                  <a href="#">Ingång 1 nivå 2.3</a>
                </li>
              </ul>
            </digi-navigation-main-menu-panel>
          </li>
          <li>
            <button>Huvudingång 2</button>
            <digi-navigation-main-menu-panel>
              ...
            </digi-navigation-main-menu-panel>
          </li>
          <li>
            <button>Huvudingång 3</button>
            <digi-navigation-main-menu-panel>
              ...
            </digi-navigation-main-menu-panel>
          </li>
          <li>
            <a href="#">Huvudingång 4 (Utan undermeny)</a>
          </li>
          <li>
            <button>Huvudingång 5</button>
            <digi-navigation-main-menu-panel>
              <a href="#" slot="main-link">
                Huvudingång 5
              </a>
              <ul>
                <li>
                  <a href="#">Ingång 5 nivå 2.1</a>
                </li>
              </ul>
            </digi-navigation-main-menu-panel>
          </li>
        </ul>
      </nav>
    </digi-navigation-main-menu>`,
			[CodeExampleLanguage.ANGULAR]: `\
<digi-navigation-main-menu>
  <nav>
    <ul>
      <li>
        <button>Huvudingång 1</button>
          <digi-navigation-main-menu-panel>
            <a href="#" slot="main-link">Huvudingång 1</a>
            <ul>
              <li>
                <a href="#">Ingång 1 nivå 2.1</a>
              </li>
              <li>
                <a href="#">Ingång 1 nivå 2.2</a>
                  <ul>
                    <li>
                      <a href="#">Ingång 1 nivå 2.2 nivå 3.1</a>
                    </li>
                    <li>
                      <a href="#">Ingång 1 nivå 2.2 nivå 3.2</a>
                      <ul>
                        <li>
                          <a href="#">Ingång 1 nivå 3.2 nivå 4.1</a>
                        </li>
                        <li>
                          <a href="#" aria-current="page">
                            Ingång 1 nivå 3.2 nivå 4.2
                          </a>
                        </li>
                      </ul>
                    </li>
                  </ul>
                </li>
                <li>
                  <a href="#">Ingång 1 nivå 2.3</a>
                </li>
              </ul>
            </digi-navigation-main-menu-panel>
          </li>
          <li>
            <button>Huvudingång 2</button>
            <digi-navigation-main-menu-panel>
              ...
            </digi-navigation-main-menu-panel>
          </li>
          <li>
            <button>Huvudingång 3</button>
            <digi-navigation-main-menu-panel>
              ...
            </digi-navigation-main-menu-panel>
          </li>
          <li>
            <a href="#">Utan undermeny</a>
          </li>
          <li>
            <button>Huvudingång 5</button>
            <digi-navigation-main-menu-panel>
              <a href="#" slot="main-link">
                Huvudingång 5
              </a>
              <ul>
                <li>
                  <a href="#">Ingång 5 nivå 2.1</a>
                </li>
              </ul>
            </digi-navigation-main-menu-panel>
          </li>
        </ul>
      </nav>
    </digi-navigation-main-menu>`
		};
	}

	example() {
		return (
			<Fragment>
				<digi-code-example
					af-code={JSON.stringify(this.navigationMainMenuCode)}
					af-controls-position="end"
					af-hide-controls={this.afHideControls ? 'true' : 'false'}
					af-hide-code={this.afHideCode ? 'true' : 'false'}
				>
					<digi-navigation-main-menu>
						<nav>
							<ul>
								<li>
									<button>Huvudingång 1</button>
									<digi-navigation-main-menu-panel>
										<a href="#" slot="main-link">
											Huvudingång 1
										</a>
										<ul>
											<li>
												<a href="#">Ingång 1 nivå 2.1</a>
											</li>
											<li>
												<a href="#">Ingång 1 nivå 2.2</a>
												<ul>
													<li>
														<a href="#">Ingång 1 nivå 2.2 nivå 3.1</a>
													</li>
													<li>
														<a href="#">Ingång 1 nivå 2.2 nivå 3.2</a>
														<ul>
															<li>
																<a href="#">Ingång 1 nivå 3.2 nivå 4.1</a>
															</li>
															<li>
																<a href="#" aria-current="page">
																	Ingång 1 nivå 3.2 nivå 4.2
																</a>
															</li>
														</ul>
													</li>
												</ul>
											</li>
											<li>
												<a href="#">Ingång 1 nivå 2.3</a>
											</li>
											<li>
												<a href="#">Ingång 1 nivå 2.4</a>
												<ul>
													<li>
														<a href="#">Ingång 1 nivå 2.4 nivå 3.1</a>
														<ul>
															<li>
																<a href="#">Ingång 1 nivå 2.4 nivå 4.1</a>
															</li>
															<li>
																<a href="#">Ingång 1 nivå 2.4 nivå 4.2</a>
															</li>
														</ul>
													</li>
													<li>
														<a href="#">Ingång 1 nivå 2.4 nivå 3.2</a>
													</li>
												</ul>
											</li>
										</ul>
									</digi-navigation-main-menu-panel>
								</li>
								<li>
									<button>Huvudingång 2</button>
									<digi-navigation-main-menu-panel>
										<a href="#" slot="main-link">
											Huvudingång 2
										</a>
										<ul>
											<li>
												<a href="#">Ingång 2 nivå 2.1</a>
											</li>
											<li>
												<a href="#">Ingång 2 nivå 2.2</a>
												<ul>
													<li>
														<a href="#">Ingång 2 nivå 2.2 nivå 3.1</a>
													</li>
													<li>
														<a href="#">Ingång 2 nivå 2.2 nivå 3.2</a>
													</li>
												</ul>
											</li>
											<li>
												<a href="#">Ingång 2 nivå 2.3</a>
											</li>
										</ul>
									</digi-navigation-main-menu-panel>
								</li>
								<li>
									<button>Huvudingång 3</button>
									<digi-navigation-main-menu-panel>
										<a href="#" slot="main-link">
											Huvudingång 3
										</a>
										<ul>
											<li>
												<a href="#">Ingång 3 nivå 2.1</a>
											</li>
											<li>
												<a href="#">Ingång 3 nivå 2.2</a>
												<ul>
													<li>
														<a href="#">Ingång 3 nivå 2.2 nivå 3.1</a>
													</li>
													<li>
														<a href="#">Ingång 3 nivå 2.2 nivå 3.2</a>
													</li>
												</ul>
											</li>
										</ul>
									</digi-navigation-main-menu-panel>
								</li>
								<li>
									<a href="#">Huvudingång 4</a>
								</li>
								<li>
									<button>Huvudingång 5</button>
									<digi-navigation-main-menu-panel>
										<a href="#" slot="main-link">
											Huvudingång 5
										</a>
										<ul>
											<li>
												<a href="#">Ingång 5 nivå 2.1</a>
											</li>
										</ul>
									</digi-navigation-main-menu-panel>
								</li>
							</ul>
						</nav>
					</digi-navigation-main-menu>
				</digi-code-example>
				<p>
					Notera att exemplet ovan ser lite knasigt ut, menyn är endast syftad till
					att användas i full skärmbredd.
				</p>
			</Fragment>
		);
	}

	render() {
		return (
			<ComponentDetails
				showOnlyExample={this.afShowOnlyExample}
				preamble="Huvudmenyn är den primära navigationen och finns med i sidhuvudet på alla undersidor. Den ger en överblick av sajtens innehåll och låter användare ta sig till ner till nivå 3 i strukturen. På mindre skärmar tar den även över vänstermenyns roll och låter användaren navigera ner till nivå 5."
				example={this.example()}
				description={
					<Fragment>
						<p>
							Komponenten förväntar sig ett omedelbart <digi-code af-code="<nav>" />
							-element, direkt åtföljt av en lista med huvudingångar:{' '}
							<digi-code af-code="<ul><li>" />. Huvudingångarna kan antingen vara
							länkar, om de inte har undersidor, eller knappar, om de har undersidor.
							Om ingången har undersidor så används{' '}
							<ComponentLink tag="digi-navigation-main-menu-panel" /> direkt efter
							knappen.
						</p>
						<h3>Aktuell sida</h3>
						<p>
							Aktuell sida ska markeras ut med{' '}
							<digi-code af-code="aria-current='page'" />, det är så komponenten vet
							vilken sida som är aktuell och justerar utseende därefter.
						</p>
						<h3>Att använda i sidhuvudet</h3>
						<p>
							Huvudmenyskomponenten är inbygd i{' '}
							<ComponentLink tag="digi-page-header" text="sidhuvudet" />, på så sätt
							att man där inte behöver använda sig av komponentens element. Sihuvudets
							slot "nav" förväntar sig allt från <digi-code af-code="<nav>" />
							-elementet och nedåt.
						</p>
					</Fragment>
				}
			/>
		);
	}
}
