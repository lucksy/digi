import { Component, Prop, h, State, Fragment } from '@stencil/core';
import { CodeExampleLanguage, FormCheckboxLayout } from '@digi/skolverket';
import { ComponentDetails } from '@digi/skolverket-docs/components/ComponentDetails';
import { ComponentLink } from '@digi/skolverket-docs/components/ComponentLink';

@Component({
	tag: 'digi-form-checkbox-details',
	scoped: true
})
export class DigiFormCheckboxDetails {
	@Prop() afShowOnlyExample: boolean;
	@Prop() afHideControls: boolean;
	@Prop() afHideCode: boolean;
	@State() formCheckboxLayout: FormCheckboxLayout = FormCheckboxLayout.BLOCK;
	@State() validation: boolean = false;

	get formCheckboxCode() {
		return {
			[CodeExampleLanguage.HTML]: `\
      <digi-form-checkbox 
        af-label="Kryssruta"
        af-layout="${this.formCheckboxLayout}"\
        ${this.validation ? `\n \t \taf-validation="error"` : ''}
        />`,
			[CodeExampleLanguage.ANGULAR]: `\
      <digi-form-checkbox
        [attr.af-label]="'Kryssruta'"
        [attr.af-layout]="FormCheckboxLayout.${Object.keys(
									FormCheckboxLayout
								).find((key) => FormCheckboxLayout[key] === this.formCheckboxLayout)}"
        ${
									this.validation
										? `[attr.af-validation]="FormCheckboxValidation.ERROR" \n \t \t`
										: ''
								}/>`
		};
	}

	example() {
		return (
			<digi-code-example
				af-code={JSON.stringify(this.formCheckboxCode)}
				af-hide-controls={this.afHideControls ? 'true' : 'false'}
				af-hide-code={this.afHideCode ? 'true' : 'false'}
			>
				{!this.afHideControls && (
					<div class="slot__controls" slot="controls">
						<digi-form-fieldset
							afName="Layout"
							afLegend="Layout"
							onChange={(e) => (this.formCheckboxLayout = (e.target as any).value)}
						>
							<digi-form-radiobutton
								afName="Layout"
								afLabel="Block"
								afValue={FormCheckboxLayout.BLOCK}
								afChecked={this.formCheckboxLayout === FormCheckboxLayout.BLOCK}
							/>
							<digi-form-radiobutton
								afName="Layout"
								afLabel="Inline"
								afValue={FormCheckboxLayout.INLINE}
								afChecked={this.formCheckboxLayout === FormCheckboxLayout.INLINE}
							/>
						</digi-form-fieldset>

						<digi-form-fieldset afLegend="Validering">
							<digi-form-checkbox
								afLabel="Error"
								afChecked={this.validation}
								onAfOnChange={() =>
									this.validation ? (this.validation = false) : (this.validation = true)
								}
							></digi-form-checkbox>
						</digi-form-fieldset>
					</div>
				)}

				<digi-form-checkbox
					afLabel="Kryssruta"
					afLayout={this.formCheckboxLayout}
					af-validation={this.validation ? 'error' : ''}
					afChecked={!this.validation}
				/>
			</digi-code-example>
		);
	}

	render() {
		return (
			<ComponentDetails
				showOnlyExample={this.afShowOnlyExample}
				preamble="Kryssruta används för att ge möjlighet till valfritt antal val mellan
			olika alternativ. Kryssruta kan också användas som ett ensamt element
			för att ge valmöjlighet till av/på-läge."
				example={this.example()}
				description={
					<Fragment>
						<h3>Layouter</h3>
						<p>
							Kryssrutan finns i två layouter, block och inline. Block är den som bör
							användas i första hand och innebär att komponenten tar upp en hel rad där
							hela är klickbar.
						</p>
						<h3>Validering</h3>
						<p>
							Kryssrutan kan ha två olika valideringsstatus: Felaktig eller ingen alls.
						</p>
					</Fragment>
				}
				guidelines={
					<digi-notification-detail>
						<h3 slot="heading">Övriga riktlinjer</h3>
						<ul>
							<li>Kryssrutan kan vara förvald.</li>
							<li>
								När flera kryssrutor presenteras tillsammans så behöver de ha en
								överskrift (legend) så att det blir tydligt vad användaren tar ställning
								till. Kryssrutorna bör då presenteras vertikalt. (
								<ComponentLink
									tag="digi-form-fieldset"
									text="Se fältgrupperingskomponenten"
								/>
								)
							</li>
							<li>
								Att göra ett val i en kryssruta påverkar inte någon av de övriga
								kryssrutorna.
							</li>
							<li>
								Avståndet mellan kryssrutor bör vara tillräckligt stort för att vara en
								bra klickyta oavsett enhet.
							</li>
							<li>Etiketten ska vara tydlig och beskriva alternativet.</li>
							<li>
								Om många kryssrutor presenteras i en lång lista, fundera på om de kan
								grupperas i sub-grupper för ökad tydlighet.
							</li>
							<li>Använd inte kryssruta som en action/exekverafunktion.</li>
						</ul>
					</digi-notification-detail>
				}
			/>
		);
	}
}
