import { Component, Prop, h, State } from '@stencil/core';
import {
	CodeExampleLanguage,
	FormValidationMessageVariation
} from '@digi/skolverket';
import { ComponentDetails } from '@digi/skolverket-docs/components/ComponentDetails';

@Component({
	tag: 'digi-form-validation-message-details',
	scoped: true
})
export class DigiFormValidationMessageDetails {
	@Prop() afShowOnlyExample: boolean;
	@Prop() afHideControls: boolean;
	@Prop() afHideCode: boolean;
	@State() formValidationMessageVariation: FormValidationMessageVariation =
		FormValidationMessageVariation.SUCCESS;
	@State() hasDescription: boolean = false;
	get textareaCode() {
		return {
			[CodeExampleLanguage.HTML]: `\
       <digi-form-validation-message
         af-variation="${this.formValidationMessageVariation}"\ 
       >
       </digi-form-validation-message>`,
			[CodeExampleLanguage.ANGULAR]: `\
       <digi-form-validation-message
         [attr.af-variation]="FormTextareaVariation.${Object.keys(
										FormValidationMessageVariation
									).find(
										(key) =>
											FormValidationMessageVariation[key] ===
											this.formValidationMessageVariation
									)}"
       >
       </digi-form-validation-message>`
		};
	}

	example() {
		return (
			<digi-code-example
				af-code={JSON.stringify(this.textareaCode)}
				af-hide-controls={this.afHideControls ? 'true' : 'false'}
				af-hide-code={this.afHideCode ? 'true' : 'false'}
			>
				<div style={{ width: '400px' }}>
					<digi-form-validation-message
						af-variation={this.formValidationMessageVariation}
					>
						Jag är ett valideringsmeddelande
					</digi-form-validation-message>
				</div>
				{!this.afHideControls && (
					<div class="slot__controls" slot="controls">
						<digi-form-fieldset
							afName="Validering"
							afLegend="Valideringstyp"
							onChange={(e) =>
								(this.formValidationMessageVariation = (e.target as any).value)
							}
						>
							<digi-form-radiobutton
								afName="Validering"
								afLabel="Godkänd"
								afValue={FormValidationMessageVariation.SUCCESS}
								afChecked={
									this.formValidationMessageVariation ===
									FormValidationMessageVariation.SUCCESS
								}
							/>
							<digi-form-radiobutton
								afName="Validering"
								afLabel="Felaktig"
								afValue={FormValidationMessageVariation.ERROR}
								afChecked={
									this.formValidationMessageVariation ===
									FormValidationMessageVariation.ERROR
								}
							/>
							<digi-form-radiobutton
								afName="Validering"
								afLabel="Varning"
								afValue={FormValidationMessageVariation.WARNING}
								afChecked={
									this.formValidationMessageVariation ===
									FormValidationMessageVariation.WARNING
								}
							/>
						</digi-form-fieldset>
					</div>
				)}
			</digi-code-example>
		);
	}

	render() {
		return (
			<ComponentDetails
				showOnlyExample={this.afShowOnlyExample}
				preamble="Används för att visa
			valideringsmeddelanden. Den är redan inbygd i inmatningsfältskomponenten."
				example={this.example()}
				description={
					<p>
						Valideringsmeddelandekomponenten använder en slot för att visa
						meddelandetexten och förväntar sig en textnod eller liknande element.
						Komponenten används i många formulärkomponenter men går även att använda
						fristående. Komponenten förser inte sina egna tillgänglighetsfunktioner
						för levande regiontyp, dessa förväntas att förses av applikationen som
						använder komponenten. Använd till exempel{' '}
						<digi-code af-code="aria-atomic" /> och{' '}
						<digi-code af-code="role='alert'" /> på dess överordnade element.
					</p>
				}
			/>
		);
	}
}
