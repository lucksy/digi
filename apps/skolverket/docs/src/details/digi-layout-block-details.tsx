import { Component, Prop, h, State, Fragment } from '@stencil/core';
import { CodeExampleLanguage, LayoutBlockVariation } from '@digi/skolverket';
import { ComponentDetails } from '@digi/skolverket-docs/components/ComponentDetails';

@Component({
	tag: 'digi-layout-block-details',
	scoped: true
})
export class DigiLayoutBlockDetails {
	@Prop() afShowOnlyExample: boolean;
	@Prop() afHideControls: boolean;
	@Prop() afHideCode: boolean;
	@State() layoutBlockVariation: LayoutBlockVariation =
		LayoutBlockVariation.PRIMARY;

	get layoutBlockCode() {
		return {
			[CodeExampleLanguage.HTML]: `\
<digi-layout-block
  af-variation="${this.layoutBlockVariation}"
>
<digi-typography>
<h2>Det här är en primär blockvariant</h2>
<p>
  Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam magna neque, interdum vel massa eget, condimentum rutrum velit. Sed vitae ullamcorper sem. Aliquam malesuada nunc sed purus mollis scelerisque. Curabitur bibendum leo quis ante porttitor tincidunt. Nam tincidunt imperdiet tortor eu suscipit. Maecenas ut dui est.
</p>
</digi-typography>
</digi-layout-block>`,
			[CodeExampleLanguage.ANGULAR]: `\
<digi-layout-block
  [attr.af-variation]="LinkButtonVariation.${Object.keys(
			LayoutBlockVariation
		).find((key) => LayoutBlockVariation[key] === this.layoutBlockVariation)}"
>
<digi-typography>
<h2>Det här är en primär blockvariant</h2>
<p>
  Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam magna neque, interdum vel massa eget, condimentum rutrum velit. Sed vitae ullamcorper sem. Aliquam malesuada nunc sed purus mollis scelerisque. Curabitur bibendum leo quis ante porttitor tincidunt. Nam tincidunt imperdiet tortor eu suscipit. Maecenas ut dui est.
</p>
</digi-typography>
</digi-layout-block>`
		};
	}

	example() {
		return (
			<digi-code-example
				af-code={JSON.stringify(this.layoutBlockCode)}
				af-hide-controls={this.afHideControls ? 'true' : 'false'}
				af-hide-code={this.afHideCode ? 'true' : 'false'}
			>
				{!this.afHideControls && (
					<div class="slot__controls" slot="controls">
						<digi-form-select
							afLabel="Variant"
							onAfOnChange={(e) =>
								(this.layoutBlockVariation = (e.target as any).value)
							}
							af-variation="small"
							af-start-selected="0"
						>
							<option value="primary">Primär</option>
							<option value="secondary">Sekundär</option>
							<option value="profile">Profil</option>
							<option value="symbol">Symbol</option>
							<option value="tertiary">Tertiär</option>
							<option value="transparent">Transparent</option>
						</digi-form-select>
					</div>
				)}

				<digi-layout-block
					afMarginBottom
					afMarginTop
					afVerticalPadding
					af-variation={this.layoutBlockVariation}
				>
					<digi-typography>
						<div
							style={
								this.layoutBlockVariation == 'profile' ||
								this.layoutBlockVariation == 'tertiary'
									? { color: 'white' }
									: {}
							}
						>
							<h2>Det här är en primär blockvariant</h2>
							<p>
								Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam magna
								neque, interdum vel massa eget, condimentum rutrum velit. Sed vitae
								ullamcorper sem. Aliquam malesuada nunc sed purus mollis scelerisque.
								Curabitur bibendum leo quis ante porttitor tincidunt. Nam tincidunt
								imperdiet tortor eu suscipit. Maecenas ut dui est.
							</p>
						</div>
					</digi-typography>
				</digi-layout-block>
			</digi-code-example>
		);
	}

	render() {
		return (
			<ComponentDetails
				showOnlyExample={this.afShowOnlyExample}
				preamble="Det här är en layoutkomponent som är avsedd för att skapa block i full
		bredd med en inre container. Använd den främst då det inte finns
		specifika sidblock som fyller dina behov"
				example={this.example()}
				description={
					<Fragment>
						<h3>Varianter</h3>
						<p>
							Blockkomponenten finns i olika varianter som styr blockets bakgrundsfärg:
							primär, sekundär, tertiär, transparent symbol och profil. Det ställs in
							genom att använda <digi-code af-code="af-variation" />.
						</p>
					</Fragment>
				}
			/>
		);
	}
}
