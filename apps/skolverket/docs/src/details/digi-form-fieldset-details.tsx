import { Component, Prop, h } from '@stencil/core';
import { CodeExampleLanguage } from '@digi/skolverket';
import { ComponentDetails } from '@digi/skolverket-docs/components/ComponentDetails';

@Component({
	tag: 'digi-form-fieldset-details',
	scoped: true
})
export class DigiFormFieldsetDetails {
	@Prop() afShowOnlyExample: boolean;
	@Prop() afHideControls: boolean;
	@Prop() afHideCode: boolean;
	get fieldsetCode() {
		return {
			[CodeExampleLanguage.HTML]: `\
<digi-form-fieldset 
  af-form="Formulärnamnet" 
  af-legend="Det här är en legend" 
  af-name="Fältgruppnamn"
>
  <digi-form-checkbox af-label="Alternativ 1"></digi-form-checkbox>
  <digi-form-checkbox af-label="Alternativ 2"></digi-form-checkbox>
  <digi-form-checkbox af-label="Alternativ 3"></digi-form-checkbox>
  <digi-button>
    Skicka
  </digi-button>
</digi-form-fieldset>
`,
			[CodeExampleLanguage.ANGULAR]: `\
<digi-form-fieldset 
  [attr.af-form]="'Formulärnamnet'" 
  [attr.af-legend]="'Det här är en legend'" 
  [attr.af-name]="'Fältgruppnamn'"
>
  <digi-form-checkbox af-label="Alternativ 1"></digi-form-checkbox>
  <digi-form-checkbox af-label="Alternativ 2"></digi-form-checkbox>
  <digi-form-checkbox af-label="Alternativ 3"></digi-form-checkbox>
  <digi-button>
    Skicka
  </digi-button>
</digi-form-fieldset>`
		};
	}

	example() {
		return (
			<digi-code-example
				af-code={JSON.stringify(this.fieldsetCode)}
				af-hide-controls={this.afHideControls ? 'true' : 'false'}
				af-hide-code={this.afHideCode ? 'true' : 'false'}
			>
				<digi-form-fieldset
					afForm="Formulärnamnet"
					afLegend="Det här är en legend"
					afName="Fältgruppnamn"
				>
					<digi-form-checkbox afLabel="Alternativ 1"></digi-form-checkbox>
					<digi-form-checkbox afLabel="Alternativ 2"></digi-form-checkbox>
					<digi-form-checkbox afLabel="Alternativ 3"></digi-form-checkbox>
					<br />
					<digi-button>Skicka</digi-button>
				</digi-form-fieldset>
			</digi-code-example>
		);
	}

	render() {
		return (
			<ComponentDetails
				showOnlyExample={this.afShowOnlyExample}
				preamble="Fältgrupperingskomponenten använder en container för att gruppera
			formulärelement."
				example={this.example()}
				description={
					<p>
						Det första elementet i en fältkomponenten måste vara en{' '}
						<digi-code af-code="af-legend" /> som beskriver fältgruppen. Attributet{' '}
						<digi-code af-code="af-form" /> används för att ange vilket formulär
						fältgrupperingskomponenten tillhör och <digi-code af-code="af-name" />{' '}
						anger ett namn för fältgruppen.
					</p>
				}
			/>
		);
	}
}
