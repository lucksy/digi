import { Component, Prop, h, State, Fragment } from '@stencil/core';
import {
	CodeExampleLanguage,
	PageBlockSidebarVariation
} from '@digi/skolverket';
import { ComponentDetails } from '@digi/skolverket-docs/components/ComponentDetails';
import { ComponentLink } from '@digi/skolverket-docs/components/ComponentLink';

@Component({
	tag: 'digi-page-block-sidebar-details',
	scoped: true
})
export class DigiPageBlockSidebarDetails {
	@Prop() afShowOnlyExample: boolean;
	@Prop() afHideControls: boolean;
	@Prop() afHideCode: boolean;
	@State() variation: PageBlockSidebarVariation =
		PageBlockSidebarVariation.START;

	get code() {
		return {
			[CodeExampleLanguage.HTML]: `\
<digi-page-block-sidebar
  af-variation="${this.variation}"
>
  <digi-card-box slot="sidebar">
    <aside>
      <digi-typography>Innehåll i sidebar</digi-typography>
    </aside>
  </digi-card-box>
  <digi-card-box>
    <main>
      <digi-typography>Innehåll i huvudytan</digi-typography>
    </main>
  </digi-card-box>
</digi-page-block-sidebar>`,
			[CodeExampleLanguage.ANGULAR]: `\
<digi-page-block-sidebar
  [attr.af-variation]="PageBlockSidebarVariation.${Object.keys(
			PageBlockSidebarVariation
		).find((key) => PageBlockSidebarVariation[key] === this.variation)}"
>
  <digi-card-box slot="sidebar">
    <aside>
      <digi-typography>Innehåll i sidebar</digi-typography>
    </aside>
  </digi-card-box>
  <digi-card-box>
    <main>
      <digi-typography>Innehåll i huvudytan</digi-typography>
    </main>
  </digi-card-box>
</digi-page-block-sidebar>`
		};
	}

	example() {
		return (
			<digi-code-example
				af-code={JSON.stringify(this.code)}
				af-hide-controls={this.afHideControls ? 'true' : 'false'}
				af-hide-code={this.afHideCode ? 'true' : 'false'}
			>
				{!this.afHideControls && (
					<div class="slot__controls" slot="controls">
						<digi-form-fieldset
							afName="Variation"
							afLegend="Variation"
							onChange={(e) => (this.variation = (e.target as any).value)}
						>
							<digi-form-radiobutton
								afName="Variation"
								afLabel="Startsida"
								afValue={PageBlockSidebarVariation.START}
								afChecked={this.variation === PageBlockSidebarVariation.START}
							/>
							<digi-form-radiobutton
								afName="Variation"
								afLabel="Undersida"
								afValue={PageBlockSidebarVariation.SUB}
								afChecked={this.variation === PageBlockSidebarVariation.SUB}
							/>
							<digi-form-radiobutton
								afName="Variation"
								afLabel="Sektionssida"
								afValue={PageBlockSidebarVariation.SECTION}
								afChecked={this.variation === PageBlockSidebarVariation.SECTION}
							/>
							<digi-form-radiobutton
								afName="Variation"
								afLabel="Processida"
								afValue={PageBlockSidebarVariation.PROCESS}
								afChecked={this.variation === PageBlockSidebarVariation.PROCESS}
							/>
							<digi-form-radiobutton
								afName="Variation"
								afLabel="Artikelsida"
								afValue={PageBlockSidebarVariation.ARTICLE}
								afChecked={this.variation === PageBlockSidebarVariation.ARTICLE}
							/>
						</digi-form-fieldset>
					</div>
				)}
				<digi-page-block-sidebar af-variation={this.variation}>
					<digi-card-box slot="sidebar">
						<aside>
							<digi-typography>Innehåll i sidebar</digi-typography>
						</aside>
					</digi-card-box>
					<digi-card-box>
						<main>
							<digi-typography>Innehåll i huvudytan</digi-typography>
						</main>
					</digi-card-box>
				</digi-page-block-sidebar>
			</digi-code-example>
		);
	}

	render() {
		return (
			<ComponentDetails
				showOnlyExample={this.afShowOnlyExample}
				preamble="Sidblockskomponent med sidebar och huvudinnehåll."
				example={this.example()}
				description={
					<Fragment>
						<p>
							Används med fördel för att snabbt bygga upp sidor med sido- och
							huvudinnehåll.
						</p>
						<p>
							Exemplet ovan använder sig av{' '}
							<ComponentLink tag="digi-card-box" text="skuggade boxar" />, men det är
							främst för att förtydliga exemplet och absolut inget krav.
						</p>
					</Fragment>
				}
			/>
		);
	}
}
