import { ComponentDetails } from '@digi/skolverket-docs/components/ComponentDetails';
import { Component, h, Fragment } from '@stencil/core';

@Component({
	tag: 'digi-util-detect-click-outside-details',
	scoped: true
})
export class DigiUtilDetectClickOutSideDetails {
	render() {
		return (
			<ComponentDetails
				showOnlyExample={false}
				preamble="Övervakar klickhändelser och skapar events när klicket är inuti eller
      utanför."
				description={
					<Fragment>
						<p>
							Kan till exempel användas för att stänga en meny när man klickar utanför
							den.
						</p>
						<p>Pseudokodsexempel i Angularformat nedan:</p>
						<digi-code-block
							afCode={`<digi-util-detect-click-outside (afOnClickOutside)="isOpen === false">
  <div *ngIf="isOpen">Meny</div>
</digi-util-detect-click-outside>`}
						/>
					</Fragment>
				}
			/>
		);
	}
}
