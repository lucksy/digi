import { Component, Prop, h, Fragment } from '@stencil/core';
import { CodeExampleLanguage } from '@digi/skolverket';
import { ComponentDetails } from '@digi/skolverket-docs/components/ComponentDetails';
import { ComponentLink } from '@digi/skolverket-docs/components/ComponentLink';

@Component({
	tag: 'digi-typography-heading-section-details',
	scoped: true
})
export class DigiTypographyHeadingSectionDetails {
	@Prop() afShowOnlyExample: boolean;
	@Prop() afHideControls: boolean;
	@Prop() afHideCode: boolean;

	get code() {
		return {
			[CodeExampleLanguage.HTML]: `\
<digi-typography-heading-section>
  <h2>Avsnittsrubrik</h2>
</digi-typography-heading-section>`,
			[CodeExampleLanguage.ANGULAR]: `\
<digi-typography-heading-section>
  <h2>Avsnittsrubrik</h2>
</digi-typography-heading-section>`
		};
	}

	example() {
		return (
			<digi-code-example
				af-code={JSON.stringify(this.code)}
				af-hide-controls={this.afHideControls ? 'true' : 'false'}
				af-hide-code={this.afHideCode ? 'true' : 'false'}
			>
				<digi-typography-heading-section>
					<h2>Avsnittsrubrik</h2>
				</digi-typography-heading-section>
			</digi-code-example>
		);
	}

	render() {
		return (
			<ComponentDetails
				showOnlyExample={this.afShowOnlyExample}
				preamble="Markera syfte eller namn på en sektion eller block."
				example={this.example()}
				description={
					<Fragment>
						<p>
							Kan till exempel användas för att markera att en box i slutet av en sida
							är länkar till relaterade artiklar.
						</p>
						<p>
							Flera av sidblockskomponenterna har den här rubriken inbygd, t.ex.{' '}
							<ComponentLink tag="digi-page-block-cards" />.
						</p>
					</Fragment>
				}
			/>
		);
	}
}
