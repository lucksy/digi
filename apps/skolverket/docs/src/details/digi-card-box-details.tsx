import { ComponentDetails } from '@digi/skolverket-docs/components/ComponentDetails';
import { Component, Prop, h, Fragment, State } from '@stencil/core';
import { CardBoxWidth, CodeExampleLanguage } from '@digi/skolverket';
import { ComponentLink } from '../components/ComponentLink';

@Component({
	tag: 'digi-card-box-details',
	scoped: true
})
export class DigiCardBoxDetails {
	@Prop() afShowOnlyExample: boolean;
	@Prop() afHideControls: boolean;
	@Prop() afHideCode: boolean;
	@State() isFullWidth: boolean = false;

	get cardBoxCode() {
		return {
			[CodeExampleLanguage.HTML]: `\
<digi-card-box${this.isFullWidth ? ' af-width="full"' : ''}>
  <p>
    Curabitur blandit tempus porttitor. Vestibulum id ligula porta felis
    euismod semper. Aenean lacinia bibendum nulla sed consectetur. Morbi leo
    risus, porta ac consectetur ac, vestibulum at eros. Cum sociis natoque
    penatibus et magnis dis parturient montes, nascetur ridiculus mus.
    Vestibulum id ligula porta felis euismod semper.
  </p>
</digi-card-box>
				`,
			[CodeExampleLanguage.ANGULAR]: `\
<digi-card-box${this.isFullWidth ? ' [attr.af-width]="CardBoxWidth.FULL"' : ''}>
  <p>
    Curabitur blandit tempus porttitor. Vestibulum id ligula porta felis
    euismod semper. Aenean lacinia bibendum nulla sed consectetur. Morbi leo
    risus, porta ac consectetur ac, vestibulum at eros. Cum sociis natoque
    penatibus et magnis dis parturient montes, nascetur ridiculus mus.
    Vestibulum id ligula porta felis euismod semper.
  </p>
</digi-card-box>
				`
		};
	}

	example() {
		return (
			<digi-code-example
				af-code={JSON.stringify(this.cardBoxCode)}
				af-hide-controls={this.afHideControls ? 'true' : 'false'}
				af-hide-code={this.afHideCode ? 'true' : 'false'}
			>
				{!this.afHideControls && (
					<div class="slot__controls" slot="controls">
						<digi-form-fieldset af-legend="Fullbredd">
							<digi-form-checkbox
								afLabel="Ja"
								afChecked={this.isFullWidth}
								onAfOnChange={() => (this.isFullWidth = !this.isFullWidth)}
							></digi-form-checkbox>
						</digi-form-fieldset>
					</div>
				)}
				<digi-card-box afWidth={this.isFullWidth ? CardBoxWidth.FULL : null}>
					<p>
						Curabitur blandit tempus porttitor. Vestibulum id ligula porta felis
						euismod semper. Aenean lacinia bibendum nulla sed consectetur. Morbi leo
						risus, porta ac consectetur ac, vestibulum at eros. Cum sociis natoque
						penatibus et magnis dis parturient montes, nascetur ridiculus mus.
						Vestibulum id ligula porta felis euismod semper.
					</p>
				</digi-card-box>
			</digi-code-example>
		);
	}

	render() {
		return (
			<ComponentDetails
				showOnlyExample={this.afShowOnlyExample}
				example={this.example()}
				preamble="Används för att skapar tydliga sektioner, rama in formulär, verktyg och overlays."
				description={
					<Fragment>
						<p>
							Den skuggade boxen ska följa bredden för{' '}
							<ComponentLink tag="digi-layout-grid" text="basgriddens" /> kolumner i
							brytpunkterna extrastor, stor och - beroende på vad layouten stödjer -
							även i medium. Vid små skärmar faller boxen alltid ut till
							webbläsarfönstrets fulla bredd.
						</p>
						<p>
							<digi-notification-detail>
								<h3 slot="heading">Om utfallande box</h3>
								<p>
									Komponenten känner av om den befinner sig inuti{' '}
									<ComponentLink
										tag="digi-layout-container"
										text="containerkomponenten"
									/>
									, eller någon komponent som i sin tur inehåller en sådan, och kommer då
									automatiskt att bli utfallande vid små skärmstorlekar.
								</p>
							</digi-notification-detail>
						</p>
						<h3>Fullbredd</h3>
						<p>
							Box för fullbredd används för större sektioner, som en ram för större
							processer och formulär eller som ram för overlay. Boxen utnyttjar den
							fulla maxbredden för gridden och den interna paddingen gör att text och
							element i boxen linjerar med element utanför boxen.
						</p>
						<p>
							Från stor brytpunkt och nedåt innebär det att boxen faller ända ut till
							webbläsarfönstrets kant. På det här viset kan samma layout och kolumner
							användas i och utanför boxen och boxen stjäl inte onödigt horisontellt
							utrymme från innehållet.
						</p>
					</Fragment>
				}
			/>
		);
	}
}
