import { ComponentDetails } from '@digi/skolverket-docs/components/ComponentDetails';
import { Component, Prop, h, Fragment } from '@stencil/core';
import { CodeExampleLanguage } from '@digi/skolverket';
import { ComponentLink } from '../components/ComponentLink';

@Component({
	tag: 'digi-notification-cookie-details',
	scoped: true
})
export class DigiNotificationCookieDetails {
	@Prop() afShowOnlyExample: boolean;
	@Prop() afHideControls: boolean;
	@Prop() afHideCode: boolean;

	get notificationCookieCode() {
		return {
			[CodeExampleLanguage.HTML]: `\
<digi-notification-cookie>
  <a href="#" slot="link">
    Läs mer i vår cookie policy
  </a>
  <digi-form-checkbox
    slot="settings"
    af-label="Analytiska cookies"
    af-description="Ger oss information om hur vår webbplats används som gör att vi kan underhålla, driva och förbättra användarupplevelsen."
    af-layout="block"
    af-name="cookie"
    af-value="analytical"
  ></digi-form-checkbox>
  <digi-form-checkbox
    slot="settings"
    af-label="Ännu fler cookies"
    af-description="Ger oss information om hur vår webbplats används som gör att vi kan underhålla, driva och förbättra användarupplevelsen."
    af-layout="block"
    af-name="cookie"
    af-value="more"
  ></digi-form-checkbox>
</digi-notification-cookie>
				`,
			[CodeExampleLanguage.ANGULAR]: `\
<digi-notification-cookie>
  <a href="#" slot="link">
    Läs mer i vår cookiepolicy
  </a>
  <digi-form-checkbox
    slot="settings"
    af-label="Analytiska cookies"
    af-description="Ger oss information om hur vår webbplats används som gör att vi kan underhålla, driva och förbättra användarupplevelsen."
    af-layout="block"
    af-name="cookie"
    af-value="analytical"
  ></digi-form-checkbox>
  <digi-form-checkbox
    slot="settings"
    af-label="Ännu fler cookies"
    af-description="Ger oss information om hur vår webbplats används som gör att vi kan underhålla, driva och förbättra användarupplevelsen."
    af-layout="block"
    af-name="cookie"
    af-value="more"
  ></digi-form-checkbox>
</digi-notification-cookie>
				`
		};
	}

	example() {
		return (
			<digi-code-example
				af-code={JSON.stringify(this.notificationCookieCode)}
				af-hide-controls={this.afHideControls ? 'true' : 'false'}
				af-hide-code={this.afHideCode ? 'true' : 'false'}
			>
				<digi-notification-cookie>
					<a href="#" slot="link">
						Läs mer i vår cookiepolicy
					</a>
					<digi-form-checkbox
						slot="settings"
						afLabel={'Analytiska cookies'}
						af-description="Ger oss information om hur vår webbplats används som gör att vi kan underhålla, driva och förbättra användarupplevelsen."
						af-layout="block"
						af-name="cookie"
						af-value="analytical"
					></digi-form-checkbox>
					<digi-form-checkbox
						slot="settings"
						afLabel={'Ännu fler cookies'}
						af-description="Ger oss information om hur vår webbplats används som gör att vi kan underhålla, driva och förbättra användarupplevelsen."
						af-layout="block"
						af-name="cookie"
						af-value="more"
					></digi-form-checkbox>
				</digi-notification-cookie>
			</digi-code-example>
		);
	}

	render() {
		return (
			<ComponentDetails
				showOnlyExample={this.afShowOnlyExample}
				example={this.example()}
				preamble="Cookiebanner med inbygd modal."
				description={
					<Fragment>
						<p>
							Cookiebanner ska placeras över{' '}
							<ComponentLink tag="digi-page-header" text="sidhuvudet" /> på samma sätt
							som{' '}
							<ComponentLink
								tag="digi-notification-alert"
								text="globala informationsmeddelanden"
							/>
							.
						</p>
						<p>
							Avbrytknappen ska stänga overlay utan att göra några val. Spara och
							godkänn ska spara valen, stänger overlay och döljer cookiebanner.
						</p>
					</Fragment>
				}
				guidelines={
					<digi-notification-detail>
						<h3 slot="heading">Om hantering av cookies</h3>
						<p>
							Komponenten tar inte hand om själva cookiehanteringen, den är främst till
							för hantering av designmönster gällande cookiebannern. Eventhantering
							finns vid acceptera alla och vid inskickat formulär.
						</p>
					</digi-notification-detail>
				}
			/>
		);
	}
}
