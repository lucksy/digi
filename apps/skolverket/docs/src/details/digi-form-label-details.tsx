import { Component, Prop, h, State } from '@stencil/core';
import { CodeExampleLanguage } from '@digi/skolverket';
import { ComponentDetails } from '@digi/skolverket-docs/components/ComponentDetails';

@Component({
	tag: 'digi-form-label-details',
	scoped: true
})
export class DigiFormLabelDetails {
	@Prop() afShowOnlyExample: boolean;
	@Prop() afHideControls: boolean;
	@Prop() afHideCode: boolean;

	@State() hasDescription: boolean = false;
	@State() hasIcon: boolean = false;

	get labelCode() {
		return {
			[CodeExampleLanguage.HTML]: `\
       <digi-form-label
        af-label="Jag är en etikett"
        af-for="ett-element-id"\ ${
									this.hasDescription
										? '\n        af-description="Beskrivande text"\t '
										: ''
								}
       >
       ${
								this.hasIcon
									? ' <digi-icon af-name="notification-info" slot="actions"/>"\n\t   '
									: ''
							}\</digi-form-label>`,
			[CodeExampleLanguage.ANGULAR]: `\
       <digi-form-label
        [attr.af-label]="'Etikett'"
        [attr.af-for]="'ett-element-id'"\ ${
									this.hasDescription
										? `\n        [attr.af-description]="'Beskrivande text'"\t `
										: ''
								}
       >
        ${
									this.hasIcon
										? '<digi-icon [attr.af-name]="notification-info" slot="actions" />\n\t   '
										: ''
								}\</digi-form-label>`
		};
	}

	example() {
		return (
			<digi-code-example
				af-code={JSON.stringify(this.labelCode)}
				af-hide-controls={this.afHideControls ? 'true' : 'false'}
				af-hide-code={this.afHideCode ? 'true' : 'false'}
			>
				{!this.afHideControls && (
					<div class="slot__controls" slot="controls">
						<digi-form-fieldset afLegend="Beskrivande text">
							<digi-form-checkbox
								afLabel="Ja"
								afChecked={this.hasDescription}
								onAfOnChange={() =>
									this.hasDescription
										? (this.hasDescription = false)
										: (this.hasDescription = true)
								}
							></digi-form-checkbox>
						</digi-form-fieldset>
						<digi-form-fieldset afLegend="Ikon">
							<digi-form-checkbox
								afLabel="Ja"
								afChecked={this.hasIcon}
								onAfOnChange={() =>
									this.hasIcon ? (this.hasIcon = false) : (this.hasIcon = true)
								}
							></digi-form-checkbox>
						</digi-form-fieldset>
					</div>
				)}

				{!this.hasDescription && !this.hasIcon && (
					<digi-form-label
						af-label="Jag är en etikett"
						afLabel="Jag är en etikett"
						afFor="some-element-id"
					></digi-form-label>
				)}
				{this.hasDescription && !this.hasIcon && (
					<digi-form-label
						af-label="Jag är en etikett"
						afLabel="Jag är en etikett"
						afFor="some-element-id"
						af-description="Beskrivande text"
					></digi-form-label>
				)}

				{this.hasIcon && !this.hasDescription && (
					<digi-form-label
						af-label="Jag är en etikett"
						afLabel="Jag är en etikett"
						afFor="some-element-id"
					>
						<digi-icon afName={'notification-info'} slot="actions" />
					</digi-form-label>
				)}
				{this.hasIcon && this.hasDescription && (
					<digi-form-label
						af-label="Jag är en etikett"
						afLabel="Jag är en etikett"
						afFor="some-element-id"
						af-description="Beskrivande text"
					>
						<digi-icon afName={'notification-info'} slot="actions" />
					</digi-form-label>
				)}
			</digi-code-example>
		);
	}

	render() {
		return (
			<ComponentDetails
				showOnlyExample={this.afShowOnlyExample}
				preamble="Etikettkomponenten är kompatibel med alla typer av formelement och kan
			till exempel användas för att ge ett formulärselement en etikett."
				example={this.example()}
				description={
					<p>
						Med hjälp av <digi-code af-code="af-label" /> så anges etikettexten.
						Attributet <digi-code af-code="af-for" /> är obligatoriskt och ska vara
						identiskt med id-attributet på inmatningselementet. Etiketten har också
						stöd för en beskrivningstext som anges med{' '}
						<digi-code af-code="af-description" />. Det går också att lägga in en ikon
						i komponenten genom att lägga in <digi-code af-code='slot="actions"' /> i
						ikon-taggen.
					</p>
				}
			/>
		);
	}
}
