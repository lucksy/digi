import { Component, Prop, h, State, Fragment } from '@stencil/core';
import { CodeExampleLanguage } from '@digi/skolverket';
import { ComponentDetails } from '@digi/skolverket-docs/components/ComponentDetails';
import { ComponentLink } from '@digi/skolverket-docs/components/ComponentLink';

@Component({
	tag: 'digi-navigation-tabs-details',
	scoped: true
})
export class DigiNavigationTabsDetails {
	@Prop() afShowOnlyExample: boolean;
	@Prop() afHideControls: boolean;
	@Prop() afHideCode: boolean;
	@State() hasActiveTab: boolean = false;

	get navigationTabsCode() {
		return {
			[CodeExampleLanguage.HTML]: `\
<digi-navigation-tabs
  af-aria-label="En tablist label"
  >
  <digi-navigation-tab 
    af-aria-label="Flik 1" 
    af-id="flikb1"
  >
    <p>Jag är flik 1</p>
  </digi-navigation-tab>
  <digi-navigation-tab 
    af-aria-label="Flik 2" 
    af-id="flikb2"
  >
    <p>Jag är flik 2</p>
  </digi-navigation-tab>
  <digi-navigation-tab 
    af-aria-label="Flik 3" 
    af-id="flikb3"
  >
    <p>Jag är flik 3</p>
  </digi-navigation-tab>
</digi-navigation-tabs>`,
			[CodeExampleLanguage.ANGULAR]: `\
<digi-navigation-tabs
  [attr.af-aria-label]="'En tablist label'"
  >
  <digi-navigation-tab 
    [attr.af-aria-label]="'Flik 1'" 
    [attr.af-id]="'flikb1'"
  >
    <p>Jag är flik 1</p>
  </digi-navigation-tab>
  <digi-navigation-tab 
    [attr.af-aria-label]="'Flik 2'" 
    [attr.af-id]="'flikb2'"
  >
    <p>Jag är flik 2</p>
  </digi-navigation-tab>
  <digi-navigation-tab 
    [attr.af-aria-label]="'Flik 3'" 
    [attr.af-id]="'flikb3'"
  >
    <p>Jag är flik 3</p>
  </digi-navigation-tab>
</digi-navigation-tabs>`
		};
	}

	example() {
		return (
			<digi-code-example
				af-code={JSON.stringify(this.navigationTabsCode)}
				af-hide-controls={this.afHideControls ? 'true' : 'false'}
				af-hide-code={this.afHideCode ? 'true' : 'false'}
			>
				<digi-navigation-tabs afAriaLabel="En tablist label">
					<digi-navigation-tab afAriaLabel="Flik 1" afId="flikb1">
						<p>Jag är flik 1</p>
					</digi-navigation-tab>
					<digi-navigation-tab afAriaLabel="Flik 2" afId="flikb2">
						<p>Jag är flik 2</p>
					</digi-navigation-tab>
					<digi-navigation-tab afAriaLabel="Flik 3" afId="flikb3">
						<p>Jag är flik 3</p>
					</digi-navigation-tab>
				</digi-navigation-tabs>
			</digi-code-example>
		);
	}

	render() {
		return (
			<ComponentDetails
				showOnlyExample={this.afShowOnlyExample}
				preamble="Flikfältskomponenten används för att navigera mellan olika vyer som är
			på samma nivå i hierarkin och som visas på samma sida. Flikar används
			för att underlätta för användaren att hitta till olika innehåll inom
			samma kontext. Flikar ska användas restriktivt."
				example={this.example()}
				description={
					<Fragment>
						<h3>Generellt</h3>
						Komponenten använder sig av{' '}
						<ComponentLink tag="digi-navigation-tab" text="flikkomponenten" /> för att
						skapa ett flikfält.
						<h3>Varianter</h3>
						<p>
							Flikfältet finns enbart i en variant. Genom att använda{' '}
							<digi-code af-code="af-aria-label" /> sätts attributet aria-label på
							flikkomponenten.
						</p>
						<h3>Förvald flik</h3>
						<p>
							<digi-code af-code="af-init-active-tab" /> sätter initial aktiv flik, om
							inget sätts första fliken. Till exempel{' '}
							<digi-code af-code="af-init-active-tab='1'" /> sätter den andra fliken
							till aktiv.
						</p>
					</Fragment>
				}
				guidelines={
					<digi-notification-detail>
						<h3 slot="heading">Bra att tänka på</h3>
						<ul>
							<li>En och endast en av flikarna ska alltid vara förvald.</li>
							<li>
								Presentera ordningen på flikarna så de följer den mest naturliga och
								logiska ordningen för användaren.
							</li>
							<li>Fliken ska ha tillräckligt stor klickyta.</li>
							<li>
								Använd inte flikar för att komprimera innehåll på exempelvis
								artikelsidor med många avsnitt. Då används rubriker och/eller
								expanderbara ytor
							</li>
							<li>
								Var restriktiv med att använda flikar, då det ofta inte är optimalt ur
								användarperspektiv. Fundera först noga igenom vilka andra
								lösningsalternativ du kan använda, innan du bestämmer dig för flikar.
							</li>
							<li>
								Det ska tydligt framgå vilken flik som är aktiverad (vald) och att det
								underliggande innehållet hör till vald flik.
							</li>
							<li>
								För en flik som inte är aktiverad ska det tydligt framgå att den är
								valbar och möjlig att gå till. Användaren ska vara medveten om att det
								finns fler val.
							</li>
							<li>
								Flikar används för att navigera mellan vyer som är relaterade till
								varandra. Använd korta namn på flikarna.
							</li>
							<li>
								Det ska tydligt framgå av fliknamnet vad användaren kan förvänta sig att
								hitta där.
							</li>
						</ul>
					</digi-notification-detail>
				}
			/>
		);
	}
}
