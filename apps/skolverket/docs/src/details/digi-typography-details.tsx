import { Component, Prop, h, State, Fragment } from '@stencil/core';
import { CodeExampleLanguage, TypographyVariation } from '@digi/skolverket';
import { ComponentDetails } from '@digi/skolverket-docs/components/ComponentDetails';

@Component({
	tag: 'digi-typography-details',
	scoped: true
})
export class DigiTypographyDetails {
	@Prop() afShowOnlyExample: boolean;
	@Prop() afHideControls: boolean;
	@Prop() afHideCode: boolean;
	@State() typographyVariation: TypographyVariation = TypographyVariation.SMALL;

	get typographyCode() {
		return {
			[CodeExampleLanguage.HTML]: `\
<digi-typography
  af-variation="${this.typographyVariation}"
> 
  <h1>Heading 1</h1>
  <h2>Heading 2</h2>
  <h3>Heading 3</h3>
  <h4>Heading 4</h4>
  <h5>Heading 5</h5>
  <h6>Heading 6</h6>
  <h1>Paragraph</h1>
  <p>
    Lorem ipsum dolor sit amet, test link adipiscing elit. Nullam dignissim convallis est. Quisque aliquam. Donec faucibus. Nunc iaculis suscipit dui. Nam sit amet sem. Aliquam libero nisi, imperdiet at, tincidunt nec, gravida vehicula, nisl. Praesent mattis, massa quis luctus fermentum, turpis mi volutpat justo, eu volutpat enim diam eget metus. Maecenas ornare tortor. Donec sed tellus eget sapien fringilla nonummy. Mauris a ante. Suspendisse quam sem, consequat at, commodo vitae, feugiat in, nunc. Morbi imperdiet augue quis tellus.
  </p>
</digi-typography>`,
			[CodeExampleLanguage.ANGULAR]: `\
<digi-typography
  [attr.af-variation]="TypographyVariation.${Object.keys(
			TypographyVariation
		).find((key) => TypographyVariation[key] === this.typographyVariation)}"
>
  <h1>Heading 1</h1>
  <h2>Heading 2</h2>
  <h3>Heading 3</h3>
  <h4>Heading 4</h4>
  <h5>Heading 5</h5>
  <h6>Heading 6</h6>
  <h1>Paragraph</h1>
  <p>
    Lorem ipsum dolor sit amet, test link adipiscing elit. Nullam dignissim convallis est. Quisque aliquam. Donec faucibus. Nunc iaculis suscipit dui. Nam sit amet sem. Aliquam libero nisi, imperdiet at, tincidunt nec, gravida vehicula, nisl. Praesent mattis, massa quis luctus fermentum, turpis mi volutpat justo, eu volutpat enim diam eget metus. Maecenas ornare tortor. Donec sed tellus eget sapien fringilla nonummy. Mauris a ante. Suspendisse quam sem, consequat at, commodo vitae, feugiat in, nunc. Morbi imperdiet augue quis tellus.
  </p>
</digi-typography>
`
		};
	}

	example() {
		return (
			<digi-code-example
				af-code={JSON.stringify(this.typographyCode)}
				af-hide-controls={this.afHideControls ? 'true' : 'false'}
				af-hide-code={this.afHideCode ? 'true' : 'false'}
			>
				<div class="slot__controls" slot="controls">
					<digi-form-fieldset
						afName="Storlek"
						afLegend="Storlek"
						onChange={(e) => (this.typographyVariation = (e.target as any).value)}
					>
						<digi-form-radiobutton
							afName="Storlek"
							afLabel="Liten"
							afValue={TypographyVariation.SMALL}
							afChecked={this.typographyVariation === TypographyVariation.SMALL}
						/>
						<digi-form-radiobutton
							afName="Storlek"
							afLabel="Stor"
							afValue={TypographyVariation.LARGE}
							afChecked={this.typographyVariation === TypographyVariation.LARGE}
						/>
					</digi-form-fieldset>
				</div>
				<digi-typography afVariation={this.typographyVariation}>
					<h1>Heading 1</h1>
					<h2>Heading 2</h2>
					<h3>Heading 3</h3>
					<h4>Heading 4</h4>
					<h5>Heading 5</h5>
					<h6>Heading 6</h6>
					<h1>Paragraph</h1>
					<p>
						Lorem ipsum dolor sit amet, test link adipiscing elit. Nullam dignissim
						convallis est. Quisque aliquam. Donec faucibus. Nunc iaculis suscipit dui.
						Nam sit amet sem. Aliquam libero nisi, imperdiet at, tincidunt nec,
						gravida vehicula, nisl. Praesent mattis, massa quis luctus fermentum,
						turpis mi volutpat justo, eu volutpat enim diam eget metus. Maecenas
						ornare tortor. Donec sed tellus eget sapien fringilla nonummy. Mauris a
						ante. Suspendisse quam sem, consequat at, commodo vitae, feugiat in, nunc.
						Morbi imperdiet augue quis tellus.
					</p>
				</digi-typography>
			</digi-code-example>
		);
	}

	render() {
		return (
			<ComponentDetails
				showOnlyExample={this.afShowOnlyExample}
				preamble="Omsluter text för att få korrekt typografi i enlighet med Skolverkets riktlinjer."
				example={this.example()}
				description={
					<Fragment>
						<h3>Storlekar</h3>
						<p>
							Komponenten finns i liten (s) och stor storlek (l) som ställs in med{' '}
							<digi-code af-code="af-variation" />. Standard är liten.
						</p>
						<digi-notification-detail>
							<h3 slot="heading">Hur påverkar komponenten sina barn?</h3>
							<p>
								Typografikomponenten har inte scopad css. Det vill säga, den kommer att
								sträcka sig ner och in i barnkomponenter och potentiellt läcka in. Var
								uppmärksam.
							</p>
						</digi-notification-detail>
					</Fragment>
				}
			/>
		);
	}
}
