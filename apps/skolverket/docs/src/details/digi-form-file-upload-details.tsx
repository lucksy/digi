import { Component, Prop, h } from '@stencil/core';
import { CodeExampleLanguage } from '@digi/skolverket';
import { ComponentDetails } from '@digi/skolverket-docs/components/ComponentDetails';

@Component({
	tag: 'digi-form-file-upload-details',
	scoped: true
})
export class DigiFormFileUploadDetails {
	@Prop() afShowOnlyExample: boolean;
	@Prop() afHideControls: boolean;
	@Prop() afHideCode: boolean;

	get formFileUploadCode() {
		return {
			[CodeExampleLanguage.HTML]: `\
<digi-form-file-upload 
	af-file-types="*"
></digi-form-file-upload>`,
			[CodeExampleLanguage.ANGULAR]: `\
<digi-form-file-upload 
	[attr.af-file-types]="*"
></digi-form-file-upload>`
		};
	}

	onUpload(e) {
		console.log(e.detail);
	}

	onRemove(e) {
		console.log(e.detail);
	}

	onCancel(e) {
		console.log(e.detail);
	}

	onRetry(e) {
		console.log(e.detail);
	}

	example() {
		return (
			<digi-code-example
				af-code={JSON.stringify(this.formFileUploadCode)}
				af-controls-position="end"
				af-hide-controls={this.afHideControls ? 'true' : 'false'}
				af-hide-code={this.afHideCode ? 'true' : 'false'}
			>
				<digi-form-file-upload
					onAfOnUploadFile={(e) => this.onUpload(e)}
					onAfOnCancelFile={(e) => this.onCancel(e)}
					onAfOnRemoveFile={(e) => this.onRemove(e)}
					onAfOnRetryFile={(e) => this.onRetry(e)}
					afFileTypes={'*'}
				></digi-form-file-upload>
			</digi-code-example>
		);
	}

	render() {
		return (
			<ComponentDetails
				showOnlyExample={this.afShowOnlyExample}
				preamble="Filuppladdare tillåter användare att välja eller dra en fil åt gången att ladda upp till en specifik plats."
				example={this.example()}
			/>
		);
	}
}
