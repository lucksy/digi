import { Component, Prop, h, State, Fragment } from '@stencil/core';
import {
	CodeExampleLanguage,
	FormTextareaValidation,
	FormTextareaVariation
} from '@digi/skolverket';
import { ComponentDetails } from '@digi/skolverket-docs/components/ComponentDetails';

@Component({
	tag: 'digi-form-textarea-details',
	scoped: true
})
export class DigiFormTextareaDetails {
	@Prop() afShowOnlyExample: boolean;
	@Prop() afHideControls: boolean;
	@Prop() afHideCode: boolean;
	@State() formTextareaVariation: FormTextareaVariation =
		FormTextareaVariation.MEDIUM;
	@State() formTextareaValidation: FormTextareaValidation =
		FormTextareaValidation.NEUTRAL;
	@State() hasDescription: boolean = false;

	get textareaCode() {
		return {
			[CodeExampleLanguage.HTML]: `\
       <digi-form-textarea
         af-label="Etikett"
         af-variation="${this.formTextareaVariation}"\ ${
				this.hasDescription
					? '\n         af-label-description="Beskrivande text"\t '
					: ''
			}
         af-validation="${this.formTextareaValidation}"\ 
      ${
							this.formTextareaValidation !== FormTextareaValidation.NEUTRAL
								? '   af-validation-text="Det här är ett valideringsmeddelande"\n\t   >'
								: ' >'
						}
       </digi-form-textarea>`,
			[CodeExampleLanguage.ANGULAR]: `\
       <digi-form-textarea
         [attr.af-label]="'Etikett'"
         [attr.af-variation]="FormTextareaVariation.${Object.keys(
										FormTextareaVariation
									).find(
										(key) => FormTextareaVariation[key] === this.formTextareaVariation
									)}"\ ${
				this.hasDescription
					? `\n         [attr.af-label-description]="'Beskrivande text'"\t `
					: ''
			}
         [attr.af-validation]="FormTextareaValidation.${Object.keys(
										FormTextareaValidation
									).find(
										(key) => FormTextareaValidation[key] === this.formTextareaValidation
									)}"
  ${
			this.formTextareaValidation !== FormTextareaValidation.NEUTRAL
				? `       [attr.af-validation-text]="'Det här är ett valideringsmeddelande'"\n\t   >`
				: ' \t   >'
		}
       </digi-form-textarea>`
		};
	}

	example() {
		return (
			<digi-code-example
				af-code={JSON.stringify(this.textareaCode)}
				af-hide-controls={this.afHideControls ? 'true' : 'false'}
				af-hide-code={this.afHideCode ? 'true' : 'false'}
			>
				<div style={{ width: '400px' }}>
					{this.formTextareaValidation !== FormTextareaValidation.NEUTRAL &&
						this.hasDescription && (
							<digi-form-textarea
								afLabel="Etikett"
								af-validation-text="Det här är ett valideringsmeddelande"
								af-variation={this.formTextareaVariation}
								af-validation={this.formTextareaValidation}
								af-label-description="Beskrivande text"
							></digi-form-textarea>
						)}
					{this.formTextareaValidation == FormTextareaValidation.NEUTRAL &&
						this.hasDescription && (
							<digi-form-textarea
								afLabel="Etikett"
								af-variation={this.formTextareaVariation}
								af-validation={this.formTextareaValidation}
								af-label-description="Beskrivande text"
							></digi-form-textarea>
						)}
					{this.formTextareaValidation !== FormTextareaValidation.NEUTRAL &&
						!this.hasDescription && (
							<digi-form-textarea
								afLabel="Etikett"
								af-validation-text="Det här är ett valideringsmeddelande"
								af-variation={this.formTextareaVariation}
								af-validation={this.formTextareaValidation}
							></digi-form-textarea>
						)}
					{this.formTextareaValidation == FormTextareaValidation.NEUTRAL &&
						!this.hasDescription && (
							<digi-form-textarea
								afLabel="Etikett"
								af-variation={this.formTextareaVariation}
								af-validation={this.formTextareaValidation}
							></digi-form-textarea>
						)}
				</div>
				{!this.afHideControls && (
					<div class="slot__controls" slot="controls">
						<digi-form-select
							afLabel="Storlek"
							onAfOnChange={(e) =>
								(this.formTextareaVariation = (e.target as any).value)
							}
							af-variation="small"
							af-value="medium"
						>
							<option value="small">Liten</option>
							<option value="medium">Mellan</option>
							<option value="large">Stor</option>
							<option value="auto">Auto</option>
						</digi-form-select>
						<digi-form-fieldset afLegend="Beskrivande text">
							<digi-form-checkbox
								afLabel="Ja"
								afChecked={this.hasDescription}
								onAfOnChange={() =>
									this.hasDescription
										? (this.hasDescription = false)
										: (this.hasDescription = true)
								}
							></digi-form-checkbox>
						</digi-form-fieldset>
						<digi-form-select
							afLabel="Validering"
							onAfOnChange={(e) =>
								(this.formTextareaValidation = (e.target as any).value)
							}
							af-variation="small"
						>
							<option value="neutral">Ingen</option>
							<option value="error">Felaktig</option>
							<option value="success">Godkänd</option>
							<option value="warning">Varning</option>
						</digi-form-select>
					</div>
				)}
			</digi-code-example>
		);
	}

	render() {
		return (
			<ComponentDetails
				showOnlyExample={this.afShowOnlyExample}
				preamble="Textarean används för att låta användare ange en mängd text som är
			längre än en rad."
				example={this.example()}
				description={
					<Fragment>
						<p>
							Textarean innehåller en obligatorisk etikett med en valfri beskrivning
							och validering i olika varianter. Med attributet{' '}
							<digi-code af-code="af-label" /> sätts etiketten på textarean och med{' '}
							<digi-code af-code="af-label-description" /> går det att sätta en
							beskrivningstexten.
						</p>
						<h3>Storlekar</h3>
						<p>
							Textarean finns i storlekarna liten, mellan (förvald), stor och auto. Den
							sätts med attributet <digi-code af-code="af-variation" /> .
						</p>
						<h3>Validering</h3>
						<p>
							Textarean finns i fyra olika valideringstyper: ingen (förvald), felaktig,
							godkänd, varning. Den ändras med attributet{' '}
							<digi-code af-code="af-validation" /> och{' '}
							<digi-code af-code="af-validation-text" /> ger textarean en
							valideringsmeddelandetext. Meddelandetexten visas enbart vid elementets
							första inmatning (onInput) eller oskärpa (onBlur).
						</p>
					</Fragment>
				}
			/>
		);
	}
}
