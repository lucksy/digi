import { dirname, join } from "path";
const path = require('path');
module.exports = {
  stories: ['../src/**/*.stories.@(ts|tsx)'],
  addons: [
    {
      name: '@storybook/addon-essentials'
    }, 
    getAbsolutePath("@storybook/addon-knobs"), 
    getAbsolutePath("@pxtrn/storybook-addon-docs-stencil")
  ],
  webpackFinal: async config => {
    config.module.rules.push({
      test: /\.scss$/,
      use: ['style-loader', 'css-loader', 'sass-loader'],
      include: path.resolve(__dirname, '../')
    });
    return config;
  },
  framework: {
    name: getAbsolutePath("@storybook/html-webpack5"),
    options: {}
  },
  docs: {
    autodocs: true
  },
  staticDirs: [
    "../../../../dist/libs/skolverket/dist", 
    "../../../../libs/skolverket/fonts/src"
  ]
};
/**
 * This function is used to resolve the absolute path of a package.
 * It is needed in projects that use Yarn PnP or are set up within a monorepo.
*/
function getAbsolutePath(value) {
  return dirname(require.resolve(join(value, "package.json")));
}