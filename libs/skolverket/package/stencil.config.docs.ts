import { Config } from '@stencil/core';

console.log('busting cache 2303282');

export const config: Config = {
	namespace: 'digi-skolverket',
	taskQueue: 'async',
	outputTargets: [
		{
			type: 'docs-json',
			file: '../../../dist/libs/skolverket/docs.json'
		}
	]
};
