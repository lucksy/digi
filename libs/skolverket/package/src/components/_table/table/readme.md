# digi-table

<!-- Auto Generated Below -->


## Properties

| Property      | Attribute      | Description | Type                                                 | Default                  |
| ------------- | -------------- | ----------- | ---------------------------------------------------- | ------------------------ |
| `afVariation` | `af-variation` |             | `TableVariation.PRIMARY \| TableVariation.SECONDARY` | `TableVariation.PRIMARY` |


## Slots

| Slot        | Description                     |
| ----------- | ------------------------------- |
| `"default"` | Ska innehålla ett table-element |


## CSS Custom Properties

| Name                                 | Description                                            |
| ------------------------------------ | ------------------------------------------------------ |
| `--digi--table--background`          | var(--digi--color--background--neutral-6);             |
| `--digi--table--background--active`  | var(--digi--color--background--complementary-1);       |
| `--digi--table--background--even`    | var(--digi--color--background--secondary);             |
| `--digi--table--cell--margin`        | var(--digi--gutter--small);                            |
| `--digi--table--cell--padding--x`    | var(--digi--gutter--larger);                           |
| `--digi--table--cell--padding--x--s` | var(--digi--gutter--small);                            |
| `--digi--table--cell--padding--y`    | var(--digi--gutter--larger);                           |
| `--digi--table--cell--padding--y--s` | var(--digi--gutter--small);                            |
| `--digi--table--color`               | var(--digi--color--text--primary);                     |
| `--digi--table--color--active`       | var(--digi--color--text--inverted);                    |
| `--digi--table--font-family`         | var(--digi--global--typography--font-family--default); |
| `--digi--table--font-size`           | var(--digi--typography--body--font-size--desktop);     |
| `--digi--table--font-weight`         | var(--digi--typography--body--font-weight--desktop);   |
| `--digi--table--td--border-color`    | var(--digi--color--border--neutral-2);                 |
| `--digi--table--th--border-color`    | var(--digi--color--border--primary);                   |


## Dependencies

### Depends on

- [digi-card-box](../../_card/card-box)

### Graph
```mermaid
graph TD;
  digi-table --> digi-card-box
  style digi-table fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
