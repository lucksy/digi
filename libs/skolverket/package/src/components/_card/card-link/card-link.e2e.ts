import { newE2EPage } from '@stencil/core/testing';

describe('digi-card-link', () => {
	it('renders', async () => {
		const page = await newE2EPage();
		await page.setContent('<digi-card-link></digi-card-link>');

		const element = await page.find('digi-card-link');
		expect(element).toHaveClass('hydrated');
	});
});
