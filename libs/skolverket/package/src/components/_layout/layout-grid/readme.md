# digi-layout-grid

<!-- Auto Generated Below -->


## Properties

| Property            | Attribute             | Description | Type                  | Default                             |
| ------------------- | --------------------- | ----------- | --------------------- | ----------------------------------- |
| `afVerticalSpacing` | `af-vertical-spacing` |             | `"none" \| "regular"` | `LayoutGridVerticalSpacing.REGULAR` |


## Slots

| Slot        | Description                 |
| ----------- | --------------------------- |
| `"default"` | kan innehålla vad som helst |


## Dependencies

### Used by

 - [digi-page-block-sidebar](../../_page/page-block-sidebar)
 - [digi-page-footer](../../_page/page-footer)

### Depends on

- [digi-layout-container](../../../__core/_layout/layout-container)

### Graph
```mermaid
graph TD;
  digi-layout-grid --> digi-layout-container
  digi-page-block-sidebar --> digi-layout-grid
  digi-page-footer --> digi-layout-grid
  style digi-layout-grid fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
