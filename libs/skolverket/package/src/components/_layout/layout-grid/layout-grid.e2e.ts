import { newE2EPage } from '@stencil/core/testing';

describe('digi-layout-grid', () => {
	it('renders', async () => {
		const page = await newE2EPage();
		await page.setContent('<digi-layout-grid></digi-layout-grid>');

		const element = await page.find('digi-layout-grid');
		expect(element).toHaveClass('hydrated');
	});
});
