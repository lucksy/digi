import { _t } from '@digi/skolverket/text';
import {
	Component,
	Event,
	EventEmitter,
	h,
	Method,
	Prop,
	Watch
} from '@stencil/core';
import { CardBoxGutter } from '../../../enums-skolverket';

/**
 * @slot default - Kan innehålla vad som helst
 * @slot heading - Rubrik
 * @slot actions - Knappar
 *
 * @swedishName Modal
 */
@Component({
	tag: 'digi-dialog',
	styleUrl: 'dialog.scss',
	scoped: true
})
export class Dialog {
	private _dialog: HTMLDialogElement;

	@Prop() afOpen: boolean = false;
	@Watch('afOpen')
	afOpenChanged(newVal, oldVal) {
		if (newVal === oldVal) return;
		this.afOpen ? this.showModal() : this.close();
		this.toggleEmitter(this.afOpen ? 'open' : 'close');
	}

	@Prop() afHideCloseButton: boolean = false;

	@Method()
	async showModal() {
		this._dialog.showModal();
		this.afOpen = true;
		this.toggleEmitter('open');
	}

	@Method()
	async close() {
		this._dialog.close();
		this.afOpen = false;
		this.toggleEmitter('close');
	}

	@Event() afOnOpen: EventEmitter;
	@Event() afOnClose: EventEmitter;

	toggleEmitter(state: 'open' | 'close') {
		state === 'open' ? this.afOnOpen.emit() : this.afOnClose.emit();
	}

	clickOutsideHandler(e) {
		if (e.detail.target !== this._dialog) return;
		this.close();
	}

	componentDidLoad() {
		this.afOpen && this.showModal();
	}

	render() {
		return (
			<dialog
				class="digi-dialog"
				ref={(el) => {
					this._dialog = el;
				}}
				onClose={() => this.close()}
			>
				<digi-util-detect-click-outside
					onAfOnClickOutside={(e) => this.clickOutsideHandler(e)}
				>
					<digi-card-box afGutter={CardBoxGutter.NONE}>
						<digi-typography>
							<div class="digi-dialog__inner">
								<header class="digi-dialog__header">
									<slot name="heading" />
								</header>
								<div class="digi-dialog__content">
									<slot></slot>
								</div>
								<div class="digi-dialog__actions">
									<slot name="actions" />
								</div>
								{!this.afHideCloseButton && (
									<div class="digi-dialog__close-button-wrapper">
										<button
											class="digi-dialog__close-button"
											onClick={() => this.close()}
											type="button"
										>
											{_t.close} <digi-icon aria-hidden="true" afName={`x`} />
										</button>
									</div>
								)}
							</div>
						</digi-typography>
					</digi-card-box>
				</digi-util-detect-click-outside>
			</dialog>
		);
	}
}
