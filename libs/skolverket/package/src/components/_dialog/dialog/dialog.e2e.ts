import { newE2EPage } from '@stencil/core/testing';

describe('dialog', () => {
	it('renders', async () => {
		const page = await newE2EPage();

		await page.setContent('<dialog></dialog>');
		const element = await page.find('dialog');
		expect(element).toHaveClass('hydrated');
	});
});
