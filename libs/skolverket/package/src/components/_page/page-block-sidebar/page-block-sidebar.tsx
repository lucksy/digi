import { Component, h, Prop } from '@stencil/core';
import { PageBlockSidebarVariation } from './page-block-sidebar-variation.enum';

/**
 * @slot default
 * @slot sidebar
 *
 * @swedishName Sidblock med sidebar
 */
@Component({
	tag: 'digi-page-block-sidebar',
	styleUrls: ['page-block-sidebar.scss'],
	scoped: true,
	assetsDirs: ['public']
})
export class PageBlockSidebar {
	@Prop() afVariation: `${PageBlockSidebarVariation}`;

	get cssModifiers() {
		return {
			[`digi-page-block-sidebar--variation-${this.afVariation}`]:
				!!this.afVariation
		};
	}

	render() {
		return (
			<digi-layout-grid
				class={{ 'digi-page-block-sidebar': true, ...this.cssModifiers }}
			>
				<div class="digi-page-block-sidebar__sidebar">
					<slot name="sidebar"></slot>
				</div>
				<div class="digi-page-block-sidebar__content">
					<slot></slot>
				</div>
			</digi-layout-grid>
		);
	}
}
