import { enumSelect, Template } from '../../../../../../shared/utils/src';
import { PageBlockHeroVariation } from './page-block-hero-variation.enum';
import { PageBlockHeroBackground } from './page-block-hero-background.enum';

export default {
	title: 'page/digi-page-block-hero',
	argTypes: {
		'af-variation': enumSelect(PageBlockHeroVariation),
		'af-background': enumSelect(PageBlockHeroBackground)
	}
};

export const Standard = Template.bind({});
Standard.args = {
	component: 'digi-page-block-hero',
	'af-variation': 'start',
	'af-background-image': '',
	/* html */
	children: `
    <h1 slot="heading">Välkommen till Skolverket</h1>
    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi vitae nisl vitae augue.</p>
  `
};

export const SectionWithSubnav = Template.bind({});
SectionWithSubnav.args = {
	component: 'digi-page-block-hero',
	'af-variation': PageBlockHeroVariation.SECTION,
	/* html */
	children: `
    <h1 slot="heading">Sektion</h1>
    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi vitae nisl vitae augue.</p>
    <digi-list-link>
      <li><a href="#">Undersida 1</a></li>
      <li><a href="#">Undersida 2</a></li>
      <li><a href="#">Undersida 3</a></li>
      <li><a href="#">Undersida 4</a></li>
    </digi-list-link>
  `
};
