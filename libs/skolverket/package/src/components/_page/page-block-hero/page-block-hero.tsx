import { Component, h, Prop } from '@stencil/core';
import { PageBlockHeroBackground } from './page-block-hero-background.enum';
import { PageBlockHeroVariation } from './page-block-hero-variation.enum';

/**
 * @slot default
 * @slot heading - Rubrik
 *
 * @swedishName Sidblock med hero
 */
@Component({
	tag: 'digi-page-block-hero',
	styleUrls: ['page-block-hero.scss'],
	scoped: true,
	assetsDirs: ['public']
})
export class PageBlockHero {
	@Prop() afVariation: `${PageBlockHeroVariation}`;
	@Prop() afBackground: `${PageBlockHeroBackground}`;
	@Prop() afBackgroundImage: string;

	get cssModifiers() {
		return {
			[`digi-page-block-hero--variation-${this.afVariation}`]: !!this.afVariation,
			[`digi-page-block-hero--background-${this.afBackground}`]:
				!!this.afBackground,
			'digi-page-block-hero--background-image': !!this.afBackgroundImage
		};
	}

	render() {
		return (
			<digi-layout-container
				class={{ 'digi-page-block-hero': true, ...this.cssModifiers }}
				style={{
					'--digi--page-block-hero--background-image': this.afBackgroundImage
						? `url('${this.afBackgroundImage}')`
						: null
				}}
			>
				<digi-typography>
					<div class="digi-page-block-hero__inner">
						<div class="digi-page-block-hero__heading">
							<slot name="heading"></slot>
						</div>
						<div class="digi-page-block-hero__content">
							<slot></slot>
						</div>
					</div>
				</digi-typography>
			</digi-layout-container>
		);
	}
}
