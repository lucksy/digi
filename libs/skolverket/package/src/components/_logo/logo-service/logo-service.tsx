import { Component, Prop, h } from '@stencil/core';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';

/**
 * @swedishName Logotyp tjänster & verktyg
 */
@Component({
	tag: 'digi-logo-service',
	styleUrls: ['logo-service.scss'],
	scoped: true
})
export class LogoService {
	/**
	 * Tjänsten eller verktygets namn
	 * @en Name of the service or tool.
	 */
	@Prop() afName!: string;

	/**
	 * Tjänsten eller verktygets beskrivning
	 * @en Description of the service or tool.
	 */
	@Prop() afDescription!: string;

	/**
	 * Sätter attributet 'id' på namntextens element.
	 * Om inget väljs så skapas ett slumpmässigt id.
	 * @en Id attribute on the name element. Defaults to random string.
	 */
	@Prop() afNameId: string = randomIdGenerator('digi-logo-service__title');

	render() {
		return (
			<div class="digi-logo-service">
				<svg
					class="digi-logo-service__svg"
					xmlns="http://www.w3.org/2000/svg"
					width="81"
					height="81"
					viewBox="0 0 81 81"
				>
					<circle cx="40.5" cy="40.5" r="40.5" fill="currentColor" />
					<path
						d="M57.025 18.717c-1.42-1.159-4.431-1.885-7.979-1.885a22.571 22.571 0 0 0-12.715 3.929c-4.5 2.956-6.272 6.273-6.2 10.209.076 4.482 3.6 7.625 9.513 10.307 4.5 1.973 10.265 4.377 10.265 10.107s-4 9.057-11.447 9.057c-5.558 0-10.512-2.345-11.945-5.819a6.236 6.236 0 0 1-.707-2.776c.237-.536-.711-.628-1.3 0-.47.626-1.773 2.051-2.715 3.133a3.038 3.038 0 0 0-1.313 2.238c0 1.345 1.066 2.863 2.841 4.03 2.128 1.518 6.6 2.622 11.1 2.622a26.9 26.9 0 0 0 12.33-3.384c5.086-2.675 9.8-6.331 9.725-11.618-.078-5.57-6.267-9.368-10.76-11.42-5.2-2.331-9.886-4.976-9.886-10.077 0-4.747 4.052-7.556 8.781-7.485 6.167.085 8.987 2.414 8.278 5.634-.237 1.171 1.064 1.345 1.771.541 1.183-1.252 1.776-1.788 2.724-2.775a2.932 2.932 0 0 0 1.061-1.968 3.508 3.508 0 0 0-1.422-2.6Z"
						fill="#fff"
					/>
				</svg>
				<div class="digi-logo-service__text">
					<p class="digi-logo-service__name">{this.afName}</p>
					<span class="digi-logo-service__description">{this.afDescription}</span>
				</div>
			</div>
		);
	}
}
