# digi-logo-sister

<!-- Auto Generated Below -->


## Properties

| Property              | Attribute    | Description                                                                                  | Type     | Default                                        |
| --------------------- | ------------ | -------------------------------------------------------------------------------------------- | -------- | ---------------------------------------------- |
| `afName` _(required)_ | `af-name`    | Syskonwebbplatsens namn                                                                      | `string` | `undefined`                                    |
| `afNameId`            | `af-name-id` | Sätter attributet 'id' på namntextens element. Om inget väljs så skapas ett slumpmässigt id. | `string` | `randomIdGenerator('digi-logo-sister__title')` |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
