import { newE2EPage } from '@stencil/core/testing';

describe('digi-logo-sister', () => {
	it('renders', async () => {
		const page = await newE2EPage();
		await page.setContent('<digi-logo-sister></digi-logo-sister>');

		const element = await page.find('digi-logo-sister');
		expect(element).toHaveClass('hydrated');
	});
});
