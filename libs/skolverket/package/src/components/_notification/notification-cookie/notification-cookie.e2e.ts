import { newE2EPage } from '@stencil/core/testing';

describe('digi-notification-cookie', () => {
	it('renders', async () => {
		const page = await newE2EPage();
		await page.setContent(
			'<digi-notification-cookie></digi-notification-cookie>'
		);

		const element = await page.find('digi-notification-cookie');
		expect(element).toHaveClass('hydrated');
	});
});
