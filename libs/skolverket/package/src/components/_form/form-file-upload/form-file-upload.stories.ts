import { Template } from '../../../../../../shared/utils/src';

export default {
	title: 'form/digi-form-file-upload'
};

export const Standard = Template.bind({});
Standard.args = {
	component: 'digi-form-file-upload',
	'af-label': 'Ladda upp fil',
	'af-file-types': '*'
};
