import { Component, Element, Event, EventEmitter, h } from '@stencil/core';
import {
	HTMLStencilElement,
	Listen,
	State,
	Watch
} from '@stencil/core/internal';
import { ButtonVariation } from '../../../enums-core';
import { BREAKPOINT_LARGE } from '../../../design-tokens/web/tokens.es6';
import { _t } from '@digi/skolverket/text';

/**
 * @slot default - Se översikten för exakt innehåll
 * @slot main-link - Länk ovanför navigationsraderna
 *
 *@swedishName Huvudmenyspanel
 */
@Component({
	tag: 'digi-navigation-main-menu-panel',
	styleUrls: ['navigation-main-menu-panel.scss'],
	scoped: true
})
export class NavigationMainMenuPanel {
	panel!: HTMLDivElement;

	@Element() hostElement: HTMLStencilElement;

	@State() nestedListItems: HTMLUListElement[] = [];
	@State() panelOverflowHeight: number;

	@Watch('nestedListItems')
	nestedListItemsChanged() {
		this.hydrateListItems();
	}

	/**
	 * När komponenten ändrar storlek
	 * @en When the component changes size
	 */
	@Event() afOnResize: EventEmitter<ResizeObserverEntry>;

	/**
	 * När komponenten stängs
	 * @en When the component changes size
	 */
	@Event() afOnClose: EventEmitter;

	setInnerSize(reset = false) {
		if (reset) {
			this.panelOverflowHeight = null;
			return;
		}

		if (this.panel.scrollHeight > this.panel.offsetHeight) {
			this.panelOverflowHeight = this.panel.scrollHeight;
		}
	}

	resizeHandler(e) {
		this.setInnerSize();
		this.afOnResize.emit(e);
	}

	closeHandler(e) {
		this.afOnClose.emit(e);
	}

	componentWillLoad() {
		this.getAllListItems();
		this.getNestedListItems();
	}

	getAllListItems() {
		const allListItems = this.hostElement.querySelectorAll(
			'li'
		) as NodeListOf<HTMLLIElement>;

		this.removeCurrentListItem(allListItems);
		this.setCurrentListItem(allListItems);
	}

	removeCurrentListItem(listItems: NodeListOf<HTMLLIElement>) {
		listItems.length &&
			Array.from(listItems).forEach((item) => {
				item.removeAttribute('data-current');
				item.removeAttribute('data-has-current-exact');
				item.removeAttribute('data-visible-current-exact');
			});
	}

	setCurrentListItem(listItems: NodeListOf<HTMLLIElement>) {
		listItems.length &&
			Array.from(listItems).forEach((item) => {
				// If list item is exactly the current page
				if (!!item.querySelector(':scope > a[aria-current="page"]')) {
					item.setAttribute('data-current', 'exact');
					item.closest('ul').setAttribute('data-has-current-exact', 'true');
					this.hostElement.setAttribute('data-visible-current-exact', 'true');
					return;
				}

				// If list item has a nested current page
				!!item.querySelector('[aria-current="page"]') &&
					item.setAttribute('data-current', 'nested');
			});
	}

	getNestedListItems() {
		const nestedListItems = this.hostElement.querySelectorAll(
			'digi-navigation-main-menu-panel > ul > li ul'
		) as NodeListOf<HTMLUListElement>;

		if (nestedListItems.length < 0) return;

		this.nestedListItems = Array.from(nestedListItems);
	}

	@Listen('click')
	toggleButtonClickHandler(e: MouseEvent) {
		const target = e.target as HTMLElement;

		if (
			!target.classList.contains('digi-navigation-main-menu-panel__toggle-button')
		)
			return;

		this.setExpansionState(target);
	}

	@Listen('click')
	linkClickHandler(e: MouseEvent) {
		const target = e.target as HTMLElement;
		if (target.tagName !== 'A') return;
		this.closeHandler(e);
	}

	setExpansionState(target) {
		const isExpanded = target.getAttribute('aria-expanded') === 'true';
		const closestLi = target.closest('li');
		target.setAttribute('aria-expanded', isExpanded ? 'false' : 'true');
		closestLi.setAttribute('data-expanded', !isExpanded);

		if (isExpanded) {
			closestLi.querySelectorAll('[data-expanded]').forEach((el) => {
				el.setAttribute('data-expanded', 'false');
			});

			closestLi.querySelectorAll('[aria-expanded="true"]').forEach((el) => {
				el.setAttribute('aria-expanded', 'false');
			});
		}

		const currentPage = this.hostElement.querySelector('[aria-current="page"]');
		if (currentPage) {
			this.hostElement.setAttribute(
				'data-visible-current-exact',
				window.getComputedStyle(currentPage.closest('ul')).display !== 'none'
					? 'true'
					: 'false'
			);
		}

		if (window.matchMedia(`(min-width: ${BREAKPOINT_LARGE})`).matches) {
			const buttons = this.hostElement.querySelectorAll(
				'.digi-navigation-main-menu-panel__toggle-button'
			);

			Array.from(buttons).forEach((button) => {
				if (button === target) return;

				button.setAttribute('aria-expanded', 'false');
				button.closest('li').removeAttribute('data-expanded');
			});
		}

		this.setInnerSize(isExpanded);
	}

	hydrateListItems() {
		this.nestedListItems.forEach((nestedListItem, i) => {
			const id = `${this.hostElement.id}-subnav-${i}`;
			nestedListItem.setAttribute('id', id);
			const button = document.createElement('button');
			button.setAttribute(
				'aria-expanded',
				nestedListItem.closest('[data-current="nested"]') ? 'true' : 'false'
			);
			button.setAttribute('aria-controls', id);
			button.setAttribute('aria-label', 'Visa och dölj undermeny');
			button.classList.add('digi-navigation-main-menu-panel__toggle-button');
			button.innerHTML = `
				<digi-icon af-name="plus" aria-hidden="true"></digi-icon>
				<digi-icon af-name="minus" aria-hidden="true"></digi-icon>
				<digi-icon af-name="chevron-right" aria-hidden="true"></digi-icon>
				<digi-icon af-name="chevron-left" aria-hidden="true"></digi-icon>`;
			nestedListItem.parentNode.insertBefore(button, nestedListItem);
		});
	}

	@Listen('keyup')
	keyUpHandler(e: KeyboardEvent) {
		if (
			e.key !== 'Escape' ||
			window.matchMedia(`(min-width: ${BREAKPOINT_LARGE})`).matches
		)
			return;
		this.afOnClose.emit();
	}

	mutationHandler() {
		console.log('hello');
		this.getAllListItems();
		this.getNestedListItems();
	}

	render() {
		return (
			<digi-util-resize-observer
				onAfOnChange={(e) => this.resizeHandler(e.detail)}
			>
				<digi-layout-container>
					<div
						class="digi-navigation-main-menu-panel"
						style={{ '--panel-overflow-height': `${this.panelOverflowHeight}px` }}
						ref={(el) => (this.panel = el as HTMLDivElement)}
					>
						<div class="digi-navigation-main-menu-panel__main-link" part="main-link">
							<slot name="main-link"></slot>
						</div>
						<digi-util-mutation-observer
							class="digi-navigation-main-menu-panel__sub-nav"
							onAfOnChange={() => this.mutationHandler()}
							afOptions={{
								childList: true,
								subtree: true,
								attributeFilter: ['aria-current']
							}}
						>
							<slot></slot>
						</digi-util-mutation-observer>
						<div class="digi-navigation-main-menu-panel__close-button-wrapper">
							<digi-button
								afVariation={ButtonVariation.FUNCTION}
								class="digi-navigation-main-menu-panel__close-button"
								onAfOnClick={(e) => this.closeHandler(e.detail)}
								aria-controls={this.hostElement.id}
								aria-expanded="true"
							>
								<digi-icon
									data-viewport="small"
									afName={'chevron-left'}
									aria-hidden="true"
									slot="icon"
								/>
								<digi-icon
									data-viewport="large"
									afName={'x'}
									aria-hidden="true"
									slot="icon-secondary"
								/>
								<span data-viewport="small">{_t.back}</span>
								<span data-viewport="large">{_t.close}</span>
							</digi-button>
						</div>
					</div>
				</digi-layout-container>
			</digi-util-resize-observer>
		);
	}
}
