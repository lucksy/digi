import { enumSelect, Template } from '../../../../../../shared/utils/src';

export default {
	title: 'navigation/digi-navigation-tab',
	parameters: {
		actions: {
			handles: ['afOnToggle']
		}
	}
};

export const Standard = Template.bind({});
Standard.args = {
	component: 'digi-navigation-tab',
	'af-aria-label': 'An aria label is required',
	'af-id': null,
	'af-active': true,
	/* html */
	children:
		'You cannot see me, because I only work together with digi-navigation-tabs'
};
