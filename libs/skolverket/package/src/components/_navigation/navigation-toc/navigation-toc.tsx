import { Component, Element, h, Listen, State, Watch } from '@stencil/core';
import { HTMLStencilElement } from '@stencil/core/internal';

/**
 * @slot default - Ska inehålla li-element med a-element inuti.
 * @slot heading - Rubrik
 * @swedishName Innehållsförteckning
 */
@Component({
	tag: 'digi-navigation-toc',
	styleUrls: ['navigation-toc.scss'],
	scoped: true
})
export class NavigationToc {
	@Element() hostElement: HTMLStencilElement;

	@State() currentHash: string;
	@Watch('currentHash')
	currentHashChanged() {
		this.setAriaCurrent();
	}

	componentWillLoad() {
		this.setHash();
	}

	@Listen('hashchange', { target: 'window' })
	setHash() {
		this.currentHash = window?.location?.hash || '';
	}

	setAriaCurrent() {
		const links = this.hostElement.querySelectorAll('a');
		links.forEach((link) => {
			if (link.hash === this.currentHash) {
				link.setAttribute('aria-current', 'true');
			} else {
				link.removeAttribute('aria-current');
			}
		});
	}

	render() {
		return (
			<div
				class={{
					'digi-navigation-toc': true
				}}
			>
				<div class="digi-navigation-toc__heading">
					<slot name="heading" />
				</div>
				<digi-typography>
					<ul class="digi-navigation-toc__items">
						<slot />
					</ul>
				</digi-typography>
			</div>
		);
	}
}
