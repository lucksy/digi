import { Template } from '../../../../../../shared/utils/src';

export default {
	title: 'navigation/digi-navigation-toc'
};

export const Standard = Template.bind({});
Standard.args = {
	component: 'digi-navigation-toc',
	/* html */
	children: `
		<h2 slot="heading">Innehåll på denna sida</h2>
    <li><a href="#oversikt">Översikt</a></li>
    <li><a href="#kontakt">Kontakt</a></li>
    <li><a href="#hitta_hit">Hitta hit</a></li>
		`
};
