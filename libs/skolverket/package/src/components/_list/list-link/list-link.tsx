import { Component, Element, h, Prop, State } from '@stencil/core';
import { HTMLStencilElement } from '@stencil/core/internal';
import { TypographyVariation } from '../../../enums-core';
import { ListLinkLayout } from './list-link-layout.enum';
import { ListLinkType } from './list-link-type.enum';
import { ListLinkVariation } from './list-link-variation.enum';

/**
 * @slot default - ska innehålla li-element med a-element inuti.
 * @slot heading - Rubrik
 *
 * @swedishName Länklista
 */
@Component({
	tag: 'digi-list-link',
	styleUrls: ['list-link.scss'],
	scoped: true
})
export class ListLink {
	@Element() hostElement: HTMLStencilElement;
	@State() hasHeading: boolean;
	@Prop() afType: ListLinkType = ListLinkType.UNORDERED;
	@Prop() afLayout: ListLinkLayout = ListLinkLayout.BLOCK;
	@Prop() afVariation: ListLinkVariation = ListLinkVariation.REGULAR;

	componentWillLoad() {
		this.setHasHeading();
	}

	componentWillUpdate() {
		this.setHasHeading();
	}

	setHasHeading() {
		this.hasHeading = !!this.hostElement.querySelector('[slot="heading"]');
	}

	get cssModifiers() {
		return {
			[`digi-list-link--type-${this.afType}`]: !!this.afType,
			[`digi-list-link--layout-${this.afLayout}`]: !!this.afLayout,
			[`digi-list-link--variation-${this.afVariation}`]: !!this.afVariation
		};
	}

	render() {
		return (
			<digi-typography
				class={{
					'digi-list-link': true,
					...this.cssModifiers
				}}
				afVariation={TypographyVariation.SMALL}
			>
				<div class="digi-list-link__content">
					{this.hasHeading && (
						<div class="digi-list-link__heading">
							<slot name="heading"></slot>
						</div>
					)}
					<this.afType class="digi-list-link__list">
						<slot></slot>
					</this.afType>
				</div>
			</digi-typography>
		);
	}
}
