import { newE2EPage } from '@stencil/core/testing';

describe('digi-typography-heading-section', () => {
	it('renders', async () => {
		const page = await newE2EPage();
		await page.setContent(
			'<digi-typography-heading-section></digi-typography-heading-section>'
		);

		const element = await page.find('digi-typography-heading-section');
		expect(element).toHaveClass('hydrated');
	});
});
