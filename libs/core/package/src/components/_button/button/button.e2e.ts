import { newE2EPage } from '@stencil/core/testing';

describe('digi-button', () => {
  it('renders', async () => {
    const page = await newE2EPage();
    await page.setContent('<digi-button></digi-button>');

    const element = await page.find('digi-button');
    expect(element).toHaveClass('hydrated');
  });
});
