import { newSpecPage } from '@stencil/core/testing';
import { ExpandableAccordion } from './expandable-accordion';

describe('digi-expandable-accordion', () => {
  it('renders', async () => {
    const { root } = await newSpecPage({
      components: [ExpandableAccordion],
      html: '<digi-expandable-accordion></digi-expandable-accordion>'
    });
    expect(root).toEqualHtml(`
      <digi-expandable-accordion>
        <mock:shadow-root>
          <div>
            Hello, World! I'm
          </div>
        </mock:shadow-root>
      </digi-expandable-accordion>
    `);
  });

  it('renders with values', async () => {
    const { root } = await newSpecPage({
      components: [ExpandableAccordion],
      html: `<digi-expandable-accordion first="Stencil" last="'Don't call me a framework' JS"></digi-expandable-accordion>`
    });
    expect(root).toEqualHtml(`
      <digi-expandable-accordion first="Stencil" last="'Don't call me a framework' JS">
        <mock:shadow-root>
          <div>
            Hello, World! I'm Stencil 'Don't call me a framework' JS
          </div>
        </mock:shadow-root>
      </digi-expandable-accordion>
    `);
  });
});
