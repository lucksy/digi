import {
  Component,
  Event,
  EventEmitter,
  Prop,
  State,
  h,
  Watch,
} from '@stencil/core';
/**
 * 
 * @swedishName Mediebild
 */
@Component({
  tag: 'digi-media-image',
  styleUrls: ['media-image.scss'],
  scoped: true,
})
export class MediaImage {
  isPlaceholderLoaded = false;
  hasSetSrc = false;
  hasAspectRatio = false;
  paddedBoxPadding = '0%';

  // Need to be a state to trigger rerender of css-classes
  @State() isLoaded = false;

  // Default src to 1*1px placeholder image.
  // TODO: DO NOT USE BASE64, UNSAFE!
  @State() src =
    'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR42mNk+A8AAQUBAScY42YAAAAASUVORK5CYII=';

  @State() srcset: string;

  /**
   * Skicka options till komponentens interna Intersection Observer. T.ex. för att kontrollera när bilden lazy loadas.
   * @en Accepts an Intersection Observer options object. Controls when the image should lazy-load.
   * 

   */
  @Prop() afObserverOptions = {
    rootMargin: '200px',
    threshold: 0,
  };

  /**
   * Sätter attributet 'src'.
   * @en Set `src` attribute.
   */
  @Prop() afSrc: string;

  /**
   * Sätter attributet 'srcset'.
   * @en Set `srcset` attribute.
   */
  @Prop() afSrcset: string;

  /**
   * Sätter attributet 'alt'.
   * @en Set `alt` attribute.
   */
  @Prop() afAlt!: string;

  /**
   * Sätter attributet 'title'.
   * @en Set `title` attribute.
   */
  @Prop() afTitle: string;

  /**
   * Sätter attributet 'aria-label'.
   * @en Set `aria-label` attribute.
   */
  @Prop() afAriaLabel: string;

  /**
   * Sätter attributet 'width'. Om både 'width' och 'height' är satta så kommer komponenten att ha rätt aspektratio även innan bilden laddats in.
   * @en Set `width` attribute. if both 'width' and 'height' are set, the component will have the correct apect ratio even before the image source is loaded.
   */
  @Prop() afWidth: string;

  /**
   * Sätter attributet 'height'. Om både 'width' och 'height' är satta så kommer komponenten att ha rätt aspektratio även innan bilden laddats in.
   * @en Set `height` attribute. if both 'width' and 'height' are set, the component will have the correct apect ratio even before the image source is loaded.
   */
  @Prop() afHeight: string;

  /**
   * Sätter bilden till 100% i bredd och anpassar höjden automatiskt efter bredden.
   * @en Will set the image width to 100% and height to auto.
   */
  @Prop() afFullwidth: boolean;

  /**
   * Tar bort lazy loading av bilden.
   * @en Removes lazy laoding of the image.
   */
  @Prop() afUnlazy = false;

  /**
   * Bildlementets 'onload'-event.
   * @en The image element's 'onload' event.
   */
  @Event() afOnLoad: EventEmitter;

  @Watch('afSrc')
  srcChangeHandler() {
    this.setImageSrc();
  }

  connectedCallback() {
    if (this.afUnlazy) {
      this.setImageSrc();
    }

    // Add padded box to wrapper if width and height props exists
    if (this.afWidth && this.afHeight) {
      this.hasAspectRatio = true;
      this.setPaddedBox();
    }
  }

  setImageSrc() {
    this.src = this.afSrc;
    this.srcset = this.afSrcset;
    this.hasSetSrc = true;
  }

  setPaddedBox() {
    this.paddedBoxPadding =
      (parseInt(this.afHeight, 10) / parseInt(this.afWidth, 10)) * 100 + '%';
  }

  intersectHandler() {
    if (this.afUnlazy) {
      return;
    }

    this.setImageSrc();
  }

  loadHandler(e: Event) {
    if (this.afUnlazy) {
      return;
    }

    if (this.isPlaceholderLoaded) {
      this.isLoaded = true;
      this.afOnLoad.emit(e);
      return;
    }

    this.isPlaceholderLoaded = true;
  }

  get cssModifiers() {
    return {
      'digi-media-image--unlazy': this.afUnlazy,
      'digi-media-image--loaded': this.isLoaded,
      'digi-media-image--aspect-ratio': this.hasAspectRatio,
      'digi-media-image--fullwidth': this.afFullwidth
    };
  }

  get cssInlineStyles() {
    return {
      '--digi--media-image--padded-box': this.paddedBoxPadding,
    };
  }

  render() {
    return (
      <host>
        <div
          class={{
            'digi-media-image': true,
            ...this.cssModifiers,
          }}
          style={{
            '--digi--media-image--width': this.afWidth ? `${this.afWidth}px` : null
          }}
        >
          <digi-util-intersection-observer
            class="digi-media-image__observer"
            onAfOnIntersect={() => this.intersectHandler()}
            afOnce
            afOptions={this.afObserverOptions}
          ></digi-util-intersection-observer>
         {this.hasAspectRatio && (
          <div 
              class="digi-media-image__padded-box"
              style={this.cssInlineStyles}
            ></div>
          )}
          <img
            class="digi-media-image__image"
            onLoad={(e) => this.loadHandler(e)}
            src={this.src}
            alt={this.afAlt}
            aria-label={this.afAriaLabel}
            height={this.afHeight}
            srcset={this.srcset}
            title={this.afTitle}
            width={this.afWidth}
            loading={this.afUnlazy ? 'eager' : 'lazy'}
          />
        </div>
      </host>
    );
  }
}
