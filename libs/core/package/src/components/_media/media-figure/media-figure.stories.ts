import { enumSelect, Template } from '../../../../../../shared/utils/src';
import { MediaFigureAlignment } from './media-figure-alignment.enum';

export default {
	title: 'media/digi-media-figure',
	argTypes: {
		'af-alignment': enumSelect(MediaFigureAlignment)
	}
};

export const Standard = Template.bind({});
Standard.args = {
	component: 'digi-media-figure',
	'af-figcaption': '',
	'af-alignment': MediaFigureAlignment.START,
	/* html */
	children: `
    <digi-media-image
        af-src="https://picsum.photos/id/237/1280/720"
        af-alt="En bild"
        af-width="1280"
        af-height="720"
    ></digi-media-image>`
};
