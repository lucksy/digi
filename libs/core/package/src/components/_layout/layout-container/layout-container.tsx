import { Component, Prop, h, State, Watch } from '@stencil/core';
import { LayoutContainerVariation } from './layout-container-variation.enum';

/**
 * @slot default - kan innehålla vad som helst
 *
 * @enums LayoutContainerVariation - layout-container-variation.enum.ts
 * @swedishName Container
 */
@Component({
	tag: 'digi-layout-container',
	styleUrls: ['layout-container.scss'],
	scoped: true
})
export class LayoutContainer {
	@State() _variation: LayoutContainerVariation =
		LayoutContainerVariation.STATIC;
	/**
	 * Sätter containervarianten. Kan vara 'static' eller 'fluid'.
	 * @en Set container variation. Can be 'static' or 'fluid'.
	 */
	@Prop() afVariation: LayoutContainerVariation =
		LayoutContainerVariation.STATIC;

	@Watch('afVariation')
	variationChangeHandler() {
		this._variation = this.afVariation;
	}

	componentWillLoad() {
		this.variationChangeHandler();
	}

	/**
	 * Tar bort marginalen från sidorna av containern
	 * @en Remove gutters from container sides
	 */
	@Prop() afNoGutter: boolean;

	/**
	 * Lägger till vertikal padding inuti containern
	 * @en Adds vertical padding inside the container
	 */
	@Prop() afVerticalPadding: boolean;

	/**
	 * Lägger till marginal i toppen på containern
	 * @en Adds margin on top of the container
	 */
	@Prop() afMarginTop: boolean;

	/**
	 * Lägger till marginal i botten på containern
	 * @en Adds margin at bottom of the container
	 */
	@Prop() afMarginBottom: boolean;

	get cssModifiers() {
		if (this._variation === LayoutContainerVariation.NONE) {
			return {};
		}
		return {
			'digi-layout-container': true,
			'digi-layout-container--static':
				this._variation === LayoutContainerVariation.STATIC,
			'digi-layout-container--fluid':
				this._variation === LayoutContainerVariation.FLUID,
			'digi-layout-container--no-gutter': this.afNoGutter,
			'digi-layout-container--vertical-padding': this.afVerticalPadding,
			'digi-layout-container--margin-top': this.afMarginTop,
			'digi-layout-container--margin-bottom': this.afMarginBottom
		};
	}

	render() {
		return (
			<div
				class={{
					...this.cssModifiers
				}}
			>
				<slot></slot>
			</div>
		);
	}
}
