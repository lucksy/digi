import { enumSelect, Template } from '../../../../../../shared/utils/src';
import { LayoutContainerVariation } from './layout-container-variation.enum';

export default {
	title: 'layout/digi-layout-container',
	argTypes: {
		'af-variation': enumSelect(LayoutContainerVariation)
	}
};

export const Standard = Template.bind({});
Standard.args = {
	component: 'digi-layout-container',
	'af-variation': LayoutContainerVariation.STATIC,
	'af-no-gutter': false,
	'af-vertical-padding': false,
	'af-margin-top': false,
	'af-margin-bottom': false,
	/* html */
	children: `
    <digi-typography>
        <h2>I am a standard layout container.</h2>
        <p>I should function exactly like Bootstrap's container class. Things inside of me are contained within the current max-width, as determined by whatever viewport width you happen to view me in.</p>
        <digi-media-image
            af-src="https://picsum.photos/id/237/1280/720"
            af-alt="En bild"
            af-width="1280"
            af-height="720"
            style="margin-bottom: var(--digi--typography--margin--text); display: block;"
            ></digi-media-image>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam magna neque, interdum vel massa eget, condimentum rutrum velit. Sed vitae ullamcorper sem. Aliquam malesuada nunc sed purus mollis scelerisque. Curabitur bibendum leo quis ante porttitor tincidunt. Nam tincidunt imperdiet tortor eu suscipit. Maecenas ut dui est. Pellentesque in magna molestie, vulputate nibh non, ullamcorper magna. Donec ullamcorper vehicula orci, sit amet ultrices tellus finibus non. Curabitur varius consequat est, ut eleifend urna ornare vel. Aliquam efficitur dictum consectetur. Suspendisse vehicula, velit fringilla faucibus euismod, nisl dolor rutrum nisi, ut consequat neque magna sit amet nunc. Donec porttitor gravida diam ut iaculis. Proin cursus elementum faucibus. Ut eu tincidunt sem. Donec velit massa, bibendum in turpis eu, ornare consectetur risus. Vestibulum in ipsum nibh. </p>
    </digi-typography>`
};
