import { newE2EPage } from '@stencil/core/testing';

describe('digi-layout-media-object', () => {
  it('renders', async () => {
    const page = await newE2EPage();
    await page.setContent('<digi-layout-media-object></digi-layout-media-object>');

    const element = await page.find('digi-layout-media-object');
    expect(element).toHaveClass('hydrated');
  });
});
