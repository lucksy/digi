import { newE2EPage } from '@stencil/core/testing';

describe('digi-layout-block', () => {
  it('renders', async () => {
    const page = await newE2EPage();
    await page.setContent('<digi-layout-block></digi-layout-block>');

    const element = await page.find('digi-layout-block');
    expect(element).toHaveClass('hydrated');
  });
});
