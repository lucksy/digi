import { Component, Prop, Element, h } from '@stencil/core';
import { HTMLStencilElement } from '@stencil/core/internal';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';
import { ProgressStepStatus } from './progress-step-status.enum';
import { ProgressStepVariation } from './progress-step-variation.enum';
import { ProgressStepHeadingLevel } from './progress-step-heading-level.enum';

/**
 * @enums ProgressStepStatus - progress-step-status.enum.ts
 * @enums ProgressStepHeadingLevel- progress-step-heading-level.enum.ts
 * @enums ProgressbarVariation - progressbar-variation.enum.ts
 * @swedishName Förloppssteg - enskilt steg

 */
@Component({
  tag: 'digi-progress-step',
  styleUrls: ['progress-step.scss'],
  scoped: true
})
export class ProgressStep {
  @Element() hostElement: HTMLStencilElement;

  /**
   * Rubrikens text
   * @en The heading of text
   */
  @Prop() afHeading!: string;

  /**
   * Sätter rubriknivå. 'h2' är förvalt.
   * @en Set heading level. Default is 'h2'
   */
  @Prop() afHeadingLevel: ProgressStepHeadingLevel = ProgressStepHeadingLevel.H2;

  /**
   * Sätter attributet 'afStepStatus'. Förvalt är 'upcoming'.
   * @en Set role attribute. Defaults to 'upcoming'.
   */
  @Prop() afStepStatus: ProgressStepStatus = ProgressStepStatus.UPCOMING;

  /**
   * Sätter variant. Kan vara 'Primary' eller 'Secondary'
   * @en Set variation. Can be 'Primary' or 'Secondary'
   */
  @Prop() afVariation: ProgressStepVariation = ProgressStepVariation.PRIMARY;

  /**
   * Kollar om det är sista steget
   * @en checks if it is the last step
   */
  @Prop() afIsLast: boolean = false;

  /**
   * Sätter attributet 'id'. Om inget väljs så skapas ett slumpmässigt id.
   * @en Input id attribute. Defaults to random string.
   */
  @Prop() afId: string = randomIdGenerator('digi-progress-step');

  render() {
    return (
      <div
        aria-current={this.afStepStatus == ProgressStepStatus.CURRENT && 'step'}
        role="listitem"
        class={{
          'digi-progress-step': true,
          [`digi-progress-step--${this.afStepStatus}`]: true,
          'digi-progress-step--last': this.afIsLast,
          'digi-progress-step--primary':
            this.afVariation == ProgressStepVariation.PRIMARY,
          'digi-progress-step--secondary':
            this.afVariation == ProgressStepVariation.SECONDARY
        }}
      >
        <span class="digi-progress-step__indicator">
          <span class="digi-progress-step__indicator--circle"></span>
          <span class="digi-progress-step__indicator--line"></span>
        </span>
        <div class="digi-progress-step__content">
          <this.afHeadingLevel
            class="digi-progress-step__content--heading"
            id={`${this.afId}--heading`}
          >
            {this.afHeading}
          </this.afHeadingLevel>
          <digi-typography>
            <slot></slot>
          </digi-typography>
        </div>
      </div>
    );
  }
}
