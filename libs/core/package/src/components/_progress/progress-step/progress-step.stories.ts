import { enumSelect, Template } from '../../../../../../shared/utils/src';
import { ProgressStepStatus } from './progress-step-status.enum';
import { ProgressStepVariation } from './progress-step-variation.enum';
import { ProgressStepHeadingLevel } from './progress-step-heading-level.enum';

export default {
	title: 'progress/digi-progress-step',
	argTypes: {
		'af-heading-level': enumSelect(ProgressStepHeadingLevel),
		'af-step-status': enumSelect(ProgressStepStatus),
		'af-variation': enumSelect(ProgressStepVariation)
	}
};

export const Standard = Template.bind({});
Standard.args = {
	component: 'digi-progress-step',
	'af-heading': 'A heading is required',
	'af-heading-level': ProgressStepHeadingLevel.H2,
	'af-step-status': ProgressStepStatus.UPCOMING,
	'af-variation': ProgressStepVariation.PRIMARY,
	'af-is-last': false,
	/* html */
	children:
		'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.'
};
