import { newSpecPage } from '@stencil/core/testing';
import { ProgressSteps } from './progress-steps';

describe('progress-steps', () => {
  it('renders', async () => {
    const { root } = await newSpecPage({
      components: [ProgressSteps],
      html: '<progress-steps></progress-steps>'
    });
    expect(root).toEqualHtml(`
      <progress-steps>
        <mock:shadow-root>
          <div>
            Hello, World! I'm
          </div>
        </mock:shadow-root>
      </progress-steps>
    `);
  });

  it('renders with values', async () => {
    const { root } = await newSpecPage({
      components: [ProgressSteps],
      html: `<progress-steps first="Stencil" last="'Don't call me a framework' JS"></progress-steps>`
    });
    expect(root).toEqualHtml(`
      <progress-steps first="Stencil" last="'Don't call me a framework' JS">
        <mock:shadow-root>
          <div>
            Hello, World! I'm Stencil 'Don't call me a framework' JS
          </div>
        </mock:shadow-root>
      </progress-steps>
    `);
  });
});
