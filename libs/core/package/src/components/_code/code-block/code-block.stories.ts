import { enumSelect, Template } from '../../../../../../shared/utils/src';
import { CodeBlockVariation } from './code-block-variation.enum';
import { CodeBlockLanguage } from './code-block-language.enum';

export default {
	title: 'code/digi-code-block',
	argTypes: {
		'af-variation': enumSelect(CodeBlockVariation),
		'af-language': enumSelect(CodeBlockLanguage)
	}
};

export const Standard = Template.bind({});
Standard.args = {
	component: 'digi-code-block',
	'af-code': `class SomeClass extends SomeOtherClass {
		@MyDecorator()
		myMethod(arg: MyType[]):void {
			if(arg[1].user?.name === 'Ringo') {
				console.log('username is Ringo');
			}
		}
	}`,
	'af-variation': CodeBlockVariation.DARK,
	'af-language': CodeBlockLanguage.HTML,
	'af-hide-toolbar': false
};
