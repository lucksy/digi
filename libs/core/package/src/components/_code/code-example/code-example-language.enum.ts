export enum CodeExampleLanguage {
  JSON = 'JSON',
  CSS = 'CSS',
  SCSS = 'SCSS',
  TYPESCRIPT = 'Typescript',
  JAVASCRIPT = 'Javascript',
  BASH = 'Bash',
  HTML = 'HTML',
  GIT = 'Git',
  ANGULAR = 'Angular',
  REACT = 'React'
}
