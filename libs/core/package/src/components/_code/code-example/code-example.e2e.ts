import { newE2EPage } from '@stencil/core/testing';

describe('digi-code-example', () => {
  it('renders', async () => {
    const page = await newE2EPage();
    await page.setContent('<digi-code-example></digi-code-example>');

    const element = await page.find('digi-code-example');
    expect(element).toHaveClass('hydrated');
  });
});
