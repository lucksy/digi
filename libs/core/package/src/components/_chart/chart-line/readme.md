# digi-chart-line

<!-- Auto Generated Below -->


## Properties

| Property         | Attribute          | Description | Type                      | Default                        |
| ---------------- | ------------------ | ----------- | ------------------------- | ------------------------------ |
| `afChartData`    | `af-chart-data`    |             | `ChartLineData \| string` | `undefined`                    |
| `afHeadingLevel` | `af-heading-level` |             | `string`                  | `undefined`                    |
| `afId`           | `af-id`            |             | `string`                  | `randomIdGenerator('tooltip')` |


## Slots

| Slot       | Description              |
| ---------- | ------------------------ |
| `"mySlot"` | Slot description, if any |


## Dependencies

### Depends on

- [digi-typography](../../_typography/typography)
- [digi-button](../../_button/button)
- [digi-icon-x](../../_icon/icon-x)

### Graph
```mermaid
graph TD;
  digi-chart-line --> digi-typography
  digi-chart-line --> digi-button
  digi-chart-line --> digi-icon-x
  style digi-chart-line fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
