import { enumSelect, Template } from '../../../../../../shared/utils/src';

export default {
	title: 'util/digi-util-detect-click-outside',
	parameters: {
		actions: {
			handles: ['afOnClickOutside', 'afOnClickInside']
		}
	}
};

export const Standard = Template.bind({});
Standard.args = {
	component: 'digi-util-detect-click-outside',
	'af-data-identifier': null,
	/* html */
	children: `
    <digi-typography>
        <p>Open the actions panel and try clicking on me and outside me!</p>
    </digi-typography>`
};
