import { enumSelect, Template } from '../../../../../../shared/utils/src';
import { UtilBreakpointObserverBreakpoints } from './util-breakpoint-observer-breakpoints.enum';

export default {
	title: 'util/digi-util-breakpoint-observer',
	parameters: {
		actions: {
			handles: ['afOnChange', 'afOnSmall', 'afOnMedium', 'afOnLarge', 'afOnXLarge']
		}
	}
};

export const Standard = Template.bind({});
Standard.args = {
	component: 'digi-util-breakpoint-observer',
	/* html */
	children: `
    <digi-typography>
        <p class="message"></p>
    </digi-typography>`
};
