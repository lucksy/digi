import { newE2EPage } from '@stencil/core/testing';

describe('digi-util-resize-observer', () => {
	it('renders', async () => {
		const page = await newE2EPage();
		await page.setContent(
			'<digi-util-resize-observer></digi-util-resize-observer>'
		);

		const element = await page.find('digi-util-resize-observer');
		expect(element).toHaveClass('hydrated');
	});
});
