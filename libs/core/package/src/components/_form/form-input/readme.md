# digi-form-input

This is a general purpose input component. It supports all input types and encapsulates all design patterns and guidelines for validation etc.

## Enums

If used in a Typescript environment, you will need to import a couple of enums:

```ts
import { FormInputType, FormInputValidation, FormInputVariation } from '@digi/core';
```

<!-- Auto Generated Below -->


## Properties

| Property                   | Attribute                      | Description                                                                                  | Type                                                                                                                                                                                                                                                                                                                                    | Default                                |
| -------------------------- | ------------------------------ | -------------------------------------------------------------------------------------------- | --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | -------------------------------------- |
| `afAnnounceIfOptional`     | `af-announce-if-optional`      | Sätt denna till true om formuläret innehåller fler obligatoriska fält än valfria.            | `boolean`                                                                                                                                                                                                                                                                                                                               | `false`                                |
| `afAnnounceIfOptionalText` | `af-announce-if-optional-text` | Sätter text för afAnnounceIfOptional.                                                        | `string`                                                                                                                                                                                                                                                                                                                                | `undefined`                            |
| `afAriaActivedescendant`   | `af-aria-activedescendant`     | Sätter attributet 'aria-activedescendant'.                                                   | `string`                                                                                                                                                                                                                                                                                                                                | `undefined`                            |
| `afAriaAutocomplete`       | `af-aria-autocomplete`         | Sätter attributet 'aria-autocomplete'.                                                       | `string`                                                                                                                                                                                                                                                                                                                                | `undefined`                            |
| `afAriaControls`           | `af-aria-controls`             | Sätter attributet 'aria-controls'.                                                           | `string`                                                                                                                                                                                                                                                                                                                                | `undefined`                            |
| `afAriaDescribedby`        | `af-aria-describedby`          | Sätter attributet 'aria-describedby'.                                                        | `string`                                                                                                                                                                                                                                                                                                                                | `undefined`                            |
| `afAriaLabelledby`         | `af-aria-labelledby`           | Sätter attributet 'aria-labelledby'.                                                         | `string`                                                                                                                                                                                                                                                                                                                                | `undefined`                            |
| `afAutocomplete`           | `af-autocomplete`              | Sätter attributet 'autocomplete'.                                                            | `string`                                                                                                                                                                                                                                                                                                                                | `undefined`                            |
| `afAutofocus`              | `af-autofocus`                 | Sätter attributet 'autofocus'.                                                               | `boolean`                                                                                                                                                                                                                                                                                                                               | `undefined`                            |
| `afButtonVariation`        | `af-button-variation`          | Layout för hur inmatningsfältet ska visas då det finns en knapp                              | `FormInputButtonVariation.PRIMARY \| FormInputButtonVariation.SECONDARY`                                                                                                                                                                                                                                                                | `FormInputButtonVariation.PRIMARY`     |
| `afDirname`                | `af-dirname`                   | Sätter attributet 'dirname'.                                                                 | `string`                                                                                                                                                                                                                                                                                                                                | `undefined`                            |
| `afId`                     | `af-id`                        | Sätter attributet 'id'. Om inget väljs så skapas ett slumpmässigt id.                        | `string`                                                                                                                                                                                                                                                                                                                                | `randomIdGenerator('digi-form-input')` |
| `afInputmode`              | `af-inputmode`                 | Sätter attributet 'inputmode'.                                                               | `FormInputMode.DECIMAL \| FormInputMode.DEFAULT \| FormInputMode.EMAIL \| FormInputMode.NONE \| FormInputMode.NUMERIC \| FormInputMode.SEARCH \| FormInputMode.TEL \| FormInputMode.TEXT \| FormInputMode.URL`                                                                                                                          | `FormInputMode.DEFAULT`                |
| `afLabel` _(required)_     | `af-label`                     | Texten till labelelementet                                                                   | `string`                                                                                                                                                                                                                                                                                                                                | `undefined`                            |
| `afLabelDescription`       | `af-label-description`         | Valfri beskrivande text                                                                      | `string`                                                                                                                                                                                                                                                                                                                                | `undefined`                            |
| `afList`                   | `af-list`                      | Sätter attributet 'list'.                                                                    | `string`                                                                                                                                                                                                                                                                                                                                | `undefined`                            |
| `afMax`                    | `af-max`                       | Sätter attributet 'max'.                                                                     | `string`                                                                                                                                                                                                                                                                                                                                | `undefined`                            |
| `afMaxlength`              | `af-maxlength`                 | Sätter attributet 'maxlength'.                                                               | `number`                                                                                                                                                                                                                                                                                                                                | `undefined`                            |
| `afMin`                    | `af-min`                       | Sätter attributet 'min'.                                                                     | `string`                                                                                                                                                                                                                                                                                                                                | `undefined`                            |
| `afMinlength`              | `af-minlength`                 | Sätter attributet 'minlength'.                                                               | `number`                                                                                                                                                                                                                                                                                                                                | `undefined`                            |
| `afName`                   | `af-name`                      | Sätter attributet 'name'.                                                                    | `string`                                                                                                                                                                                                                                                                                                                                | `undefined`                            |
| `afRequired`               | `af-required`                  | Sätter attributet 'required'.                                                                | `boolean`                                                                                                                                                                                                                                                                                                                               | `undefined`                            |
| `afRequiredText`           | `af-required-text`             | Sätter text för afRequired.                                                                  | `string`                                                                                                                                                                                                                                                                                                                                | `undefined`                            |
| `afRole`                   | `af-role`                      | Sätter attributet 'role'.                                                                    | `string`                                                                                                                                                                                                                                                                                                                                | `undefined`                            |
| `afStep`                   | `af-step`                      | Sätter attributet 'step'.                                                                    | `string`                                                                                                                                                                                                                                                                                                                                | `undefined`                            |
| `afType`                   | `af-type`                      | Sätter attributet 'type'.                                                                    | `FormInputType.COLOR \| FormInputType.DATE \| FormInputType.DATETIME_LOCAL \| FormInputType.EMAIL \| FormInputType.HIDDEN \| FormInputType.MONTH \| FormInputType.NUMBER \| FormInputType.PASSWORD \| FormInputType.SEARCH \| FormInputType.TEL \| FormInputType.TEXT \| FormInputType.TIME \| FormInputType.URL \| FormInputType.WEEK` | `FormInputType.TEXT`                   |
| `afValidation`             | `af-validation`                | Sätter valideringsstatus. Kan vara 'success', 'error', 'warning', 'neutral' eller ingenting. | `FormInputValidation.ERROR \| FormInputValidation.NEUTRAL \| FormInputValidation.SUCCESS \| FormInputValidation.WARNING`                                                                                                                                                                                                                | `FormInputValidation.NEUTRAL`          |
| `afValidationText`         | `af-validation-text`           | Sätter valideringsmeddelandet                                                                | `string`                                                                                                                                                                                                                                                                                                                                | `undefined`                            |
| `afValue`                  | `af-value`                     | Sätter attributet 'value'.                                                                   | `number \| string`                                                                                                                                                                                                                                                                                                                      | `undefined`                            |
| `afVariation`              | `af-variation`                 | Sätter variant. Kan vara 'small', 'medium' eller 'large'                                     | `FormInputVariation.LARGE \| FormInputVariation.MEDIUM \| FormInputVariation.SMALL`                                                                                                                                                                                                                                                     | `FormInputVariation.MEDIUM`            |
| `value`                    | `value`                        | Sätter attributet 'value'.                                                                   | `number \| string`                                                                                                                                                                                                                                                                                                                      | `undefined`                            |


## Events

| Event          | Description                                                                     | Type               |
| -------------- | ------------------------------------------------------------------------------- | ------------------ |
| `afOnBlur`     | Inputelementets 'onblur'-event.                                                 | `CustomEvent<any>` |
| `afOnChange`   | Inputelementets 'onchange'-event.                                               | `CustomEvent<any>` |
| `afOnDirty`    | Sker vid inputfältets första 'oninput'                                          | `CustomEvent<any>` |
| `afOnFocus`    | Inputelementets 'onfocus'-event.                                                | `CustomEvent<any>` |
| `afOnFocusout` | Inputelementets 'onfocusout'-event.                                             | `CustomEvent<any>` |
| `afOnInput`    | Inputelementets 'oninput'-event.                                                | `CustomEvent<any>` |
| `afOnKeyup`    | Inputelementets 'onkeyup'-event.                                                | `CustomEvent<any>` |
| `afOnReady`    | När komponenten och slotsen är laddade och initierade så skickas detta eventet. | `CustomEvent<any>` |
| `afOnTouched`  | Sker vid inputfältets första 'onblur'                                           | `CustomEvent<any>` |


## Methods

### `afMGetFormControlElement() => Promise<HTMLInputElement>`

Hämtar en referens till inputelementet. Bra för att t.ex. sätta fokus programmatiskt.

#### Returns

Type: `Promise<HTMLInputElement>`




## CSS Custom Properties

| Name                                        | Description                                        |
| ------------------------------------------- | -------------------------------------------------- |
| `--digi--form-input--background--empty`     | var(--digi--color--background--input-empty);       |
| `--digi--form-input--background--error`     | var(--digi--color--background--danger-2);          |
| `--digi--form-input--background--neutral`   | var(--digi--color--background--input);             |
| `--digi--form-input--background--success`   | var(--digi--color--background--success-2);         |
| `--digi--form-input--background--warning`   | var(--digi--color--background--warning-3);         |
| `--digi--form-input--border--error`         | var(--digi--border-width--input-validation) solid; |
| `--digi--form-input--border--neutral`       | var(--digi--border-width--input-regular) solid;    |
| `--digi--form-input--border--success`       | var(--digi--border-width--input-validation) solid; |
| `--digi--form-input--border--warning`       | var(--digi--border-width--input-validation) solid; |
| `--digi--form-input--border-color--error`   | var(--digi--color--border--danger);                |
| `--digi--form-input--border-color--neutral` | var(--digi--color--border--neutral-3);             |
| `--digi--form-input--border-color--success` | var(--digi--color--border--success);               |
| `--digi--form-input--border-color--warning` | var(--digi--color--border--neutral-3);             |
| `--digi--form-input--border-radius`         | var(--digi--border-radius--input);                 |
| `--digi--form-input--height--large`         | var(--digi--input-height--large);                  |
| `--digi--form-input--height--medium`        | var(--digi--input-height--medium);                 |
| `--digi--form-input--height--small`         | var(--digi--input-height--small);                  |
| `--digi--form-input--padding`               | 0 var(--digi--gutter--medium);                     |


## Dependencies

### Used by

 - [digi-form-input-search](../form-input-search)

### Depends on

- [digi-form-label](../form-label)
- [digi-form-validation-message](../form-validation-message)

### Graph
```mermaid
graph TD;
  digi-form-input --> digi-form-label
  digi-form-input --> digi-form-validation-message
  digi-form-validation-message --> digi-icon
  digi-form-input-search --> digi-form-input
  style digi-form-input fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
