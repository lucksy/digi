import { enumSelect, Template } from '../../../../../../shared/utils/src';
import { FormValidationMessageVariation } from './form-validation-message-variation.enum';

export default {
	title: 'form/digi-form-validation-message',
	argTypes: {
		'af-variation': enumSelect(FormValidationMessageVariation)
	}
};

export const Standard = Template.bind({});
Standard.args = {
	component: 'digi-form-validation-message',
	'af-variation': FormValidationMessageVariation.SUCCESS,
	/* html */
	children: 'I am a validation message'
};
