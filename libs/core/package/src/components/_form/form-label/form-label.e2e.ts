import { newE2EPage } from '@stencil/core/testing';

describe('digi-form-label', () => {
  it('renders', async () => {
    const page = await newE2EPage();
    await page.setContent('<digi-form-label></digi-form-label>');

    const element = await page.find('digi-form-label');
    expect(element).toHaveClass('hydrated');
  });
});
