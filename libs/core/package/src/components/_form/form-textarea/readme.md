# digi-form-textarea

This is a standard textarea component. It contains a required label with an optional description, and standard validation messaging. See guidelines for when to use what.

## Enums

If used in a Typescript environment, you will need to import a couple of enums:

```ts
import { FormTextareaVariation, FormTextareaValidation } from '@digi/core';
```

<!-- Auto Generated Below -->


## Properties

| Property                   | Attribute                      | Description                                                                                  | Type                                                                                                                                 | Default                                   |
| -------------------------- | ------------------------------ | -------------------------------------------------------------------------------------------- | ------------------------------------------------------------------------------------------------------------------------------------ | ----------------------------------------- |
| `afAnnounceIfOptional`     | `af-announce-if-optional`      | Sätt denna till true om formuläret innehåller fler obligatoriska fält än valfria.            | `boolean`                                                                                                                            | `false`                                   |
| `afAnnounceIfOptionalText` | `af-announce-if-optional-text` | Sätter text för afAnnounceIfOptional.                                                        | `string`                                                                                                                             | `undefined`                               |
| `afAriaActivedescendant`   | `af-aria-activedescendant`     | Sätter attributet 'aria-activedescendant'.                                                   | `string`                                                                                                                             | `undefined`                               |
| `afAriaDescribedby`        | `af-aria-describedby`          | Sätter attributet 'aria-describedby'.                                                        | `string`                                                                                                                             | `undefined`                               |
| `afAriaLabelledby`         | `af-aria-labelledby`           | Sätter attributet 'aria-labelledby'.                                                         | `string`                                                                                                                             | `undefined`                               |
| `afAutofocus`              | `af-autofocus`                 | Sätter attributet 'autofocus'.                                                               | `boolean`                                                                                                                            | `undefined`                               |
| `afId`                     | `af-id`                        | Sätter attributet 'id'. Om inget väljs så skapas ett slumpmässigt id.                        | `string`                                                                                                                             | `randomIdGenerator('digi-form-textarea')` |
| `afLabel` _(required)_     | `af-label`                     | Texten till labelelementet                                                                   | `string`                                                                                                                             | `undefined`                               |
| `afLabelDescription`       | `af-label-description`         | Valfri beskrivande text                                                                      | `string`                                                                                                                             | `undefined`                               |
| `afMaxlength`              | `af-maxlength`                 | Sätter attributet 'maxlength'.                                                               | `number`                                                                                                                             | `undefined`                               |
| `afMinlength`              | `af-minlength`                 | Sätter attributet 'minlength'.                                                               | `number`                                                                                                                             | `undefined`                               |
| `afName`                   | `af-name`                      | Sätter attributet 'name'.                                                                    | `string`                                                                                                                             | `undefined`                               |
| `afRequired`               | `af-required`                  | Sätter attributet 'required'.                                                                | `boolean`                                                                                                                            | `undefined`                               |
| `afRequiredText`           | `af-required-text`             | Sätter text för afRequired.                                                                  | `string`                                                                                                                             | `undefined`                               |
| `afRole`                   | `af-role`                      | Sätter attributet 'role'.                                                                    | `string`                                                                                                                             | `undefined`                               |
| `afValidation`             | `af-validation`                | Sätter valideringsstatus. Kan vara 'success', 'error', 'warning', 'neutral' eller ingenting. | `FormTextareaValidation.ERROR \| FormTextareaValidation.NEUTRAL \| FormTextareaValidation.SUCCESS \| FormTextareaValidation.WARNING` | `FormTextareaValidation.NEUTRAL`          |
| `afValidationText`         | `af-validation-text`           | Sätter valideringsmeddelandet                                                                | `string`                                                                                                                             | `undefined`                               |
| `afValue`                  | `af-value`                     | Sätter attributet 'value'.                                                                   | `string`                                                                                                                             | `undefined`                               |
| `afVariation`              | `af-variation`                 | Sätter variant. Kan vara 'small', 'medium', 'large' eller 'auto'                             | `FormTextareaVariation.AUTO \| FormTextareaVariation.LARGE \| FormTextareaVariation.MEDIUM \| FormTextareaVariation.SMALL`           | `FormTextareaVariation.MEDIUM`            |
| `value`                    | `value`                        | Sätter attributet 'value'.                                                                   | `string`                                                                                                                             | `undefined`                               |


## Events

| Event          | Description                                                                     | Type               |
| -------------- | ------------------------------------------------------------------------------- | ------------------ |
| `afOnBlur`     | Textareatelementets 'onblur'-event.                                             | `CustomEvent<any>` |
| `afOnChange`   | Textareatelementets 'onchange'-event.                                           | `CustomEvent<any>` |
| `afOnDirty`    | Sker vid textareaelementets första 'oninput'                                    | `CustomEvent<any>` |
| `afOnFocus`    | Textareatelementets 'onfocus'-event.                                            | `CustomEvent<any>` |
| `afOnFocusout` | Textareatelementets 'onfocusout'-event.                                         | `CustomEvent<any>` |
| `afOnInput`    | Textareatelementets 'oninput'-event.                                            | `CustomEvent<any>` |
| `afOnKeyup`    | Textareatelementets 'onkeyup'-event.                                            | `CustomEvent<any>` |
| `afOnReady`    | När komponenten och slotsen är laddade och initierade så skickas detta eventet. | `CustomEvent<any>` |
| `afOnTouched`  | Sker vid textareaelementets första 'onblur'                                     | `CustomEvent<any>` |


## Methods

### `afMGetFormControlElement() => Promise<HTMLTextAreaElement>`

Hämtar en referens till textareaelementet. Bra för att t.ex. sätta fokus programmatiskt.

#### Returns

Type: `Promise<HTMLTextAreaElement>`




## CSS Custom Properties

| Name                                           | Description                                                        |
| ---------------------------------------------- | ------------------------------------------------------------------ |
| `--digi--form-textarea--background--empty`     | var(--digi--color--background--input-empty);                       |
| `--digi--form-textarea--background--error`     | var(--digi--color--background--danger-2);                          |
| `--digi--form-textarea--background--neutral`   | var(--digi--color--background--input);                             |
| `--digi--form-textarea--background--success`   | var(--digi--color--background--success-2);                         |
| `--digi--form-textarea--background--warning`   | var(--digi--color--background--warning-2);                         |
| `--digi--form-textarea--border--error`         | var(--digi--border-width--input-validation) solid;                 |
| `--digi--form-textarea--border--neutral`       | var(--digi--border-width--input-regular) solid;                    |
| `--digi--form-textarea--border--success`       | var(--digi--border-width--input-validation) solid;                 |
| `--digi--form-textarea--border--warning`       | var(--digi--border-width--input-validation) solid;                 |
| `--digi--form-textarea--border-color--error`   | var(--digi--color--border--danger);                                |
| `--digi--form-textarea--border-color--neutral` | var(--digi--color--border--neutral-3);                             |
| `--digi--form-textarea--border-color--success` | var(--digi--color--border--success);                               |
| `--digi--form-textarea--border-color--warning` | var(--digi--color--border--neutral-3);                             |
| `--digi--form-textarea--border-radius`         | var(--digi--border-radius--input);                                 |
| `--digi--form-textarea--height--auto`          | auto;                                                              |
| `--digi--form-textarea--height--large`         | 10.3125rem;                                                        |
| `--digi--form-textarea--height--medium`        | 8.4375rem;                                                         |
| `--digi--form-textarea--height--small`         | 4.6875rem;                                                         |
| `--digi--form-textarea--padding`               | calc(var(--digi--gutter--medium) / 2) var(--digi--gutter--medium); |


## Dependencies

### Depends on

- [digi-form-label](../form-label)
- [digi-form-validation-message](../form-validation-message)

### Graph
```mermaid
graph TD;
  digi-form-textarea --> digi-form-label
  digi-form-textarea --> digi-form-validation-message
  digi-form-validation-message --> digi-icon
  style digi-form-textarea fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
