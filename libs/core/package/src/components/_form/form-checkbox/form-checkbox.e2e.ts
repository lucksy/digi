import { newE2EPage } from '@stencil/core/testing';

describe('digi-checkbox', () => {
  it('renders', async () => {
    const page = await newE2EPage();
    await page.setContent('<digi-form-checkbox></digi-form-checkbox>');

    const element = await page.find('digi-form-checkbox');
    expect(element).toHaveClass('hydrated');
  });
});
