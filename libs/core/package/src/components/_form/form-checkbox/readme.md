# digi-checkbox

This is a checkbox component.

## Enums

If used in a Typescript environment, you will need to import a couple of enums:

```ts
import { FormCheckboxVariation, FormCheckboxValidation } from '@digi/core';
```

<!-- Auto Generated Below -->


## Properties

| Property               | Attribute             | Description                                                             | Type                                                               | Default                                   |
| ---------------------- | --------------------- | ----------------------------------------------------------------------- | ------------------------------------------------------------------ | ----------------------------------------- |
| `afAriaDescribedby`    | `af-aria-describedby` | Sätter attributet 'aria-describedby'                                    | `string`                                                           | `undefined`                               |
| `afAriaLabel`          | `af-aria-label`       | Sätter attributet 'aria-label'                                          | `string`                                                           | `undefined`                               |
| `afAriaLabelledby`     | `af-aria-labelledby`  | Sätter attributet 'aria-labelledby'                                     | `string`                                                           | `undefined`                               |
| `afAutofocus`          | `af-autofocus`        | Sätter attributet 'autofocus'.                                          | `boolean`                                                          | `undefined`                               |
| `afChecked`            | `af-checked`          | Sätter attributet 'checked'.                                            | `boolean`                                                          | `undefined`                               |
| `afDescription`        | `af-description`      | Ytterligare beskrivande text                                            | `string`                                                           | `undefined`                               |
| `afId`                 | `af-id`               | Sätter attributet 'id'. Om inget väljs så skapas ett slumpmässigt id.   | `string`                                                           | `randomIdGenerator('digi-form-checkbox')` |
| `afIndeterminate`      | `af-indeterminate`    | Sätter property 'indeterminate' på inputelementet.                      | `boolean`                                                          | `false`                                   |
| `afLabel` _(required)_ | `af-label`            | Texten till labelelementet                                              | `string`                                                           | `undefined`                               |
| `afLayout`             | `af-layout`           | Sätter layouten.                                                        | `FormCheckboxLayout.BLOCK \| FormCheckboxLayout.INLINE`            | `FormCheckboxLayout.INLINE`               |
| `afName`               | `af-name`             | Sätter attributet 'name'.                                               | `string`                                                           | `undefined`                               |
| `afRequired`           | `af-required`         | Sätter attributet 'required'                                            | `boolean`                                                          | `undefined`                               |
| `afValidation`         | `af-validation`       | Sätter valideringsstatus. Kan vara 'error', 'varning', eller ingenting. | `FormCheckboxValidation.ERROR \| FormCheckboxValidation.WARNING`   | `undefined`                               |
| `afValue`              | `af-value`            | Sätter attributet 'value'.                                              | `string`                                                           | `undefined`                               |
| `afVariation`          | `af-variation`        | Sätter variant. Kan vara 'primary' eller 'secondary'                    | `FormCheckboxVariation.PRIMARY \| FormCheckboxVariation.SECONDARY` | `FormCheckboxVariation.PRIMARY`           |
| `checked`              | `checked`             | Sätter attributet 'checked'.                                            | `boolean`                                                          | `undefined`                               |
| `value`                | `value`               | Sätter attributet 'value'                                               | `string`                                                           | `undefined`                               |


## Events

| Event          | Description                                                                     | Type               |
| -------------- | ------------------------------------------------------------------------------- | ------------------ |
| `afOnBlur`     | Inputelementets 'onblur'-event.                                                 | `CustomEvent<any>` |
| `afOnChange`   | Inputelementets 'onchange'-event.                                               | `CustomEvent<any>` |
| `afOnDirty`    | Sker vid inputfältets första 'oninput'                                          | `CustomEvent<any>` |
| `afOnFocus`    | Inputelementets 'onfocus'-event.                                                | `CustomEvent<any>` |
| `afOnFocusout` | Inputelementets 'onfocusout'-event.                                             | `CustomEvent<any>` |
| `afOnInput`    | Inputelementets 'oninput'-event.                                                | `CustomEvent<any>` |
| `afOnReady`    | När komponenten och slotsen är laddade och initierade så skickas detta eventet. | `CustomEvent<any>` |
| `afOnTouched`  | Sker vid inputfältets första 'onblur'                                           | `CustomEvent<any>` |


## Methods

### `afMGetFormControlElement() => Promise<HTMLInputElement>`

Hämta en referens till inputelementet. Bra för att t.ex. sätta fokus programmatiskt.

#### Returns

Type: `Promise<HTMLInputElement>`




## CSS Custom Properties

| Name                                                    | Description                                         |
| ------------------------------------------------------- | --------------------------------------------------- |
| `--digi--form-checkbox--background-color`               | var(--digi--color--background--primary);            |
| `--digi--form-checkbox--background-color--error`        | var(--digi--color--background--danger-2);           |
| `--digi--form-checkbox--background-color--primary`      | var(--digi--color--background--checkbox-primary);   |
| `--digi--form-checkbox--background-color--secondary`    | var(--digi--color--background--checkbox-secondary); |
| `--digi--form-checkbox--background-color--warning`      | var(--digi--color--background--warning-3);          |
| `--digi--form-checkbox--border-color`                   | var(--digi--color--border--primary);                |
| `--digi--form-checkbox--border-color--error`            | var(--digi--color--border--danger);                 |
| `--digi--form-checkbox--border-color--primary`          | var(--digi--color--border--checkbox-primary);       |
| `--digi--form-checkbox--border-color--secondary`        | var(--digi--color--border--checkbox-secondary);     |
| `--digi--form-checkbox--border-color--warning`          | var(--digi--color--border--neutral-5);              |
| `--digi--form-checkbox--border-radius`                  | var(--digi--border-radius--tertiary);               |
| `--digi--form-checkbox--border-width`                   | var(--digi--border-width--primary);                 |
| `--digi--form-checkbox--box-shadow--error`              | var(--digi--color--border--danger);                 |
| `--digi--form-checkbox--box-shadow--warning`            | var(--digi--color--border--neutral-5);              |
| `--digi--form-checkbox--color--marker`                  | var(--digi--color--background--checkbox-marker);    |
| `--digi--form-checkbox--height`                         | var(--digi--input-height--large);                   |
| `--digi--form-checkbox--input--focus--shadow`           | var(--digi--shadow--input-focus);                   |
| `--digi--form-checkbox--label--background-color--hover` | var(--digi--color--background--input-empty);        |
| `--digi--form-checkbox--padding--block`                 | var(--digi--gutter--small);                         |
| `--digi--form-checkbox--padding--inline`                | var(--digi--gutter--icon);                          |
| `--digi--form-checkbox--size`                           | 1.25rem;                                            |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
