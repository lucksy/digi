import { newE2EPage } from '@stencil/core/testing';

describe('digi-form-filter', () => {
  it('renders', async () => {
    const page = await newE2EPage();
    await page.setContent('<digi-form-filter></digi-form-filter>');

    const element = await page.find('digi-form-filter');
    expect(element).toHaveClass('hydrated');
  });
});
