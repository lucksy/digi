export enum LoaderSpinnerSize {
    SMALL = 'small',
    MEDIUM = 'medium',
    LARGE = 'large'
  }
  