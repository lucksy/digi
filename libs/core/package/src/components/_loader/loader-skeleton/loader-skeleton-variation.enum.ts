export enum LoaderSkeletonVariation {
  LINE = 'line',
  CIRCLE = 'circle',
  TEXT = 'text',
  HEADER ='header',
  ROUNDED = 'rounded',
  SECTION = 'section'
}
