import { enumSelect, Template } from '../../../../../../shared/utils/src';
import { NavigationContextMenuItemType } from '../navigation-context-menu-item/navigation-context-menu-item-type.enum';
import { withKnobs, number, select } from '@storybook/addon-knobs';

export default {
	title: 'navigation/digi-navigation-context-menu',
	parameters: {
		actions: {
			handles: [
				'afOnInactive',
				'afOnActive',
				'afOnBlur',
				'afOnChange',
				'afOnToggle',
				'afOnSelect'
			]
		}
	}
};

export const Standard = Template.bind({});
Standard.args = {
	component: 'digi-navigation-context-menu',
	'af-text': 'A text is required',
	'af-id': null,
	'af-start-selected': 1,
	/* html */
	children: `
    <digi-navigation-context-menu-item
        af-text="A regular link"
        af-href="https://arbetsformedlingen.se/"
        af-lang="sv"
        af-dir="ltr"
        af-type="${select(
									'Type',
									[
										NavigationContextMenuItemType.LINK,
										NavigationContextMenuItemType.BUTTON
									],
									NavigationContextMenuItemType.LINK
								)}"
    ></digi-navigation-context-menu-item>
    <digi-navigation-context-menu-item
        af-text="Another link"
        af-href="https://www.google.com"
        af-type="${select(
									'Type',
									[
										NavigationContextMenuItemType.LINK,
										NavigationContextMenuItemType.BUTTON
									],
									NavigationContextMenuItemType.LINK
								)}"
    ></digi-navigation-context-menu-item>
    <digi-navigation-context-menu-item
        af-text="One more link"
        af-href="https://arbetsformedlingen.se/"
        af-type="${select(
									'Type',
									[
										NavigationContextMenuItemType.LINK,
										NavigationContextMenuItemType.BUTTON
									],
									NavigationContextMenuItemType.LINK
								)}"
    ></digi-navigation-context-menu-item>
    <digi-navigation-context-menu-item
        af-text="A regular link"
        af-href="https://arbetsformedlingen.se/"
        af-type="${select(
									'Type',
									[
										NavigationContextMenuItemType.LINK,
										NavigationContextMenuItemType.BUTTON
									],
									NavigationContextMenuItemType.LINK
								)}"
    ></digi-navigation-context-menu-item>`
};

export const WithButtons = Template.bind({});
WithButtons.args = {
	component: 'digi-navigation-context-menu',
	'af-text': 'A text is required',
	'af-id': null,
	'af-start-selected': 1,
	/* html */
	children: `
    <digi-navigation-context-menu-item
        af-text="A regular button"
        af-type="${NavigationContextMenuItemType.BUTTON}"
    ></digi-navigation-context-menu-item>
    `
};

export const WithButtonsValue = Template.bind({});
WithButtonsValue.args = {
	component: 'digi-navigation-context-menu',
	'af-text': 'A text is required',
	'af-id': null,
	'af-start-selected': 1,
	/* html */
	children: `
    <digi-navigation-context-menu-item
        af-text="A regular link"
        af-type="${NavigationContextMenuItemType.BUTTON}"
				data-value="Not same as af-text"
    ></digi-navigation-context-menu-item>
    `
};

export const WithArrayButtons = Template.bind({});
WithArrayButtons.args = {
	component: 'digi-navigation-context-menu',
	'af-text': 'A text is required',
	'af-id': null,
	'af-start-selected': 1,
	'af-navigation-context-menu-items': `[{\"text\":\"a array link\",\"value\":\"hey\"}]`
};

export const WithArrayLinks = Template.bind({});
WithArrayLinks.args = {
	component: 'digi-navigation-context-menu',
	'af-text': 'A text is required',
	'af-id': null,
	'af-start-selected': 1,
	'af-navigation-context-menu-items': `[{\"text\":\"a array link\",\"href\":\"http://www.google.se\"}]`
};

export const LanguagePicker = Template.bind({});
LanguagePicker.args = {
	component: 'digi-navigation-context-menu',
	'af-text': 'Languages',
	'af-id': null,
	'af-start-selected': 1,
	'af-navigation-context-menu-items': `[{"text":"العربية (Arabiska)","lang":"ar","value":"ar","dir":"rtl"},{"text":"دری (Dari)","lang":"prs","value":"prs","dir":"rtl"},{"text":"به پارسی (Persiska)","lang":"fa","value":"fa","dir":"rtl"},{"text":"English (Engelska)","lang":"en","value":"en","dir":"ltr"},{"text":"Русский (Ryska)","lang":"ru","value":"ru","dir":"ltr"},{"text":"Af soomaali (Somaliska)","lang":"so","value":"so","dir":"ltr"},{"text":"Svenska","lang":"sv","value":"sv","dir":"ltr"},{"text":"ትግርኛ (Tigrinska)","lang":"ti","value":"ti","dir":"ltr"}]`,
	'af-icon': 'language'
};
