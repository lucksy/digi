import { enumSelect, Template } from '../../../../../../shared/utils/src';

export default {
	title: 'navigation/digi-navigation-sidebar-button',
	parameters: {
		actions: {
			handles: ['afOnToggle']
		}
	}
};

export const Standard = Template.bind({});
Standard.args = {
	component: 'digi-navigation-sidebar-button',
	'af-text': '',
	'af-aria-label': '',
	'af-id': null,
	children: ''
};
