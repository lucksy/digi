export enum NavigationVerticalMenuActiveIndicatorSize {
  PRIMARY = 'primary',
  SECONDARY = 'secondary'
}
