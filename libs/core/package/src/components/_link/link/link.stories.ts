import { enumSelect, Template } from '../../../../../../shared/utils/src';
import { LinkVariation } from './link-variation.enum';

export default {
	title: 'link/digi-link',
	parameters: {
		actions: {
			handles: ['afOnClick']
		}
	},
	argTypes: {
		'af-variation': enumSelect(LinkVariation)
	}
};

export const Standard = Template.bind({});
Standard.args = {
	component: 'digi-link',
	'af-href': 'A link is required',
	'af-variation': LinkVariation.SMALL,
	'af-target': '',
	children: 'I am a link'
};
