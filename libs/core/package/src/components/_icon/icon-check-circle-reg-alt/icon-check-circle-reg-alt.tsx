import { Component, Prop, State, h } from '@stencil/core';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';

@Component({
	tag: 'digi-icon-check-circle-reg-alt',
	styleUrls: ['icon-check-circle-reg-alt.scss'],
	scoped: true
})
export class IconCheckCircleRegAlt {
	/**
	 * Lägger till ett titleelement i svg:n
	 * @en Adds a title element inside the svg
	 */
	@Prop() afTitle: string;

	/**
	 * Lägger till ett descelement i svg:n
	 * @en Adds a desc element inside the svg
	 */
	@Prop() afDesc: string;

	/**
	 * För att dölja ikonen för skärmläsare. Default är satt till true.
	 * @en Hides the icon for screen readers. Default is set to true.
	 */
	@Prop() afSvgAriaHidden: boolean = true;

	

	/**
* Referera till andra element på sidan för att definiera ett tillgängligt namn.
* @en Reference other elements on the page to define an accessible name.
*/
@Prop() afSvgAriaLabelledby: string;

@State() titleId: string = randomIdGenerator('icontitle');

	render() {
		return (
			<svg
				class="digi-icon-check-circle-reg-alt"
				width="42"
				height="42"
				viewBox="0 0 42 42"
				xmlns="http://www.w3.org/2000/svg"
				aria-hidden={this.afSvgAriaHidden ? 'true' : 'false'}
				aria-labelledby={this.afSvgAriaLabelledby ? this.afSvgAriaLabelledby : this.afTitle ? this.titleId : undefined}
			>
				{this.afTitle && <title id={this.titleId}>{this.afTitle}</title>}
				{this.afDesc && <desc>{this.afDesc}</desc>}
				<g
					class="digi-icon-check-circle-reg-alt__shape"
					fill="currentColor"
					fill-rule="nonzero"
				>
					<path d="M21 39c9.941 0 18-8.059 18-18S30.941 3 21 3 3 11.059 3 21s8.059 18 18 18zm0 3C9.402 42 0 32.598 0 21S9.402 0 21 0s21 9.402 21 21-9.402 21-21 21z" />
					<path d="M17.72 28.415a1.98 1.98 0 002.817-.007l8.905-9.09a1.945 1.945 0 00-.039-2.764 1.98 1.98 0 00-2.785.038l-7.41 7.765-3.833-3.889a1.98 1.98 0 00-2.786-.023 1.945 1.945 0 00-.024 2.765l5.156 5.205z" />
				</g>
			</svg>
		);
	}
}
