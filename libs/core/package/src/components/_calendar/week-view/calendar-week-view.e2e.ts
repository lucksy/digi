import { newE2EPage } from '@stencil/core/testing';

describe('digi-calendar-week-view', () => {
  it('renders', async () => {
    const page = await newE2EPage();
    await page.setContent('<digi-calendar-week-view></digi-calendar-week-view>');

    const element = await page.find('digi-calendar-week-view');
    expect(element).toHaveClass('hydrated');
  });
});
