import { Config } from '@stencil/core';

console.log('AF stencil package config bump bump!!!');

export const config: Config = {
	namespace: 'digi-arbetsformedlingen',
	taskQueue: 'async',
	outputTargets: [
		{
			type: 'docs-json',
			file: '../../../dist/libs/arbetsformedlingen/docs.json'
		}
	]
};
