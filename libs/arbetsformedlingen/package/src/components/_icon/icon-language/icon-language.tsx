import { Component, Prop, State, h } from '@stencil/core';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';

@Component({
	tag: 'digi-icon-language',
	styleUrls: ['icon-language.scss'],
	scoped: true
})
export class IconLanguage {
	/**
	 * Lägger till ett titleelement i svg:n
	 * @en Adds a title element inside the svg
	 */
	@Prop() afTitle: string;

	/**
	 * Lägger till ett descelement i svg:n
	 * @en Adds a desc element inside the svg
	 */
	@Prop() afDesc: string;

	/**
	 * För att dölja ikonen för skärmläsare. Default är satt till true.
	 * @en Hides the icon for screen readers. Default is set to true.
	 */
	@Prop() afSvgAriaHidden: boolean = true;

	

	/**
* Referera till andra element på sidan för att definiera ett tillgängligt namn.
* @en Reference other elements on the page to define an accessible name.
*/
@Prop() afSvgAriaLabelledby: string;

@State() titleId: string = randomIdGenerator('icontitle');

	render() {
		return (
			<svg
				class="digi-icon-language"
				width="26"
				height="20"
				viewBox="0 0 26 20"
				xmlns="http://www.w3.org/2000/svg"
				aria-hidden={this.afSvgAriaHidden ? 'true' : 'false'}
				aria-labelledby={this.afSvgAriaLabelledby ? this.afSvgAriaLabelledby : this.afTitle ? this.titleId : undefined}
			>
				{this.afTitle && <title id={this.titleId}>{this.afTitle}</title>}
				{this.afDesc && <desc>{this.afDesc}</desc>}
				<g class="digi-icon-language__shape" fill="none" fill-rule="evenodd">
					<path
						d="M25.025 0H.975C.437 0 0 .672 0 1.5v17c0 .828.437 1.5.975 1.5h24.05c.538 0 .975-.672.975-1.5v-17c0-.828-.437-1.5-.975-1.5z"
						fill="currentColor"
						fill-rule="nonzero"
					/>
					<path
						d="M13 8.708v-.416c0-.345.28-.625.625-.625h3.542V6.625c0-.345.28-.625.625-.625h.416c.345 0 .625.28.625.625v1.042h3.542c.345 0 .625.28.625.625v.416c0 .345-.28.625-.625.625h-.818c-.39 1.292-1.22 2.654-2.338 3.898.522.473 1.066.9 1.61 1.255.277.18.365.545.2.832l-.207.364a.626.626 0 01-.885.213 14.327 14.327 0 01-1.904-1.485 15.261 15.261 0 01-1.979 1.494.624.624 0 01-.875-.22l-.206-.362a.627.627 0 01.211-.84 13.68 13.68 0 001.68-1.262 16.118 16.118 0 01-1.339-1.72.626.626 0 01.198-.886l.358-.214a.624.624 0 01.838.188c.332.496.715.986 1.13 1.455.781-.882 1.39-1.818 1.746-2.71h-6.17A.625.625 0 0113 8.708zM8.138 3c.291 0 .551.184.648.46l3.39 9.624a.688.688 0 01-.65.916h-.592a.688.688 0 01-.653-.472l-.774-2.348H5.65l-.774 2.348a.688.688 0 01-.653.472H3.63a.687.687 0 01-.648-.916l3.39-9.625A.688.688 0 017.02 3zm-.536 1.906H7.57S7.323 5.99 7.106 6.61L6.13 9.554h2.882L8.036 6.61c-.202-.62-.434-1.704-.434-1.704z"
						fill="#fff"
					/>
				</g>
			</svg>
		);
	}
}
