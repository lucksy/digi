import { Component, Prop, State, h } from '@stencil/core';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';

@Component({
	tag: 'digi-icon-twitter',
	styleUrls: ['icon-twitter.scss'],
	scoped: true
})
export class IconTwitter {
	/**
	 * Lägger till ett titleelement i svg:n
	 * @en Adds a title element inside the svg
	 */
	@Prop() afTitle: string;

	/**
	 * Lägger till ett descelement i svg:n
	 * @en Adds a desc element inside the svg
	 */
	@Prop() afDesc: string;

	/**
	 * För att dölja ikonen för skärmläsare. Default är satt till true.
	 * @en Hides the icon for screen readers. Default is set to true.
	 */
	@Prop() afSvgAriaHidden: boolean = true;

	

	/**
* Referera till andra element på sidan för att definiera ett tillgängligt namn.
* @en Reference other elements on the page to define an accessible name.
*/
@Prop() afSvgAriaLabelledby: string;

@State() titleId: string = randomIdGenerator('icontitle');

	render() {
		return (
			<svg
				class="digi-icon-twitter"
				width="26"
				height="21"
				viewBox="0 0 48 48"
				aria-hidden={this.afSvgAriaHidden ? 'true' : 'false'}
				aria-labelledby={this.afSvgAriaLabelledby ? this.afSvgAriaLabelledby : this.afTitle ? this.titleId : undefined}
				xmlns="http://www.w3.org/2000/svg"
			>
				{this.afTitle && <title id={this.titleId}>{this.afTitle}</title>}
				{this.afDesc && <desc>{this.afDesc}</desc>}
				{/* <path
					class="digi-icon-twitter__shape"
					d="M23.327 5.236c.017.23.017.459.017.688 0 7.003-5.362 15.072-15.161 15.072-3.02 0-5.824-.87-8.183-2.378.429.05.841.066 1.287.066 2.49 0 4.784-.837 6.615-2.264a5.334 5.334 0 01-4.982-3.673c.33.049.66.082 1.006.082.479 0 .957-.066 1.403-.18-2.442-.493-4.273-2.625-4.273-5.2v-.065a5.39 5.39 0 002.408.672A5.285 5.285 0 011.814.972a15.176 15.176 0 0010.988 5.543A5.946 5.946 0 0112.67 5.3c0-2.919 2.376-5.297 5.329-5.297 1.534 0 2.92.64 3.893 1.673a10.53 10.53 0 003.382-1.28 5.289 5.289 0 01-2.343 2.92A10.733 10.733 0 0026 2.497a11.418 11.418 0 01-2.673 2.739z"
					fill="currentColor"
					fill-rule="nonzero"
				/> */}
				<g stroke="none" stroke-width="1" fill-rule="evenodd">
        <g transform="translate(-1343, -720)">
            <g transform="translate(1343, 720)">
                <path class="digi-icon-twitter__shape" d="M41.3220428,0 L26.1295151,17.6476724 L13.9953192,0 L0,0 L18.3492853,26.6864062 L0,48 L4.14642179,48 L20.190081,29.363458 L33.0046417,48 L47,48 L27.9703891,20.3247242 L27.9713683,20.3247242 L45.4682296,0 L41.3220428,0 Z M21.7995653,26.8581887 L21.7995653,26.8571687 L19.9238763,24.1936791 L5,3 L11.4251309,3 L23.4689124,20.1039785 L25.3445619,22.7674681 L41,45 L34.5748691,45 L21.7995653,26.8581887 Z" id="Icon"/>
            </g>
        </g>
    </g>
			</svg>
		);
	}
}
