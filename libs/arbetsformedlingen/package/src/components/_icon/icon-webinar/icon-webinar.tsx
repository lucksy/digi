import { Component, h, Prop, State } from '@stencil/core';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';

@Component({
	tag: 'digi-icon-webinar',
	styleUrl: 'icon-webinar.scss',
	scoped: true
})
export class IconWebinar {
	/**
	 * Lägger till ett titleelement i svg:n
	 * @en Adds a title element inside the svg
	 */
	@Prop() afTitle: string;

	/**
	 * Lägger till ett descelement i svg:n
	 * @en Adds a desc element inside the svg
	 */
	@Prop() afDesc: string;

	/**
	 * För att dölja ikonen för skärmläsare. Default är satt till true.
	 * @en Hides the icon for screen readers. Default is set to true.
	 */
	@Prop() afSvgAriaHidden: boolean = true;

	

	/**
* Referera till andra element på sidan för att definiera ett tillgängligt namn.
* @en Reference other elements on the page to define an accessible name.
*/
@Prop() afSvgAriaLabelledby: string;

@State() titleId: string = randomIdGenerator('icontitle');

	render() {
		return (
			<svg
				class="digi-icon-webinar"
				width="48"
				height="48"
				viewBox="0 0 48 48"
				aria-hidden={this.afSvgAriaHidden ? 'true' : 'false'}
				aria-labelledby={this.afSvgAriaLabelledby ? this.afSvgAriaLabelledby : this.afTitle ? this.titleId : undefined}
				xmlns="http://www.w3.org/2000/svg"
			>
				{this.afTitle && <title id={this.titleId}>{this.afTitle}</title>}
				{this.afDesc && <desc>{this.afDesc}</desc>}
				<path
					class="digi-icon-webinar__shape"
					d="M47,3.55271368e-15 C47.5522847,3.55271368e-15 48,0.44771525 48,1 L48,41.4615385 C48,42.0138232 47.5522847,42.4615385 47,42.4615385 L38.769,42.461 L38.7692308,47 C38.7692308,47.5522847 38.3215155,48 37.7692308,48 L10.2307692,48 C9.67848448,48 9.23076923,47.5522847 9.23076923,47 L9.23,42.461 L1,42.4615385 C0.44771525,42.4615385 0,42.0138232 0,41.4615385 L0,1 C0,0.44771525 0.44771525,3.55271368e-15 1,3.55271368e-15 L47,3.55271368e-15 Z M42.4615385,5.53846154 L5.53846154,5.53846154 L5.53846154,33.2307692 L42.4615385,33.2307692 L42.4615385,5.53846154 Z M29.4230769,24 C33.5652125,24 36.9230769,26.4796537 36.9230769,29.5384615 L12.9230769,29.5384615 C12.9230769,26.4796537 16.2809413,24 20.4230769,24 L29.4230769,24 Z M24.9230769,9.23076923 C28.4916861,9.23076923 31.3846154,12.5369742 31.3846154,16.6153846 C31.3846154,20.6937951 28.4916861,24 24.9230769,24 C21.3544678,24 18.4615385,20.6937951 18.4615385,16.6153846 C18.4615385,12.5369742 21.3544678,9.23076923 24.9230769,9.23076923 Z"
					fill="currentColor"
					fill-rule="nonzero"
				/>
			</svg>
		);
	}
}
