import { Component, Prop, State, h } from '@stencil/core';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';

@Component({
  tag: 'digi-icon-envelope-filled',
  styleUrls: ['icon-envelope-filled.scss'],
  scoped: true,
})
export class IconEnvelopeFilled {
  /**
   * Lägger till ett titleelement i svg:n
   * @en Adds a title element inside the svg
   */
  @Prop() afTitle: string;

  /**
   * Lägger till ett descelement i svg:n
   * @en Adds a desc element inside the svg
   */
  @Prop() afDesc: string;

  /**
   * För att dölja ikonen för skärmläsare. Default är satt till true.
   * @en Hides the icon for screen readers. Default is set to true.
   */
  @Prop() afSvgAriaHidden: boolean = true;

  /**
* Referera till andra element på sidan för att definiera ett tillgängligt namn.
* @en Reference other elements on the page to define an accessible name.
*/
@Prop() afSvgAriaLabelledby: string;

@State() titleId: string = randomIdGenerator('icontitle');

  render() {
    return (
      <svg
        class="digi-icon-envelope-filled"
        width="25"
        height="19"
        viewBox="0 0 25 19"
        xmlns="http://www.w3.org/2000/svg"
        aria-hidden={this.afSvgAriaHidden ? 'true' : 'false'}
        aria-labelledby={this.afSvgAriaLabelledby ? this.afSvgAriaLabelledby : this.afTitle ? this.titleId : undefined}
      >
        {this.afTitle && <title id={this.titleId}>{this.afTitle}</title>}
        {this.afDesc && <desc>{this.afDesc}</desc>}
        <path
          class="digi-icon-envelope-filled__shape"
          fill="currentColor"
          d="M25 16.63V6.51a.28.28 0 00-.06-.18.29.29 0 00-.41 0c-1.1.86-2.54 2-7.52 5.62-1 .74-2.78 2.35-4.51 2.36S9 12.67 8 11.9C3 8.28 1.57 7.14.47 6.28a.26.26 0 00-.19-.06.28.28 0 00-.28.3v10.11A2.36 2.36 0 002.35 19h20.28A2.36 2.36 0 0025 16.63z"
        />
        <path
          class="digi-icon-envelope-filled__shape"
          fill="currentColor"
          d="M2.35 0A2.37 2.37 0 000 2.38v.94a1.2 1.2 0 00.45.93c1.49 1.19 2 1.61 8.47 6.37.82.61 2.45 2.07 3.58 2.05s2.77-1.44 3.59-2.05c6.47-4.76 7-5.18 8.46-6.37a1.14 1.14 0 00.45-.93v-.94A2.36 2.36 0 0022.66 0H2.35z"
        />
      </svg>
    );
  }
}
