import { Component, h, Prop, State } from '@stencil/core';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';

@Component({
	tag: 'digi-icon-comunication-play',
	styleUrl: 'icon-comunication-play.scss',
	scoped: true
})
export class IconComunicationPlay {
	/**
	 * Lägger till ett titleelement i svg:n
	 * @en Adds a title element inside the svg
	 */
	@Prop() afTitle: string;

	/**
	 * Lägger till ett descelement i svg:n
	 * @en Adds a desc element inside the svg
	 */
	@Prop() afDesc: string;

	/**
	 * För att dölja ikonen för skärmläsare. Default är satt till true.
	 * @en Hides the icon for screen readers. Default is set to true.
	 */
	@Prop() afSvgAriaHidden: boolean = true;

	

	/**
* Referera till andra element på sidan för att definiera ett tillgängligt namn.
* @en Reference other elements on the page to define an accessible name.
*/
@Prop() afSvgAriaLabelledby: string;

@State() titleId: string = randomIdGenerator('icontitle');

	render() {
		return (
			<svg
				class="digi-icon-comunication-play"
				width="48"
				height="48"
				viewBox="0 0 48 48"
				xmlns="http://www.w3.org/2000/svg"
				aria-hidden={this.afSvgAriaHidden ? 'true' : 'false'}
				aria-labelledby={this.afSvgAriaLabelledby ? this.afSvgAriaLabelledby : this.afTitle ? this.titleId : undefined}
			>
				{this.afTitle && <title id={this.titleId}>{this.afTitle}</title>}
				{this.afDesc && <desc>{this.afDesc}</desc>}
				<path
					class="digi-icon-comunication-play__shape"
					d="M20.1290323,33.5709677 L20.1290323,14.4290323 C20.1290323,13.3935484 21.3870968,12.8709677 22.1129032,13.6064516 L31.6258065,23.1774194 C32.0806452,23.6322581 32.0806452,24.3580645 31.6258065,24.8129032 L22.1129032,34.383871 C21.3870968,35.1290323 20.1290323,34.6064516 20.1290323,33.5709677 Z M24,0 C37.2580645,0 48,10.7419355 48,24 C48,37.2580645 37.2580645,48 24,48 C10.7419355,48 0,37.2580645 0,24 C0,10.7419355 10.7419355,0 24,0 Z M24,4.64516129 C13.3064516,4.64516129 4.64516129,13.3064516 4.64516129,24 C4.64516129,34.6935484 13.3064516,43.3548387 24,43.3548387 C34.6935484,43.3548387 43.3548387,34.6935484 43.3548387,24 C43.3548387,13.3064516 34.6935484,4.64516129 24,4.64516129 Z"
					fill="currentColor"
					fill-rule="nonzero"
				/>
			</svg>
		);
	}
}
