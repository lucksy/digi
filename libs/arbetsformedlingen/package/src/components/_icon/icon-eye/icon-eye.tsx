import { Component, Prop, State, h } from '@stencil/core';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';

@Component({
	tag: 'digi-icon-eye',
	styleUrls: ['icon-eye.scss'],
	scoped: true
})
export class IconEye {
	/**
	 * Lägger till ett titleelement i svg:n
	 * @en Adds a title element inside the svg
	 */
	@Prop() afTitle: string;

	/**
	 * Lägger till ett descelement i svg:n
	 * @en Adds a desc element inside the svg
	 */
	@Prop() afDesc: string;

	/**
	 * För att dölja ikonen för skärmläsare. Default är satt till true.
	 * @en Hides the icon for screen readers. Default is set to true.
	 */
	@Prop() afSvgAriaHidden: boolean = true;

	

	/**
* Referera till andra element på sidan för att definiera ett tillgängligt namn.
* @en Reference other elements on the page to define an accessible name.
*/
@Prop() afSvgAriaLabelledby: string;

@State() titleId: string = randomIdGenerator('icontitle');

	render() {
		return (
			<svg
				class="digi-icon-eye"
				width="29"
				height="18"
				viewBox="0 0 29 18"
				xmlns="http://www.w3.org/2000/svg"
				aria-hidden={this.afSvgAriaHidden ? 'true' : 'false'}
				aria-labelledby={this.afSvgAriaLabelledby ? this.afSvgAriaLabelledby : this.afTitle ? this.titleId : undefined}
			>
				{this.afTitle && <title id={this.titleId}>{this.afTitle}</title>}
				{this.afDesc && <desc>{this.afDesc}</desc>}
				<path
					class="digi-icon-eye__shape"
					d="M28.616 7.808C25.782 3.128 20.496 0 14.475 0 8.45 0 3.167 3.13.334 7.808a2.294 2.294 0 000 2.384C3.168 14.872 8.454 18 14.474 18c6.025 0 11.31-3.13 14.142-7.808a2.294 2.294 0 000-2.384zm-14.141 7.844C9.32 15.652 4.82 12.977 2.412 9c2.22-3.665 6.216-6.225 10.865-6.604.492.494.796 1.166.796 1.908 0 1.513-1.26 2.74-2.815 2.74-1.554 0-2.814-1.227-2.814-2.74v-.002c-.513.934-.804 2-.804 3.133 0 3.674 3.06 6.652 6.835 6.652s6.835-2.978 6.835-6.652a6.509 6.509 0 00-1.402-4.036C22.703 4.53 25.03 6.51 26.538 9c-2.409 3.977-6.909 6.652-12.063 6.652z"
					fill="currentColor"
					fill-rule="nonzero"
				/>
			</svg>
		);
	}
}
