import { Component, Prop, State, h } from '@stencil/core';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';

@Component({
  tag: 'digi-icon-pen',
  styleUrls: ['icon-pen.scss'],
  scoped: true,
})
export class IconPen {
  /**
   * Lägger till ett titleelement i svg:n
   * @en Adds a title element inside the svg
   */
  @Prop() afTitle: string;

  /**
   * Lägger till ett descelement i svg:n
   * @en Adds a desc element inside the svg
   */
  @Prop() afDesc: string;

  /**
 * För att dölja ikonen för skärmläsare. Default är satt till true.
 * @en Hides the icon for screen readers. Default is set to true.
 */
  @Prop() afSvgAriaHidden: boolean = true;

  /**
* Referera till andra element på sidan för att definiera ett tillgängligt namn.
* @en Reference other elements on the page to define an accessible name.
*/
@Prop() afSvgAriaLabelledby: string;

@State() titleId: string = randomIdGenerator('icontitle');

  render() {
    return (
      <svg
        class="digi-icon-pen"
        width="26"
        height="26"
        viewBox="0 0 26 26"
        xmlns="http://www.w3.org/2000/svg"
        aria-hidden={this.afSvgAriaHidden ? 'true' : 'false'}
        aria-labelledby={this.afSvgAriaLabelledby ? this.afSvgAriaLabelledby : this.afTitle ? this.titleId : undefined}
      >
        {this.afTitle && <title id={this.titleId}>{this.afTitle}</title>}
        {this.afDesc && <desc>{this.afDesc}</desc>}
        <path
          class="digi-icon-pen__shape"
          d="M15.295 5.068l5.637 5.637a.61.61 0 010 .863L7.15 25.35l-5.794.64A1.218 1.218 0 01.01 24.644l.645-5.8L14.432 5.069a.61.61 0 01.863 0zm9.989-1.3L22.232.716a2.443 2.443 0 00-3.448 0l-2.341 2.341a.61.61 0 000 .863l5.637 5.637a.61.61 0 00.863 0l2.341-2.341a2.434 2.434 0 000-3.448z"
          fill="currentColor"
          fill-rule="nonzero"
        />
      </svg>
    );
  }
}
