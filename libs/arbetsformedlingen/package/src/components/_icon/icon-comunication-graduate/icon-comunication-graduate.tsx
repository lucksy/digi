import { Component, h, Prop, State } from '@stencil/core';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';

@Component({
  tag: 'digi-icon-comunication-graduate',
  styleUrl: 'icon-comunication-graduate.scss',
  scoped: true
})
export class IconComunicationGraduate {
  /**
   * Lägger till ett titleelement i svg:n
   * @en Adds a title element inside the svg
   */
  @Prop() afTitle: string;

  /**
   * Lägger till ett descelement i svg:n
   * @en Adds a desc element inside the svg
   */
  @Prop() afDesc: string;

  /**
 * För att dölja ikonen för skärmläsare. Default är satt till true.
 * @en Hides the icon for screen readers. Default is set to true.
 */
  @Prop() afSvgAriaHidden: boolean = true;

  

  /**
* Referera till andra element på sidan för att definiera ett tillgängligt namn.
* @en Reference other elements on the page to define an accessible name.
*/
@Prop() afSvgAriaLabelledby: string;

@State() titleId: string = randomIdGenerator('icontitle');

  render() {
    return (
      <svg
        class="digi-icon-comunication-graduate"
        width="48"
        height="48"
        viewBox="0 0 48 48"
        xmlns="http://www.w3.org/2000/svg"
        aria-hidden={this.afSvgAriaHidden ? 'true' : 'false'}
        aria-labelledby={this.afSvgAriaLabelledby ? this.afSvgAriaLabelledby : this.afTitle ? this.titleId : undefined}
      >
        {this.afTitle && <title id={this.titleId}>{this.afTitle}</title>}
        {this.afDesc && <desc>{this.afDesc}</desc>}
        <path
          class="digi-icon-comunication-graduate__shape"
          d="M5.17857143,9.375 L5.78571429,9.5625 L5.78571429,13.0875 C5.16071429,13.48125 4.71428571,14.165625 4.71428571,14.990625 C4.71428571,15.778125 5.125,16.434375 5.70535714,16.8375 L4.3125,22.6875 C4.16071429,23.334375 4.5,24 4.99107143,24 L8.72321429,24 C9.21428571,24 9.55357143,23.334375 9.40178571,22.6875 L8.00892857,16.846875 C8.58928571,16.44375 9,15.7875 9,15 C9,14.175 8.55357143,13.490625 7.92857143,13.096875 L7.92857143,10.2375 L12.0892857,11.53125 C11.5,13.06875 11.1428571,14.7375 11.1428571,16.5 C11.1428571,23.953125 16.9017857,30 24,30 C31.0982143,30 36.8571429,23.953125 36.8571429,16.5 C36.8571429,14.7375 36.5,13.06875 35.9107143,11.53125 L42.8214286,9.375 C44.3928571,8.8875 44.3928571,6.1125 42.8214286,5.625 L25.5089286,0.234375 C25.0089286,0.075 24.5089286,0 24,0 C23.4910714,0 22.9910714,0.075 22.4910714,0.234375 L5.17857143,5.625 C3.60714286,6.1125 3.60714286,8.8875 5.17857143,9.375 Z M24,25.5 C19.2767857,25.5 15.4285714,21.459375 15.4285714,16.5 C15.4285714,15.178125 15.7232143,13.940625 16.2142857,12.815625 L22.5,14.775 C23.8214286,15.1875 24.9285714,14.9625 25.5178571,14.775 L31.8035714,12.815625 C32.2946429,13.940625 32.5892857,15.1875 32.5892857,16.5 C32.5714286,21.459375 28.7232143,25.5 24,25.5 Z M23.7142857,4.546875 C23.8035714,4.51875 24.0089286,4.4625 24.2946429,4.546875 L33.7767857,7.5 L24.2857143,10.453125 C24.0982143,10.509375 23.9107143,10.51875 23.7053571,10.453125 L14.2232143,7.5 L23.7142857,4.546875 Z M32.5178571,30.05625 L24,37.5 L15.4821429,30.05625 C9.09821429,30.346875 4,35.83125 4,42.6 L4,43.5 C4,45.984375 5.91964286,48 8.28571429,48 L39.7142857,48 C42.0803571,48 44,45.984375 44,43.5 L44,42.6 C44,35.83125 38.9017857,30.346875 32.5178571,30.05625 L32.5178571,30.05625 Z M21.8571429,43.5 L8.28571429,43.5 L8.28571429,42.6 C8.28571429,38.8125 10.7767857,35.625 14.1785714,34.771875 L21.8571429,41.484375 L21.8571429,43.5 Z M39.7142857,43.5 L26.1428571,43.5 L26.1428571,41.484375 L33.8214286,34.771875 C37.2232143,35.625 39.7142857,38.8125 39.7142857,42.6 L39.7142857,43.5 Z"
          fill="currentColor"
          fill-rule="nonzero"
        />
      </svg>
    );
  }
}
