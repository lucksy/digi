import { Component, Prop, State, h } from '@stencil/core';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';

@Component({
	tag: 'digi-icon-unlock',
	styleUrls: ['icon-unlock.scss'],
	scoped: true
})
export class IconUnlock {
	/**
	 * Lägger till ett titleelement i svg:n
	 * @en Adds a title element inside the svg
	 */
	@Prop() afTitle: string;

	/**
	 * Lägger till ett descelement i svg:n
	 * @en Adds a desc element inside the svg
	 */
	@Prop() afDesc: string;

	/**
	 * För att dölja ikonen för skärmläsare. Default är satt till true.
	 * @en Hides the icon for screen readers. Default is set to true.
	 */
	@Prop() afSvgAriaHidden: boolean = true;

	

	/**
* Referera till andra element på sidan för att definiera ett tillgängligt namn.
* @en Reference other elements on the page to define an accessible name.
*/
@Prop() afSvgAriaLabelledby: string;

@State() titleId: string = randomIdGenerator('icontitle');

	render() {
		return (
			<svg
				class="digi-icon-unlock"
				width="29"
				height="26"
				viewBox="0 0 29 26"
				aria-hidden={this.afSvgAriaHidden ? 'true' : 'false'}
				aria-labelledby={this.afSvgAriaLabelledby ? this.afSvgAriaLabelledby : this.afTitle ? this.titleId : undefined}
				xmlns="http://www.w3.org/2000/svg"
			>
				{this.afTitle && <title id={this.titleId}>{this.afTitle}</title>}
				{this.afDesc && <desc>{this.afDesc}</desc>}
				<path
					class="digi-icon-unlock__shape"
					d="M21.322.05c-4.23.016-7.628 3.523-7.628 7.78v3.574H2.417A2.425 2.425 0 000 13.836v9.731A2.425 2.425 0 002.417 26h17.722a2.425 2.425 0 002.417-2.433v-9.73a2.425 2.425 0 00-2.417-2.433h-2.417V7.8c0-2.007 1.596-3.674 3.59-3.695 2.014-.02 3.66 1.627 3.66 3.65v4.054a1.21 1.21 0 001.209 1.216h1.61A1.21 1.21 0 0029 11.81V7.754C29 3.497 25.551.035 21.322.051z"
					fill="currentColor"
					fill-rule="nonzero"
				/>
			</svg>
		);
	}
}
