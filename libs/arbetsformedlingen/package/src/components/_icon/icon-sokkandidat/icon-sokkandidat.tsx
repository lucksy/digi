import { Component, Prop, State, h } from '@stencil/core';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';

@Component({
  tag: 'digi-icon-sokkandidat',
  styleUrls: ['icon-sokkandidat.scss'],
  scoped: true,
})
export class IconSokkandidat {
  /**
   * Lägger till ett titleelement i svg:n
   * @en Adds a title element inside the svg
   */
  @Prop() afTitle: string;

  /**
   * Lägger till ett descelement i svg:n
   * @en Adds a desc element inside the svg
   */
  @Prop() afDesc: string;

  /**
 * För att dölja ikonen för skärmläsare. Default är satt till true.
 * @en Hides the icon for screen readers. Default is set to true.
 */
  @Prop() afSvgAriaHidden: boolean = true;

  /**
* Referera till andra element på sidan för att definiera ett tillgängligt namn.
* @en Reference other elements on the page to define an accessible name.
*/
@Prop() afSvgAriaLabelledby: string;

@State() titleId: string = randomIdGenerator('icontitle');

  render() {
    return (
      <svg
        class="digi-icon-sokkandidat"
        width="29"
        height="29"
        viewBox="0 0 29 29"
        xmlns="http://www.w3.org/2000/svg"
        aria-hidden={this.afSvgAriaHidden ? 'true' : 'false'}
        aria-labelledby={this.afSvgAriaLabelledby ? this.afSvgAriaLabelledby : this.afTitle ? this.titleId : undefined}
      >
        {this.afTitle && <title id={this.titleId}>{this.afTitle}</title>}
        {this.afDesc && <desc>{this.afDesc}</desc>}
        <g
          class="digi-icon-sokkandidat__shape"
          fill="currentColor"
          fill-rule="evenodd"
        >
          <path d="M20.484 3.52A11.969 11.969 0 0011.997 0 11.969 11.969 0 003.51 3.52c-4.68 4.68-4.68 12.28 0 16.968A11.96 11.96 0 0011.997 24a11.96 11.96 0 008.487-3.512c4.688-4.688 4.688-12.288 0-16.968zm-1.696 15.272a9.561 9.561 0 01-6.791 2.808c-2.56 0-4.976-1-6.783-2.808A9.537 9.537 0 012.398 12c0-2.56 1-4.976 2.816-6.784A9.517 9.517 0 0111.997 2.4c2.568 0 4.976 1 6.791 2.816 3.744 3.744 3.744 9.832 0 13.576zM29 26.701s-.714.366-1.305.959c-.527.528-.99 1.34-.99 1.34-2.456-1.811-4.775-3.623-6.705-5.564a13.825 13.825 0 001.922-1.608A13.458 13.458 0 0023.454 20c1.938 1.925 3.738 4.248 5.546 6.701" />
          <path d="M17.177 18C18.904 16.647 20 14.63 20 12.377 20 8.303 16.416 5 12 5s-8 3.303-8 7.377C4 14.63 5.096 16.647 6.823 18c.43-.595 1.246-1.141 2.688-1.91.622-.332 1.245-.664 1.245-.996 0-1.036-1.867-1.66-1.867-4.27 0-2.037 1.244-3.365 3.111-3.365 1.867 0 3.111 1.328 3.111 3.365 0 2.61-1.867 3.238-1.867 4.27 0 .332.623.664 1.245.996 1.442.769 2.257 1.315 2.688 1.91z" />
        </g>
      </svg>
    );
  }
}
