import { Component, Prop, State, h } from '@stencil/core';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';

@Component({
	tag: 'digi-icon-cooperation',
	styleUrls: ['icon-cooperation.scss'],
	scoped: true
})
export class IconCooperation {
	/**
	 * Lägger till ett titleelement i svg:n
	 * @en Adds a title element inside the svg
	 */
	@Prop() afTitle: string;

	/**
	 * Lägger till ett descelement i svg:n
	 * @en Adds a desc element inside the svg
	 */
	@Prop() afDesc: string;

	/**
	 * För att dölja ikonen för skärmläsare. Default är satt till true.
	 * @en Hides the icon for screen readers. Default is set to true.
	 */
	@Prop() afSvgAriaHidden: boolean = true;

	

	/**
* Referera till andra element på sidan för att definiera ett tillgängligt namn.
* @en Reference other elements on the page to define an accessible name.
*/
@Prop() afSvgAriaLabelledby: string;

@State() titleId: string = randomIdGenerator('icontitle');

	render() {
		return (
			<svg
				class="digi-icon-cooperation"
				width="20"
				height="19"
				viewBox="0 0 20 19"
				xmlns="http://www.w3.org/2000/svg"
				aria-hidden={this.afSvgAriaHidden ? 'true' : 'false'}
				aria-labelledby={this.afSvgAriaLabelledby ? this.afSvgAriaLabelledby : this.afTitle ? this.titleId : undefined}
			>
				{this.afTitle && <title id={this.titleId}>{this.afTitle}</title>}
				{this.afDesc && <desc>{this.afDesc}</desc>}
				<g
					class="digi-icon-cooperation__shape"
					fill="currentColor"
					fill-rule="evenodd"
				>
					<path d="M9.697 10.222l4.508 4.75-.957 1.008-4.508-4.75-.82.863 4.645 4.607-.956 1.008-4.645-4.607-.683.72 4.508 4.75-.059.063a1.055 1.055 0 01-1.53.014s-7.908-8.085-7.99-8.205C.408 9.284 0 7.854 0 6.424 0 2.784 2.528 0 5.841 0c1.854 0 3.025.897 3.025.897C5.652 3.361 4.788 3.668 4.788 5.25c0 1.27.927 2.263 2.073 2.263.445 0 .877-.147 1.248-.427l.571-.428.33-.25s1.233 0 2.599 1.511c2.305 2.55 3.677 4.316 3.677 4.316a1.307 1.307 0 01-.068 1.67l-.194.205-4.508-4.751-.82.864z" />
					<path d="M17.11 12.275s-1.926-3.25-3.577-4.982c-1.65-1.733-2.82-2.26-4.185-2.26-.554 0-1.451 1.294-2.249 1.294-.563 0-.988-.469-.988-1.056 0-.374.173-.753.49-.99C10.931.995 11.724 0 14.114 0 17.45 0 20 2.791 20 6.444c0 3.293-1.264 4.049-2.89 5.83z" />
				</g>
			</svg>
		);
	}
}
