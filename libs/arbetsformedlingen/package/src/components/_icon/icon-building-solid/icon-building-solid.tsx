import { Component, h, Prop, State } from '@stencil/core';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';

@Component({
	tag: 'digi-icon-building-solid',
	styleUrl: 'icon-building-solid.scss',
	scoped: true
})
export class IconBuildingSolid {
	/**
	 * Lägger till ett titleelement i svg:n
	 * @en Adds a title element inside the svg
	 */
	@Prop() afTitle: string;

	/**
	 * Lägger till ett descelement i svg:n
	 * @en Adds a desc element inside the svg
	 */
	@Prop() afDesc: string;

	/**
	 * För att dölja ikonen för skärmläsare. Default är satt till true.
	 * @en Hides the icon for screen readers. Default is set to true.
	 */
	@Prop() afSvgAriaHidden: boolean = true;

	

	/**
* Referera till andra element på sidan för att definiera ett tillgängligt namn.
* @en Reference other elements on the page to define an accessible name.
*/
@Prop() afSvgAriaLabelledby: string;

@State() titleId: string = randomIdGenerator('icontitle');

	render() {
		return (
			<svg
				class="digi-icon-building-solid"
				width="48"
				height="48"
				viewBox="0 0 48 48"
				xmlns="http://www.w3.org/2000/svg"
				aria-hidden={this.afSvgAriaHidden ? 'true' : 'false'}
				aria-labelledby={this.afSvgAriaLabelledby ? this.afSvgAriaLabelledby : this.afTitle ? this.titleId : undefined}
			>
				{this.afTitle && <title id={this.titleId}>{this.afTitle}</title>}
				{this.afDesc && <desc>{this.afDesc}</desc>}
				<path
					class="digi-icon-building-solid__shape"
					d="M44.1085714,45 L42.2228571,45 L42.2228571,2.25 C42.2228571,1.00734375 41.2097571,0 39.96,0 L8.28,0 C7.03024286,0 6.01714286,1.00734375 6.01714286,2.25 L6.01714286,45 L4.13142857,45 C3.50659714,45 3,45.5037187 3,46.125 L3,48 L45.24,48 L45.24,46.125 C45.24,45.5037187 44.7334029,45 44.1085714,45 Z M15.0685714,7.125 C15.0685714,6.50371875 15.5751686,6 16.2,6 L19.9714286,6 C20.59626,6 21.1028571,6.50371875 21.1028571,7.125 L21.1028571,10.875 C21.1028571,11.4962812 20.59626,12 19.9714286,12 L16.2,12 C15.5751686,12 15.0685714,11.4962812 15.0685714,10.875 L15.0685714,7.125 Z M15.0685714,16.125 C15.0685714,15.5037187 15.5751686,15 16.2,15 L19.9714286,15 C20.59626,15 21.1028571,15.5037187 21.1028571,16.125 L21.1028571,19.875 C21.1028571,20.4962812 20.59626,21 19.9714286,21 L16.2,21 C15.5751686,21 15.0685714,20.4962812 15.0685714,19.875 L15.0685714,16.125 Z M19.9714286,30 L16.2,30 C15.5751686,30 15.0685714,29.4962813 15.0685714,28.875 L15.0685714,25.125 C15.0685714,24.5037188 15.5751686,24 16.2,24 L19.9714286,24 C20.59626,24 21.1028571,24.5037188 21.1028571,25.125 L21.1028571,28.875 C21.1028571,29.4962813 20.59626,30 19.9714286,30 Z M27.1371429,45 L21.1028571,45 L21.1028571,37.125 C21.1028571,36.5037187 21.6094543,36 22.2342857,36 L26.0057143,36 C26.6305457,36 27.1371429,36.5037187 27.1371429,37.125 L27.1371429,45 Z M33.1714286,28.875 C33.1714286,29.4962813 32.6648314,30 32.04,30 L28.2685714,30 C27.64374,30 27.1371429,29.4962813 27.1371429,28.875 L27.1371429,25.125 C27.1371429,24.5037188 27.64374,24 28.2685714,24 L32.04,24 C32.6648314,24 33.1714286,24.5037188 33.1714286,25.125 L33.1714286,28.875 Z M33.1714286,19.875 C33.1714286,20.4962812 32.6648314,21 32.04,21 L28.2685714,21 C27.64374,21 27.1371429,20.4962812 27.1371429,19.875 L27.1371429,16.125 C27.1371429,15.5037187 27.64374,15 28.2685714,15 L32.04,15 C32.6648314,15 33.1714286,15.5037187 33.1714286,16.125 L33.1714286,19.875 Z M33.1714286,10.875 C33.1714286,11.4962812 32.6648314,12 32.04,12 L28.2685714,12 C27.64374,12 27.1371429,11.4962812 27.1371429,10.875 L27.1371429,7.125 C27.1371429,6.50371875 27.64374,6 28.2685714,6 L32.04,6 C32.6648314,6 33.1714286,6.50371875 33.1714286,7.125 L33.1714286,10.875 Z"
					fill="currentColor"
					fill-rule="nonzero"
				/>
			</svg>
		);
	}
}
