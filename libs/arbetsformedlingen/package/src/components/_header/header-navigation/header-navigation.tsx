import {
	Component,
	h,
	Listen,
	Prop,
	State,
	Event,
	EventEmitter,
	Watch,
	Element
} from '@stencil/core';
import { UtilBreakpointObserverBreakpoints } from '@digi/arbetsformedlingen';
import { HTMLStencilElement } from '@stencil/core/internal';
/**
 * 
 *
 * @swedishName Sidhuvud-meny
 */
@Component({
	tag: 'digi-header-navigation',
	styleUrl: 'header-navigation.scss',
	scoped: true
})
export class HeaderNavigation {
	private _wrapper: HTMLElement;
	private _closeFocusableElement: HTMLElement;
	private _focusableElementsSelectors =
		'a, input, select, textarea, button, button:not(hidden), iframe, object, [tabindex="0"]';
	private focusableElements = [];
	private firstFocusableEl;
	private lastFocusableEl;

	@Element() hostElement: HTMLStencilElement;

	@State() isMobile: boolean;

	@State() isVisible: boolean;

	@State() children: Array<any> = [];

	/**
	 * Stäng-knappens text
	 * @en The close buttons text
	 */
	@Prop() afCloseButtonText: string;

	/**
	 * Sätter attributet 'aria-label'.
	 * @en Set `aria-label` attribute.
	 */
	@Prop() afNavAriaLabel: string;

	/**
	 * Sätt attributet 'aria-label' på stäng-knappen
	 * @en Set button `aria-label` attribute to the close button
	 */
	@Prop() afCloseButtonAriaLabel: string;

	/**
	 * Element som ska få fokus när menyn öppnas. Sätts ingen
	 * så får rubriken eller stäng-knappen fokus
	 * @en Element that is focused when menu is opened,
	 * defaults to title or close button
	 */
	@Prop() afFocusableElement: string;

	/**
	 * Element som ska få fokus när menyn stängs.
	 * Sätts ingen så får knappen som öppnade menyn fokus
	 * @en Element that is focused when menu is closed,
	 * defaults to toggle button that opened the sidebar
	 */
	@Prop() afCloseFocusableElement: string;

	/**
	 * Visar eller döljer sidmenyn i mobilläge. Om satt till true så är den öppen per standard.
	 * @en Shows or hides the sidebar in mobile. If set to true it will be open by default.
	 */
	@Prop() afActive: boolean = false;

	/**
	 * Lägger en skugga bakom menyn som täcker sidinnehållet när menyn är aktiv
	 * @en Applies a unscrollable backdrop over the page content when the sidebar is active
	 */
	@Prop() afBackdrop: boolean = true;

	/**
	 * Stängknappens 'onclick'-event
	 * @en Close button's 'onclick' event
	 */
	@Event() afOnClose: EventEmitter;

	/**
	 * Stängning av sidofält med esc-tangenten
	 * @en At close/open of sidebar using esc-key
	 */
	@Event() afOnEsc: EventEmitter;

	/**
	 * Stängning av sidofält med klick på skuggan bakom menyn
	 * @en At close of sidebar when clicking on backdrop
	 */
	@Event() afOnBackdropClick: EventEmitter;

	/**
	 * Vid öppning/stängning av sidofält
	 * @en At close/open of sidebar
	 */
	@Event() afOnToggle: EventEmitter;
	//private _observer: any;

	enablePageScroll() {
		document.body.style.height = '';
		document.body.style.overflowY = '';
		document.body.style.paddingRight = '';
	}

	disablePageScroll() {
		document.body.style.height = '100vh';
		document.body.style.overflowY = 'hidden';
	}

	@Listen('afOnToggle', { target: 'window' })
	toggleHandler(e: any) {
		this._closeFocusableElement = e.target.querySelector(
			'.digi-navigation-sidebar-button button'
		);
		if (e.target.matches('digi-navigation-sidebar-button')) {
			this.afActive = true;
		}
	}

	get shouldUseFocusTrap() {
		return this.isMobile && this.focusableElements.length > 0;
	}

	closeHandler(e: any) {
		this.afActive = false;
		this.afOnClose.emit(e);
	}

	escHandler(e) {
		if (this.isMobile) {
			this.afActive = false;
			this.afOnClose.emit(e);
		}
	}

	tabHandler(e) {
		if (this.shouldUseFocusTrap) {
			if (document.activeElement === this.lastFocusableEl) {
				e.detail.preventDefault();
				this.firstFocusableEl.focus();
			}
		}
	}

	backdropClickHandler(e) {
		this.afActive = false;
		this.afOnClose.emit(e);
	}

	shiftHandler(e) {
		if (this.shouldUseFocusTrap) {
			if (document.activeElement === this.firstFocusableEl) {
				e.detail.preventDefault();
				this.lastFocusableEl.focus();
			}
		}
	}

	@Listen('afOnClick')
	clickHandler(e) {
		// If clicking on toggle button, refresh the focusable items list
		// with the new sub level of items
		if (this.shouldUseFocusTrap) {
			const el = e.detail.target as HTMLElement;
			if (el.tagName.toLowerCase() === 'button') {
				setTimeout(() => {
					this.getFocusableItems();
				}, 100);
			}
		}
	}

	@Watch('isMobile')
	setMobileView(isMobile: boolean) {
		isMobile && this.afActive
			? (this.disablePageScroll(), this.getFocusableItems())
			: this.enablePageScroll();
	}

	@Listen('afOnChange')
	breakpointChange(e: any) {
		this.isVisible = false;
		const type = e.target.nodeName;
		if (type.toLowerCase() === 'digi-util-breakpoint-observer') {
			this.isMobile = e.detail.value === UtilBreakpointObserverBreakpoints.SMALL;
		}
		setTimeout(() => {
			this.isVisible = true;
		}, 100);
	}

	componentDidLoad() {
		const transition = this._wrapper;
		transition.addEventListener('transitionend', () => {
			this.getFocusableItems();
			this.setFocus();
		});
	}

	getFocusableItems() {
		const allElements = this._wrapper.querySelectorAll(
			this._focusableElementsSelectors
		);

		// Filters out visible items
		this.focusableElements = Array.prototype.slice
			.call(allElements)
			.filter(function (item) {
				return item.offsetParent !== null;
			});

		// Sets first and last focusable element
		if (this.focusableElements.length > 0) {
			this.firstFocusableEl = this.focusableElements[0] as HTMLElement;
			this.lastFocusableEl = this.focusableElements[
				this.focusableElements.length - 1
			] as HTMLElement;
		}
	}

	setFocus() {
		if (this.afActive) {
			let el: HTMLElement;
			let firstNavLink: HTMLElement;

			// Set first nav link
			firstNavLink = this.firstFocusableEl;

			if (!!this.afFocusableElement) {
				el = this.hostElement.querySelector(`${this.afFocusableElement}`);
			} else {
				el = firstNavLink;
			}
			if (!!el) {
				el.focus();
			}
		} else {
			if (!!this.afCloseFocusableElement) {
				const el: HTMLElement = document.querySelector(
					`${this.afCloseFocusableElement}`
				);
				el.focus();
			} else if (!!this._closeFocusableElement) {
				const el: HTMLElement = this._closeFocusableElement;
				el.focus();
			}
		}
	}

	get cssModifiers() {
		return {
			'digi-header-navigation--open': this.afActive,
			'digi-header-navigation--mobile': this.isMobile,
			'digi-header-navigation--visible': this.isVisible
		};
	}

	render() {
		return (
			<div
				class={{
					'digi-header-navigation': true,
					...this.cssModifiers
				}}
			>
				<digi-util-breakpoint-observer
					onAfOnChange={(e) =>
						(this.isMobile =
							e.detail.value === UtilBreakpointObserverBreakpoints.SMALL)
					}
				>
					<digi-util-keydown-handler
						onAfOnEsc={(e) => this.escHandler(e)}
						onAfOnTab={(e) => this.tabHandler(e)}
						onAfOnShiftTab={(e) => this.shiftHandler(e)}
					>
						<div>
							{this.isMobile && (
								<div
									class="digi-header-navigation__backdrop"
									onClick={(e) => this.backdropClickHandler(e)}
								></div>
							)}
							<nav
								class="digi-header-navigation__wrapper"
								aria-label={this.afNavAriaLabel}
								ref={(el) => {
									this._wrapper = el;
								}}
							>
								{this.isMobile && (
									<digi-button
										onClick={(e) => this.closeHandler(e)}
										af-variation="function"
										af-aria-label={this.afCloseButtonAriaLabel}
										class="digi-header-navigation__close-button"
									>
										{this.afCloseButtonText}
										<digi-icon slot="icon-secondary" afName={`x`}></digi-icon>
									</digi-button>
								)}

								<div role="list" class="digi-header-navigation__inner">
									<slot />
								</div>
							</nav>
						</div>
					</digi-util-keydown-handler>
				</digi-util-breakpoint-observer>
			</div>
		);
	}
}
