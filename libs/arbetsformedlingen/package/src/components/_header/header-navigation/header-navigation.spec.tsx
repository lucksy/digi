import { newSpecPage } from '@stencil/core/testing';
import { HeaderNavigation } from './header-navigation';

describe('header-navigation', () => {
	it('renders', async () => {
		const { root } = await newSpecPage({
			components: [HeaderNavigation],
			html: '<header-navigation></header-navigation>'
		});
		expect(root).toEqualHtml(`
      <header-navigation>
        <mock:shadow-root>
          <div>
            Hello, World! I'm
          </div>
        </mock:shadow-root>
      </header-navigation>
    `);
	});

	it('renders with values', async () => {
		const { root } = await newSpecPage({
			components: [HeaderNavigation],
			html: `<header-navigation first="Stencil" last="'Don't call me a framework' JS"></header-navigation>`
		});
		expect(root).toEqualHtml(`
      <header-navigation first="Stencil" last="'Don't call me a framework' JS">
        <mock:shadow-root>
          <div>
            Hello, World! I'm Stencil 'Don't call me a framework' JS
          </div>
        </mock:shadow-root>
      </header-navigation>
    `);
	});
});
