# header-navigation



<!-- Auto Generated Below -->


## Properties

| Property                  | Attribute                    | Description                                                                                      | Type      | Default     |
| ------------------------- | ---------------------------- | ------------------------------------------------------------------------------------------------ | --------- | ----------- |
| `afActive`                | `af-active`                  | Visar eller döljer sidmenyn i mobilläge. Om satt till true så är den öppen per standard.         | `boolean` | `false`     |
| `afBackdrop`              | `af-backdrop`                | Lägger en skugga bakom menyn som täcker sidinnehållet när menyn är aktiv                         | `boolean` | `true`      |
| `afCloseButtonAriaLabel`  | `af-close-button-aria-label` | Sätt attributet 'aria-label' på stäng-knappen                                                    | `string`  | `undefined` |
| `afCloseButtonText`       | `af-close-button-text`       | Stäng-knappens text                                                                              | `string`  | `undefined` |
| `afCloseFocusableElement` | `af-close-focusable-element` | Element som ska få fokus när menyn stängs. Sätts ingen så får knappen som öppnade menyn fokus    | `string`  | `undefined` |
| `afFocusableElement`      | `af-focusable-element`       | Element som ska få fokus när menyn öppnas. Sätts ingen så får rubriken eller stäng-knappen fokus | `string`  | `undefined` |
| `afNavAriaLabel`          | `af-nav-aria-label`          | Sätter attributet 'aria-label'.                                                                  | `string`  | `undefined` |


## Events

| Event               | Description                                            | Type               |
| ------------------- | ------------------------------------------------------ | ------------------ |
| `afOnBackdropClick` | Stängning av sidofält med klick på skuggan bakom menyn | `CustomEvent<any>` |
| `afOnClose`         | Stängknappens 'onclick'-event                          | `CustomEvent<any>` |
| `afOnEsc`           | Stängning av sidofält med esc-tangenten                | `CustomEvent<any>` |
| `afOnToggle`        | Vid öppning/stängning av sidofält                      | `CustomEvent<any>` |


## CSS Custom Properties

| Name                                         | Description                            |
| -------------------------------------------- | -------------------------------------- |
| `--digi--header--backdrop--background-color` | rgba(0, 0, 0, 0.7);                    |
| `--digi--header--border-color`               | var(--digi--color--border--neutral-4); |
| `--digi--header--close-button--padding`      | var(--digi--padding--smaller);         |


## Dependencies

### Depends on

- [digi-util-breakpoint-observer](../../../__core/_util/util-breakpoint-observer)
- [digi-util-keydown-handler](../../../__core/_util/util-keydown-handler)
- [digi-button](../../../__core/_button/button)
- [digi-icon](../../../__core/_icon/icon)

### Graph
```mermaid
graph TD;
  digi-header-navigation --> digi-util-breakpoint-observer
  digi-header-navigation --> digi-util-keydown-handler
  digi-header-navigation --> digi-button
  digi-header-navigation --> digi-icon
  style digi-header-navigation fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
