# my-component



<!-- Auto Generated Below -->


## Properties

| Property          | Attribute            | Description | Type      | Default     |
| ----------------- | -------------------- | ----------- | --------- | ----------- |
| `afSvgAriaHidden` | `af-svg-aria-hidden` |             | `boolean` | `undefined` |
| `afSvgName`       | `af-svg-name`        |             | `string`  | `undefined` |
| `afTitle`         | `af-title`           |             | `string`  | `undefined` |


## Dependencies

### Used by

 - [digi-header-avatar](..)

### Graph
```mermaid
graph TD;
  digi-header-avatar --> digi-avatar-illustration
  style digi-avatar-illustration fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
