import { h } from '@stencil/core';
import { NavigationAvatar } from './header-avatar';

export default {
	title: 'NavigationAvatar',
	component: NavigationAvatar
};

const Template = (args) => <header-avatar {...args}></header-avatar>;

export const Primary = Template.bind({});
Primary.args = { first: 'Hello', last: 'World' };
