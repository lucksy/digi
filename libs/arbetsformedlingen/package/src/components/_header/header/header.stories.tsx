import { h } from '@stencil/core';
import { Header } from './header';

export default {
	title: 'Header',
	component: Header
};

const Template = (args) => <header {...args}></header>;

export const Primary = Template.bind({});
Primary.args = { first: 'Hello', last: 'World' };
