import { Component, h, Listen, State, Prop } from '@stencil/core';
import {
	LayoutBlockContainer,
	UtilBreakpointObserverBreakpoints
} from '@digi/arbetsformedlingen';

/**
 * 
 *
 * @swedishName Sidhuvud
 */
@Component({
	tag: 'digi-header',
	styleUrl: 'header.scss',
	scoped: true
})
export class Header {
	/**
	 * Sätter systemnamnet
	 * @en Set system name.
	 */
	@Prop() afSystemName: string;

	/**
	 * Sätter text för menyknappen i mobilläge
	 * @en sets the text for menu button in mobile
	 */
	@Prop() afMenuButtonText: string;

	/**
	 * Sätter attributet 'aria-label'
	 * @en Input aria-label attribute
	 */
	@Prop() afAriaLabel: string;

	/**
	 * Döljer systemnamnet
	 * @en Hide system name
	 */
	@Prop() afHideSystemName: boolean;

	@State() afActive: boolean;

	@State() isMobile: boolean;

	@Listen('afOnToggle', { target: 'window' })
	toggleHandler(e: any) {
		if (e.target.matches('digi-navigation-sidebar-button')) {
			this.afActive = true;
		}
	}

	@Listen('afOnClose')
	closeHandler() {
		this.afActive = false;
	}

	@Listen('afOnChange')
	breakpointChange(e: any) {
		const type = e.target.nodeName;
		if (type.toLowerCase() === 'digi-util-breakpoint-observer') {
			this.isMobile = e.detail.value === UtilBreakpointObserverBreakpoints.SMALL;
		}
	}

	render() {
		return (
			<header class="digi-header">
				<digi-util-breakpoint-observer
					onAfOnChange={(e) =>
						(this.isMobile =
							e.detail.value === UtilBreakpointObserverBreakpoints.SMALL)
					}
				></digi-util-breakpoint-observer>
				<digi-layout-block afContainer={LayoutBlockContainer.NONE}>
					<div class="digi-header__logo" aria-label={this.afAriaLabel}>
						<div class="digi-header__wrapper">
							{((this.isMobile && !this.afHideSystemName) || !this.isMobile) && (
								<digi-logo
									af-system-name={this.afSystemName}
									afSvgAriaHidden={true}
								></digi-logo>
							)}
							{this.afHideSystemName && this.isMobile && (
								<digi-logo af-system-name=" " afSvgAriaHidden={true}></digi-logo>
							)}
							<slot name="header-logo" />
						</div>

						<div class="digi-header__content">
							<slot name="header-content" />
							{this.isMobile && (
								<digi-navigation-sidebar-button
									aria-hidden={this.afActive ? 'true' : 'false'}
									af-text={this.afMenuButtonText}
								></digi-navigation-sidebar-button>
							)}
						</div>
					</div>
					<div class="digi-header__navigation">
						<slot name="header-navigation" />
					</div>
				</digi-layout-block>
			</header>
		);
	}
}
