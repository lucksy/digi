import { newSpecPage } from '@stencil/core/testing';
import { HeaderNotification } from './header-notification';

describe('header-notification', () => {
	it('renders', async () => {
		const { root } = await newSpecPage({
			components: [HeaderNotification],
			html: '<header-notification></header-notification>'
		});
		expect(root).toEqualHtml(`
      <header-notification>
        <mock:shadow-root>
          <div>
            Hello, World! I'm
          </div>
        </mock:shadow-root>
      </header-notification>
    `);
	});

	it('renders with values', async () => {
		const { root } = await newSpecPage({
			components: [HeaderNotification],
			html: `<header-notification first="Stencil" last="'Don't call me a framework' JS"></header-notification>`
		});
		expect(root).toEqualHtml(`
      <header-notification first="Stencil" last="'Don't call me a framework' JS">
        <mock:shadow-root>
          <div>
            Hello, World! I'm Stencil 'Don't call me a framework' JS
          </div>
        </mock:shadow-root>
      </header-notification>
    `);
	});
});
