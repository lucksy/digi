export enum LinkButtonSize {
  MEDIUM = 'medium',
  MEDIUMLARGE = 'medium-large',
  LARGE = 'large'
}
