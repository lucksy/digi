import { newE2EPage } from '@stencil/core/testing';

describe('digi-card', () => {
  it('renders', async () => {
    const page = await newE2EPage();

    await page.setContent('<digi-card></digi-card>');
    const element = await page.find('digi-card');
    expect(element).toHaveClass('hydrated');
  });
});
