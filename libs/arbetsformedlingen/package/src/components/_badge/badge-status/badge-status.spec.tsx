import { newSpecPage } from '@stencil/core/testing';
import { BadgeStatus } from './badge-status';

describe('badge-status', () => {
	it('renders', async () => {
		const { root } = await newSpecPage({
			components: [BadgeStatus],
			html: '<badge-status></badge-status>'
		});
		expect(root).toEqualHtml(`
      <badge-status>
        <mock:shadow-root>
          <div>
            Hello, World! I'm
          </div>
        </mock:shadow-root>
      </badge-status>
    `);
	});

	it('renders with values', async () => {
		const { root } = await newSpecPage({
			components: [BadgeStatus],
			html: `<badge-status first="Stencil" last="'Don't call me a framework' JS"></badge-status>`
		});
		expect(root).toEqualHtml(`
      <badge-status first="Stencil" last="'Don't call me a framework' JS">
        <mock:shadow-root>
          <div>
            Hello, World! I'm Stencil 'Don't call me a framework' JS
          </div>
        </mock:shadow-root>
      </badge-status>
    `);
	});
});
