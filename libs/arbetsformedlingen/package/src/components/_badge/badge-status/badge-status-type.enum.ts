export enum BadgeStatusType {
	DENIED = 'denied',
	MISSING = 'missing',
	APPROVED = 'approved',
	PROMPT = 'prompt',
	BETA = 'beta',
	NEUTRAL = 'neutral'
}
