import { newSpecPage } from '@stencil/core/testing';
import { BadgeNotification } from './badge-notification';

describe('badge-notification', () => {
	it('renders', async () => {
		const { root } = await newSpecPage({
			components: [BadgeNotification],
			html: '<badge-notification></badge-notification>'
		});
		expect(root).toEqualHtml(`
      <badge-notification>
        <mock:shadow-root>
          <div>
            Hello, World! I'm
          </div>
        </mock:shadow-root>
      </badge-notification>
    `);
	});

	it('renders with values', async () => {
		const { root } = await newSpecPage({
			components: [BadgeNotification],
			html: `<badge-notification first="Stencil" last="'Don't call me a framework' JS"></badge-notification>`
		});
		expect(root).toEqualHtml(`
      <badge-notification first="Stencil" last="'Don't call me a framework' JS">
        <mock:shadow-root>
          <div>
            Hello, World! I'm Stencil 'Don't call me a framework' JS
          </div>
        </mock:shadow-root>
      </badge-notification>
    `);
	});
});
