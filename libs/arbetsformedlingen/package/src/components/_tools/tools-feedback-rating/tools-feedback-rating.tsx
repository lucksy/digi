import { Component, Event, EventEmitter, h, State, Element, Prop, Watch } from '@stencil/core';
import { HTMLStencilElement } from '@stencil/core/internal';
import { LayoutBlockVariation } from '../../../__core/_layout/layout-block/layout-block-variation.enum';
import { logger } from '../../../global/utils/logger';
import { randomIdGenerator } from '../../../global/utils/randomIdGenerator';
import { FeedbackRatingVariation } from './tools-feedback-rating-variation.enum';
import { FeedbackRatingType } from './tools-feedback-rating-type.enum';

/**
 *
 * @enums myEnum - tools-feedback-rating-myEnum.enum.ts
 *
 * @swedishName Feedback-betyg
 */
@Component({
	tag: 'digi-tools-feedback-rating',
	styleUrl: 'tools-feedback-rating.scss',
	scoped: true
})
export class ToolsFeedbackRating {
	@Element() hostElement: HTMLStencilElement;

	componentWillLoad() { this.validateQuestion(); }
	componentDidLoad() { }
	componentWillUpdate() { this.validateQuestion(); }

	/**
	 * Sätter ett id-attribut. Om inget anges används en slumpmässig sträng.
	 * @en Sets an id attribute.
	 */
	@Prop() afId: string = randomIdGenerator('digi-tools-feedback-rating');

	/**
	 * Sätter variant, kan vara primär eller sekundär
	 * @en Sets variation. Can be 'primary' or 'secondary'
	 */
	@Prop() afVariation: FeedbackRatingVariation = FeedbackRatingVariation.PRIMARY;

	/**
	 * Sätter typ som kan vara 'fullwidth' eller 'grid'.
	 * @en Sets type. Can be 'fullwidth' or 'grid'.
	 */
	@Prop() afType: FeedbackRatingType = FeedbackRatingType.FULLWIDTH;

	/**
	 * Sätter frågan
	 * @en Sets the question
	 */
	@Prop() afQuestion: string = 'Hur hjälpsam var tjänsten?';

	/**
	 * Anger svarsalternativen. Behöver följa samma struktur som förvalt
	 * @en Sets the answer options
	 */
	@Prop() afRatings: string = 'Mycket dåligt, Ganska dåligt, Helt okej, Ganska bra, Mycket bra';

	/**
	 * Frivillig hyperlänk
	 * @en Sets optional hyperlink
	 */
	@Prop() afLink: string = 'https://designsystem.arbetsformedlingen.se/';

	/**
	 * Sätter hyperlänktext
	 * @en Sets hyperlink text
	 */
	@Prop() afLinkText: string;

	/**
	 * Sätter produktnamn, användbart tex vid spårning
	 * @en Sets a product name, usable for tracking
	 */
	@Prop() afProduct!: string;

	/**
	 * Sätter applikationsnamn, användbart tex vid spårning
	 * @en Sets an application name, usable for tracking
	 */
	@Prop() afApplication!: string;

	/**
	 * Sätter nuvarande steg
	 * @en Sets current steps
	 */
	@State() currentStep: number = 1;

	/**
	 * Kontrollerar att frågan inte är en tom sträng
	 * @en Checks if mandatory content are included
	 */
	@State() validContent: boolean = true;

	@State() hoveredRating: number;
	@State() selectedRating: number;
	@State() hoveredText: string;
	@State() selectedText: string;

	@Watch('afQuestion')
	validateQuestion() {
		if (this.afQuestion == '') {
			logger.warn(
				`digi-feedback-rating must have a question. Please add af-question to the component`,
				this.hostElement
			);
			this.validContent = false;
			return false;
		}
	}

	/**
	 * Sker vid klick på Skicka-knapp 
	 * @en When click on Send-button
	 */
	@Event() afOnSubmitFeedback: EventEmitter;

	/**
	 * Sker vid klick på länk 
	 * @en When click on hyperlink
	 */
	@Event() afOnSubmitFeedbackLink: EventEmitter;

	/**
	 * Sker vid klick på radioknapp (stjärna) 
	 * @en When click on input element (star)
	 */
	@Event() afOnClickFeedback: EventEmitter;

	submitFeedback() {
		this.afOnSubmitFeedback.emit(this.getFeedbackData(true));
	}

	submitFeedbackLink() {
		this.afOnSubmitFeedbackLink.emit(this.getFeedbackLinkData());
	}

	selectRating() {
		this.afOnClickFeedback.emit(this.getFeedbackData(false));
	}

	checkAnswer() {
		if (this.selectedRating) {
			this.submitFeedback();
			this.currentStep = 2;
		}
		else {
			this.selectedText = 'Välj ditt betyg först'
		}
	}

	getFeedbackData(b: boolean) {
		return {
			id: this.afId,
			product: this.afProduct,
			application: this.afApplication,
			question: this.afQuestion,
			rating: b ? this.selectedRating : null,
			variation: this.afVariation,
		};
	}

	getFeedbackLinkData() {
		return {
			id: this.afId,
			product: this.afProduct,
			application: this.afApplication,
			question: this.afQuestion,
			linkHref: this.afLink,
			linkText: this.afLinkText,
		};
	}

	handleHover(rating: number) {
		this.hoveredRating = rating;
		this.hoveredText =
			(this.ratingsData().find((item) => item.value === this.hoveredRating) || {}).text || null;
	}

	handleRatingChange(rating: number) {
		this.selectedRating = rating;
		this.selectedText =
			(this.ratingsData().find((item) => item.value === this.selectedRating) || {}).text || null;
	}

	ratingsData() {
		const ratings = this.afRatings.split(',');
		const ratingItems = [];
		for (let i = 0; i < ratings.length; i++) {
			ratingItems.push({
				value: i + 1,
				text: ratings[i]
			});
		}
		return ratingItems;
	}

	get cssModifiers() {
		return {
			'digi-tools-feedback-rating--fullwidth':
				this.afType === FeedbackRatingType.FULLWIDTH,
			'digi-tools-feedback-rating--grid': this.afType === FeedbackRatingType.GRID
		};
	}

	get content() {
		return (
			<digi-typography>
				{this.validContent && (
					<div class="digi-tools-feedback-rating__wrapper">
						<fieldset class="digi-tools-feedback-rating__fieldset">
							<div class="digi-tools-feedback-rating__star-rating">
								<legend>
									<h2 class="digi-tools-feedback-rating__heading">{this.afQuestion}</h2>
								</legend>
								{this.currentStep === 1 && (
									<div class="digi-tools-feedback-rating__step1-wrapper">
										<div class="digi-tools-feedback-rating__star-rating-stars">
											{this.ratingsData().map(({ text, value }) => (
												<div class="digi-tools-feedback-rating__star-rating-star">
													<input
														type="radio"
														role="radio"
														name={`Feedbackrating ${this.afId}`}
														value={value}
														id={this.afId + value}
														checked={this.selectedRating === value}
														onChange={() => this.handleRatingChange(value)}
														aria-setsize="5"
													/>
													<label
														htmlFor={this.afId + value}
														onMouseEnter={() => this.handleHover(value)}
														onMouseLeave={() => this.handleHover(0)}
														onClick={() => (
															this.selectedRating = value,
															this.selectRating()
														)}
													>
														{this.afVariation === FeedbackRatingVariation.SECONDARY && (
															<span aria-hidden="true" class="digi-tools-feedback-rating__secondary">{value}</span>
														)}
														 <svg width="29px" height="29px" viewBox="0 0 32 32" version="1.1" xmlns="http://www.w3.org/2000/svg" class={`digi-tools-feedback-rating-star-icon ${value <= this.hoveredRating || this.selectedRating >= value ? 'filled' : ''}`}>
    <g stroke="none" stroke-width="1" stroke-linecap="round" stroke-linejoin="round">
        <g transform="translate(-1852, -2564)" stroke="#3B3735" stroke-width="2">
            <g transform="translate(1663, 2565)">
                <g transform="translate(190, 0)">
                    <path d="M13.0369701,1.00988613 L9.47212213,8.51643297 L1.4958694,9.72422429 C0.0655277509,9.93973122 -0.507709524,11.7709448 0.529620648,12.8199039 L6.30005635,18.6595463 L4.93529305,26.9090563 C4.6897182,28.3999832 6.20191813,29.5170473 7.46854321,28.8195669 L14.6037422,24.9244875 L21.7391705,28.8195669 C23.0057956,29.5113322 24.5179955,28.3999832 24.2721914,26.9090563 L22.9074281,18.6595463 L28.6780931,12.8199039 C29.715194,11.7709448 29.1419567,9.93973122 27.711615,9.72422429 L19.7355916,8.51643297 L16.1707436,1.00988613 C15.531928,-0.328161856 13.6810595,-0.345069029 13.0369701,1.00988613 L13.0369701,1.00988613 Z" id="Stroke-3"></path>
                </g>
            </g>
        </g>
    </g>
</svg>
														<span class="digi-tools-feedback-rating__star-rating-star-screenreader-text">
															{value} stjärnor, {text}
														</span>
													</label>
												</div>
											))}
											<div
												aria-hidden="true"
												class={`digi-tools-feedback-rating__star-rating-star-text`}>
												{this.hoveredText ? this.hoveredText : this.selectedText}
											</div>
										</div>
										<div class="digi-tools-feedback-rating__button-wrapper">
											<digi-button
												afVariation="secondary"
												afSize="small"
												onAfOnClick={() => (
													this.checkAnswer()
												)}>
												<span>Skicka in</span>
											</digi-button>
										</div>
									</div>
								)}
							</div>
						</fieldset>
						{this.currentStep === 2 && (
							<div class="digi-tools-feedback-rating__thankyou">
								<svg width="32" height="26" xmlns="http://www.w3.org/2000/svg" class="digi-tools-feedback-rating__thankyou-check-icon"><g fill="none" fill-rule="evenodd"><path d="M12.431 25.438C5.577 25.438 0 19.848 0 12.978 0 6.107 5.577.518 12.43.518c2.874 0 5.677 1.006 7.893 2.833l-1.441 1.755a10.163 10.163 0 0 0-6.452-2.315c-5.604 0-10.163 4.57-10.163 10.187 0 5.616 4.56 10.186 10.163 10.186 4.346 0 8.21-2.766 9.617-6.883l2.146.737a12.435 12.435 0 0 1-11.763 8.42" fill="#333" /><path d="M13.073 20.935a.725.725 0 0 1-1.022 0l-8.042-8.013a.72.72 0 0 1 0-1.019l1.704-1.699a.725.725 0 0 1 1.023 0l5.826 5.805L28.417.211a.724.724 0 0 1 1.023 0l1.704 1.699a.72.72 0 0 1 0 1.02l-18.07 18.005Z" fill="#95C13E" /></g></svg>
								<span class="digi-tools-feedback-rating__thankyou-text">
									Tack för din feedback!
								</span>
							</div>
						)}
						{this.currentStep === 2 && this.afLinkText && (
							<div class="digi-tools-feedback-rating__link">
								<digi-link-external
									afHref={this.afLink}
									af-target="_blank"
									onAfOnClick={() => this.submitFeedbackLink()}
									af-variation="small">
									{this.afLinkText}
								</digi-link-external>
							</div>
						)}
					</div>
				)}
			</digi-typography>
		);
	}


	render() {
		return (
			<div
				class={{
					'digi-tools-feedback-rating': true,
					...this.cssModifiers
				}}
			>
				{this.afType === FeedbackRatingType.FULLWIDTH && (
					<digi-layout-block afVariation={LayoutBlockVariation.SYMBOL}
						afMarginTop
						af-container="none">
						{this.content}
					</digi-layout-block>
				)}
				{this.afType !== FeedbackRatingType.FULLWIDTH && this.content}
			</div>
		)
	}
}