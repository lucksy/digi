import { newE2EPage } from '@stencil/core/testing';

describe('tools-feedback-rating', () => {
	it('renders', async () => {
		const page = await newE2EPage();

		await page.setContent(
			'<tools-feedback-rating></tools-tools-feedback-rating>'
		);
		const element = await page.find('tools-feedback-rating');
		expect(element).toHaveClass('hydrated');
	});

	it('renders changes to the name data', async () => {
		const page = await newE2EPage();

		await page.setContent(
			'<tools-feedback-rating></tools-feedback-rating>'
		);
		const component = await page.find('tools-feedback-rating');
		const element = await page.find('tools-feedback-rating >>> div');
		expect(element.textContent).toEqual(`Hello, World! I'm `);

		component.setProperty('first', 'James');
		await page.waitForChanges();
		expect(element.textContent).toEqual(`Hello, World! I'm James`);

		component.setProperty('last', 'Quincy');
		await page.waitForChanges();
		expect(element.textContent).toEqual(`Hello, World! I'm James Quincy`);

		component.setProperty('middle', 'Earl');
		await page.waitForChanges();
		expect(element.textContent).toEqual(`Hello, World! I'm James Earl Quincy`);
	});
});
