export enum FeedbackRatingType {
  FULLWIDTH = 'fullwidth',
  GRID = 'grid'
}