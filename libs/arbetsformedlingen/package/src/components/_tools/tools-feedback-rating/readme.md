# digi-tools-feedback-rating

<!-- Auto Generated Below -->


## Properties

| Property                     | Attribute        | Description                                                           | Type                                                                   | Default                                                             |
| ---------------------------- | ---------------- | --------------------------------------------------------------------- | ---------------------------------------------------------------------- | ------------------------------------------------------------------- |
| `afApplication` _(required)_ | `af-application` | Sätter applikationsnamn, användbart tex vid spårning                  | `string`                                                               | `undefined`                                                         |
| `afId`                       | `af-id`          | Sätter ett id-attribut. Om inget anges används en slumpmässig sträng. | `string`                                                               | `randomIdGenerator('digi-tools-feedback-rating')`                   |
| `afLink`                     | `af-link`        | Frivillig hyperlänk                                                   | `string`                                                               | `'https://designsystem.arbetsformedlingen.se/'`                     |
| `afLinkText`                 | `af-link-text`   | Sätter hyperlänktext                                                  | `string`                                                               | `undefined`                                                         |
| `afProduct` _(required)_     | `af-product`     | Sätter produktnamn, användbart tex vid spårning                       | `string`                                                               | `undefined`                                                         |
| `afQuestion`                 | `af-question`    | Sätter frågan                                                         | `string`                                                               | `'Hur hjälpsam var tjänsten?'`                                      |
| `afRatings`                  | `af-ratings`     | Anger svarsalternativen. Behöver följa samma struktur som förvalt     | `string`                                                               | `'Mycket dåligt, Ganska dåligt, Helt okej, Ganska bra, Mycket bra'` |
| `afType`                     | `af-type`        | Sätter typ som kan vara 'fullwidth' eller 'grid'.                     | `FeedbackRatingType.FULLWIDTH \| FeedbackRatingType.GRID`              | `FeedbackRatingType.FULLWIDTH`                                      |
| `afVariation`                | `af-variation`   | Sätter variant, kan vara primär eller sekundär                        | `FeedbackRatingVariation.PRIMARY \| FeedbackRatingVariation.SECONDARY` | `FeedbackRatingVariation.PRIMARY`                                   |


## Events

| Event                    | Description                            | Type               |
| ------------------------ | -------------------------------------- | ------------------ |
| `afOnClickFeedback`      | Sker vid klick på radioknapp (stjärna) | `CustomEvent<any>` |
| `afOnSubmitFeedback`     | Sker vid klick på Skicka-knapp         | `CustomEvent<any>` |
| `afOnSubmitFeedbackLink` | Sker vid klick på länk                 | `CustomEvent<any>` |


## Dependencies

### Depends on

- [digi-typography](../../../__core/_typography/typography)
- [digi-button](../../../__core/_button/button)
- [digi-link-external](../../../__core/_link/link-external)
- [digi-layout-block](../../../__core/_layout/layout-block)

### Graph
```mermaid
graph TD;
  digi-tools-feedback-rating --> digi-typography
  digi-tools-feedback-rating --> digi-button
  digi-tools-feedback-rating --> digi-link-external
  digi-tools-feedback-rating --> digi-layout-block
  digi-link-external --> digi-link
  digi-link-external --> digi-icon
  digi-layout-block --> digi-layout-container
  style digi-tools-feedback-rating fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
