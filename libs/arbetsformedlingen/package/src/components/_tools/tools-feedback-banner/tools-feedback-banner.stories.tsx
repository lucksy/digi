import { h } from '@stencil/core';
// import { FeedbackBanner } from '@digi/arbetsformedlingen/tools/feedback-banner';

export default {
	title: 'ToolsFeedbackBanner',
	// component: FeedbackBanner
};

const Template = (args) => (
	<tools-feedback-banner {...args}></tools-feedback-banner>
);

export const Primary = Template.bind({});
Primary.args = { first: 'Hello', last: 'World' };
