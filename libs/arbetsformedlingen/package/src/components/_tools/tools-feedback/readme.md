# digi-tools-feedback

<!-- Auto Generated Below -->


## Properties

| Property            | Attribute              | Description                                                                                          | Type                                                                                                                                                                                           | Default                                    |
| ------------------- | ---------------------- | ---------------------------------------------------------------------------------------------------- | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ------------------------------------------ |
| `afCloseButtonText` | `af-close-button-text` | Visa stäng-knapp                                                                                     | `string`                                                                                                                                                                                       | `'Stäng'`                                  |
| `afFeedback`        | `af-feedback`          | Texten på feedback                                                                                   | `string`                                                                                                                                                                                       | `undefined`                                |
| `afHeadingLevel`    | `af-heading-level`     | Sätter textens vikt (h1-h6)                                                                          | `ToolsFeedbackHeadingLevel.H1 \| ToolsFeedbackHeadingLevel.H2 \| ToolsFeedbackHeadingLevel.H3 \| ToolsFeedbackHeadingLevel.H4 \| ToolsFeedbackHeadingLevel.H5 \| ToolsFeedbackHeadingLevel.H6` | `ToolsFeedbackHeadingLevel.H4`             |
| `afId`              | `af-id`                | Sätter attributet 'id'. Om inget väljs så skapas ett slumpmässigt id.                                | `string`                                                                                                                                                                                       | `randomIdGenerator('digi-tools-feedback')` |
| `afIsDone`          | `af-is-done`           | Gör det möjligt att visa ett tack-meddelande som visas när användaren har delat med sig av feedback. | `boolean`                                                                                                                                                                                      | `false`                                    |
| `afText`            | `af-text`              | Texten på frågan                                                                                     | `string`                                                                                                                                                                                       | `undefined`                                |
| `afType`            | `af-type`              | Sätter typ på kortet. Kan vara 'fullwidth' eller 'grid'.                                             | `ToolsFeedbackType.FULLWIDTH \| ToolsFeedbackType.GRID`                                                                                                                                        | `ToolsFeedbackType.FULLWIDTH`              |
| `afVariation`       | `af-variation`         | Sätter variant. Kan vara 'primär' eller 'sekundär'.                                                  | `FeedbackVariation.PRIMARY \| FeedbackVariation.SECONDARY`                                                                                                                                     | `FeedbackVariation.PRIMARY`                |


## Events

| Event       | Description                                                                     | Type               |
| ----------- | ------------------------------------------------------------------------------- | ------------------ |
| `afOnReady` | När komponenten och slotsen är laddade och initierade så skickas detta eventet. | `CustomEvent<any>` |


## Methods

### `afMReset() => Promise<void>`

Används för att nollställa.

#### Returns

Type: `Promise<void>`



### `afMSetStep(step: 1 | 2 | 3) => Promise<void>`

Används för att ändra steg mellan 1-3.

#### Returns

Type: `Promise<void>`




## Slots

| Slot     | Description                 |
| -------- | --------------------------- |
| `"form"` | kan innehålla vad som helst |


## CSS Custom Properties

| Name                                                  | Description                                             |
| ----------------------------------------------------- | ------------------------------------------------------- |
| `--digi--tools-feedback--background-color--primary`   | var(--digi--color--background--neutral-5);              |
| `--digi--tools-feedback--background-color--secondary` | var(--digi--color--background--primary);                |
| `--digi--tools-feedback--font-size--desktop`          | var(--digi--typography--heading-2--font-size--desktop); |
| `--digi--tools-feedback--font-size--mobile`           | var(--digi--typography--heading-2--font-size--mobile);  |
| `--digi--tools-feedback--padding`                     | var(--digi--padding--large);                            |


## Dependencies

### Depends on

- [digi-button](../../../__core/_button/button)
- [digi-layout-block](../../../__core/_layout/layout-block)

### Graph
```mermaid
graph TD;
  digi-tools-feedback --> digi-button
  digi-tools-feedback --> digi-layout-block
  digi-layout-block --> digi-layout-container
  style digi-tools-feedback fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
