import { newSpecPage } from '@stencil/core/testing';
import { ExpandableFaq } from './expandable-faq';


describe('digi-expandable-faq', () => {
  it('renders', async () => {
    const { root } = await newSpecPage({
      components: [ExpandableFaq],
      html: '<digi-expandable-faq></digi-expandable-faq>'
    });
    expect(root).toEqualHtml(`
      <digi-expandable-faq>
        <mock:shadow-root>
          <div>
            Hello, World! I'm
          </div>
        </mock:shadow-root>
      </digi-expandable-faq>
    `);
  });

  it('renders with values', async () => {
    const { root } = await newSpecPage({
      components: [ExpandableFaq],
      html: `<digi-expandable-faq first="Stencil" last="'Don't call me a framework' JS"></digi-expandable-faq>`
    });
    expect(root).toEqualHtml(`
      <digi-expandable-faq first="Stencil" last="'Don't call me a framework' JS">
        <mock:shadow-root>
          <div>
            Hello, World! I'm Stencil 'Don't call me a framework' JS
          </div>
        </mock:shadow-root>
      </digi-expandable-faq>
    `);
  });
});
