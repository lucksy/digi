import { enumSelect, Template } from '../../../../../../shared/utils/src';
import { ExpandableFaqHeadingLevel } from './expandable-faq-heading-level.enum';
import { ExpandableFaqVariation } from './expandable-faq-variation.enum';

export default {
	title: 'expandable/digi-expandable-faq',
	argTypes: {
		'af-heading-level': enumSelect(ExpandableFaqHeadingLevel),
		'af-variation': enumSelect(ExpandableFaqVariation)
	}
};

export const Standard = Template.bind({});
Standard.args = {
	component: 'digi-expandable-faq',
	'af-heading': '',
	'af-heading-level': ExpandableFaqHeadingLevel.H2,
	'af-variation': ExpandableFaqVariation.PRIMARY,
	/* HTML */
	children: `
    <digi-expandable-faq-item
        af-heading="Expandable FAQ header1"><p>FAQ content</p></digi-expandable-faq-item>
    <digi-expandable-faq-item
        af-heading="Expandable FAQ header 2"><p>FAQ content</p><p>FAQ content</p></digi-expandable-faq-item>
    <digi-expandable-faq-item
        af-heading="Expandable FAQ header 3"><p>FAQ content</p><p>FAQ content</p></digi-expandable-faq-item>
    <digi-expandable-faq-item
        af-heading="Expandable FAQ header 4"><p>FAQ content</p><p>FAQ content</p></digi-expandable-faq-item>`
};
