import { Template } from '../../../../../../shared/utils/src';

export default {
	title: 'form/digi-form-filter',
	parameters: {
		actions: {
			handles: [
				'afOnFocusout',
				'afOnSubmitFilters',
				'afOnResetFilters',
				'afOnChange'
			]
		}
	}
};

export const Standard = Template.bind({});
Standard.args = {
	component: 'digi-form-select-filter',
	'af-id': null,
	'af-filter-button-text': 'Button text required',
	'af-submit-button-text': 'Submit button text required',
	'af-align-right': false,
	'af-name': '',
	/* html */
	children: `
    <digi-form-checkbox
        af-label="Arbetsförmedlare"
        af-variation="primary"></digi-form-checkbox>
    <digi-form-checkbox
        af-label="Handläggare"
        af-variation="primary"></digi-form-checkbox>
    <digi-form-checkbox
        af-label="Beslutshandläggare"
        af-variation="primary"></digi-form-checkbox>
    <digi-form-checkbox
        af-label="Konsult"
        af-variation="primary"></digi-form-checkbox>`
};
