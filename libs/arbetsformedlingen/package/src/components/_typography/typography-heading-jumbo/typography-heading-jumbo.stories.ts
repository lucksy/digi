import { enumSelect, Template } from '../../../../../../shared/utils/src';
import { TypographyHeadingJumboLevel } from './typography-heading-jumbo-level.enum';

export default {
	title: 'typography/digi-typography-heading-jumbo',
	argTypes: {
		'af-level': enumSelect(TypographyHeadingJumboLevel)
	}
};

export const Standard = Template.bind({});
Standard.args = {
	component: 'digi-typography-heading-jumbo',
	'af-text': '',
	'af-level': TypographyHeadingJumboLevel.H1,
	children: ''
};
