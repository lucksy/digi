import { newSpecPage } from '@stencil/core/testing';
import { InfoCardMulti } from './info-card-multi';

describe('info-card-multi', () => {
	it('renders', async () => {
		const { root } = await newSpecPage({
			components: [InfoCardMulti],
			html: '<info-card-multi></info-card-multi>'
		});
		expect(root).toEqualHtml(`
      <info-card-multi>
        <mock:shadow-root>
          <div>
            Hello, World! I'm
          </div>
        </mock:shadow-root>
      </info-card-multi>
    `);
	});

	it('renders with values', async () => {
		const { root } = await newSpecPage({
			components: [InfoCardMulti],
			html: `<info-card-multi first="Stencil" last="'Don't call me a framework' JS"></info-card-multi>`
		});
		expect(root).toEqualHtml(`
      <info-card-multi first="Stencil" last="'Don't call me a framework' JS">
        <mock:shadow-root>
          <div>
            Hello, World! I'm Stencil 'Don't call me a framework' JS
          </div>
        </mock:shadow-root>
      </info-card-multi>
    `);
	});
});
