# digi-info-card-multi-container-info

This is a container component meant to wrap 1-3 `digi-info-card` components of type multi.

## Enums

If used in a Typescript environment, you will need to import a couple of enums:

```ts
import { InfoCardMultiContainerHeadingLevel } from '@digi/core';
```

<!-- Auto Generated Below -->


## Properties

| Property         | Attribute          | Description                 | Type                                                                                                                                                                                                                                                 | Default     |
| ---------------- | ------------------ | --------------------------- | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ----------- |
| `afHeading`      | `af-heading`       | Rubrikens text              | `string`                                                                                                                                                                                                                                             | `undefined` |
| `afHeadingLevel` | `af-heading-level` | Sätt rubrikens vikt (h1-h6) | `InfoCardMultiContainerHeadingLevel.H1 \| InfoCardMultiContainerHeadingLevel.H2 \| InfoCardMultiContainerHeadingLevel.H3 \| InfoCardMultiContainerHeadingLevel.H4 \| InfoCardMultiContainerHeadingLevel.H5 \| InfoCardMultiContainerHeadingLevel.H6` | `undefined` |


## Slots

| Slot        | Description                                       |
| ----------- | ------------------------------------------------- |
| `"default"` | Ska vara flera digi-info-card med af-type 'multi' |


## Dependencies

### Depends on

- [digi-layout-columns](../../../__core/_layout/layout-columns)

### Graph
```mermaid
graph TD;
  digi-info-card-multi-container --> digi-layout-columns
  style digi-info-card-multi-container fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
