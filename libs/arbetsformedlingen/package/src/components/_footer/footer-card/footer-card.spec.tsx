import { newSpecPage } from '@stencil/core/testing';
import { FooterFooterCard } from './footer-card';

describe('footer-card', () => {
	it('renders', async () => {
		const { root } = await newSpecPage({
			components: [FooterFooterCard],
			html: '<footer-card></footer-card>'
		});
		expect(root).toEqualHtml(`
      <footer-card>
        <mock:shadow-root>
          <div>
            Hello, World! I'm
          </div>
        </mock:shadow-root>
      </footer-card>
    `);
	});

	it('renders with values', async () => {
		const { root } = await newSpecPage({
			components: [FooterFooterCard],
			html: `<footer-card first="Stencil" last="'Don't call me a framework' JS"></footer-card>`
		});
		expect(root).toEqualHtml(`
      <footer-card first="Stencil" last="'Don't call me a framework' JS">
        <mock:shadow-root>
          <div>
            Hello, World! I'm Stencil 'Don't call me a framework' JS
          </div>
        </mock:shadow-root>
      </footer-card>
    `);
	});
});
