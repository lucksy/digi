export enum FooterCardVariation {
  NONE = 'none',
  ICON = 'icon',
  BORDER = 'border'
}