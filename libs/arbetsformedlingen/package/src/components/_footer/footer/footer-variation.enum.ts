export enum FooterVariation {
  LARGE = 'large',
  SMALL = 'small',
}