import { kebabCase } from './string';

/**
 * Create a Storybook Arg type select from an enum prop
 *
 * @param enumSource A component property enum
 * @returns Storybook select arg control object
 */
export const enumSelect = (enumSource: { [key: number]: string | number }) => {
	return {
		control: 'select',
		options: [...Object.values(enumSource)]
	};
};

export const parseArgs = (args: object) =>
	Object.entries(args)
		.filter((arg) => arg[1] !== undefined && arg[1] !== null)
		.map(
			(arg) => `
        ${kebabCase(arg[0])}='${arg[1]}'`
		)
		.join(' ');

/**
 * Template for automatic mapping of args in stories.
 */
export const Template = ({ component = '', children = '', ...args }) =>
	`<${component} ${parseArgs(args)}>${children}</${component}>`;
