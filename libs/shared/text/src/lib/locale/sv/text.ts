const common = {
	close: 'Stäng',
	loading: 'Laddar',
	show: 'Visa',
	showing: 'Visar',
	show_action: (action: string) => `Visa ${action}`,
	hide: 'Dölj',
	hide_action: (action: string) => `Dölj ${action}`,
	clear: 'Rensa',
	required: 'Obligatoriskt',
	optional: 'Frivilligt',
	of: 'av',
	today: 'Idag',
	cancel: 'Avbryt',
	canceled: 'Avbruten',
	step: 'Steg',
	breadcrumbs: 'Din plats i webbplatsstrukturen',
	back: 'Tillbaka',
	hits: 'Träffar',
	previous: 'Föregående',
	next: 'Nästa',
	menu: 'Meny',
	settings: 'Inställningar'
};

const calendar = {
	week: 'Vecka',
	weekdays: {
		mon: 'Måndag',
		tue: 'Tisdag',
		wed: 'Onsdag',
		thu: 'Torsdag',
		fri: 'Fredag',
		sat: 'Lördag',
		sun: 'Söndag'
	},
	weekdays_short: {
		mon: 'Må',
		tue: 'Ti',
		wed: 'On',
		thu: 'To',
		fri: 'Fr',
		sat: 'Lö',
		sun: 'Sö'
	}
};

const code = {
	copy: 'Kopiera kod',
	show_more: 'Visa mer kod',
	show_less: 'Visa mindre kod',
	language: 'Kodspråk'
};

const form = {
	file_upload: 'Ladda upp fil',
	file_choose: 'Välj fil',
	file_dropzone: 'eller dra och släpp en fil inuti det streckade området',
	file_uploaded_files: 'Uppladdade filer',
	file_cancel_upload: 'Avbryt uppladdning',
	file_cancel_upload_long: (fileName: string) =>
		`Avbryt uppladdning av filen ${fileName}`,
	file_error: (error: string, fileName = 'Filen') =>
		`${fileName} gick inte att ladda upp. ${error}`,
	file_error_canceled: 'Uppladdningen avbruten',
	file_error_too_many: 'Max antal filer uppladdade',
	file_error_already_uploaded: 'Alla valda filer är redan uppladdade',
	file_error_filetype_not_allowed: 'Filtypen tillåts inte att ladda upp',
	file_error_duplicate: (fileName: string) =>
		`Det finns redan en fil med filnamnet (${fileName})`,
	file_error_too_large: (fileName: string) =>
		`Filens storlek är för stor (${fileName})`,
	file_error_try_again: (fileName: string) =>
		`Misslyckad uppladdning, försök ladda upp filen igen ${fileName}`,
	file_error_remove: (fileName: string) =>
		`Misslyckad uppladdning, ta bort ${fileName} från fillistan`
};

const progress = {
	questions_answered: 'Frågor besvarade',
	questions_next: 'Nästa:'
};

const time = {
	days_ago: (days: number | string) => `För ${days} dagar sedan`,
	in_days: (days: number | string) => `Om ${days} dagar`
};

export const _t = {
	...common,
	calendar,
	code,
	form,
	progress,
	time
};
