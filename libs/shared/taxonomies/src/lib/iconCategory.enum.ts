export enum IconCategory {
	USER = 'Användare',
	DOCUMENT = 'Dokument',
	ARROWS = 'Pilar',
	ALERT = 'Meddelanden',
	MEDIA = 'Media',
	UNCATEGORIZED = 'Övrigt'
}
